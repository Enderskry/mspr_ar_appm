using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
public class RecogniationMultiImage : MonoBehaviour
{
    [SerializeField] private GameObject[] placeablePrefabs;

    private Dictionary<string, GameObject> _spawnedPrefabs = new Dictionary<string, GameObject>();
    private ARTrackedImageManager _trackedImageManager;

    private void Awake()
    {
        _trackedImageManager = FindObjectOfType<ARTrackedImageManager>();

        foreach (GameObject prefab in placeablePrefabs)
        {
            GameObject newPrefab = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            newPrefab.name = prefab.name;
            _spawnedPrefabs.Add(prefab.name, newPrefab);
        }
    }

    private void OnEnable()
    {
        Debug.Log("On ENABLE ");

        _trackedImageManager.trackedImagesChanged += ImageChange;
    }
    
    private void OnDisable()
    {
        Debug.Log("On DISABLE ");

        _trackedImageManager.trackedImagesChanged -= ImageChange;
    }
    
    private void ImageChange(ARTrackedImagesChangedEventArgs args)
    {   
        Debug.Log("IMAGE CHANGE ");
        
        foreach (ARTrackedImage trackedImage in args.added)
        {
            Debug.Log("ADD ");

            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in args.updated)
        {
            Debug.Log("UPDATE ");

            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in args.removed)
        {
            Debug.Log("REMOVED ");

            _spawnedPrefabs[trackedImage.name].SetActive(false);
        }
    }

    private void UpdateImage(ARTrackedImage trackedImage)
    {
        string imageName = trackedImage.referenceImage.name;
        Vector3 position = trackedImage.transform.position;
        Debug.Log("NAME : " + imageName);

        foreach (GameObject o in _spawnedPrefabs.Values)
        {
            if (o.name != imageName)
            {
                Debug.Log("FALSE : " + o.name);
                o.SetActive(false);
            }
        }
        GameObject prefab = _spawnedPrefabs[imageName];
        prefab.transform.position = position;
        prefab.SetActive(true);
    }
}
