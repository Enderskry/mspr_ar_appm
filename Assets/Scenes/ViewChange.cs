using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Scenes
{
    public class ViewChange : MonoBehaviour
    {
        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
        public void BlankARView() {
            Debug.Log("CHANGE VUE");
            SceneManager.LoadScene("BlankAR");
        }
        public void MenuView() {  
            SceneManager.LoadScene("Menu");
        }

        public void FormView()
        {
            SceneManager.LoadScene("Form");
        }
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
