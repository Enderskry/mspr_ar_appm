astroid==2.4.2
astropy==4.2
certifi==2022.5.18.1
charset-normalizer==2.0.12
colorama==0.4.4
idna==3.3
isort==5.6.4
lazy-object-proxy==1.4.3
mccabe==0.6.1
MouseInfo==0.1.3
numpy==1.22.2
oauthlib==3.2.0
opencv-python==4.5.5.62
Pillow==9.0.1
PyAutoGUI==0.9.53
pyerfa==1.7.1.1
PyGetWindow==0.0.9
pylint==2.6.0
PyMsgBox==1.0.9
pyperclip==1.8.2
PyRect==0.2.0
PyScreeze==0.1.28
pytweening==1.0.4
pywin32==303
requests==2.27.1
requests-oauthlib==1.3.1
six==1.15.0
toml==0.10.2
tweepy==4.10.0
urllib3==1.26.9
wrapt==1.12.1
