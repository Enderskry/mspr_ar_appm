using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToggleChange : MonoBehaviour
{
    //[SerializeField] private GameObject toggle;
    
    private Image toggleRenderer;

    private Color newToggleColor;

    private List<string> addHashtag;
    
    public string tweet;
    
    //public Toggle toggle ;
    //[SerializeField] public Text _text ;

    
    // Start is called before the first frame update
    void Start()
    {
        addHashtag = new List<string>();
        //toggleRenderer = toggle2.GetComponent<Image>();
        //toggle = GetComponent<Toggle>();
        //toggle.onValueChanged.AddListener( ChangeColor ) ;
    }
    
    public void ChangeColor( [SerializeField] Toggle toggle)
    {
        //toggle = GetComponent<Toggle>();
        TextMeshProUGUI hash = toggle.GetComponentInChildren(typeof(TextMeshProUGUI)) as TextMeshProUGUI;
        bool isOn = toggle.isOn;
        ColorBlock cb = toggle.colors;
        if (isOn)
        {
            addHashtag.Add(hash.text);
            cb.normalColor = new Color(0.4862745f, 0.5882353f, 0.9803922f, 1);
            cb.highlightedColor = new Color(0.5f, 0.5f, 0.5f, 1);
            cb.selectedColor = new Color(0.4862745f, 0.5882353f, 0.9803922f, 1);

        }
        else
        {
            int index = addHashtag.FindIndex((s => s == hash.text));
            addHashtag.Remove(hash.text);
            cb.normalColor = new Color(1f,1f,1f,1f);
            cb.highlightedColor = new Color(1f,1f,1f,1f);
            cb.selectedColor = new Color(1f,1f,1f,1f);

        }

        string hashTag = "#Cerealis, #Coloring, #AR, ";
        foreach (var s in addHashtag)
        {
            hashTag += s + ", ";
        }

        tweet = hashTag;
        toggle.colors = cb;
        // newToggleColor = new Color(124, 150, 250, 1);
        // toggleRenderer.color = newToggleColor;
        Debug.Log("Tweet : " + tweet);

    }
    
}
