using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;

public class ImageRecogniation : MonoBehaviour
{
    private ARTrackedImageManager _arTackedImageManager;

    void Awake()
    {
        _arTackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    private void OnEnable()
    {
        _arTackedImageManager.trackedImagesChanged += OnImageChange;
    }

    private void OnDisable()
    {
        _arTackedImageManager.trackedImagesChanged -= OnImageChange;

    }

    public void OnImageChange(ARTrackedImagesChangedEventArgs args)
    {
        foreach (var trackedImage in args.added)
        {
            Debug.Log(trackedImage.name);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
