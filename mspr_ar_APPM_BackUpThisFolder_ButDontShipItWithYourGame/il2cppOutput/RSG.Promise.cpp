﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<System.Exception>
struct Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<RSG.ProgressHandler>
struct Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC;
// System.Action`1<RSG.RejectHandler>
struct Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF;
// System.Action`1<System.Single>
struct Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A;
// System.Action`1<RSG.Promise/ResolveHandler>
struct Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751;
// System.Action`2<System.Action,System.Action`1<System.Exception>>
struct Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF;
// System.Action`2<RSG.IPromise,System.Int32>
struct Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706;
// System.Action`2<System.Object,System.Int32>
struct Action_2_tAC461AE4F7B507965CE2E6A32853473F8C02CD75;
// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C;
// System.EventHandler`1<RSG.ExceptionEventArgs>
struct EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47;
// System.EventHandler`1<System.Object>
struct EventHandler_1_tD8C4A5BE1F7C91B1A7E99AE87AFD2F5432C38746;
// System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>
struct Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09;
// System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>
struct Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020;
// System.Func`1<RSG.IPromise>
struct Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057;
// System.Func`1<System.Object>
struct Func_1_tD5C081AE11746B200C711DD48DBEB00E3A9276D4;
// System.Func`2<RSG.TimeData,System.Boolean>
struct Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519;
// System.Func`3<RSG.IPromise,System.Func`1<RSG.IPromise>,RSG.IPromise>
struct Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566;
// System.Collections.Generic.HashSet`1<RSG.IPromiseInfo>
struct HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885;
// System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>
struct IEnumerable_1_t8FCA341CBC758B89D77835A9E717053461960365;
// System.Collections.Generic.IEnumerable`1<RSG.IPromise>
struct IEnumerable_1_t98BB06EAF89E62DF2E53F7389821CC5E946A01B4;
// System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo>
struct IEnumerable_1_tFF9DBF5C552809F322A1F4A688B67B5446FC1E36;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<RSG.ProgressHandler>
struct IEnumerable_1_t9D2F23C9B37E474748E4ED6C2A57D672D79313DC;
// System.Collections.Generic.IEnumerable`1<RSG.RejectHandler>
struct IEnumerable_1_tF14E3D480154EF6F71EA59E3F17A1D9002898431;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t352FDDEA001ABE8E1D67849D2E2F3D1D75B03D41;
// System.Collections.Generic.IEnumerable`1<RSG.Promise/ResolveHandler>
struct IEnumerable_1_t3833FC0EFF0B06A0D804963012F32C04A8692481;
// System.Collections.Generic.IEqualityComparer`1<RSG.IPromiseInfo>
struct IEqualityComparer_1_tCF2AC22BDA1AA3DD804CE79463B51474E5BBECDD;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C;
// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>
struct LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76;
// System.Collections.Generic.LinkedList`1<RSG.PredicateWait>
struct LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80;
// System.Collections.Generic.List`1<RSG.ProgressHandler>
struct List_1_t43490118A5560875E8A4FE9F26A448F004750640;
// System.Collections.Generic.List`1<RSG.RejectHandler>
struct List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF;
// System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>
struct List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8;
// System.Func`1<RSG.IPromise>[]
struct Func_1U5BU5D_t99CB06D0355E1DD5621F8A9A0D7730BC569F7449;
// System.Collections.Generic.HashSet`1/Slot<RSG.IPromiseInfo>[]
struct SlotU5BU5D_t793C1D61AFE4C6058423F67901C3ACEB84B3AABB;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// RSG.IPromise[]
struct IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// RSG.ProgressHandler[]
struct ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89;
// RSG.RejectHandler[]
struct RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// RSG.Promise/ResolveHandler[]
struct ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377;
// System.Exception
struct Exception_t;
// RSG.ExceptionEventArgs
struct ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// RSG.IPendingPromise
struct IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59;
// RSG.IPromise
struct IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE;
// RSG.IPromiseInfo
struct IPromiseInfo_t883EED89B28ECE4246A1BC8D8D3E90A7D8A6FCD5;
// RSG.IRejectable
struct IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361;
// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// RSG.PredicateWait
struct PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E;
// RSG.Promise
struct Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C;
// RSG.PromiseCancelledException
struct PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D;
// RSG.Exceptions.PromiseException
struct PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C;
// RSG.Exceptions.PromiseStateException
struct PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5;
// RSG.PromiseTimer
struct PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37;
// System.String
struct String_t;
// RSG.Tuple
struct Tuple_tD84879F942A5C7C66FD4285DEFF4856294C641DE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// RSG.Promise/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD;
// RSG.Promise/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D;
// RSG.Promise/<>c__DisplayClass44_0
struct U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114;
// RSG.Promise/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF;
// RSG.Promise/<>c__DisplayClass53_0
struct U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD;
// RSG.Promise/<>c__DisplayClass56_0
struct U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27;
// RSG.Promise/<>c__DisplayClass59_0
struct U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC;
// RSG.Promise/<>c__DisplayClass59_1
struct U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D;
// RSG.Promise/<>c__DisplayClass60_0
struct U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130;
// RSG.Promise/<>c__DisplayClass62_0
struct U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4;
// RSG.Promise/<>c__DisplayClass62_1
struct U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD;
// RSG.Promise/<>c__DisplayClass63_0
struct U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1;
// RSG.Promise/<>c__DisplayClass66_0
struct U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3;
// RSG.Promise/<>c__DisplayClass66_1
struct U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0;
// RSG.Promise/<>c__DisplayClass69_0
struct U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D;
// RSG.Promise/<>c__DisplayClass70_0
struct U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8;
// RSG.PromiseTimer/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178;
// RSG.PromiseTimer/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t43490118A5560875E8A4FE9F26A448F004750640_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral304B1BC7B1FAA5EA26BCEEA9C69D283F782FD8FC;
IL2CPP_EXTERN_C String_t* _stringLiteral3818CF5AE60CBE5E09B0C6F2B2C25B959DE1E784;
IL2CPP_EXTERN_C String_t* _stringLiteral4C37184ABD38BF9788EE23119802EC101741E082;
IL2CPP_EXTERN_C String_t* _stringLiteral71E2B3599FE35851F669D4F2F749EBC636263B21;
IL2CPP_EXTERN_C String_t* _stringLiteral84AEEFD7EDC59735921DA60B7B4B82FBFCA4278D;
IL2CPP_EXTERN_C String_t* _stringLiteralA1B04F482AB3587406E00E182DC98A1187A31FCB;
IL2CPP_EXTERN_C String_t* _stringLiteralB3959F17E24B23C57ED29A79C653091BC73F5B12;
IL2CPP_EXTERN_C String_t* _stringLiteralBAEB86B408E931CE2BB53E3D0839576F5491514F;
IL2CPP_EXTERN_C String_t* _stringLiteralCCDF22F0BA1FC534FC6656104D7D41A8D396BCE5;
IL2CPP_EXTERN_C String_t* _stringLiteralCD50CB6668BE1AF6B25D8CB27AFFA8E4536FAF32;
IL2CPP_EXTERN_C const RuntimeMethod* EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Aggregate_TisFunc_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mF56D75615C1ACB3B5BD8F167EF9822719DB356CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1__ctor_mFE82769CA93910E0390206067960C72201679309_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddLast_mD499A5DF665E3F253168E79A6CCA4D0F8C4E026E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_mD60204AAC26CC97B7E30EE552CDF6F22622242E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Func_1U5BU5D_t99CB06D0355E1DD5621F8A9A0D7730BC569F7449;
struct IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89;
struct RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t268C8079E64748796677929D74325479FB85105E 
{
};

// System.Collections.Generic.HashSet`1<RSG.IPromiseInfo>
struct HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t793C1D61AFE4C6058423F67901C3ACEB84B3AABB* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_14;
};

// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C  : public RuntimeObject
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	RuntimeObject* ___item_3;
};

// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>
struct LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B  : public RuntimeObject
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* ___item_3;
};

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76  : public RuntimeObject
{
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject* ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_4;
};

// System.Collections.Generic.LinkedList`1<RSG.PredicateWait>
struct LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80  : public RuntimeObject
{
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject* ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_4;
};

// System.Collections.Generic.List`1<RSG.ProgressHandler>
struct List_1_t43490118A5560875E8A4FE9F26A448F004750640  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t43490118A5560875E8A4FE9F26A448F004750640_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<RSG.RejectHandler>
struct List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>
struct List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// RSG.Promises.EnumerableExt
struct EnumerableExt_t2660B9F4F67473D7E1954EF963F2572649BC0802  : public RuntimeObject
{
};

// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377  : public RuntimeObject
{
};

struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_StaticFields
{
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377* ___Empty_0;
};

// RSG.PromiseHelpers
struct PromiseHelpers_t7783F214F2091C9E80CBDB57E0A2D98C7C7A3F30  : public RuntimeObject
{
};

// RSG.PromiseTimer
struct PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B  : public RuntimeObject
{
	// System.Single RSG.PromiseTimer::curTime
	float ___curTime_0;
	// System.Int32 RSG.PromiseTimer::curFrame
	int32_t ___curFrame_1;
	// System.Collections.Generic.LinkedList`1<RSG.PredicateWait> RSG.PromiseTimer::waiting
	LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* ___waiting_2;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// RSG.Tuple
struct Tuple_tD84879F942A5C7C66FD4285DEFF4856294C641DE  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// RSG.Promise/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD  : public RuntimeObject
{
	// RSG.Promise RSG.Promise/<>c__DisplayClass34_0::<>4__this
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___U3CU3E4__this_0;
	// System.Exception RSG.Promise/<>c__DisplayClass34_0::ex
	Exception_t* ___ex_1;
};

// RSG.Promise/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D  : public RuntimeObject
{
	// RSG.Promise RSG.Promise/<>c__DisplayClass36_0::<>4__this
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___U3CU3E4__this_0;
	// System.Single RSG.Promise/<>c__DisplayClass36_0::progress
	float ___progress_1;
};

// RSG.Promise/<>c__DisplayClass44_0
struct U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114  : public RuntimeObject
{
	// RSG.Promise RSG.Promise/<>c__DisplayClass44_0::resultPromise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___resultPromise_0;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass44_0::onRejected
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected_1;
};

// RSG.Promise/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF  : public RuntimeObject
{
	// System.Func`1<RSG.IPromise> RSG.Promise/<>c__DisplayClass52_0::onResolved
	Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved_0;
	// RSG.Promise RSG.Promise/<>c__DisplayClass52_0::resultPromise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___resultPromise_1;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass52_0::onRejected
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected_2;
	// System.Action`1<System.Single> RSG.Promise/<>c__DisplayClass52_0::<>9__2
	Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___U3CU3E9__2_3;
	// System.Action RSG.Promise/<>c__DisplayClass52_0::<>9__3
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3CU3E9__3_4;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass52_0::<>9__4
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___U3CU3E9__4_5;
};

// RSG.Promise/<>c__DisplayClass53_0
struct U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD  : public RuntimeObject
{
	// System.Action RSG.Promise/<>c__DisplayClass53_0::onResolved
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved_0;
	// RSG.Promise RSG.Promise/<>c__DisplayClass53_0::resultPromise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___resultPromise_1;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass53_0::onRejected
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected_2;
};

// RSG.Promise/<>c__DisplayClass56_0
struct U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27  : public RuntimeObject
{
	// System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>> RSG.Promise/<>c__DisplayClass56_0::chain
	Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* ___chain_0;
};

// RSG.Promise/<>c__DisplayClass59_0
struct U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC  : public RuntimeObject
{
	// System.Single[] RSG.Promise/<>c__DisplayClass59_0::progress
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___progress_0;
	// RSG.Promise RSG.Promise/<>c__DisplayClass59_0::resultPromise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___resultPromise_1;
	// System.Int32 RSG.Promise/<>c__DisplayClass59_0::remainingCount
	int32_t ___remainingCount_2;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass59_0::<>9__3
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___U3CU3E9__3_3;
};

// RSG.Promise/<>c__DisplayClass59_1
struct U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D  : public RuntimeObject
{
	// System.Int32 RSG.Promise/<>c__DisplayClass59_1::index
	int32_t ___index_0;
	// RSG.Promise/<>c__DisplayClass59_0 RSG.Promise/<>c__DisplayClass59_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* ___CSU24U3CU3E8__locals1_1;
};

// RSG.Promise/<>c__DisplayClass60_0
struct U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130  : public RuntimeObject
{
	// System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>> RSG.Promise/<>c__DisplayClass60_0::chain
	Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09* ___chain_0;
};

// RSG.Promise/<>c__DisplayClass62_0
struct U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4  : public RuntimeObject
{
	// System.Int32 RSG.Promise/<>c__DisplayClass62_0::count
	int32_t ___count_0;
	// RSG.Promise RSG.Promise/<>c__DisplayClass62_0::promise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___promise_1;
};

// RSG.Promise/<>c__DisplayClass62_1
struct U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD  : public RuntimeObject
{
	// System.Int32 RSG.Promise/<>c__DisplayClass62_1::itemSequence
	int32_t ___itemSequence_0;
	// System.Func`1<RSG.IPromise> RSG.Promise/<>c__DisplayClass62_1::fn
	Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___fn_1;
	// RSG.Promise/<>c__DisplayClass62_0 RSG.Promise/<>c__DisplayClass62_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* ___CSU24U3CU3E8__locals1_2;
};

// RSG.Promise/<>c__DisplayClass63_0
struct U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1  : public RuntimeObject
{
	// System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>> RSG.Promise/<>c__DisplayClass63_0::chain
	Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* ___chain_0;
};

// RSG.Promise/<>c__DisplayClass66_0
struct U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3  : public RuntimeObject
{
	// System.Single[] RSG.Promise/<>c__DisplayClass66_0::progress
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___progress_0;
	// RSG.Promise RSG.Promise/<>c__DisplayClass66_0::resultPromise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___resultPromise_1;
	// System.Action`1<System.Exception> RSG.Promise/<>c__DisplayClass66_0::<>9__2
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___U3CU3E9__2_2;
	// System.Action RSG.Promise/<>c__DisplayClass66_0::<>9__3
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3CU3E9__3_3;
};

// RSG.Promise/<>c__DisplayClass66_1
struct U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0  : public RuntimeObject
{
	// System.Int32 RSG.Promise/<>c__DisplayClass66_1::index
	int32_t ___index_0;
	// RSG.Promise/<>c__DisplayClass66_0 RSG.Promise/<>c__DisplayClass66_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* ___CSU24U3CU3E8__locals1_1;
};

// RSG.Promise/<>c__DisplayClass69_0
struct U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D  : public RuntimeObject
{
	// RSG.Promise RSG.Promise/<>c__DisplayClass69_0::promise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___promise_0;
	// System.Action RSG.Promise/<>c__DisplayClass69_0::onComplete
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onComplete_1;
};

// RSG.Promise/<>c__DisplayClass70_0
struct U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8  : public RuntimeObject
{
	// RSG.Promise RSG.Promise/<>c__DisplayClass70_0::promise
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* ___promise_0;
};

// RSG.PromiseTimer/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178  : public RuntimeObject
{
	// System.Single RSG.PromiseTimer/<>c__DisplayClass3_0::seconds
	float ___seconds_0;
};

// RSG.PromiseTimer/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75  : public RuntimeObject
{
	// System.Func`2<RSG.TimeData,System.Boolean> RSG.PromiseTimer/<>c__DisplayClass4_0::predicate
	Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* ___predicate_0;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};

struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_StaticFields
{
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___enumSeperatorCharArray_0;
};
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};

// RSG.ExceptionEventArgs
struct ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777  : public EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377
{
	// System.Exception RSG.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t* ___U3CExceptionU3Ek__BackingField_1;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// RSG.ProgressHandler
struct ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF 
{
	// System.Action`1<System.Single> RSG.ProgressHandler::callback
	Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___callback_0;
	// RSG.IRejectable RSG.ProgressHandler::rejectable
	RuntimeObject* ___rejectable_1;
};
// Native definition for P/Invoke marshalling of RSG.ProgressHandler
struct ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_pinvoke
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};
// Native definition for COM marshalling of RSG.ProgressHandler
struct ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_com
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};

// RSG.RejectHandler
struct RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E 
{
	// System.Action`1<System.Exception> RSG.RejectHandler::callback
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___callback_0;
	// RSG.IRejectable RSG.RejectHandler::rejectable
	RuntimeObject* ___rejectable_1;
};
// Native definition for P/Invoke marshalling of RSG.RejectHandler
struct RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_pinvoke
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};
// Native definition for COM marshalling of RSG.RejectHandler
struct RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_com
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// RSG.TimeData
struct TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 
{
	// System.Single RSG.TimeData::elapsedTime
	float ___elapsedTime_0;
	// System.Single RSG.TimeData::deltaTime
	float ___deltaTime_1;
	// System.Int32 RSG.TimeData::elapsedUpdates
	int32_t ___elapsedUpdates_2;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// RSG.Promise/ResolveHandler
struct ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 
{
	// System.Action RSG.Promise/ResolveHandler::callback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___callback_0;
	// RSG.IRejectable RSG.Promise/ResolveHandler::rejectable
	RuntimeObject* ___rejectable_1;
};
// Native definition for P/Invoke marshalling of RSG.Promise/ResolveHandler
struct ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_pinvoke
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};
// Native definition for COM marshalling of RSG.Promise/ResolveHandler
struct ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_com
{
	Il2CppMethodPointer ___callback_0;
	RuntimeObject* ___rejectable_1;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// RSG.PredicateWait
struct PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E  : public RuntimeObject
{
	// System.Func`2<RSG.TimeData,System.Boolean> RSG.PredicateWait::predicate
	Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* ___predicate_0;
	// System.Single RSG.PredicateWait::timeStarted
	float ___timeStarted_1;
	// RSG.IPendingPromise RSG.PredicateWait::pendingPromise
	RuntimeObject* ___pendingPromise_2;
	// RSG.TimeData RSG.PredicateWait::timeData
	TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___timeData_3;
	// System.Int32 RSG.PredicateWait::frameStarted
	int32_t ___frameStarted_4;
};

// RSG.PromiseState
struct PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C 
{
	// System.Int32 RSG.PromiseState::value__
	int32_t ___value___2;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// RSG.Promise
struct Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C  : public RuntimeObject
{
	// System.Exception RSG.Promise::rejectionException
	Exception_t* ___rejectionException_4;
	// System.Collections.Generic.List`1<RSG.RejectHandler> RSG.Promise::rejectHandlers
	List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* ___rejectHandlers_5;
	// System.Collections.Generic.List`1<RSG.Promise/ResolveHandler> RSG.Promise::resolveHandlers
	List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* ___resolveHandlers_6;
	// System.Collections.Generic.List`1<RSG.ProgressHandler> RSG.Promise::progressHandlers
	List_1_t43490118A5560875E8A4FE9F26A448F004750640* ___progressHandlers_7;
	// System.Int32 RSG.Promise::id
	int32_t ___id_8;
	// System.String RSG.Promise::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_9;
	// RSG.PromiseState RSG.Promise::<CurState>k__BackingField
	int32_t ___U3CCurStateU3Ek__BackingField_10;
};

struct Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields
{
	// System.Boolean RSG.Promise::EnablePromiseTracking
	bool ___EnablePromiseTracking_0;
	// System.EventHandler`1<RSG.ExceptionEventArgs> RSG.Promise::unhandlerException
	EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* ___unhandlerException_1;
	// System.Int32 RSG.Promise::nextPromiseId
	int32_t ___nextPromiseId_2;
	// System.Collections.Generic.HashSet`1<RSG.IPromiseInfo> RSG.Promise::PendingPromises
	HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* ___PendingPromises_3;
};

// RSG.PromiseCancelledException
struct PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D  : public Exception_t
{
};

// RSG.Exceptions.PromiseException
struct PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C  : public Exception_t
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Action`1<System.Exception>
struct Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04  : public MulticastDelegate_t
{
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<RSG.ProgressHandler>
struct Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC  : public MulticastDelegate_t
{
};

// System.Action`1<RSG.RejectHandler>
struct Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF  : public MulticastDelegate_t
{
};

// System.Action`1<System.Single>
struct Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A  : public MulticastDelegate_t
{
};

// System.Action`1<RSG.Promise/ResolveHandler>
struct Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751  : public MulticastDelegate_t
{
};

// System.Action`2<System.Action,System.Action`1<System.Exception>>
struct Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF  : public MulticastDelegate_t
{
};

// System.Action`2<RSG.IPromise,System.Int32>
struct Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706  : public MulticastDelegate_t
{
};

// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C  : public MulticastDelegate_t
{
};

// System.EventHandler`1<RSG.ExceptionEventArgs>
struct EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47  : public MulticastDelegate_t
{
};

// System.EventHandler`1<System.Object>
struct EventHandler_1_tD8C4A5BE1F7C91B1A7E99AE87AFD2F5432C38746  : public MulticastDelegate_t
{
};

// System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>
struct Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09  : public MulticastDelegate_t
{
};

// System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>
struct Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020  : public MulticastDelegate_t
{
};

// System.Func`1<RSG.IPromise>
struct Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057  : public MulticastDelegate_t
{
};

// System.Func`1<System.Object>
struct Func_1_tD5C081AE11746B200C711DD48DBEB00E3A9276D4  : public MulticastDelegate_t
{
};

// System.Func`2<RSG.TimeData,System.Boolean>
struct Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519  : public MulticastDelegate_t
{
};

// System.Func`3<RSG.IPromise,System.Func`1<RSG.IPromise>,RSG.IPromise>
struct Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA  : public MulticastDelegate_t
{
};

// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07  : public MulticastDelegate_t
{
};

// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// RSG.Exceptions.PromiseStateException
struct PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5  : public PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C
{
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// RSG.IPromise[]
struct IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Func`1<RSG.IPromise>[]
struct Func_1U5BU5D_t99CB06D0355E1DD5621F8A9A0D7730BC569F7449  : public RuntimeArray
{
	ALIGN_FIELD (8) Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* m_Items[1];

	inline Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// RSG.RejectHandler[]
struct RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F  : public RuntimeArray
{
	ALIGN_FIELD (8) RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E m_Items[1];

	inline RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
	inline RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
};
// RSG.Promise/ResolveHandler[]
struct ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248  : public RuntimeArray
{
	ALIGN_FIELD (8) ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 m_Items[1];

	inline ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
	inline ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
};
// RSG.ProgressHandler[]
struct ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89  : public RuntimeArray
{
	ALIGN_FIELD (8) ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF m_Items[1];

	inline ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
	inline ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___callback_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___rejectable_1), (void*)NULL);
		#endif
	}
};


// System.Void System.Func`2<RSG.TimeData,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC3DF7949B8417B26796244608836C15F61521602_gshared (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* LinkedList_1_AddLast_mF5239C871EADC44D51C6B621592A9CAC43449A2E_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, RuntimeObject* ___value0, const RuntimeMethod* method) ;
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* LinkedListNode_1_get_Value_m8F67264DC98EF442B34CE4947044BCE18BF26053_gshared_inline (LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_Remove_m6B592B94D9AEF003EAE59FCB5455DA67AB4E423C_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ___node0, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* LinkedList_1_get_First_mF743AE65DDD0324290E33D3F433F37AC83216E18_gshared_inline (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* LinkedListNode_1_get_Next_mF9F997F067DC152AB327110F922FB789EB1DE249_gshared (LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* __this, const RuntimeMethod* method) ;
// TResult System.Func`2<RSG.TimeData,System.Boolean>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_gshared_inline (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* __this, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___arg0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1__ctor_m2732A2EC5597469086D48C79A12B3563DEA501C5_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HashSet_1_Add_m2CD7657B3459B61DD4BBA47024AC71F7D319658B_gshared (HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.RejectHandler>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17_gshared (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.RejectHandler>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_gshared_inline (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* __this, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E ___item0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845_gshared (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_gshared_inline (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* __this, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 ___item0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.ProgressHandler>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757_gshared (List_1_t43490118A5560875E8A4FE9F26A448F004750640* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.ProgressHandler>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_gshared_inline (List_1_t43490118A5560875E8A4FE9F26A448F004750640* __this, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF ___item0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Single>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mA8F89FB04FEA0F48A4F22EC84B5F9ADB2914341F_gshared_inline (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* __this, float ___obj0, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.RejectHandler>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mBD51797D30E12139F80EE3C9A7B0D92DCBE60747_gshared (Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void RSG.Promises.EnumerableExt::Each<RSG.RejectHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757_gshared (RuntimeObject* ___source0, Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF* ___fn1, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.Promise/ResolveHandler>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m7BEC9E77170CE05FAB46F1BB1CFABF949814E50B_gshared (Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void RSG.Promises.EnumerableExt::Each<RSG.Promise/ResolveHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6_gshared (RuntimeObject* ___source0, Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751* ___fn1, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.ProgressHandler>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mD7CDC7503E613F3280245D351F7272519A7AB17B_gshared (Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void RSG.Promises.EnumerableExt::Each<RSG.ProgressHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29_gshared (RuntimeObject* ___source0, Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC* ___fn1, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HashSet_1_Remove_mF1D84C0A2829DDA2A0CEE1D82A5B999B5F6627CB_gshared (HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859_gshared (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_1__ctor_m663374A863E492A515BE9626B6F0E444991834E8_gshared (Func_1_tD5C081AE11746B200C711DD48DBEB00E3A9276D4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Enumerable_ToArray_TisRuntimeObject_m6B1F26FB2B3EA7B18B82FC81035440AAAEFCE924_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m07C6392BB276FFCCFD4E495842992EA26FA44882_gshared (Action_2_tAC461AE4F7B507965CE2E6A32853473F8C02CD75* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void RSG.Promises.EnumerableExt::Each<System.Object>(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumerableExt_Each_TisRuntimeObject_mBAE2D76A56B932321D152002CDE005D8E689FB78_gshared (RuntimeObject* ___source0, Action_2_tAC461AE4F7B507965CE2E6A32853473F8C02CD75* ___fn1, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_3__ctor_m7A3CDF8CC909FAEEA005D42C71F113B505F766DD_gshared (Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// TAccumulate System.Linq.Enumerable::Aggregate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Aggregate_TisRuntimeObject_TisRuntimeObject_mB8EE5BC99A611E57CA705344C56A84E9D7072F0A_gshared (RuntimeObject* ___source0, RuntimeObject* ___seed1, Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566* ___func2, const RuntimeMethod* method) ;
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,TEventArgs)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void EventHandler_1_Invoke_mBD72C04FF5A08A2EA93DFD21037CD1C27A48D07A_gshared_inline (EventHandler_1_tD8C4A5BE1F7C91B1A7E99AE87AFD2F5432C38746* __this, RuntimeObject* ___sender0, RuntimeObject* ___e1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashSet_1__ctor_m9132EE1422BAA45E44B7FFF495F378790D36D90E_gshared (HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885* __this, const RuntimeMethod* method) ;
// TResult System.Func`1<System.Object>::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_1_Invoke_m1412272198DFA4066C83206E5B43353AF10A2EEE_gshared_inline (Func_1_tD5C081AE11746B200C711DD48DBEB00E3A9276D4* __this, const RuntimeMethod* method) ;

// System.Void System.Exception::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F (Exception_t* __this, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_m3C9E276C0B8D1955883B8B927CEC3C7AF5A6593C (U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<RSG.TimeData,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC3DF7949B8417B26796244608836C15F61521602 (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC3DF7949B8417B26796244608836C15F61521602_gshared)(__this, ___object0, ___method1, method);
}
// RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* ___predicate0, const RuntimeMethod* method) ;
// System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m1D3457A043DF6BA5F5391DA47B8DA35673635FAB (U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// System.Void RSG.PredicateWait::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PredicateWait__ctor_m412EFDE9D40AEEC03FFF90B533F73FBBB5E7DBC4 (PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<RSG.PredicateWait>::AddLast(T)
inline LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* LinkedList_1_AddLast_mD499A5DF665E3F253168E79A6CCA4D0F8C4E026E (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* __this, PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* ___value0, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* (*) (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80*, PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E*, const RuntimeMethod*))LinkedList_1_AddLast_mF5239C871EADC44D51C6B621592A9CAC43449A2E_gshared)(__this, ___value0, method);
}
// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* PromiseTimer_FindInWaiting_m5EB9C79697DF112F8377897420659AEB19DA8CF6 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, RuntimeObject* ___promise0, const RuntimeMethod* method) ;
// T System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>::get_Value()
inline PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_inline (LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* __this, const RuntimeMethod* method)
{
	return ((  PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* (*) (LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*, const RuntimeMethod*))LinkedListNode_1_get_Value_m8F67264DC98EF442B34CE4947044BCE18BF26053_gshared_inline)(__this, method);
}
// System.Void RSG.PromiseCancelledException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseCancelledException__ctor_m0361C3DB96557B1C1DD6749C5951B276ADF7AEB6 (PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<RSG.PredicateWait>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
inline void LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7 (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* __this, LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___node0, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80*, LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*, const RuntimeMethod*))LinkedList_1_Remove_m6B592B94D9AEF003EAE59FCB5455DA67AB4E423C_gshared)(__this, ___node0, method);
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<RSG.PredicateWait>::get_First()
inline LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_inline (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* __this, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* (*) (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80*, const RuntimeMethod*))LinkedList_1_get_First_mF743AE65DDD0324290E33D3F433F37AC83216E18_gshared_inline)(__this, method);
}
// System.Boolean System.Int32::Equals(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_Equals_mC819D19A661B95BE253FB1769FD4D91961D20722 (int32_t* __this, int32_t ___obj0, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>::get_Next()
inline LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C (LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* __this, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* (*) (LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*, const RuntimeMethod*))LinkedListNode_1_get_Next_mF9F997F067DC152AB327110F922FB789EB1DE249_gshared)(__this, method);
}
// TResult System.Func`2<RSG.TimeData,System.Boolean>::Invoke(T)
inline bool Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_inline (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* __this, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___arg0, const RuntimeMethod* method)
{
	return ((  bool (*) (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519*, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923, const RuntimeMethod*))Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_gshared_inline)(__this, ___arg0, method);
}
// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___node0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<RSG.PredicateWait>::.ctor()
inline void LinkedList_1__ctor_mD60204AAC26CC97B7E30EE552CDF6F22622242E0 (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80*, const RuntimeMethod*))LinkedList_1__ctor_m2732A2EC5597469086D48C79A12B3563DEA501C5_gshared)(__this, method);
}
// System.Void System.EventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventArgs__ctor_mC6F9412D03203ADEF854117542C8EBF61624C8C3 (EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377* __this, const RuntimeMethod* method) ;
// System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970_inline (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, Exception_t* ___value0, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C (Delegate_t* ___a0, Delegate_t* ___b1, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116 (Delegate_t* ___source0, Delegate_t* ___value1, const RuntimeMethod* method) ;
// System.Void RSG.Promise::set_CurState(RSG.PromiseState)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Int32 RSG.Promise::NextId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D (const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.HashSet`1<RSG.IPromiseInfo>::Add(T)
inline bool HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* __this, RuntimeObject* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F*, RuntimeObject*, const RuntimeMethod*))HashSet_1_Add_m2CD7657B3459B61DD4BBA47024AC71F7D319658B_gshared)(__this, ___item0, method);
}
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Exception>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<System.Action,System.Action`1<System.Exception>>::Invoke(T1,T2)
inline void Action_2_Invoke_m73E0CEC184610BC599D0F1FDBC2BC5088BBEA144_inline (Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___arg10, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___arg21, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF*, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*, const RuntimeMethod*))Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline)(__this, ___arg10, ___arg21, method);
}
// System.Void RSG.Promise::Reject(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<RSG.RejectHandler>::.ctor()
inline void List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17 (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*, const RuntimeMethod*))List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<RSG.RejectHandler>::Add(T)
inline void List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_inline (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* __this, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E, const RuntimeMethod*))List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>::.ctor()
inline void List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845 (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*, const RuntimeMethod*))List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<RSG.Promise/ResolveHandler>::Add(T)
inline void List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_inline (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* __this, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80, const RuntimeMethod*))List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<RSG.ProgressHandler>::.ctor()
inline void List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757 (List_1_t43490118A5560875E8A4FE9F26A448F004750640* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t43490118A5560875E8A4FE9F26A448F004750640*, const RuntimeMethod*))List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<RSG.ProgressHandler>::Add(T)
inline void List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_inline (List_1_t43490118A5560875E8A4FE9F26A448F004750640* __this, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t43490118A5560875E8A4FE9F26A448F004750640*, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF, const RuntimeMethod*))List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Action`1<System.Exception>::Invoke(T)
inline void Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* __this, Exception_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*, Exception_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
// System.Void System.Action::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Single>::Invoke(T)
inline void Action_1_Invoke_mA8F89FB04FEA0F48A4F22EC84B5F9ADB2914341F_inline (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* __this, float ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*, float, const RuntimeMethod*))Action_1_Invoke_mA8F89FB04FEA0F48A4F22EC84B5F9ADB2914341F_gshared_inline)(__this, ___obj0, method);
}
// System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass34_0__ctor_m777DA7CE17AD10D18C6A866CC996D64D966176C2 (U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.RejectHandler>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mBD51797D30E12139F80EE3C9A7B0D92DCBE60747 (Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_mBD51797D30E12139F80EE3C9A7B0D92DCBE60747_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RSG.Promises.EnumerableExt::Each<RSG.RejectHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
inline void EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757 (RuntimeObject* ___source0, Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF* ___fn1, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF*, const RuntimeMethod*))EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757_gshared)(___source0, ___fn1, method);
}
// System.Void RSG.Promise::ClearHandlers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.Promise/ResolveHandler>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m7BEC9E77170CE05FAB46F1BB1CFABF949814E50B (Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m7BEC9E77170CE05FAB46F1BB1CFABF949814E50B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RSG.Promises.EnumerableExt::Each<RSG.Promise/ResolveHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
inline void EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6 (RuntimeObject* ___source0, Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751* ___fn1, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751*, const RuntimeMethod*))EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6_gshared)(___source0, ___fn1, method);
}
// System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_m1CC4BF0264C238FF06EBD6D51182AD9D9F00A44C (U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<RSG.ProgressHandler>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mD7CDC7503E613F3280245D351F7272519A7AB17B (Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_mD7CDC7503E613F3280245D351F7272519A7AB17B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RSG.Promises.EnumerableExt::Each<RSG.ProgressHandler>(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
inline void EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29 (RuntimeObject* ___source0, Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC* ___fn1, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC*, const RuntimeMethod*))EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29_gshared)(___source0, ___fn1, method);
}
// RSG.PromiseState RSG.Promise::get_CurState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m33F64A335B610F305AEA5FA8CF4C8BAAAAF257DC (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args0, const RuntimeMethod* method) ;
// System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24 (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.HashSet`1<RSG.IPromiseInfo>::Remove(T)
inline bool HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* __this, RuntimeObject* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F*, RuntimeObject*, const RuntimeMethod*))HashSet_1_Remove_mF1D84C0A2829DDA2A0CEE1D82A5B999B5F6627CB_gshared)(__this, ___item0, method);
}
// System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeRejectHandlers_m20A1B3583BF638CC7C2F0EA5B739FE7E1327BDFF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) ;
// System.Void RSG.Promise::InvokeResolveHandlers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeResolveHandlers_m5DC60EAD55ADE888A16689C026BBF0FD72CE4354 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeProgressHandlers_mDEDCF0A1858DF08B90C273FAD03AA33B15FC247F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, float ___progress0, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_m91F1EE136222A59062D07FA0A2857F6F439B74A7 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Then(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected0, const RuntimeMethod* method) ;
// System.Void RSG.Promise::set_Name(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass44_0__ctor_m1489BE8776752DD4703EBFDAA409ABA61E6CC649 (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* __this, const RuntimeMethod* method) ;
// System.String RSG.Promise::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::WithName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, String_t* ___name0, const RuntimeMethod* method) ;
// System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, RuntimeObject* ___resultPromise0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___resolveHandler1, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___rejectHandler2, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859 (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, RuntimeObject* ___resultPromise0, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___progressHandler1, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress2, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress2, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0__ctor_mE8F288904D95E3DC0025D1FBE2C15EEB625049AB (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass53_0__ctor_m92B2808BB6DFC101EDB128ADCE074E66706EA045 (U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___callback0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) ;
// System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___callback0, RuntimeObject* ___rejectable1, Exception_t* ___value2, const RuntimeMethod* method) ;
// System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddResolveHandler_m99E30F6C68B539076C0E8D07FF05B4E6BE2A56E6 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) ;
// System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddRejectHandler_mE5BC8F2F50C3C7B1A2AC8CA7FCBF933FECDA0550 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) ;
// System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddProgressHandler_m0898CAB05023C68BD94E91C3496CA3B30C20471E (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass56_0__ctor_mB81C3730446AE8C66A7D3EAFE2F0D416873CD51C (U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* __this, const RuntimeMethod* method) ;
// System.Void System.Func`1<RSG.IPromise>::.ctor(System.Object,System.IntPtr)
inline void Func_1__ctor_m79F0C3727A7C602376F5C93BBCCEF4F36FC7F6E0 (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_1__ctor_m663374A863E492A515BE9626B6F0E444991834E8_gshared)(__this, ___object0, ___method1, method);
}
// RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved0, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2 (RuntimeObject* ___promises0, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_0__ctor_m2D14CCC0D09F29E28A2479BD4966A6EEAE384627 (U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* __this, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<RSG.IPromise>(System.Collections.Generic.IEnumerable`1<TSource>)
inline IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m6B1F26FB2B3EA7B18B82FC81035440AAAEFCE924_gshared)(___source0, method);
}
// RSG.IPromise RSG.Promise::Resolved()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C (const RuntimeMethod* method) ;
// System.Void System.Action`2<RSG.IPromise,System.Int32>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m4637341E7264727D66C053B4D7EA1A5A1F656CCF (Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m07C6392BB276FFCCFD4E495842992EA26FA44882_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RSG.Promises.EnumerableExt::Each<RSG.IPromise>(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
inline void EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F (RuntimeObject* ___source0, Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706* ___fn1, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706*, const RuntimeMethod*))EnumerableExt_Each_TisRuntimeObject_mBAE2D76A56B932321D152002CDE005D8E689FB78_gshared)(___source0, ___fn1, method);
}
// System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass60_0__ctor_m658D101B81BC36F252DED37541EFDB385BA526E4 (U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* __this, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F (RuntimeObject* ___fns0, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_0__ctor_mD8148B137ABC682149B7AC033FFC943A4ED35CD1 (U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* __this, const RuntimeMethod* method) ;
// System.Void System.Func`3<RSG.IPromise,System.Func`1<RSG.IPromise>,RSG.IPromise>::.ctor(System.Object,System.IntPtr)
inline void Func_3__ctor_m1F201FC7B30B14E42021E35E5108587C9FF84975 (Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_3__ctor_m7A3CDF8CC909FAEEA005D42C71F113B505F766DD_gshared)(__this, ___object0, ___method1, method);
}
// TAccumulate System.Linq.Enumerable::Aggregate<System.Func`1<RSG.IPromise>,RSG.IPromise>(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
inline RuntimeObject* Enumerable_Aggregate_TisFunc_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mF56D75615C1ACB3B5BD8F167EF9822719DB356CE (RuntimeObject* ___source0, RuntimeObject* ___seed1, Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA* ___func2, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA*, const RuntimeMethod*))Enumerable_Aggregate_TisRuntimeObject_TisRuntimeObject_mB8EE5BC99A611E57CA705344C56A84E9D7072F0A_gshared)(___source0, ___seed1, ___func2, method);
}
// System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass63_0__ctor_mF531911D8BEFC44215B51FEBB012E039F51266C8 (U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* __this, const RuntimeMethod* method) ;
// RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E (RuntimeObject* ___promises0, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_0__ctor_m2B345ACF4133F168C6638758CDE9864531934908 (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* __this, const RuntimeMethod* method) ;
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mE4CB6F4712AB6D99A2358FBAE2E052B3EE976162 (InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void RSG.Promise::Resolve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass69_0__ctor_m46CE508BDC0469757590835B30D4A0169716ABF3 (U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass70_0__ctor_mE72A5373F806CE548BA3484362F2F7FE433EEBFE (U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* __this, const RuntimeMethod* method) ;
// System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionEventArgs__ctor_m1D932BFCF3873476A40B88F8AA75EFB400812224 (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, Exception_t* ___exception0, const RuntimeMethod* method) ;
// System.Void System.EventHandler`1<RSG.ExceptionEventArgs>::Invoke(System.Object,TEventArgs)
inline void EventHandler_1_Invoke_m191057E67DEAF55D715D23C721E75751DF50A522_inline (EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* __this, RuntimeObject* ___sender0, ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* ___e1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*, RuntimeObject*, ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777*, const RuntimeMethod*))EventHandler_1_Invoke_mBD72C04FF5A08A2EA93DFD21037CD1C27A48D07A_gshared_inline)(__this, ___sender0, ___e1, method);
}
// System.Void System.Collections.Generic.HashSet`1<RSG.IPromiseInfo>::.ctor()
inline void HashSet_1__ctor_mFE82769CA93910E0390206067960C72201679309 (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F*, const RuntimeMethod*))HashSet_1__ctor_m9132EE1422BAA45E44B7FFF495F378790D36D90E_gshared)(__this, method);
}
// System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48 (RuntimeObject* ___sender0, Exception_t* ___ex1, const RuntimeMethod* method) ;
// System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeProgressHandler_m3E4DD72481BD449FA36A24A6324DD23FA8367589 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___callback0, RuntimeObject* ___rejectable1, float ___progress2, const RuntimeMethod* method) ;
// System.Void RSG.Promise::ReportProgress(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, float ___progress0, const RuntimeMethod* method) ;
// TResult System.Func`1<RSG.IPromise>::Invoke()
inline RuntimeObject* Func_1_Invoke_m7A181FD07D8925150B330643541A778696741881_inline (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*, const RuntimeMethod*))Func_1_Invoke_m1412272198DFA4066C83206E5B43353AF10A2EEE_gshared_inline)(__this, method);
}
// TResult System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>::Invoke()
inline RuntimeObject* Func_1_Invoke_mFC397D76F95BD65F3B978512AFAA4921FDA70A27_inline (Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020*, const RuntimeMethod*))Func_1_Invoke_m1412272198DFA4066C83206E5B43353AF10A2EEE_gshared_inline)(__this, method);
}
// System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_1__ctor_mBB0688A58C068F7B05F67132C0437CA689E3540E (U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* __this, const RuntimeMethod* method) ;
// System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Enumerable_Average_m49C1D6444AF329FA3500CE9D16E043FCE590D9C9 (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// TResult System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>::Invoke()
inline RuntimeObject* Func_1_Invoke_mD078A377AF06EC32292E6C2AA35582D0EE566CF3_inline (Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09* __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09*, const RuntimeMethod*))Func_1_Invoke_m1412272198DFA4066C83206E5B43353AF10A2EEE_gshared_inline)(__this, method);
}
// System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_1__ctor_m378111927982C2FF6C8AAA87AC31A3F631D36495 (U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* __this, const RuntimeMethod* method) ;
// System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_1__ctor_mF9A6583E6F209770FC8B76F1E90E1AE9BE64653A (U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* __this, const RuntimeMethod* method) ;
// System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Enumerable_Max_mA30ECB22B118A464652A20E12E0097D8A952531D (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9BC141AAB08F47C34B7ED40C1A6C0C1ADDEC5CB3 (Exception_t* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void RSG.Exceptions.PromiseException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_m555AAF5903923E345DE642D648F00143A5442218 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, const RuntimeMethod* method) ;
// System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_mA3A95CC09C60213917698EA0D1F4E49ADE76AB20 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_m39A3D4D0F18B6923549CB458A5FB67BD56524EA0 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, String_t* ___message0, Exception_t* ___inner1, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.PromiseCancelledException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseCancelledException__ctor_m44309A22F64391524EDCB4DB860D41D28DD0AA34 (PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F(__this, NULL);
		return;
	}
}
// System.Void RSG.PromiseCancelledException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseCancelledException__ctor_m0361C3DB96557B1C1DD6749C5951B276ADF7AEB6 (PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(__this, L_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.PredicateWait::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PredicateWait__ctor_m412EFDE9D40AEEC03FFF90B533F73FBBB5E7DBC4 (PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PromiseTimer_WaitFor_mFBADA409CEBBCA185DF032B513BA1F9B55AF8D0B (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, float ___seconds0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* L_0 = (U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass3_0__ctor_m3C9E276C0B8D1955883B8B927CEC3C7AF5A6593C(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* L_1 = V_0;
		float L_2 = ___seconds0;
		NullCheck(L_1);
		L_1->___seconds_0 = L_2;
		U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* L_3 = V_0;
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_4 = (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519*)il2cpp_codegen_object_new(Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_mC3DF7949B8417B26796244608836C15F61521602(L_4, L_3, (intptr_t)((void*)U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E_RuntimeMethod_var), NULL);
		RuntimeObject* L_5;
		L_5 = PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE(__this, L_4, NULL);
		V_1 = L_5;
		goto IL_0023;
	}

IL_0023:
	{
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PromiseTimer_WaitWhile_m31CC9654EF84376642C87603F4506322F7AEAC41 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* ___predicate0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* L_0 = (U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass4_0__ctor_m1D3457A043DF6BA5F5391DA47B8DA35673635FAB(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* L_1 = V_0;
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_2 = ___predicate0;
		NullCheck(L_1);
		L_1->___predicate_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___predicate_0), (void*)L_2);
		U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* L_3 = V_0;
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_4 = (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519*)il2cpp_codegen_object_new(Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_mC3DF7949B8417B26796244608836C15F61521602(L_4, L_3, (intptr_t)((void*)U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4_RuntimeMethod_var), NULL);
		RuntimeObject* L_5;
		L_5 = PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE(__this, L_4, NULL);
		V_1 = L_5;
		goto IL_0023;
	}

IL_0023:
	{
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* ___predicate0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_AddLast_mD499A5DF665E3F253168E79A6CCA4D0F8C4E026E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* V_0 = NULL;
	PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_0, NULL);
		V_0 = L_0;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_1 = (PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E*)il2cpp_codegen_object_new(PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		PredicateWait__ctor_m412EFDE9D40AEEC03FFF90B533F73FBBB5E7DBC4(L_1, NULL);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_2 = L_1;
		float L_3 = __this->___curTime_0;
		NullCheck(L_2);
		L_2->___timeStarted_1 = L_3;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_4 = L_2;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_5 = V_0;
		NullCheck(L_4);
		L_4->___pendingPromise_2 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&L_4->___pendingPromise_2), (void*)L_5);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_6 = L_4;
		NullCheck(L_6);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923* L_7 = (&L_6->___timeData_3);
		il2cpp_codegen_initobj(L_7, sizeof(TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923));
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_8 = L_6;
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_9 = ___predicate0;
		NullCheck(L_8);
		L_8->___predicate_0 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&L_8->___predicate_0), (void*)L_9);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_10 = L_8;
		int32_t L_11 = __this->___curFrame_1;
		NullCheck(L_10);
		L_10->___frameStarted_4 = L_11;
		V_1 = L_10;
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_12 = __this->___waiting_2;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_13 = V_1;
		NullCheck(L_12);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_14;
		L_14 = LinkedList_1_AddLast_mD499A5DF665E3F253168E79A6CCA4D0F8C4E026E(L_12, L_13, LinkedList_1_AddLast_mD499A5DF665E3F253168E79A6CCA4D0F8C4E026E_RuntimeMethod_var);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_15 = V_0;
		V_2 = L_15;
		goto IL_0050;
	}

IL_0050:
	{
		RuntimeObject* L_16 = V_2;
		return L_16;
	}
}
// System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PromiseTimer_Cancel_m470F6FFA6B830E6F7D97D2A68C6B71C26DCD6C57 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, RuntimeObject* ___promise0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral304B1BC7B1FAA5EA26BCEEA9C69D283F782FD8FC);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		RuntimeObject* L_0 = ___promise0;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_1;
		L_1 = PromiseTimer_FindInWaiting_m5EB9C79697DF112F8377897420659AEB19DA8CF6(__this, L_0, NULL);
		V_0 = L_1;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_2 = V_0;
		V_1 = (bool)((((RuntimeObject*)(LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*)L_2) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0016;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0042;
	}

IL_0016:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_4 = V_0;
		NullCheck(L_4);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_5;
		L_5 = LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_inline(L_4, LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		NullCheck(L_5);
		RuntimeObject* L_6 = L_5->___pendingPromise_2;
		PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D* L_7 = (PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D*)il2cpp_codegen_object_new(PromiseCancelledException_tE9D6401DEE8132EEEC6C3AED768D8BC51588AE8D_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		PromiseCancelledException__ctor_m0361C3DB96557B1C1DD6749C5951B276ADF7AEB6(L_7, _stringLiteral304B1BC7B1FAA5EA26BCEEA9C69D283F782FD8FC, NULL);
		NullCheck(L_6);
		InterfaceActionInvoker1< Exception_t* >::Invoke(0 /* System.Void RSG.IRejectable::Reject(System.Exception) */, IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var, L_6, L_7);
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_8 = __this->___waiting_2;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_9 = V_0;
		NullCheck(L_8);
		LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7(L_8, L_9, LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7_RuntimeMethod_var);
		V_2 = (bool)1;
		goto IL_0042;
	}

IL_0042:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* PromiseTimer_FindInWaiting_m5EB9C79697DF112F8377897420659AEB19DA8CF6 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, RuntimeObject* ___promise0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_3 = NULL;
	bool V_4 = false;
	{
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_0 = __this->___waiting_2;
		NullCheck(L_0);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_1;
		L_1 = LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_inline(L_0, LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_003f;
	}

IL_000f:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_2 = V_0;
		NullCheck(L_2);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_3;
		L_3 = LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_inline(L_2, LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		NullCheck(L_3);
		RuntimeObject* L_4 = L_3->___pendingPromise_2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 RSG.IPendingPromise::get_Id() */, IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59_il2cpp_TypeInfo_var, L_4);
		V_2 = L_5;
		RuntimeObject* L_6 = ___promise0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 RSG.IPromise::get_Id() */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_6);
		bool L_8;
		L_8 = Int32_Equals_mC819D19A661B95BE253FB1769FD4D91961D20722((&V_2), L_7, NULL);
		V_1 = L_8;
		bool L_9 = V_1;
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_10 = V_0;
		V_3 = L_10;
		goto IL_004d;
	}

IL_0037:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_11 = V_0;
		NullCheck(L_11);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_12;
		L_12 = LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C(L_11, LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		V_0 = L_12;
	}

IL_003f:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_13 = V_0;
		V_4 = (bool)((!(((RuntimeObject*)(LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*)L_13) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_14 = V_4;
		if (L_14)
		{
			goto IL_000f;
		}
	}
	{
		V_3 = (LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*)NULL;
		goto IL_004d;
	}

IL_004d:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_15 = V_3;
		return L_15;
	}
}
// System.Void RSG.PromiseTimer::Update(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseTimer_Update_mF954CE03B679A20DAF5DD99B33A5CB419E841013 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, float ___deltaTime0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_0 = NULL;
	PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* V_1 = NULL;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	bool V_4 = false;
	Exception_t* V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		float L_0 = __this->___curTime_0;
		float L_1 = ___deltaTime0;
		__this->___curTime_0 = ((float)il2cpp_codegen_add(L_0, L_1));
		int32_t L_2 = __this->___curFrame_1;
		__this->___curFrame_1 = ((int32_t)il2cpp_codegen_add(L_2, 1));
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_3 = __this->___waiting_2;
		NullCheck(L_3);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_4;
		L_4 = LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_inline(L_3, LinkedList_1_get_First_m70654B35C2C218E6A69BB649AECAB1430DB04DCF_RuntimeMethod_var);
		V_0 = L_4;
		goto IL_00de;
	}

IL_002e:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_5 = V_0;
		NullCheck(L_5);
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_6;
		L_6 = LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_inline(L_5, LinkedListNode_1_get_Value_m32C7B805BAB7407EBD9C4A34818FC590A84D89A1_RuntimeMethod_var);
		V_1 = L_6;
		float L_7 = __this->___curTime_0;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_8 = V_1;
		NullCheck(L_8);
		float L_9 = L_8->___timeStarted_1;
		V_2 = ((float)il2cpp_codegen_subtract(L_7, L_9));
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_10 = V_1;
		NullCheck(L_10);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923* L_11 = (&L_10->___timeData_3);
		float L_12 = V_2;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_13 = V_1;
		NullCheck(L_13);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923* L_14 = (&L_13->___timeData_3);
		float L_15 = L_14->___elapsedTime_0;
		L_11->___deltaTime_1 = ((float)il2cpp_codegen_subtract(L_12, L_15));
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_16 = V_1;
		NullCheck(L_16);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923* L_17 = (&L_16->___timeData_3);
		float L_18 = V_2;
		L_17->___elapsedTime_0 = L_18;
		int32_t L_19 = __this->___curFrame_1;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_20 = V_1;
		NullCheck(L_20);
		int32_t L_21 = L_20->___frameStarted_4;
		V_3 = ((int32_t)il2cpp_codegen_subtract(L_19, L_21));
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_22 = V_1;
		NullCheck(L_22);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923* L_23 = (&L_22->___timeData_3);
		int32_t L_24 = V_3;
		L_23->___elapsedUpdates_2 = L_24;
	}
	try
	{// begin try (depth: 1)
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_25 = V_1;
		NullCheck(L_25);
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_26 = L_25->___predicate_0;
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_27 = V_1;
		NullCheck(L_27);
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 L_28 = L_27->___timeData_3;
		NullCheck(L_26);
		bool L_29;
		L_29 = Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_inline(L_26, L_28, NULL);
		V_4 = L_29;
		goto IL_00b4;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0099;
		}
		throw e;
	}

CATCH_0099:
	{// begin catch(System.Exception)
		V_5 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_30 = V_1;
		NullCheck(L_30);
		RuntimeObject* L_31 = L_30->___pendingPromise_2;
		Exception_t* L_32 = V_5;
		NullCheck(L_31);
		InterfaceActionInvoker1< Exception_t* >::Invoke(0 /* System.Void RSG.IRejectable::Reject(System.Exception) */, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var)), L_31, L_32);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_33 = V_0;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_34;
		L_34 = PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F(__this, L_33, NULL);
		V_0 = L_34;
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_00de;
	}// end catch (depth: 1)

IL_00b4:
	{
		bool L_35 = V_4;
		V_6 = L_35;
		bool L_36 = V_6;
		if (!L_36)
		{
			goto IL_00d4;
		}
	}
	{
		PredicateWait_tE5BDE52B329404C58C927AEF6D26BC1FC90B553E* L_37 = V_1;
		NullCheck(L_37);
		RuntimeObject* L_38 = L_37->___pendingPromise_2;
		NullCheck(L_38);
		InterfaceActionInvoker0::Invoke(1 /* System.Void RSG.IPendingPromise::Resolve() */, IPendingPromise_tD14FAB130A215AA2C523BA24F1AB17F17D87FA59_il2cpp_TypeInfo_var, L_38);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_39 = V_0;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_40;
		L_40 = PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F(__this, L_39, NULL);
		V_0 = L_40;
		goto IL_00dd;
	}

IL_00d4:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_41 = V_0;
		NullCheck(L_41);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_42;
		L_42 = LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C(L_41, LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		V_0 = L_42;
	}

IL_00dd:
	{
	}

IL_00de:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_43 = V_0;
		V_7 = (bool)((!(((RuntimeObject*)(LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B*)L_43) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_44 = V_7;
		if (L_44)
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* ___node0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_0 = NULL;
	LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* V_1 = NULL;
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_0 = ___node0;
		V_0 = L_0;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_1 = ___node0;
		NullCheck(L_1);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_2;
		L_2 = LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C(L_1, LinkedListNode_1_get_Next_mF6E67EE888125B8862B3CDEFE0ECBEED9154D76C_RuntimeMethod_var);
		___node0 = L_2;
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_3 = __this->___waiting_2;
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_4 = V_0;
		NullCheck(L_3);
		LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7(L_3, L_4, LinkedList_1_Remove_m0D8AC4551872ACAE24D389F83E396CC48412E4D7_RuntimeMethod_var);
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_5 = ___node0;
		V_1 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		LinkedListNode_1_t58674A015BC41173681C6ED48BB28D4724A1807B* L_6 = V_1;
		return L_6;
	}
}
// System.Void RSG.PromiseTimer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseTimer__ctor_m57B7BDB6A53486AFE172E95E855F0D37EF1F3341 (PromiseTimer_t1583671A55C64ED69E8A516294EA07302EF7067B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1__ctor_mD60204AAC26CC97B7E30EE552CDF6F22622242E0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80* L_0 = (LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80*)il2cpp_codegen_object_new(LinkedList_1_t129C60B78B75EC0A8784D1859EEB00F5119D7B80_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		LinkedList_1__ctor_mD60204AAC26CC97B7E30EE552CDF6F22622242E0(L_0, LinkedList_1__ctor_mD60204AAC26CC97B7E30EE552CDF6F22622242E0_RuntimeMethod_var);
		__this->___waiting_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___waiting_2), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_m3C9E276C0B8D1955883B8B927CEC3C7AF5A6593C (U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Boolean RSG.PromiseTimer/<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E (U3CU3Ec__DisplayClass3_0_tB185ECB0B52BB6709389D216CB9354DD9F92B178* __this, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___t0, const RuntimeMethod* method) 
{
	{
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 L_0 = ___t0;
		float L_1 = L_0.___elapsedTime_0;
		float L_2 = __this->___seconds_0;
		return (bool)((((int32_t)((!(((float)L_1) >= ((float)L_2)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m1D3457A043DF6BA5F5391DA47B8DA35673635FAB (U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Boolean RSG.PromiseTimer/<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4 (U3CU3Ec__DisplayClass4_0_tCA5047B76959EEBB732C6BA4FAD7F8EAFFEF5C75* __this, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___t0, const RuntimeMethod* method) 
{
	{
		Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* L_0 = __this->___predicate_0;
		TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 L_1 = ___t0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_inline(L_0, L_1, NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionEventArgs__ctor_m1D932BFCF3873476A40B88F8AA75EFB400812224 (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, Exception_t* ___exception0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_il2cpp_TypeInfo_var);
		EventArgs__ctor_mC6F9412D03203ADEF854117542C8EBF61624C8C3(__this, NULL);
		Exception_t* L_0 = ___exception0;
		ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970_inline(__this, L_0, NULL);
		return;
	}
}
// System.Exception RSG.ExceptionEventArgs::get_Exception()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t* ExceptionEventArgs_get_Exception_mA8561281DD20ADE854ED9177B6B116BD5301FCEF (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, const RuntimeMethod* method) 
{
	{
		Exception_t* L_0 = __this->___U3CExceptionU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970 (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, Exception_t* ___value0, const RuntimeMethod* method) 
{
	{
		Exception_t* L_0 = ___value0;
		__this->___U3CExceptionU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CExceptionU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RSG.RejectHandler
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_pinvoke(const RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E& unmarshaled, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_pinvoke& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'RejectHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_pinvoke_back(const RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_pinvoke& marshaled, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'RejectHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.RejectHandler
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_pinvoke_cleanup(RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: RSG.RejectHandler
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_com(const RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E& unmarshaled, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_com& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'RejectHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_com_back(const RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_com& marshaled, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'RejectHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.RejectHandler
IL2CPP_EXTERN_C void RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshal_com_cleanup(RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RSG.ProgressHandler
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_pinvoke(const ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF& unmarshaled, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_pinvoke& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ProgressHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_pinvoke_back(const ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_pinvoke& marshaled, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ProgressHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.ProgressHandler
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_pinvoke_cleanup(ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: RSG.ProgressHandler
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_com(const ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF& unmarshaled, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_com& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ProgressHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_com_back(const ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_com& marshaled, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ProgressHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.ProgressHandler
IL2CPP_EXTERN_C void ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshal_com_cleanup(ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_add_UnhandledException_m0FBD6C63F54715BEF0B694A2D41408FD229E8DBC (EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_0 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1;
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_1 = ___value0;
		Delegate_t* L_2;
		L_2 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_0, L_1, NULL);
		((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1 = ((EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*)Castclass((RuntimeObject*)L_2, EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((void**)(&((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1), (void*)((EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*)Castclass((RuntimeObject*)L_2, EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_remove_UnhandledException_m578D0DFBFEAEE169BEDD5EF654E0CC150ACC3E4F (EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_0 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1;
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_1 = ___value0;
		Delegate_t* L_2;
		L_2 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_0, L_1, NULL);
		((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1 = ((EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*)Castclass((RuntimeObject*)L_2, EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((void**)(&((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1), (void*)((EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*)Castclass((RuntimeObject*)L_2, EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_GetPendingPromises_mC8A8CF26DF363FB48A483EEA030A25EF5CC61F72 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_0 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 RSG.Promise::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Promise_get_Id_m8A3E1CBF297D2437C988C4DE730EE80CF360058F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___id_8;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.String RSG.Promise::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void RSG.Promise::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___value0;
		__this->___U3CNameU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
// RSG.PromiseState RSG.Promise::get_CurState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CCurStateU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void RSG.Promise::set_CurState(RSG.PromiseState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___value0;
		__this->___U3CCurStateU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.Void RSG.Promise::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline(__this, 0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		int32_t L_0;
		L_0 = Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D(NULL);
		__this->___id_8 = L_0;
		bool L_1 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___EnablePromiseTracking_0;
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_3 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3;
		NullCheck(L_3);
		bool L_4;
		L_4 = HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF(L_3, __this, HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF_RuntimeMethod_var);
	}

IL_0032:
	{
		return;
	}
}
// System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise__ctor_mD6C7CBA18CF33E760A9A853224E3C31C5C102DC3 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF* ___resolver0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t* V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline(__this, 0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		int32_t L_0;
		L_0 = Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D(NULL);
		__this->___id_8 = L_0;
		bool L_1 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___EnablePromiseTracking_0;
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_3 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3;
		NullCheck(L_3);
		bool L_4;
		L_4 = HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF(L_3, __this, HashSet_1_Add_m068728DDDBF159DD59C03E047180D3B2038F80CF_RuntimeMethod_var);
	}

IL_0032:
	{
	}
	try
	{// begin try (depth: 1)
		Action_2_tDA8D8864F154F4861A3356B1A77C7C729AF6CFEF* L_5 = ___resolver0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_6 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_6, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 29)), NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_7 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_7, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 31)), NULL);
		NullCheck(L_5);
		Action_2_Invoke_m73E0CEC184610BC599D0F1FDBC2BC5088BBEA144_inline(L_5, L_6, L_7, NULL);
		goto IL_0065;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0058;
		}
		throw e;
	}

CATCH_0058:
	{// begin catch(System.Exception)
		V_1 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		Exception_t* L_8 = V_1;
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(__this, L_8, NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0065;
	}// end catch (depth: 1)

IL_0065:
	{
		return;
	}
}
// System.Int32 RSG.Promise::NextId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___nextPromiseId_2;
		int32_t L_1 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___nextPromiseId_2 = L_1;
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddRejectHandler_mE5BC8F2F50C3C7B1A2AC8CA7FCBF933FECDA0550 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* L_0 = __this->___rejectHandlers_5;
		V_0 = (bool)((((RuntimeObject*)(List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* L_2 = (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*)il2cpp_codegen_object_new(List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17(L_2, List_1__ctor_m51FEA4BDEC0B469507E4811F5B997764BEEF1C17_RuntimeMethod_var);
		__this->___rejectHandlers_5 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___rejectHandlers_5), (void*)L_2);
	}

IL_001b:
	{
		List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* L_3 = __this->___rejectHandlers_5;
		il2cpp_codegen_initobj((&V_1), sizeof(RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E));
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_4 = ___onRejected0;
		(&V_1)->___callback_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___callback_0), (void*)L_4);
		RuntimeObject* L_5 = ___rejectable1;
		(&V_1)->___rejectable_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___rejectable_1), (void*)L_5);
		RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E L_6 = V_1;
		NullCheck(L_3);
		List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_inline(L_3, L_6, List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_RuntimeMethod_var);
		return;
	}
}
// System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddResolveHandler_m99E30F6C68B539076C0E8D07FF05B4E6BE2A56E6 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* L_0 = __this->___resolveHandlers_6;
		V_0 = (bool)((((RuntimeObject*)(List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* L_2 = (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*)il2cpp_codegen_object_new(List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845(L_2, List_1__ctor_m1176CCB618D735E8953BEEE754F88DC3E46AF845_RuntimeMethod_var);
		__this->___resolveHandlers_6 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___resolveHandlers_6), (void*)L_2);
	}

IL_001b:
	{
		List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* L_3 = __this->___resolveHandlers_6;
		il2cpp_codegen_initobj((&V_1), sizeof(ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80));
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___onResolved0;
		(&V_1)->___callback_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___callback_0), (void*)L_4);
		RuntimeObject* L_5 = ___rejectable1;
		(&V_1)->___rejectable_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___rejectable_1), (void*)L_5);
		ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 L_6 = V_1;
		NullCheck(L_3);
		List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_inline(L_3, L_6, List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_RuntimeMethod_var);
		return;
	}
}
// System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_AddProgressHandler_m0898CAB05023C68BD94E91C3496CA3B30C20471E (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t43490118A5560875E8A4FE9F26A448F004750640_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		List_1_t43490118A5560875E8A4FE9F26A448F004750640* L_0 = __this->___progressHandlers_7;
		V_0 = (bool)((((RuntimeObject*)(List_1_t43490118A5560875E8A4FE9F26A448F004750640*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		List_1_t43490118A5560875E8A4FE9F26A448F004750640* L_2 = (List_1_t43490118A5560875E8A4FE9F26A448F004750640*)il2cpp_codegen_object_new(List_1_t43490118A5560875E8A4FE9F26A448F004750640_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757(L_2, List_1__ctor_m13D956F2944437E94D4DE53ACC39A15A0863D757_RuntimeMethod_var);
		__this->___progressHandlers_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___progressHandlers_7), (void*)L_2);
	}

IL_001b:
	{
		List_1_t43490118A5560875E8A4FE9F26A448F004750640* L_3 = __this->___progressHandlers_7;
		il2cpp_codegen_initobj((&V_1), sizeof(ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF));
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_4 = ___onProgress0;
		(&V_1)->___callback_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___callback_0), (void*)L_4);
		RuntimeObject* L_5 = ___rejectable1;
		(&V_1)->___rejectable_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_1)->___rejectable_1), (void*)L_5);
		ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF L_6 = V_1;
		NullCheck(L_3);
		List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_inline(L_3, L_6, List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_RuntimeMethod_var);
		return;
	}
}
// System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___callback0, RuntimeObject* ___rejectable1, Exception_t* ___value2, const RuntimeMethod* method) 
{
	Exception_t* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
	}
	try
	{// begin try (depth: 1)
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_0 = ___callback0;
		Exception_t* L_1 = ___value2;
		NullCheck(L_0);
		Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline(L_0, L_1, NULL);
		goto IL_001a;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_000d;
		}
		throw e;
	}

CATCH_000d:
	{// begin catch(System.Exception)
		V_0 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		RuntimeObject* L_2 = ___rejectable1;
		Exception_t* L_3 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker1< Exception_t* >::Invoke(0 /* System.Void RSG.IRejectable::Reject(System.Exception) */, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var)), L_2, L_3);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_001a;
	}// end catch (depth: 1)

IL_001a:
	{
		return;
	}
}
// System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___callback0, RuntimeObject* ___rejectable1, const RuntimeMethod* method) 
{
	Exception_t* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
	}
	try
	{// begin try (depth: 1)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = ___callback0;
		NullCheck(L_0);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_0, NULL);
		goto IL_0019;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_000c;
		}
		throw e;
	}

CATCH_000c:
	{// begin catch(System.Exception)
		V_0 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		RuntimeObject* L_1 = ___rejectable1;
		Exception_t* L_2 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker1< Exception_t* >::Invoke(0 /* System.Void RSG.IRejectable::Reject(System.Exception) */, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var)), L_1, L_2);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0019;
	}// end catch (depth: 1)

IL_0019:
	{
		return;
	}
}
// System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeProgressHandler_m3E4DD72481BD449FA36A24A6324DD23FA8367589 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___callback0, RuntimeObject* ___rejectable1, float ___progress2, const RuntimeMethod* method) 
{
	Exception_t* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
	}
	try
	{// begin try (depth: 1)
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_0 = ___callback0;
		float L_1 = ___progress2;
		NullCheck(L_0);
		Action_1_Invoke_mA8F89FB04FEA0F48A4F22EC84B5F9ADB2914341F_inline(L_0, L_1, NULL);
		goto IL_001a;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_000d;
		}
		throw e;
	}

CATCH_000d:
	{// begin catch(System.Exception)
		V_0 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		RuntimeObject* L_2 = ___rejectable1;
		Exception_t* L_3 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker1< Exception_t* >::Invoke(0 /* System.Void RSG.IRejectable::Reject(System.Exception) */, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IRejectable_tA7307F9536B35C85EEFA2372C5A6E726E9B92361_il2cpp_TypeInfo_var)), L_2, L_3);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_001a;
	}// end catch (depth: 1)

IL_001a:
	{
		return;
	}
}
// System.Void RSG.Promise::ClearHandlers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	{
		__this->___rejectHandlers_5 = (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___rejectHandlers_5), (void*)(List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*)NULL);
		__this->___resolveHandlers_6 = (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___resolveHandlers_6), (void*)(List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*)NULL);
		__this->___progressHandlers_7 = (List_1_t43490118A5560875E8A4FE9F26A448F004750640*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___progressHandlers_7), (void*)(List_1_t43490118A5560875E8A4FE9F26A448F004750640*)NULL);
		return;
	}
}
// System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeRejectHandlers_m20A1B3583BF638CC7C2F0EA5B739FE7E1327BDFF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* L_0 = (U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass34_0__ctor_m777DA7CE17AD10D18C6A866CC996D64D966176C2(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_0 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_0), (void*)__this);
		U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* L_2 = V_0;
		Exception_t* L_3 = ___ex0;
		NullCheck(L_2);
		L_2->___ex_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___ex_1), (void*)L_3);
		List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* L_4 = __this->___rejectHandlers_5;
		V_1 = (bool)((!(((RuntimeObject*)(List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*)L_4) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* L_6 = __this->___rejectHandlers_5;
		U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* L_7 = V_0;
		Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF* L_8 = (Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF*)il2cpp_codegen_object_new(Action_1_t032C5F0DFF9C0FE7C556043723A99902E51C87CF_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		Action_1__ctor_mBD51797D30E12139F80EE3C9A7B0D92DCBE60747(L_8, L_7, (intptr_t)((void*)U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61_RuntimeMethod_var), NULL);
		EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757(L_6, L_8, EnumerableExt_Each_TisRejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E_m904547938F6DBA8E04E0C36FC341F67769F3C757_RuntimeMethod_var);
	}

IL_003c:
	{
		Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise::InvokeResolveHandlers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeResolveHandlers_m5DC60EAD55ADE888A16689C026BBF0FD72CE4354 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* L_0 = __this->___resolveHandlers_6;
		V_0 = (bool)((!(((RuntimeObject*)(List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* L_2 = __this->___resolveHandlers_6;
		Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751* L_3 = (Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751*)il2cpp_codegen_object_new(Action_1_tB2BA527749E35DAD194C7473AD1657A21272D751_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		Action_1__ctor_m7BEC9E77170CE05FAB46F1BB1CFABF949814E50B(L_3, __this, (intptr_t)((void*)Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF_RuntimeMethod_var), NULL);
		EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6(L_2, L_3, EnumerableExt_Each_TisResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_m5E28C575AA5BC4FB493C3A4D0356FEA2D27F87C6_RuntimeMethod_var);
	}

IL_0028:
	{
		Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_InvokeProgressHandlers_mDEDCF0A1858DF08B90C273FAD03AA33B15FC247F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, float ___progress0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* L_0 = (U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass36_0__ctor_m1CC4BF0264C238FF06EBD6D51182AD9D9F00A44C(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_0 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_0), (void*)__this);
		U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* L_2 = V_0;
		float L_3 = ___progress0;
		NullCheck(L_2);
		L_2->___progress_1 = L_3;
		List_1_t43490118A5560875E8A4FE9F26A448F004750640* L_4 = __this->___progressHandlers_7;
		V_1 = (bool)((!(((RuntimeObject*)(List_1_t43490118A5560875E8A4FE9F26A448F004750640*)L_4) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		List_1_t43490118A5560875E8A4FE9F26A448F004750640* L_6 = __this->___progressHandlers_7;
		U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* L_7 = V_0;
		Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC* L_8 = (Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC*)il2cpp_codegen_object_new(Action_1_t248BE535A537AA9766C245FA7FE16812729C39BC_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		Action_1__ctor_mD7CDC7503E613F3280245D351F7272519A7AB17B(L_8, L_7, (intptr_t)((void*)U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506_RuntimeMethod_var), NULL);
		EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29(L_6, L_8, EnumerableExt_Each_TisProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF_m6E0521C1F42ABD20C2DCFA5D3ABBC87BE48E4F29_RuntimeMethod_var);
	}

IL_003c:
	{
		return;
	}
}
// System.Void RSG.Promise::Reject(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		int32_t L_0;
		L_0 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_0 = (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var)), (uint32_t)4);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral71E2B3599FE35851F669D4F2F749EBC636263B21)));
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral71E2B3599FE35851F669D4F2F749EBC636263B21)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_3;
		int32_t L_5;
		L_5 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		int32_t L_6 = L_5;
		RuntimeObject* L_7 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_7);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4C37184ABD38BF9788EE23119802EC101741E082)));
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4C37184ABD38BF9788EE23119802EC101741E082)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_9 = L_8;
		int32_t L_10 = 0;
		RuntimeObject* L_11 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_11);
		String_t* L_12;
		L_12 = String_Concat_m33F64A335B610F305AEA5FA8CF4C8BAAAAF257DC(L_9, NULL);
		PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* L_13 = (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5_il2cpp_TypeInfo_var)));
		NullCheck(L_13);
		PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24(L_13, L_12, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF_RuntimeMethod_var)));
	}

IL_0047:
	{
		Exception_t* L_14 = ___ex0;
		__this->___rejectionException_4 = L_14;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___rejectionException_4), (void*)L_14);
		Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline(__this, 1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		bool L_15 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___EnablePromiseTracking_0;
		V_1 = L_15;
		bool L_16 = V_1;
		if (!L_16)
		{
			goto IL_006d;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_17 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3;
		NullCheck(L_17);
		bool L_18;
		L_18 = HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE(L_17, __this, HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE_RuntimeMethod_var);
	}

IL_006d:
	{
		Exception_t* L_19 = ___ex0;
		Promise_InvokeRejectHandlers_m20A1B3583BF638CC7C2F0EA5B739FE7E1327BDFF(__this, L_19, NULL);
		return;
	}
}
// System.Void RSG.Promise::Resolve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		int32_t L_0;
		L_0 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_0 = (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var)), (uint32_t)4);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral3818CF5AE60CBE5E09B0C6F2B2C25B959DE1E784)));
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral3818CF5AE60CBE5E09B0C6F2B2C25B959DE1E784)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_3;
		int32_t L_5;
		L_5 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		int32_t L_6 = L_5;
		RuntimeObject* L_7 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_7);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralBAEB86B408E931CE2BB53E3D0839576F5491514F)));
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralBAEB86B408E931CE2BB53E3D0839576F5491514F)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_9 = L_8;
		int32_t L_10 = 0;
		RuntimeObject* L_11 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_11);
		String_t* L_12;
		L_12 = String_Concat_m33F64A335B610F305AEA5FA8CF4C8BAAAAF257DC(L_9, NULL);
		PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* L_13 = (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5_il2cpp_TypeInfo_var)));
		NullCheck(L_13);
		PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24(L_13, L_12, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808_RuntimeMethod_var)));
	}

IL_0047:
	{
		Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline(__this, 2, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		bool L_14 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___EnablePromiseTracking_0;
		V_1 = L_14;
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_16 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3;
		NullCheck(L_16);
		bool L_17;
		L_17 = HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE(L_16, __this, HashSet_1_Remove_mD83B6E412F414D838E8C5950BCDFA0E35AC89ECE_RuntimeMethod_var);
	}

IL_0066:
	{
		Promise_InvokeResolveHandlers_m5DC60EAD55ADE888A16689C026BBF0FD72CE4354(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise::ReportProgress(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, float ___progress0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		int32_t L_0;
		L_0 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_0 = (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_2 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var)), (uint32_t)4);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral84AEEFD7EDC59735921DA60B7B4B82FBFCA4278D)));
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral84AEEFD7EDC59735921DA60B7B4B82FBFCA4278D)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_3;
		int32_t L_5;
		L_5 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		int32_t L_6 = L_5;
		RuntimeObject* L_7 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_7);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA1B04F482AB3587406E00E182DC98A1187A31FCB)));
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralA1B04F482AB3587406E00E182DC98A1187A31FCB)));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_9 = L_8;
		int32_t L_10 = 0;
		RuntimeObject* L_11 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseState_tCF6AAE59A46893D9019C6E5340DC87475999B54C_il2cpp_TypeInfo_var)), &L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_11);
		String_t* L_12;
		L_12 = String_Concat_m33F64A335B610F305AEA5FA8CF4C8BAAAAF257DC(L_9, NULL);
		PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* L_13 = (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5_il2cpp_TypeInfo_var)));
		NullCheck(L_13);
		PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24(L_13, L_12, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0_RuntimeMethod_var)));
	}

IL_0047:
	{
		float L_14 = ___progress0;
		Promise_InvokeProgressHandlers_mDEDCF0A1858DF08B90C273FAD03AA33B15FC247F(__this, L_14, NULL);
		return;
	}
}
// System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Done_m54CBCA8B3EB300A7C4C2534ADE5AF53D71D4FBB4 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = ___onResolved0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_1 = ___onRejected1;
		RuntimeObject* L_2;
		L_2 = Promise_Then_m91F1EE136222A59062D07FA0A2857F6F439B74A7(__this, L_0, L_1, NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_3 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_3, __this, (intptr_t)((void*)Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(5 /* RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_2, L_3);
		return;
	}
}
// System.Void RSG.Promise::Done(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Done_m66CDC87BCAB708C6F999D1CB838EE4B27FBE7CA5 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = ___onResolved0;
		RuntimeObject* L_1;
		L_1 = Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D(__this, L_0, NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_2 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_2, __this, (intptr_t)((void*)Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11_RuntimeMethod_var), NULL);
		NullCheck(L_1);
		RuntimeObject* L_3;
		L_3 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(5 /* RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
// System.Void RSG.Promise::Done()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_Done_m218F80C8F8959377CB7BCB637AE12150BEE3D979 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_0 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_0, __this, (intptr_t)((void*)Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF_RuntimeMethod_var), NULL);
		RuntimeObject* L_1;
		L_1 = Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088(__this, L_0, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise::WithName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, String_t* ___name0, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059_inline(__this, L_0, NULL);
		V_0 = __this;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* V_0 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* V_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_0 = (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass44_0__ctor_m1489BE8776752DD4703EBFDAA409ABA61E6CC649(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_1 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_2 = ___onRejected0;
		NullCheck(L_1);
		L_1->___onRejected_1 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___onRejected_1), (void*)L_2);
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_3 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_4 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_4, NULL);
		NullCheck(L_3);
		L_3->___resultPromise_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___resultPromise_0), (void*)L_4);
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_5 = V_0;
		NullCheck(L_5);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_6 = L_5->___resultPromise_0;
		String_t* L_7;
		L_7 = Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline(__this, NULL);
		NullCheck(L_6);
		RuntimeObject* L_8;
		L_8 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_6, L_7, NULL);
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_9 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_10, L_9, (intptr_t)((void*)U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68_RuntimeMethod_var), NULL);
		V_1 = L_10;
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_11 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_12 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_12, L_11, (intptr_t)((void*)U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47_RuntimeMethod_var), NULL);
		V_2 = L_12;
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_13 = V_0;
		NullCheck(L_13);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_14 = L_13->___resultPromise_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_15 = V_1;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_16 = V_2;
		Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888(__this, L_14, L_15, L_16, NULL);
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_17 = V_0;
		NullCheck(L_17);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_18 = L_17->___resultPromise_0;
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_19 = V_0;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_20 = (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)il2cpp_codegen_object_new(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		NullCheck(L_20);
		Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859(L_20, L_19, (intptr_t)((void*)U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19_RuntimeMethod_var), NULL);
		Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD(__this, L_18, L_20, NULL);
		U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* L_21 = V_0;
		NullCheck(L_21);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_22 = L_21->___resultPromise_0;
		V_3 = L_22;
		goto IL_0076;
	}

IL_0076:
	{
		RuntimeObject* L_23 = V_3;
		return L_23;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved0, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_0 = ___onResolved0;
		RuntimeObject* L_1;
		L_1 = Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93(__this, L_0, (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)NULL, (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)NULL, NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = ___onResolved0;
		RuntimeObject* L_1;
		L_1 = Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7(__this, L_0, (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)NULL, (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)NULL, NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_m87C269B2CA1E15DAAF0BBB2A7F389575F86AE4BE (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_0 = ___onResolved0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_1 = ___onRejected1;
		RuntimeObject* L_2;
		L_2 = Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93(__this, L_0, L_1, (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)NULL, NULL);
		V_0 = L_2;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_m91F1EE136222A59062D07FA0A2857F6F439B74A7 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = ___onResolved0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_1 = ___onRejected1;
		RuntimeObject* L_2;
		L_2 = Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7(__this, L_0, L_1, (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)NULL, NULL);
		V_0 = L_2;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* V_0 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* V_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_2 = NULL;
	bool V_3 = false;
	RuntimeObject* V_4 = NULL;
	{
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_0 = (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass52_0__ctor_mE8F288904D95E3DC0025D1FBE2C15EEB625049AB(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_1 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_2 = ___onResolved0;
		NullCheck(L_1);
		L_1->___onResolved_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___onResolved_0), (void*)L_2);
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_3 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_4 = ___onRejected1;
		NullCheck(L_3);
		L_3->___onRejected_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___onRejected_2), (void*)L_4);
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_5 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_6 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_6, NULL);
		NullCheck(L_5);
		L_5->___resultPromise_1 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___resultPromise_1), (void*)L_6);
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_7 = V_0;
		NullCheck(L_7);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_8 = L_7->___resultPromise_1;
		String_t* L_9;
		L_9 = Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline(__this, NULL);
		NullCheck(L_8);
		RuntimeObject* L_10;
		L_10 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_8, L_9, NULL);
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_11 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_12 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_12, L_11, (intptr_t)((void*)U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B_RuntimeMethod_var), NULL);
		V_1 = L_12;
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_13 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_14 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_14);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_14, L_13, (intptr_t)((void*)U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792_RuntimeMethod_var), NULL);
		V_2 = L_14;
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_15 = V_0;
		NullCheck(L_15);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_16 = L_15->___resultPromise_1;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_17 = V_1;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_18 = V_2;
		Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888(__this, L_16, L_17, L_18, NULL);
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_19 = ___onProgress2;
		V_3 = (bool)((!(((RuntimeObject*)(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)L_19) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_20 = V_3;
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_21 = ___onProgress2;
		Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD(__this, __this, L_21, NULL);
	}

IL_006e:
	{
		U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* L_22 = V_0;
		NullCheck(L_22);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_23 = L_22->___resultPromise_1;
		V_4 = L_23;
		goto IL_0078;
	}

IL_0078:
	{
		RuntimeObject* L_24 = V_4;
		return L_24;
	}
}
// RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onResolved0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___onRejected1, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* V_0 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* V_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_2 = NULL;
	bool V_3 = false;
	RuntimeObject* V_4 = NULL;
	{
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_0 = (U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass53_0__ctor_m92B2808BB6DFC101EDB128ADCE074E66706EA045(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_1 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_2 = ___onResolved0;
		NullCheck(L_1);
		L_1->___onResolved_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___onResolved_0), (void*)L_2);
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_3 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_4 = ___onRejected1;
		NullCheck(L_3);
		L_3->___onRejected_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___onRejected_2), (void*)L_4);
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_5 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_6 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_6, NULL);
		NullCheck(L_5);
		L_5->___resultPromise_1 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___resultPromise_1), (void*)L_6);
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_7 = V_0;
		NullCheck(L_7);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_8 = L_7->___resultPromise_1;
		String_t* L_9;
		L_9 = Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline(__this, NULL);
		NullCheck(L_8);
		RuntimeObject* L_10;
		L_10 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_8, L_9, NULL);
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_11 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_12 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_12, L_11, (intptr_t)((void*)U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13_RuntimeMethod_var), NULL);
		V_1 = L_12;
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_13 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_14 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_14);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_14, L_13, (intptr_t)((void*)U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5_RuntimeMethod_var), NULL);
		V_2 = L_14;
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_15 = V_0;
		NullCheck(L_15);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_16 = L_15->___resultPromise_1;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_17 = V_1;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_18 = V_2;
		Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888(__this, L_16, L_17, L_18, NULL);
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_19 = ___onProgress2;
		V_3 = (bool)((!(((RuntimeObject*)(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)L_19) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_20 = V_3;
		if (!L_20)
		{
			goto IL_006e;
		}
	}
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_21 = ___onProgress2;
		Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD(__this, __this, L_21, NULL);
	}

IL_006e:
	{
		U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* L_22 = V_0;
		NullCheck(L_22);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_23 = L_22->___resultPromise_1;
		V_4 = L_23;
		goto IL_0078;
	}

IL_0078:
	{
		RuntimeObject* L_24 = V_4;
		return L_24;
	}
}
// System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, RuntimeObject* ___resultPromise0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___resolveHandler1, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___rejectHandler2, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	{
		int32_t L_0;
		L_0 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_2 = ___resolveHandler1;
		RuntimeObject* L_3 = ___resultPromise0;
		Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E(__this, L_2, L_3, NULL);
		goto IL_004f;
	}

IL_001b:
	{
		int32_t L_4;
		L_4 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_6 = ___rejectHandler2;
		RuntimeObject* L_7 = ___resultPromise0;
		Exception_t* L_8 = __this->___rejectionException_4;
		Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54(__this, L_6, L_7, L_8, NULL);
		goto IL_004f;
	}

IL_003b:
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_9 = ___resolveHandler1;
		RuntimeObject* L_10 = ___resultPromise0;
		Promise_AddResolveHandler_m99E30F6C68B539076C0E8D07FF05B4E6BE2A56E6(__this, L_9, L_10, NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_11 = ___rejectHandler2;
		RuntimeObject* L_12 = ___resultPromise0;
		Promise_AddRejectHandler_mE5BC8F2F50C3C7B1A2AC8CA7FCBF933FECDA0550(__this, L_11, L_12, NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, RuntimeObject* ___resultPromise0, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___progressHandler1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		int32_t L_0;
		L_0 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(__this, NULL);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_2 = ___progressHandler1;
		RuntimeObject* L_3 = ___resultPromise0;
		Promise_AddProgressHandler_m0898CAB05023C68BD94E91C3496CA3B30C20471E(__this, L_2, L_3, NULL);
	}

IL_0019:
	{
		return;
	}
}
// RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_ThenAll_mAFAA6572E4C049D282DEF096CF8A3662816C2DCE (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* ___chain0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* L_0 = (U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass56_0__ctor_mB81C3730446AE8C66A7D3EAFE2F0D416873CD51C(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* L_1 = V_0;
		Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* L_2 = ___chain0;
		NullCheck(L_1);
		L_1->___chain_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___chain_0), (void*)L_2);
		U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* L_3 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_4 = (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*)il2cpp_codegen_object_new(Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_1__ctor_m79F0C3727A7C602376F5C93BBCCEF4F36FC7F6E0(L_4, L_3, (intptr_t)((void*)U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A_RuntimeMethod_var), NULL);
		RuntimeObject* L_5;
		L_5 = Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05(__this, L_4, NULL);
		V_1 = L_5;
		goto IL_0023;
	}

IL_0023:
	{
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// RSG.IPromise RSG.Promise::All(RSG.IPromise[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_All_m4E3CD9398A6FE307C1F7B30E39E15AF6D028D277 (IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* ___promises0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_0 = ___promises0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_1;
		L_1 = Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2((RuntimeObject*)L_0, NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2 (RuntimeObject* ___promises0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCDF22F0BA1FC534FC6656104D7D41A8D396BCE5);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* V_0 = NULL;
	IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* V_1 = NULL;
	bool V_2 = false;
	RuntimeObject* V_3 = NULL;
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_0 = (U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass59_0__ctor_m2D14CCC0D09F29E28A2479BD4966A6EEAE384627(L_0, NULL);
		V_0 = L_0;
		RuntimeObject* L_1 = ___promises0;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_2;
		L_2 = Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E(L_1, Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E_RuntimeMethod_var);
		V_1 = L_2;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_3 = V_1;
		NullCheck(L_3);
		V_2 = (bool)((((int32_t)(((RuntimeArray*)L_3)->max_length)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_0020;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_5;
		L_5 = Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C(NULL);
		V_3 = L_5;
		goto IL_0072;
	}

IL_0020:
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_6 = V_0;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_7 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		L_6->___remainingCount_2 = ((int32_t)(((RuntimeArray*)L_7)->max_length));
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_8 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_9 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_9, NULL);
		NullCheck(L_8);
		L_8->___resultPromise_1 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&L_8->___resultPromise_1), (void*)L_9);
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_10 = V_0;
		NullCheck(L_10);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_11 = L_10->___resultPromise_1;
		NullCheck(L_11);
		RuntimeObject* L_12;
		L_12 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_11, _stringLiteralCCDF22F0BA1FC534FC6656104D7D41A8D396BCE5, NULL);
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_13 = V_0;
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = L_14->___remainingCount_2;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_16 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)L_15);
		NullCheck(L_13);
		L_13->___progress_0 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&L_13->___progress_0), (void*)L_16);
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_17 = V_1;
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_18 = V_0;
		Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706* L_19 = (Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706*)il2cpp_codegen_object_new(Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706_il2cpp_TypeInfo_var);
		NullCheck(L_19);
		Action_2__ctor_m4637341E7264727D66C053B4D7EA1A5A1F656CCF(L_19, L_18, (intptr_t)((void*)U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF_RuntimeMethod_var), NULL);
		EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F((RuntimeObject*)L_17, L_19, EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F_RuntimeMethod_var);
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_20 = V_0;
		NullCheck(L_20);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_21 = L_20->___resultPromise_1;
		V_3 = L_21;
		goto IL_0072;
	}

IL_0072:
	{
		RuntimeObject* L_22 = V_3;
		return L_22;
	}
}
// RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_ThenSequence_mEA544C9E1A6ECAF979AFB7072BAF1E497B0888FB (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09* ___chain0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* L_0 = (U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass60_0__ctor_m658D101B81BC36F252DED37541EFDB385BA526E4(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* L_1 = V_0;
		Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09* L_2 = ___chain0;
		NullCheck(L_1);
		L_1->___chain_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___chain_0), (void*)L_2);
		U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* L_3 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_4 = (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*)il2cpp_codegen_object_new(Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_1__ctor_m79F0C3727A7C602376F5C93BBCCEF4F36FC7F6E0(L_4, L_3, (intptr_t)((void*)U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92_RuntimeMethod_var), NULL);
		RuntimeObject* L_5;
		L_5 = Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05(__this, L_4, NULL);
		V_1 = L_5;
		goto IL_0023;
	}

IL_0023:
	{
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Sequence_mA9C41F9AA1B15AE32B13AAEB918C9C39C9CB0A93 (Func_1U5BU5D_t99CB06D0355E1DD5621F8A9A0D7730BC569F7449* ___fns0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Func_1U5BU5D_t99CB06D0355E1DD5621F8A9A0D7730BC569F7449* L_0 = ___fns0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_1;
		L_1 = Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F((RuntimeObject*)L_0, NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F (RuntimeObject* ___fns0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Aggregate_TisFunc_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mF56D75615C1ACB3B5BD8F167EF9822719DB356CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_0 = (U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass62_0__ctor_mD8148B137ABC682149B7AC033FFC943A4ED35CD1(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_1 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_2 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_2, NULL);
		NullCheck(L_1);
		L_1->___promise_1 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___promise_1), (void*)L_2);
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_3 = V_0;
		NullCheck(L_3);
		L_3->___count_0 = 0;
		RuntimeObject* L_4 = ___fns0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_5;
		L_5 = Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C(NULL);
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_6 = V_0;
		Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA* L_7 = (Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA*)il2cpp_codegen_object_new(Func_3_t23F94FD478B665406D6BBE8DBE62D6BFD9D0ADEA_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		Func_3__ctor_m1F201FC7B30B14E42021E35E5108587C9FF84975(L_7, L_6, (intptr_t)((void*)U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC_RuntimeMethod_var), NULL);
		RuntimeObject* L_8;
		L_8 = Enumerable_Aggregate_TisFunc_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mF56D75615C1ACB3B5BD8F167EF9822719DB356CE(L_4, L_5, L_7, Enumerable_Aggregate_TisFunc_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mF56D75615C1ACB3B5BD8F167EF9822719DB356CE_RuntimeMethod_var);
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_9 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_10, L_9, (intptr_t)((void*)U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7_RuntimeMethod_var), NULL);
		NullCheck(L_8);
		RuntimeObject* L_11;
		L_11 = InterfaceFuncInvoker1< RuntimeObject*, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* >::Invoke(8 /* RSG.IPromise RSG.IPromise::Then(System.Action) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_8, L_10);
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_12 = V_0;
		NullCheck(L_12);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_13 = L_12->___promise_1;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_14 = L_13;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_15 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_15);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_15, L_14, (intptr_t)((void*)GetVirtualMethodInfo(L_14, 31)), NULL);
		NullCheck(L_11);
		RuntimeObject* L_16;
		L_16 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(5 /* RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_11, L_15);
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_17 = V_0;
		NullCheck(L_17);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_18 = L_17->___promise_1;
		V_1 = L_18;
		goto IL_0062;
	}

IL_0062:
	{
		RuntimeObject* L_19 = V_1;
		return L_19;
	}
}
// RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_ThenRace_mBEBF13A1B643D5BD8C8AA3C8C1E34F46033E2C37 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* ___chain0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* L_0 = (U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass63_0__ctor_mF531911D8BEFC44215B51FEBB012E039F51266C8(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* L_1 = V_0;
		Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* L_2 = ___chain0;
		NullCheck(L_1);
		L_1->___chain_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___chain_0), (void*)L_2);
		U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* L_3 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_4 = (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*)il2cpp_codegen_object_new(Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_1__ctor_m79F0C3727A7C602376F5C93BBCCEF4F36FC7F6E0(L_4, L_3, (intptr_t)((void*)U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1_RuntimeMethod_var), NULL);
		RuntimeObject* L_5;
		L_5 = Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05(__this, L_4, NULL);
		V_1 = L_5;
		goto IL_0023;
	}

IL_0023:
	{
		RuntimeObject* L_6 = V_1;
		return L_6;
	}
}
// RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Race_m174412F14AF9AF5E72ACC54AFE424CBAFCB0FCE7 (IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* ___promises0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_0 = ___promises0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_1;
		L_1 = Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E((RuntimeObject*)L_0, NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E (RuntimeObject* ___promises0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3959F17E24B23C57ED29A79C653091BC73F5B12);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* V_0 = NULL;
	IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* V_1 = NULL;
	bool V_2 = false;
	RuntimeObject* V_3 = NULL;
	{
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_0 = (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass66_0__ctor_m2B345ACF4133F168C6638758CDE9864531934908(L_0, NULL);
		V_0 = L_0;
		RuntimeObject* L_1 = ___promises0;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_2;
		L_2 = Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E(L_1, Enumerable_ToArray_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_mD01BED1FBD69219ADD8059DA1DF44657C4484E8E_RuntimeMethod_var);
		V_1 = L_2;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_3 = V_1;
		NullCheck(L_3);
		V_2 = (bool)((((int32_t)(((RuntimeArray*)L_3)->max_length)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB* L_5 = (InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB_il2cpp_TypeInfo_var)));
		NullCheck(L_5);
		InvalidOperationException__ctor_mE4CB6F4712AB6D99A2358FBAE2E052B3EE976162(L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCD50CB6668BE1AF6B25D8CB27AFFA8E4536FAF32)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E_RuntimeMethod_var)));
	}

IL_0023:
	{
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_6 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_7 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_7, NULL);
		NullCheck(L_6);
		L_6->___resultPromise_1 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&L_6->___resultPromise_1), (void*)L_7);
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_8 = V_0;
		NullCheck(L_8);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_9 = L_8->___resultPromise_1;
		NullCheck(L_9);
		RuntimeObject* L_10;
		L_10 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_9, _stringLiteralB3959F17E24B23C57ED29A79C653091BC73F5B12, NULL);
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_11 = V_0;
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_12 = V_1;
		NullCheck(L_12);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)));
		NullCheck(L_11);
		L_11->___progress_0 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&L_11->___progress_0), (void*)L_13);
		IPromiseU5BU5D_t23F9975F41A48F73B430E1BF0A015B9797A2515F* L_14 = V_1;
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_15 = V_0;
		Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706* L_16 = (Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706*)il2cpp_codegen_object_new(Action_2_t62D714DD25B2A579FC444CF28CAA9B87C588A706_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Action_2__ctor_m4637341E7264727D66C053B4D7EA1A5A1F656CCF(L_16, L_15, (intptr_t)((void*)U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632_RuntimeMethod_var), NULL);
		EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F((RuntimeObject*)L_14, L_16, EnumerableExt_Each_TisIPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_m237FBEDEA37825C294C7B8F2A9D4C4B5215EAB4F_RuntimeMethod_var);
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_17 = V_0;
		NullCheck(L_17);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_18 = L_17->___resultPromise_1;
		V_3 = L_18;
		goto IL_0069;
	}

IL_0069:
	{
		RuntimeObject* L_19 = V_3;
		return L_19;
	}
}
// RSG.IPromise RSG.Promise::Resolved()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_0, NULL);
		V_0 = L_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_1 = V_0;
		NullCheck(L_1);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_1, NULL);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_2 = V_0;
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		RuntimeObject* L_3 = V_1;
		return L_3;
	}
}
// RSG.IPromise RSG.Promise::Rejected(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Rejected_m75966446ECB8170A186463C054F5E40CCC479B25 (Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_0, NULL);
		V_0 = L_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_1 = V_0;
		Exception_t* L_2 = ___ex0;
		NullCheck(L_1);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_1, L_2, NULL);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = V_0;
		V_1 = L_3;
		goto IL_0013;
	}

IL_0013:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// RSG.IPromise RSG.Promise::Finally(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Finally_mB06879F20FE6D9871B1B0677771662B65531FB7F (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onComplete0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_0 = (U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass69_0__ctor_m46CE508BDC0469757590835B30D4A0169716ABF3(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_1 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_2 = ___onComplete0;
		NullCheck(L_1);
		L_1->___onComplete_1 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___onComplete_1), (void*)L_2);
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_3 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_4 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_4, NULL);
		NullCheck(L_3);
		L_3->___promise_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___promise_0), (void*)L_4);
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_5 = V_0;
		NullCheck(L_5);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_6 = L_5->___promise_0;
		String_t* L_7;
		L_7 = Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline(__this, NULL);
		NullCheck(L_6);
		RuntimeObject* L_8;
		L_8 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_6, L_7, NULL);
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_9 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_10, L_9, (intptr_t)((void*)U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E_RuntimeMethod_var), NULL);
		RuntimeObject* L_11;
		L_11 = Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D(__this, L_10, NULL);
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_12 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_13 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_13, L_12, (intptr_t)((void*)U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED_RuntimeMethod_var), NULL);
		RuntimeObject* L_14;
		L_14 = Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088(__this, L_13, NULL);
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_15 = V_0;
		NullCheck(L_15);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_16 = L_15->___promise_0;
		U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* L_17 = V_0;
		NullCheck(L_17);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_18 = L_17->___onComplete_1;
		NullCheck(L_16);
		RuntimeObject* L_19;
		L_19 = Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D(L_16, L_18, NULL);
		V_1 = L_19;
		goto IL_0065;
	}

IL_0065:
	{
		RuntimeObject* L_20 = V_1;
		return L_20;
	}
}
// RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_ContinueWith_m05367014E6744D377DEBA484EDE08F5A6A7E72F6 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___onComplete0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_0 = (U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass70_0__ctor_mE72A5373F806CE548BA3484362F2F7FE433EEBFE(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_1 = V_0;
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_2 = (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C*)il2cpp_codegen_object_new(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF(L_2, NULL);
		NullCheck(L_1);
		L_1->___promise_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___promise_0), (void*)L_2);
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_3 = V_0;
		NullCheck(L_3);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_4 = L_3->___promise_0;
		String_t* L_5;
		L_5 = Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline(__this, NULL);
		NullCheck(L_4);
		RuntimeObject* L_6;
		L_6 = Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58(L_4, L_5, NULL);
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_7 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_8 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_8, L_7, (intptr_t)((void*)U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928_RuntimeMethod_var), NULL);
		RuntimeObject* L_9;
		L_9 = Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D(__this, L_8, NULL);
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_10 = V_0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_11 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_11, L_10, (intptr_t)((void*)U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273_RuntimeMethod_var), NULL);
		RuntimeObject* L_12;
		L_12 = Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088(__this, L_11, NULL);
		U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* L_13 = V_0;
		NullCheck(L_13);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_14 = L_13->___promise_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_15 = ___onComplete0;
		NullCheck(L_14);
		RuntimeObject* L_16;
		L_16 = Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05(L_14, L_15, NULL);
		V_1 = L_16;
		goto IL_0059;
	}

IL_0059:
	{
		RuntimeObject* L_17 = V_1;
		return L_17;
	}
}
// RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Promise_Progress_mC151B05499CAD04419AE9C3A30F7A5036332DCC6 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* ___onProgress0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	RuntimeObject* V_1 = NULL;
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_0 = ___onProgress0;
		V_0 = (bool)((!(((RuntimeObject*)(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_2 = ___onProgress0;
		Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD(__this, __this, L_2, NULL);
	}

IL_0014:
	{
		V_1 = __this;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject* L_3 = V_1;
		return L_3;
	}
}
// System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48 (RuntimeObject* ___sender0, Exception_t* ___ex1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_0 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1;
		V_0 = (bool)((!(((RuntimeObject*)(EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		EventHandler_1_t8F87292546F3C97CF4082C6F6641918F8C2E9B47* L_2 = ((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___unhandlerException_1;
		RuntimeObject* L_3 = ___sender0;
		Exception_t* L_4 = ___ex1;
		ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* L_5 = (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777*)il2cpp_codegen_object_new(ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		ExceptionEventArgs__ctor_m1D932BFCF3873476A40B88F8AA75EFB400812224(L_5, L_4, NULL);
		NullCheck(L_2);
		EventHandler_1_Invoke_m191057E67DEAF55D715D23C721E75751DF50A522_inline(L_2, L_3, L_5, NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void RSG.Promise::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise__cctor_m49F60296CB19D2264E020FBF75CC32C42657A778 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1__ctor_mFE82769CA93910E0390206067960C72201679309_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___EnablePromiseTracking_0 = (bool)0;
		HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F* L_0 = (HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F*)il2cpp_codegen_object_new(HashSet_1_tF9D73009A5DF8A7B7FA1194826D1A81A8DDAB26F_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		HashSet_1__ctor_mFE82769CA93910E0390206067960C72201679309(L_0, HashSet_1__ctor_mFE82769CA93910E0390206067960C72201679309_RuntimeMethod_var);
		((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_StaticFields*)il2cpp_codegen_static_fields_for(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var))->___PendingPromises_3), (void*)L_0);
		return;
	}
}
// System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 ___handler0, const RuntimeMethod* method) 
{
	{
		ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 L_0 = ___handler0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_1 = L_0.___callback_0;
		ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 L_2 = ___handler0;
		RuntimeObject* L_3 = L_2.___rejectable_1;
		Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E(__this, L_1, L_3, NULL);
		return;
	}
}
// System.Void RSG.Promise::<Done>b__40_0(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t* L_0 = ___ex0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48(__this, L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise::<Done>b__41_0(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11 (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t* L_0 = ___ex0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48(__this, L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise::<Done>b__42_0(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t* L_0 = ___ex0;
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48(__this, L_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RSG.Promise/ResolveHandler
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_pinvoke(const ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80& unmarshaled, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_pinvoke& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ResolveHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_pinvoke_back(const ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_pinvoke& marshaled, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ResolveHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.Promise/ResolveHandler
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_pinvoke_cleanup(ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: RSG.Promise/ResolveHandler
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_com(const ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80& unmarshaled, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_com& marshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ResolveHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_com_back(const ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_com& marshaled, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80& unmarshaled)
{
	Exception_t* ___rejectable_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'rejectable' of type 'ResolveHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___rejectable_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: RSG.Promise/ResolveHandler
IL2CPP_EXTERN_C void ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshal_com_cleanup(ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass34_0__ctor_m777DA7CE17AD10D18C6A866CC996D64D966176C2 (U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61 (U3CU3Ec__DisplayClass34_0_t44C7F2D2FF0F9011A4024F9D605B9C6934E39CBD* __this, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E ___handler0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___U3CU3E4__this_0;
		RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E L_1 = ___handler0;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_2 = L_1.___callback_0;
		RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E L_3 = ___handler0;
		RuntimeObject* L_4 = L_3.___rejectable_1;
		Exception_t* L_5 = __this->___ex_1;
		NullCheck(L_0);
		Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54(L_0, L_2, L_4, L_5, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_m1CC4BF0264C238FF06EBD6D51182AD9D9F00A44C (U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506 (U3CU3Ec__DisplayClass36_0_tB56692A4D67DC69F4CD43A3AC16D762498C87A5D* __this, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF ___handler0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___U3CU3E4__this_0;
		ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF L_1 = ___handler0;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_2 = L_1.___callback_0;
		ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF L_3 = ___handler0;
		RuntimeObject* L_4 = L_3.___rejectable_1;
		float L_5 = __this->___progress_1;
		NullCheck(L_0);
		Promise_InvokeProgressHandler_m3E4DD72481BD449FA36A24A6324DD23FA8367589(L_0, L_2, L_4, L_5, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass44_0__ctor_m1489BE8776752DD4703EBFDAA409ABA61E6CC649 (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68 (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* __this, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_0;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47 (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	Exception_t* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
	}
	try
	{// begin try (depth: 1)
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_0 = __this->___onRejected_1;
		Exception_t* L_1 = ___ex0;
		NullCheck(L_0);
		Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline(L_0, L_1, NULL);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_2 = __this->___resultPromise_0;
		NullCheck(L_2);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_2, NULL);
		goto IL_0030;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001e;
		}
		throw e;
	}

CATCH_001e:
	{// begin catch(System.Exception)
		V_0 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___resultPromise_0;
		Exception_t* L_4 = V_0;
		NullCheck(L_3);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_3, L_4, NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0030;
	}// end catch (depth: 1)

IL_0030:
	{
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19 (U3CU3Ec__DisplayClass44_0_t2016C1F3C101D6605B0FEA73E807D3C5C8A0E114* __this, float ___v0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_0;
		float L_1 = ___v0;
		NullCheck(L_0);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_0, L_1, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0__ctor_mE8F288904D95E3DC0025D1FBE2C15EEB625049AB (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* V_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* V_2 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_3 = NULL;
	Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* G_B3_0 = NULL;
	RuntimeObject* G_B3_1 = NULL;
	Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B5_0 = NULL;
	RuntimeObject* G_B5_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B4_0 = NULL;
	RuntimeObject* G_B4_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B7_0 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B7_1 = NULL;
	RuntimeObject* G_B7_2 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B6_0 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B6_1 = NULL;
	RuntimeObject* G_B6_2 = NULL;
	{
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_0 = __this->___onResolved_0;
		V_0 = (bool)((!(((RuntimeObject*)(Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0085;
		}
	}
	{
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_2 = __this->___onResolved_0;
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = Func_1_Invoke_m7A181FD07D8925150B330643541A778696741881_inline(L_2, NULL);
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_4 = __this->___U3CU3E9__2_3;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_5 = L_4;
		G_B2_0 = L_5;
		G_B2_1 = L_3;
		if (L_5)
		{
			G_B3_0 = L_5;
			G_B3_1 = L_3;
			goto IL_0039;
		}
	}
	{
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_6 = (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)il2cpp_codegen_object_new(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859(L_6, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668_RuntimeMethod_var), NULL);
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_7 = L_6;
		V_1 = L_7;
		__this->___U3CU3E9__2_3 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__2_3), (void*)L_7);
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_8 = V_1;
		G_B3_0 = L_8;
		G_B3_1 = G_B2_1;
	}

IL_0039:
	{
		NullCheck(G_B3_1);
		RuntimeObject* L_9;
		L_9 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* >::Invoke(23 /* RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, G_B3_1, G_B3_0);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = __this->___U3CU3E9__3_4;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_11 = L_10;
		G_B4_0 = L_11;
		G_B4_1 = L_9;
		if (L_11)
		{
			G_B5_0 = L_11;
			G_B5_1 = L_9;
			goto IL_005d;
		}
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_12 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_12, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328_RuntimeMethod_var), NULL);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_13 = L_12;
		V_2 = L_13;
		__this->___U3CU3E9__3_4 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__3_4), (void*)L_13);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_14 = V_2;
		G_B5_0 = L_14;
		G_B5_1 = G_B4_1;
	}

IL_005d:
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_15 = __this->___U3CU3E9__4_5;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_16 = L_15;
		G_B6_0 = L_16;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		if (L_16)
		{
			G_B7_0 = L_16;
			G_B7_1 = G_B5_0;
			G_B7_2 = G_B5_1;
			goto IL_007c;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_17 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_17);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_17, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3_RuntimeMethod_var), NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_18 = L_17;
		V_3 = L_18;
		__this->___U3CU3E9__4_5 = L_18;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__4_5), (void*)L_18);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_19 = V_3;
		G_B7_0 = L_19;
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
	}

IL_007c:
	{
		NullCheck(G_B7_2);
		RuntimeObject* L_20;
		L_20 = InterfaceFuncInvoker2< RuntimeObject*, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(11 /* RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, G_B7_2, G_B7_1, G_B7_0);
		goto IL_0093;
	}

IL_0085:
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_21 = __this->___resultPromise_1;
		NullCheck(L_21);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_21, NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668 (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, float ___progress0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		float L_1 = ___progress0;
		NullCheck(L_0);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_0, L_1, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328 (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3 (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		Exception_t* L_1 = ___ex0;
		NullCheck(L_0);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_0, L_1, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792 (U3CU3Ec__DisplayClass52_0_t4F550600B5634FE932181413C8BB373A4C795ACF* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_0 = __this->___onRejected_2;
		V_0 = (bool)((!(((RuntimeObject*)(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_2 = __this->___onRejected_2;
		Exception_t* L_3 = ___ex0;
		NullCheck(L_2);
		Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline(L_2, L_3, NULL);
	}

IL_001d:
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_4 = __this->___resultPromise_1;
		Exception_t* L_5 = ___ex0;
		NullCheck(L_4);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_4, L_5, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass53_0__ctor_m92B2808BB6DFC101EDB128ADCE074E66706EA045 (U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13 (U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = __this->___onResolved_0;
		V_0 = (bool)((!(((RuntimeObject*)(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_2 = __this->___onResolved_0;
		NullCheck(L_2);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_2, NULL);
	}

IL_001c:
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___resultPromise_1;
		NullCheck(L_3);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_3, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5 (U3CU3Ec__DisplayClass53_0_t5F2E7AC148AE417C5B2F87207E57F4DEB9A6D2BD* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_0 = __this->___onRejected_2;
		V_0 = (bool)((!(((RuntimeObject*)(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_2 = __this->___onRejected_2;
		Exception_t* L_3 = ___ex0;
		NullCheck(L_2);
		Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline(L_2, L_3, NULL);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_4 = __this->___resultPromise_1;
		NullCheck(L_4);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_4, NULL);
		goto IL_0037;
	}

IL_002a:
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_5 = __this->___resultPromise_1;
		Exception_t* L_6 = ___ex0;
		NullCheck(L_5);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_5, L_6, NULL);
	}

IL_0037:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass56_0__ctor_mB81C3730446AE8C66A7D3EAFE2F0D416873CD51C (U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A (U3CU3Ec__DisplayClass56_0_tF00A1FC37A99941B3C68B63016258B910495BB27* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* L_0 = __this->___chain_0;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = Func_1_Invoke_mFC397D76F95BD65F3B978512AFAA4921FDA70A27_inline(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_2;
		L_2 = Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2(L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_0__ctor_m2D14CCC0D09F29E28A2479BD4966A6EEAE384627 (U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF (U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* __this, RuntimeObject* ___promise0, int32_t ___index1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* V_0 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B1_0 = NULL;
	RuntimeObject* G_B1_1 = NULL;
	{
		U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* L_0 = (U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass59_1__ctor_mBB0688A58C068F7B05F67132C0437CA689E3540E(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* L_1 = V_0;
		NullCheck(L_1);
		L_1->___CSU24U3CU3E8__locals1_1 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___CSU24U3CU3E8__locals1_1), (void*)__this);
		U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck(L_2);
		L_2->___index_0 = L_3;
		RuntimeObject* L_4 = ___promise0;
		U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* L_5 = V_0;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_6 = (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)il2cpp_codegen_object_new(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859(L_6, L_5, (intptr_t)((void*)U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149_RuntimeMethod_var), NULL);
		NullCheck(L_4);
		RuntimeObject* L_7;
		L_7 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* >::Invoke(23 /* RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_4, L_6);
		U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* L_8 = V_0;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_9 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_9, L_8, (intptr_t)((void*)U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095_RuntimeMethod_var), NULL);
		NullCheck(L_7);
		RuntimeObject* L_10;
		L_10 = InterfaceFuncInvoker1< RuntimeObject*, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* >::Invoke(8 /* RSG.IPromise RSG.IPromise::Then(System.Action) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_7, L_9);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_11 = __this->___U3CU3E9__3_3;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_12 = L_11;
		G_B1_0 = L_12;
		G_B1_1 = L_10;
		if (L_12)
		{
			G_B2_0 = L_12;
			G_B2_1 = L_10;
			goto IL_0057;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_13 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_13, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971_RuntimeMethod_var), NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_14 = L_13;
		V_1 = L_14;
		__this->___U3CU3E9__3_3 = L_14;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__3_3), (void*)L_14);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_15 = V_1;
		G_B2_0 = L_15;
		G_B2_1 = G_B1_1;
	}

IL_0057:
	{
		NullCheck(G_B2_1);
		RuntimeObject* L_16;
		L_16 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(5 /* RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, G_B2_1, G_B2_0);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(4 /* System.Void RSG.IPromise::Done() */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_16);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971 (U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(L_0, NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___resultPromise_1;
		Exception_t* L_4 = ___ex0;
		NullCheck(L_3);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_3, L_4, NULL);
	}

IL_0022:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_1__ctor_mBB0688A58C068F7B05F67132C0437CA689E3540E (U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149 (U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* __this, float ___v0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_0 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_0);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = L_0->___progress_0;
		int32_t L_2 = __this->___index_0;
		float L_3 = ___v0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (float)L_3);
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_4 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_4);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_5 = L_4->___resultPromise_1;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(L_5, NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_8 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_8);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_9 = L_8->___resultPromise_1;
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_10 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_10);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = L_10->___progress_0;
		float L_12;
		L_12 = Enumerable_Average_m49C1D6444AF329FA3500CE9D16E043FCE590D9C9((RuntimeObject*)L_11, NULL);
		NullCheck(L_9);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_9, L_12, NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095 (U3CU3Ec__DisplayClass59_1_t150E0C71997543682EF0F34F0294F3D34FD6325D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_0 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_0);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = L_0->___progress_0;
		int32_t L_2 = __this->___index_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (float)(1.0f));
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_3 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_3);
		int32_t L_4 = L_3->___remainingCount_2;
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_4, 1));
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_5 = __this->___CSU24U3CU3E8__locals1_1;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		L_5->___remainingCount_2 = L_6;
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_7 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_7);
		int32_t L_8 = L_7->___remainingCount_2;
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_9 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_9);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_10 = L_9->___resultPromise_1;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(L_10, NULL);
		G_B3_0 = ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		goto IL_0056;
	}

IL_0055:
	{
		G_B3_0 = 0;
	}

IL_0056:
	{
		V_1 = (bool)G_B3_0;
		bool L_12 = V_1;
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		U3CU3Ec__DisplayClass59_0_t8674C9572CA1E80BEF987EEA3D228913A9CA49CC* L_13 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_13);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_14 = L_13->___resultPromise_1;
		NullCheck(L_14);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_14, NULL);
	}

IL_006d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass60_0__ctor_m658D101B81BC36F252DED37541EFDB385BA526E4 (U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92 (U3CU3Ec__DisplayClass60_0_t019193FD20A9EAD3A9AB4EA1BE423A5495F24130* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_tE892EDB74C0FEBC5E9AF0F52924C0EFF00528A09* L_0 = __this->___chain_0;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = Func_1_Invoke_mD078A377AF06EC32292E6C2AA35582D0EE566CF3_inline(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_2;
		L_2 = Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F(L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_0__ctor_mD8148B137ABC682149B7AC033FFC943A4ED35CD1 (U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC (U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* __this, RuntimeObject* ___prevPromise0, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* ___fn1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	{
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_0 = (U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass62_1__ctor_m378111927982C2FF6C8AAA87AC31A3F631D36495(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_1 = V_0;
		NullCheck(L_1);
		L_1->___CSU24U3CU3E8__locals1_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___CSU24U3CU3E8__locals1_2), (void*)__this);
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_2 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_3 = ___fn1;
		NullCheck(L_2);
		L_2->___fn_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___fn_1), (void*)L_3);
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_4 = V_0;
		int32_t L_5 = __this->___count_0;
		NullCheck(L_4);
		L_4->___itemSequence_0 = L_5;
		int32_t L_6 = __this->___count_0;
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, 1));
		int32_t L_7 = V_1;
		__this->___count_0 = L_7;
		RuntimeObject* L_8 = ___prevPromise0;
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_9 = V_0;
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_10 = (Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057*)il2cpp_codegen_object_new(Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Func_1__ctor_m79F0C3727A7C602376F5C93BBCCEF4F36FC7F6E0(L_10, L_9, (intptr_t)((void*)U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92_RuntimeMethod_var), NULL);
		NullCheck(L_8);
		RuntimeObject* L_11;
		L_11 = InterfaceFuncInvoker1< RuntimeObject*, Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* >::Invoke(7 /* RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_8, L_10);
		U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* L_12 = V_0;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_13 = (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)il2cpp_codegen_object_new(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859(L_13, L_12, (intptr_t)((void*)U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223_RuntimeMethod_var), NULL);
		NullCheck(L_11);
		RuntimeObject* L_14;
		L_14 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* >::Invoke(23 /* RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_11, L_13);
		V_2 = L_14;
		goto IL_0057;
	}

IL_0057:
	{
		RuntimeObject* L_15 = V_2;
		return L_15;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7 (U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* __this, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___promise_1;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_1__ctor_m378111927982C2FF6C8AAA87AC31A3F631D36495 (U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92 (U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_0 = __this->___CSU24U3CU3E8__locals1_2;
		NullCheck(L_0);
		int32_t L_1 = L_0->___count_0;
		V_0 = ((float)((1.0f)/((float)L_1)));
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_2 = __this->___CSU24U3CU3E8__locals1_2;
		NullCheck(L_2);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = L_2->___promise_1;
		float L_4 = V_0;
		int32_t L_5 = __this->___itemSequence_0;
		NullCheck(L_3);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_3, ((float)il2cpp_codegen_multiply(L_4, ((float)L_5))), NULL);
		Func_1_tCC28ECD5D9ACFA0BCC4564423C6443A336D57057* L_6 = __this->___fn_1;
		NullCheck(L_6);
		RuntimeObject* L_7;
		L_7 = Func_1_Invoke_m7A181FD07D8925150B330643541A778696741881_inline(L_6, NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_003c:
	{
		RuntimeObject* L_8 = V_1;
		return L_8;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223 (U3CU3Ec__DisplayClass62_1_t26EE98A448C9843C69A93BE29851CB8C3599CFCD* __this, float ___v0, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_0 = __this->___CSU24U3CU3E8__locals1_2;
		NullCheck(L_0);
		int32_t L_1 = L_0->___count_0;
		V_0 = ((float)((1.0f)/((float)L_1)));
		U3CU3Ec__DisplayClass62_0_t1879DA8ECB494DD37577B8F490051F0FA55DDCF4* L_2 = __this->___CSU24U3CU3E8__locals1_2;
		NullCheck(L_2);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = L_2->___promise_1;
		float L_4 = V_0;
		float L_5 = ___v0;
		int32_t L_6 = __this->___itemSequence_0;
		NullCheck(L_3);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_3, ((float)il2cpp_codegen_multiply(L_4, ((float)il2cpp_codegen_add(L_5, ((float)L_6))))), NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass63_0__ctor_mF531911D8BEFC44215B51FEBB012E039F51266C8 (U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1 (U3CU3Ec__DisplayClass63_0_t9958D6CB3355A48E2900EDF5C5E8F624390331A1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Func_1_tD0C4753092141601A9071A44C74DA6917C7EF020* L_0 = __this->___chain_0;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = Func_1_Invoke_mFC397D76F95BD65F3B978512AFAA4921FDA70A27_inline(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C_il2cpp_TypeInfo_var);
		RuntimeObject* L_2;
		L_2 = Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E(L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_0__ctor_m2B345ACF4133F168C6638758CDE9864531934908 (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632 (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* __this, RuntimeObject* ___promise0, int32_t ___index1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* V_0 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* V_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* V_2 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* G_B1_0 = NULL;
	RuntimeObject* G_B1_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B4_0 = NULL;
	RuntimeObject* G_B4_1 = NULL;
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* G_B3_0 = NULL;
	RuntimeObject* G_B3_1 = NULL;
	{
		U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* L_0 = (U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass66_1__ctor_mF9A6583E6F209770FC8B76F1E90E1AE9BE64653A(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* L_1 = V_0;
		NullCheck(L_1);
		L_1->___CSU24U3CU3E8__locals1_1 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___CSU24U3CU3E8__locals1_1), (void*)__this);
		U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck(L_2);
		L_2->___index_0 = L_3;
		RuntimeObject* L_4 = ___promise0;
		U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* L_5 = V_0;
		Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* L_6 = (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A*)il2cpp_codegen_object_new(Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action_1__ctor_m770CD2F8BB65F2EDA5128CA2F96D71C35B23E859(L_6, L_5, (intptr_t)((void*)U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253_RuntimeMethod_var), NULL);
		NullCheck(L_4);
		RuntimeObject* L_7;
		L_7 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* >::Invoke(23 /* RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_4, L_6);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_8 = __this->___U3CU3E9__2_2;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_9 = L_8;
		G_B1_0 = L_9;
		G_B1_1 = L_7;
		if (L_9)
		{
			G_B2_0 = L_9;
			G_B2_1 = L_7;
			goto IL_0046;
		}
	}
	{
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_10 = (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*)il2cpp_codegen_object_new(Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Action_1__ctor_m55F05090D04B2CE985AB61F6DB2C073AECD12A2E(L_10, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6_RuntimeMethod_var), NULL);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_11 = L_10;
		V_1 = L_11;
		__this->___U3CU3E9__2_2 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__2_2), (void*)L_11);
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_12 = V_1;
		G_B2_0 = L_12;
		G_B2_1 = G_B1_1;
	}

IL_0046:
	{
		NullCheck(G_B2_1);
		RuntimeObject* L_13;
		L_13 = InterfaceFuncInvoker1< RuntimeObject*, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* >::Invoke(5 /* RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, G_B2_1, G_B2_0);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_14 = __this->___U3CU3E9__3_3;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_15 = L_14;
		G_B3_0 = L_15;
		G_B3_1 = L_13;
		if (L_15)
		{
			G_B4_0 = L_15;
			G_B4_1 = L_13;
			goto IL_006a;
		}
	}
	{
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_16 = (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07*)il2cpp_codegen_object_new(Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Action__ctor_mBDC7B0B4A3F583B64C2896F01BDED360772F67DC(L_16, __this, (intptr_t)((void*)U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98_RuntimeMethod_var), NULL);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_17 = L_16;
		V_2 = L_17;
		__this->___U3CU3E9__3_3 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E9__3_3), (void*)L_17);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_18 = V_2;
		G_B4_0 = L_18;
		G_B4_1 = G_B3_1;
	}

IL_006a:
	{
		NullCheck(G_B4_1);
		RuntimeObject* L_19;
		L_19 = InterfaceFuncInvoker1< RuntimeObject*, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* >::Invoke(8 /* RSG.IPromise RSG.IPromise::Then(System.Action) */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, G_B4_1, G_B4_0);
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(4 /* System.Void RSG.IPromise::Done() */, IPromise_tBE3CB63F3B2066BBB30F32CD44F799120492FBDE_il2cpp_TypeInfo_var, L_19);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6 (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* __this, Exception_t* ___ex0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(L_0, NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___resultPromise_1;
		Exception_t* L_4 = ___ex0;
		NullCheck(L_3);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_3, L_4, NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98 (U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* __this, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___resultPromise_1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline(L_0, NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___resultPromise_1;
		NullCheck(L_3);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_3, NULL);
	}

IL_0021:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_1__ctor_mF9A6583E6F209770FC8B76F1E90E1AE9BE64653A (U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253 (U3CU3Ec__DisplayClass66_1_t40C7097C427F67B99939832DFDD395F03CD891A0* __this, float ___v0, const RuntimeMethod* method) 
{
	{
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_0 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_0);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = L_0->___progress_0;
		int32_t L_2 = __this->___index_0;
		float L_3 = ___v0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (float)L_3);
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_4 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_4);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_5 = L_4->___resultPromise_1;
		U3CU3Ec__DisplayClass66_0_t91C7C89F2AC81E6B95080DDF4C6A3A1CFE1E8FC3* L_6 = __this->___CSU24U3CU3E8__locals1_1;
		NullCheck(L_6);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_7 = L_6->___progress_0;
		float L_8;
		L_8 = Enumerable_Max_mA30ECB22B118A464652A20E12E0097D8A952531D((RuntimeObject*)L_7, NULL);
		NullCheck(L_5);
		Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0(L_5, L_8, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass69_0__ctor_m46CE508BDC0469757590835B30D4A0169716ABF3 (U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E (U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* __this, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___promise_0;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED (U3CU3Ec__DisplayClass69_0_t04128C4978C0FCE78964853BF0DD3F96C2F6537D* __this, Exception_t* ___e0, const RuntimeMethod* method) 
{
	Exception_t* V_0 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
	}
	try
	{// begin try (depth: 1)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_0 = __this->___onComplete_1;
		NullCheck(L_0);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_0, NULL);
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_1 = __this->___promise_0;
		Exception_t* L_2 = ___e0;
		NullCheck(L_1);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_1, L_2, NULL);
		goto IL_0030;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001e;
		}
		throw e;
	}

CATCH_001e:
	{// begin catch(System.Exception)
		V_0 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_3 = __this->___promise_0;
		Exception_t* L_4 = V_0;
		NullCheck(L_3);
		Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF(L_3, L_4, NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0030;
	}// end catch (depth: 1)

IL_0030:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass70_0__ctor_mE72A5373F806CE548BA3484362F2F7FE433EEBFE (U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928 (U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* __this, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___promise_0;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
// System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273 (U3CU3Ec__DisplayClass70_0_t369239BF4BF58E7DC705748FC2ED936FA35804B8* __this, Exception_t* ___e0, const RuntimeMethod* method) 
{
	{
		Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* L_0 = __this->___promise_0;
		NullCheck(L_0);
		Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808(L_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Tuple::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tuple__ctor_mEE4C2AB59B8C048706F3AC40529D78301FFA74A8 (Tuple_tD84879F942A5C7C66FD4285DEFF4856294C641DE* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Exceptions.PromiseException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_m555AAF5903923E345DE642D648F00143A5442218 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F(__this, NULL);
		return;
	}
}
// System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_mA3A95CC09C60213917698EA0D1F4E49ADE76AB20 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(__this, L_0, NULL);
		return;
	}
}
// System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseException__ctor_m39A3D4D0F18B6923549CB458A5FB67BD56524EA0 (PromiseException_tA4BFA6BC5952EEF85B3049DD0ADB38B47B611B0C* __this, String_t* ___message0, Exception_t* ___inner1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		Exception_t* L_1 = ___inner1;
		il2cpp_codegen_runtime_class_init_inline(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m9BC141AAB08F47C34B7ED40C1A6C0C1ADDEC5CB3(__this, L_0, L_1, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RSG.Exceptions.PromiseStateException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseStateException__ctor_m5705CBFF638E07568E352F95697FEC35697938F5 (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* __this, const RuntimeMethod* method) 
{
	{
		PromiseException__ctor_m555AAF5903923E345DE642D648F00143A5442218(__this, NULL);
		return;
	}
}
// System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24 (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___message0;
		PromiseException__ctor_mA3A95CC09C60213917698EA0D1F4E49ADE76AB20(__this, L_0, NULL);
		return;
	}
}
// System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PromiseStateException__ctor_m9286CD3BDADD656A97C9398D21666B1CCDAF82DB (PromiseStateException_t416A91367F79EF6B5DEDCFF1CE1F07D7C72467F5* __this, String_t* ___message0, Exception_t* ___inner1, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___message0;
		Exception_t* L_1 = ___inner1;
		PromiseException__ctor_m39A3D4D0F18B6923549CB458A5FB67BD56524EA0(__this, L_0, L_1, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970_inline (ExceptionEventArgs_t67B41AEE68221EA5419B0F04C14387D558469777* __this, Exception_t* ___value0, const RuntimeMethod* method) 
{
	{
		Exception_t* L_0 = ___value0;
		__this->___U3CExceptionU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CExceptionU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___value0;
		__this->___U3CCurStateU3Ek__BackingField_10 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CCurStateU3Ek__BackingField_10;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___value0;
		__this->___U3CNameU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205_inline (Promise_tEC1CF5C2BB06BC757B2362F3AC20E6834EC7730C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_9;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* LinkedListNode_1_get_Value_m8F67264DC98EF442B34CE4947044BCE18BF26053_gshared_inline (LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->___item_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* LinkedList_1_get_First_mF743AE65DDD0324290E33D3F433F37AC83216E18_gshared_inline (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, const RuntimeMethod* method) 
{
	{
		LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* L_0 = (LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C*)__this->___head_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Func_2_Invoke_m4B083A58F01C8C1C40A224A5F9E4577A701A0E01_gshared_inline (Func_2_t1AF8B0AB2721BF2624914E63D7609A317EC09519* __this, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923 ___arg0, const RuntimeMethod* method) 
{
	typedef bool (*FunctionPointerType) (RuntimeObject*, TimeData_t22AE4B051C3663B8D7ADFF36EDF631BB218D2923, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___arg10, RuntimeObject* ___arg21, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg10, ___arg21, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m370BB47BE44B5A315BA39205EFE78103BDFB86DB_gshared_inline (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF* __this, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E ___item0, const RuntimeMethod* method) 
{
	RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* L_1 = (RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		RejectHandlerU5BU5D_tFD96E2B2E5274DEE3A70B65A6108448FF898963F* L_6 = V_0;
		int32_t L_7 = V_1;
		RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E)L_8);
		return;
	}

IL_0034:
	{
		RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E L_9 = ___item0;
		((  void (*) (List_1_tC81EB492C3D1AC7F4DDEEBDA53A7F404A6990ECF*, RejectHandler_t8351138924F26A8B99828B10F47F69022DA5119E, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mCE013E5B79F6B610B3D0EBAEAA9C1588E5475CF2_gshared_inline (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8* __this, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 ___item0, const RuntimeMethod* method) 
{
	ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* L_1 = (ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ResolveHandlerU5BU5D_t948776FA05937A49AF699A49D6CC38987261C248* L_6 = V_0;
		int32_t L_7 = V_1;
		ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80)L_8);
		return;
	}

IL_0034:
	{
		ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80 L_9 = ___item0;
		((  void (*) (List_1_tDAFDF6D1CC10131AF81DEC2F8672A66742AB8DD8*, ResolveHandler_tCEAC7157BFB4EAF436AA8185A0CF168898841B80, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m4C3EAC14C4A7720DD0D35768BC434D735957F640_gshared_inline (List_1_t43490118A5560875E8A4FE9F26A448F004750640* __this, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF ___item0, const RuntimeMethod* method) 
{
	ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* L_1 = (ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ProgressHandlerU5BU5D_t10B4805F41AB75BF890DBF9DA0794AC02532DA89* L_6 = V_0;
		int32_t L_7 = V_1;
		ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF)L_8);
		return;
	}

IL_0034:
	{
		ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF L_9 = ___item0;
		((  void (*) (List_1_t43490118A5560875E8A4FE9F26A448F004750640*, ProgressHandler_t76189278FB3DA31229AA6386C9C131E8DC6385CF, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mA8F89FB04FEA0F48A4F22EC84B5F9ADB2914341F_gshared_inline (Action_1_t310F18CB4338A2740CA701F160C62E2C3198E66A* __this, float ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, float, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void EventHandler_1_Invoke_mBD72C04FF5A08A2EA93DFD21037CD1C27A48D07A_gshared_inline (EventHandler_1_tD8C4A5BE1F7C91B1A7E99AE87AFD2F5432C38746* __this, RuntimeObject* ___sender0, RuntimeObject* ___e1, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___sender0, ___e1, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_1_Invoke_m1412272198DFA4066C83206E5B43353AF10A2EEE_gshared_inline (Func_1_tD5C081AE11746B200C711DD48DBEB00E3A9276D4* __this, const RuntimeMethod* method) 
{
	typedef RuntimeObject* (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
