﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String Twity.Oauth::get_consumerKey()
extern void Oauth_get_consumerKey_m61A846BA517257F029E8E139FEEC56E9921E1346 (void);
// 0x00000002 System.Void Twity.Oauth::set_consumerKey(System.String)
extern void Oauth_set_consumerKey_mD68460CC3E5F8367190F2B42C275154D3AFEC0D9 (void);
// 0x00000003 System.String Twity.Oauth::get_consumerSecret()
extern void Oauth_get_consumerSecret_m5A65F7AD328F0A61DF0B0A4B613D2ABA976FCF9B (void);
// 0x00000004 System.Void Twity.Oauth::set_consumerSecret(System.String)
extern void Oauth_set_consumerSecret_m3B1C7C14A340A37E4B185158889DB758A293606C (void);
// 0x00000005 System.String Twity.Oauth::get_accessToken()
extern void Oauth_get_accessToken_m2EBC8E59004D1269320CB6F6A7B7FFCA7426B6FB (void);
// 0x00000006 System.Void Twity.Oauth::set_accessToken(System.String)
extern void Oauth_set_accessToken_mF41BD5D9A21D4E2B0A4CAAF65495E3015701E93B (void);
// 0x00000007 System.String Twity.Oauth::get_accessTokenSecret()
extern void Oauth_get_accessTokenSecret_m999E61ABEA354C54661982635EFCF8B3BBC03D5B (void);
// 0x00000008 System.Void Twity.Oauth::set_accessTokenSecret(System.String)
extern void Oauth_set_accessTokenSecret_m19D3AF5A70D4FB192E4CF09643D61765755E543B (void);
// 0x00000009 System.String Twity.Oauth::get_bearerToken()
extern void Oauth_get_bearerToken_m674E6FD041A0B9FEA694ED51542B4317B53A380B (void);
// 0x0000000A System.Void Twity.Oauth::set_bearerToken(System.String)
extern void Oauth_set_bearerToken_mD5CEE7AC4541D7AA548D84D3461A9ADF1BA1F64A (void);
// 0x0000000B System.String Twity.Oauth::get_requestToken()
extern void Oauth_get_requestToken_mF01F8A8422811D62CE2AC37C8D6B28099DE3771D (void);
// 0x0000000C System.Void Twity.Oauth::set_requestToken(System.String)
extern void Oauth_set_requestToken_mEEE71E1BB5D6DFC4679152D0E91811B5F6DD1E11 (void);
// 0x0000000D System.String Twity.Oauth::get_requestTokenSecret()
extern void Oauth_get_requestTokenSecret_mB3347333C65AD21267525A9013E0E3D5E9BE499A (void);
// 0x0000000E System.Void Twity.Oauth::set_requestTokenSecret(System.String)
extern void Oauth_set_requestTokenSecret_mD8B0944DC530A61E5B83F083BED3478AB4DBB011 (void);
// 0x0000000F System.String Twity.Oauth::get_authorizeURL()
extern void Oauth_get_authorizeURL_m30207CBCB785E42EDA3C021E506226F97DDC2318 (void);
// 0x00000010 System.Void Twity.Oauth::set_authorizeURL(System.String)
extern void Oauth_set_authorizeURL_mA77A9BC6F7F5E8EFCAB3613642C0929770B1C340 (void);
// 0x00000011 System.String Twity.Oauth::GenerateHeaderWithAccessToken(System.Collections.Generic.SortedDictionary`2<System.String,System.String>,System.String,System.String)
extern void Oauth_GenerateHeaderWithAccessToken_m9BE322169C1A12B41519032A0E5862E5B87B855F (void);
// 0x00000012 System.String Twity.Oauth::GenerateSignature(System.Collections.Generic.SortedDictionary`2<System.String,System.String>,System.String,System.String)
extern void Oauth_GenerateSignature_mBA4A1C46E37CE49D4D6C2FCE64985E55B002370F (void);
// 0x00000013 System.Void Twity.Oauth::AddDefaultOauthParams(System.Collections.Generic.SortedDictionary`2<System.String,System.String>,System.String)
extern void Oauth_AddDefaultOauthParams_m13702BAB523CF3B342124875DEFC595538927EA5 (void);
// 0x00000014 System.String Twity.Oauth::GenerateTimeStamp()
extern void Oauth_GenerateTimeStamp_m56AF846A0BE7117EF3AFF19F825EC081C9A5AC6B (void);
// 0x00000015 System.String Twity.Oauth::GenerateNonce()
extern void Oauth_GenerateNonce_m0A45A228B37EB6052FF8410CF5E32589F89A703E (void);
// 0x00000016 System.Void Twity.Oauth::.ctor()
extern void Oauth__ctor_mDDEED119A948B5DB1E678BE70A7CC53266F3D045 (void);
// 0x00000017 System.Void Twity.TwitterCallback::.ctor(System.Object,System.IntPtr)
extern void TwitterCallback__ctor_mF0E71E7438D170681D1C750E60974610A99BC3F9 (void);
// 0x00000018 System.Void Twity.TwitterCallback::Invoke(System.Boolean,System.String)
extern void TwitterCallback_Invoke_mC6989AD60A35A57F9807075055EFBEB926243868 (void);
// 0x00000019 System.IAsyncResult Twity.TwitterCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern void TwitterCallback_BeginInvoke_m40F5B4BA8E1288AD6F2255278A88F0261E2760ED (void);
// 0x0000001A System.Void Twity.TwitterCallback::EndInvoke(System.IAsyncResult)
extern void TwitterCallback_EndInvoke_m207CF462953108317879C66174B26399F57BCB87 (void);
// 0x0000001B System.Void Twity.TwitterAuthenticationCallback::.ctor(System.Object,System.IntPtr)
extern void TwitterAuthenticationCallback__ctor_m9C4A19703E6495A85176C121D730CE097CA0A36C (void);
// 0x0000001C System.Void Twity.TwitterAuthenticationCallback::Invoke(System.Boolean)
extern void TwitterAuthenticationCallback_Invoke_m1AD31EA8277D265CAF1124AD5A462C5AFD33C357 (void);
// 0x0000001D System.IAsyncResult Twity.TwitterAuthenticationCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void TwitterAuthenticationCallback_BeginInvoke_m15E81CDC4CBA5688EC8381DD5752A3FDE678FB23 (void);
// 0x0000001E System.Void Twity.TwitterAuthenticationCallback::EndInvoke(System.IAsyncResult)
extern void TwitterAuthenticationCallback_EndInvoke_m1969361091C93EE9AAB3C4ADC13410C61406EA1D (void);
// 0x0000001F System.Collections.IEnumerator Twity.Client::Get(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,Twity.TwitterCallback)
extern void Client_Get_m77B7CBC5231C803CE813F811390E332E13CF8C0A (void);
// 0x00000020 System.Collections.IEnumerator Twity.Client::Post(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,Twity.TwitterCallback)
extern void Client_Post_m8F11395F0F9BD57C15725531E2F278D72039D6E0 (void);
// 0x00000021 System.Collections.IEnumerator Twity.Client::GetOauth2BearerToken(Twity.TwitterAuthenticationCallback)
extern void Client_GetOauth2BearerToken_m7D63D358CF5F1D48DBC6F97229F1F0712F78CD3F (void);
// 0x00000022 System.Collections.IEnumerator Twity.Client::GenerateRequestToken(Twity.TwitterAuthenticationCallback)
extern void Client_GenerateRequestToken_m89EEF80AAB6C7CA299E44C28B17A47B2377253F1 (void);
// 0x00000023 System.Collections.IEnumerator Twity.Client::GenerateRequestToken(Twity.TwitterAuthenticationCallback,System.String)
extern void Client_GenerateRequestToken_m76B83A0EF0246B6367DD49C8130BD1376BA7A099 (void);
// 0x00000024 System.Collections.IEnumerator Twity.Client::GenerateAccessToken(System.String,Twity.TwitterAuthenticationCallback)
extern void Client_GenerateAccessToken_mC7057AF92BD4801271958D8B78B4A69CBF8C86BD (void);
// 0x00000025 System.Collections.IEnumerator Twity.Client::SendRequest(UnityEngine.Networking.UnityWebRequest,System.Collections.Generic.SortedDictionary`2<System.String,System.String>,System.String,System.String,Twity.TwitterCallback)
extern void Client_SendRequest_m085BA40712294B20881B87FCA3A42F84763EFBE5 (void);
// 0x00000026 System.Void Twity.Client::ClearTokens()
extern void Client_ClearTokens_m1AFE2D0EEFC30BF006147F603E6455BA6745227B (void);
// 0x00000027 System.Void Twity.Client::.ctor()
extern void Client__ctor_m1AA10B63F860CB77F0AF1E9CCB3F5C0D77C96144 (void);
// 0x00000028 System.Void Twity.Client/<Get>d__1::.ctor(System.Int32)
extern void U3CGetU3Ed__1__ctor_mB45A7109B8F2001A6DAD50231CC61CEC9DA4DBF6 (void);
// 0x00000029 System.Void Twity.Client/<Get>d__1::System.IDisposable.Dispose()
extern void U3CGetU3Ed__1_System_IDisposable_Dispose_m82749FC73D1782662E0CB7AA51E1D5112D2E8C56 (void);
// 0x0000002A System.Boolean Twity.Client/<Get>d__1::MoveNext()
extern void U3CGetU3Ed__1_MoveNext_m545037CB597E77B024B6A800CD7E735A104655E6 (void);
// 0x0000002B System.Object Twity.Client/<Get>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF57169BBB97FCB0C899391F8EE7E8C748B7138EC (void);
// 0x0000002C System.Void Twity.Client/<Get>d__1::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__1_System_Collections_IEnumerator_Reset_mC92BD8C812B48EFA022304D9DA27D60E3956A31D (void);
// 0x0000002D System.Object Twity.Client/<Get>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__1_System_Collections_IEnumerator_get_Current_m3DD2C851AD024F742A7D8889FF634ED1F5FFF58A (void);
// 0x0000002E System.Void Twity.Client/<Post>d__2::.ctor(System.Int32)
extern void U3CPostU3Ed__2__ctor_mDB5C1B699A7427961D426CB512A13718F8DFE12D (void);
// 0x0000002F System.Void Twity.Client/<Post>d__2::System.IDisposable.Dispose()
extern void U3CPostU3Ed__2_System_IDisposable_Dispose_mCF015EB8019B34A8EFFD4A08094283FBBAA4D31B (void);
// 0x00000030 System.Boolean Twity.Client/<Post>d__2::MoveNext()
extern void U3CPostU3Ed__2_MoveNext_mEA88825B5895BF700FAEC2BD713F2CC96DD866FC (void);
// 0x00000031 System.Object Twity.Client/<Post>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m604530C78E6599983B29DFF9654F3E7A92413F8B (void);
// 0x00000032 System.Void Twity.Client/<Post>d__2::System.Collections.IEnumerator.Reset()
extern void U3CPostU3Ed__2_System_Collections_IEnumerator_Reset_m99E157F2D8CC526FF82DE83A7F0CA0B0C620934B (void);
// 0x00000033 System.Object Twity.Client/<Post>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CPostU3Ed__2_System_Collections_IEnumerator_get_Current_m16AEE6D7393D5E3561C9591DD2B76033CC4EE94E (void);
// 0x00000034 System.Void Twity.Client/<GetOauth2BearerToken>d__3::.ctor(System.Int32)
extern void U3CGetOauth2BearerTokenU3Ed__3__ctor_mE9E732178D0D4E66F25C8B901562714A949E0235 (void);
// 0x00000035 System.Void Twity.Client/<GetOauth2BearerToken>d__3::System.IDisposable.Dispose()
extern void U3CGetOauth2BearerTokenU3Ed__3_System_IDisposable_Dispose_m82CA1B1A5E08A65F773BDAACB4156204F87EB898 (void);
// 0x00000036 System.Boolean Twity.Client/<GetOauth2BearerToken>d__3::MoveNext()
extern void U3CGetOauth2BearerTokenU3Ed__3_MoveNext_mF3AF72C038C5A8A8D67D777224AE233660EC4AD9 (void);
// 0x00000037 System.Object Twity.Client/<GetOauth2BearerToken>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetOauth2BearerTokenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBB9AD093BD70B71DD9B1D82D20CA64A238282B1 (void);
// 0x00000038 System.Void Twity.Client/<GetOauth2BearerToken>d__3::System.Collections.IEnumerator.Reset()
extern void U3CGetOauth2BearerTokenU3Ed__3_System_Collections_IEnumerator_Reset_m482F5B7AB1ACFA8A62F3B40F8D6203B83F3D55A9 (void);
// 0x00000039 System.Object Twity.Client/<GetOauth2BearerToken>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CGetOauth2BearerTokenU3Ed__3_System_Collections_IEnumerator_get_Current_mC2E3116949C07DB5A2E2D6480F3ED10257FBEAF6 (void);
// 0x0000003A System.Void Twity.Client/<GenerateRequestToken>d__4::.ctor(System.Int32)
extern void U3CGenerateRequestTokenU3Ed__4__ctor_m0422279F9383699B382B6469BD518A51E0F7BEA3 (void);
// 0x0000003B System.Void Twity.Client/<GenerateRequestToken>d__4::System.IDisposable.Dispose()
extern void U3CGenerateRequestTokenU3Ed__4_System_IDisposable_Dispose_m02B18DBC777D20C5D04CE27A211277F2596D98E1 (void);
// 0x0000003C System.Boolean Twity.Client/<GenerateRequestToken>d__4::MoveNext()
extern void U3CGenerateRequestTokenU3Ed__4_MoveNext_m6C0BE6471ED709A9B36FEFEF482A875816471820 (void);
// 0x0000003D System.Object Twity.Client/<GenerateRequestToken>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateRequestTokenU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF62D7C04F45AC2DB56CBCFEEA0D1AB86125D1793 (void);
// 0x0000003E System.Void Twity.Client/<GenerateRequestToken>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGenerateRequestTokenU3Ed__4_System_Collections_IEnumerator_Reset_m683EC7A3C8DBF8E9FFB8BB98DCFE420E20C8AD88 (void);
// 0x0000003F System.Object Twity.Client/<GenerateRequestToken>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateRequestTokenU3Ed__4_System_Collections_IEnumerator_get_Current_m84A58B5D7276B5325A7CC31CD0E32BBCB34B2BE6 (void);
// 0x00000040 System.Void Twity.Client/<GenerateRequestToken>d__5::.ctor(System.Int32)
extern void U3CGenerateRequestTokenU3Ed__5__ctor_mD7560F442C9228D7C47C7C4BC3C986F9AB47F668 (void);
// 0x00000041 System.Void Twity.Client/<GenerateRequestToken>d__5::System.IDisposable.Dispose()
extern void U3CGenerateRequestTokenU3Ed__5_System_IDisposable_Dispose_m4F2164B4395E383978E0AF04D40A814CF79B1C28 (void);
// 0x00000042 System.Boolean Twity.Client/<GenerateRequestToken>d__5::MoveNext()
extern void U3CGenerateRequestTokenU3Ed__5_MoveNext_mEF38A9EBFC2993781E48AD0B847A7B4607CD7A3E (void);
// 0x00000043 System.Object Twity.Client/<GenerateRequestToken>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateRequestTokenU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DFF339A4E7ACBB748A5A9C42D5851CD6526C825 (void);
// 0x00000044 System.Void Twity.Client/<GenerateRequestToken>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGenerateRequestTokenU3Ed__5_System_Collections_IEnumerator_Reset_m6FB87E0E0771B7FEFDAB8B5641F1271D5020243F (void);
// 0x00000045 System.Object Twity.Client/<GenerateRequestToken>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateRequestTokenU3Ed__5_System_Collections_IEnumerator_get_Current_mC02B4264E85634FC6121CEE55E67805EEA10A075 (void);
// 0x00000046 System.Void Twity.Client/<GenerateAccessToken>d__6::.ctor(System.Int32)
extern void U3CGenerateAccessTokenU3Ed__6__ctor_mE0C0AA3B3A9B3B812D28CABDBB5A727C08515558 (void);
// 0x00000047 System.Void Twity.Client/<GenerateAccessToken>d__6::System.IDisposable.Dispose()
extern void U3CGenerateAccessTokenU3Ed__6_System_IDisposable_Dispose_m090EB9398BE78F117F97FA80FBF75CF49476C64A (void);
// 0x00000048 System.Boolean Twity.Client/<GenerateAccessToken>d__6::MoveNext()
extern void U3CGenerateAccessTokenU3Ed__6_MoveNext_mF5BC6A7E4F558AF6F4AD3FBF2DD906F7FBB4D7B7 (void);
// 0x00000049 System.Object Twity.Client/<GenerateAccessToken>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateAccessTokenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m509D69C93AEC3D19110C55479A0940EB8804847F (void);
// 0x0000004A System.Void Twity.Client/<GenerateAccessToken>d__6::System.Collections.IEnumerator.Reset()
extern void U3CGenerateAccessTokenU3Ed__6_System_Collections_IEnumerator_Reset_m3109BDF2C6D338A0A85432CF233C98DD3AEED3F0 (void);
// 0x0000004B System.Object Twity.Client/<GenerateAccessToken>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateAccessTokenU3Ed__6_System_Collections_IEnumerator_get_Current_m59F08CE41356D443A6E36E4206D9D03823201ED1 (void);
// 0x0000004C System.Void Twity.Client/<SendRequest>d__7::.ctor(System.Int32)
extern void U3CSendRequestU3Ed__7__ctor_mAB75823F6837F7A47C988536B29EB9427C584A81 (void);
// 0x0000004D System.Void Twity.Client/<SendRequest>d__7::System.IDisposable.Dispose()
extern void U3CSendRequestU3Ed__7_System_IDisposable_Dispose_mE83E95BA8164FF65D029CF51FB4FA0B90D85FC66 (void);
// 0x0000004E System.Boolean Twity.Client/<SendRequest>d__7::MoveNext()
extern void U3CSendRequestU3Ed__7_MoveNext_mB10C8969FC7E1E77004CBEFBF563B05AAE1897D8 (void);
// 0x0000004F System.Object Twity.Client/<SendRequest>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendRequestU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA26F8D60F411AD29F8B569391C15C71DD956FEDB (void);
// 0x00000050 System.Void Twity.Client/<SendRequest>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSendRequestU3Ed__7_System_Collections_IEnumerator_Reset_mF831DEAE4619D7D93A4EA074713DFFDBB19F415C (void);
// 0x00000051 System.Object Twity.Client/<SendRequest>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSendRequestU3Ed__7_System_Collections_IEnumerator_get_Current_mC22DB8D4C9DBD6593535B63EB24054BE839816B8 (void);
// 0x00000052 System.Void Twity.TwitterStreamCallback::.ctor(System.Object,System.IntPtr)
extern void TwitterStreamCallback__ctor_m66CE22E00700643F8AF0F6B1EC28A6419A2C4A8C (void);
// 0x00000053 System.Void Twity.TwitterStreamCallback::Invoke(System.String,Twity.StreamMessageType)
extern void TwitterStreamCallback_Invoke_mA293742C1200156136C550D3C886719CD28765BB (void);
// 0x00000054 System.IAsyncResult Twity.TwitterStreamCallback::BeginInvoke(System.String,Twity.StreamMessageType,System.AsyncCallback,System.Object)
extern void TwitterStreamCallback_BeginInvoke_mAAD892163BFDA19A210E212E0BB96D024C2410C0 (void);
// 0x00000055 System.Void Twity.TwitterStreamCallback::EndInvoke(System.IAsyncResult)
extern void TwitterStreamCallback_EndInvoke_m3C18F358015937307B423E132F65AA459B47C387 (void);
// 0x00000056 System.Void Twity.Stream::.ctor(Twity.StreamType)
extern void Stream__ctor_mF780F7F0CBF90AAACEF2B620DBB3FD2207A3E3C5 (void);
// 0x00000057 System.Collections.IEnumerator Twity.Stream::On(System.Collections.Generic.Dictionary`2<System.String,System.String>,Twity.TwitterStreamCallback)
extern void Stream_On_m60761E1797368DDA68CE77B47B4D2C27C1C276BF (void);
// 0x00000058 System.Void Twity.Stream::Off()
extern void Stream_Off_m670D876D3F3EAEE2A524308113B867CA589C00B1 (void);
// 0x00000059 System.Void Twity.Stream/<On>d__3::.ctor(System.Int32)
extern void U3COnU3Ed__3__ctor_m94C9C2B061FC7AE723E3A1490B935E96BD40049C (void);
// 0x0000005A System.Void Twity.Stream/<On>d__3::System.IDisposable.Dispose()
extern void U3COnU3Ed__3_System_IDisposable_Dispose_m76DB636B5714C3DC6DBE9C340B0B8CC9A06EDD26 (void);
// 0x0000005B System.Boolean Twity.Stream/<On>d__3::MoveNext()
extern void U3COnU3Ed__3_MoveNext_m9BE84548231EF8ACE554AF5730AD38F2A24ABA63 (void);
// 0x0000005C System.Object Twity.Stream/<On>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3815F036CBBC0EF9420AA22C725B9AB2F8A1DF16 (void);
// 0x0000005D System.Void Twity.Stream/<On>d__3::System.Collections.IEnumerator.Reset()
extern void U3COnU3Ed__3_System_Collections_IEnumerator_Reset_m855F120E76C13BD3915D992778CD4F3CE0BD46E0 (void);
// 0x0000005E System.Object Twity.Stream/<On>d__3::System.Collections.IEnumerator.get_Current()
extern void U3COnU3Ed__3_System_Collections_IEnumerator_get_Current_mDD08942A991CEDC8F5DBFE94852702F5A6B378CE (void);
// 0x0000005F System.Void Twity.StreamingDownloadHandler::.ctor(Twity.TwitterStreamCallback)
extern void StreamingDownloadHandler__ctor_m5FA6E1765A52CB2D7C0F2115E67361989796B21B (void);
// 0x00000060 System.Boolean Twity.StreamingDownloadHandler::ReceiveData(System.Byte[],System.Int32)
extern void StreamingDownloadHandler_ReceiveData_mC5D4F4A32CF0182E0E543B643F6C50614E10D328 (void);
// 0x00000061 System.Void Twity.StreamingDownloadHandler::CheckMessageType(System.String)
extern void StreamingDownloadHandler_CheckMessageType_mBA7BCA9A8EFBB47D276A3F9A2EF948B40864EE10 (void);
// 0x00000062 System.Void Twity.FilterTrack::.ctor(System.String)
extern void FilterTrack__ctor_mBDF698C4A6639D440FD517A8A494F6947441BE7D (void);
// 0x00000063 System.Void Twity.FilterTrack::.ctor(System.Collections.Generic.List`1<System.String>)
extern void FilterTrack__ctor_m2CC99562495898F5B2A1815EBB6634C70976FD51 (void);
// 0x00000064 System.Void Twity.FilterTrack::AddTrack(System.String)
extern void FilterTrack_AddTrack_mFCCF5BCA3E0A1DBE5AA08D0E7010E64D0C993CFF (void);
// 0x00000065 System.Void Twity.FilterTrack::AddTracks(System.Collections.Generic.List`1<System.String>)
extern void FilterTrack_AddTracks_m1FF5F03C45CCA1C52DB49595F0789CD75B2CEC8A (void);
// 0x00000066 System.String Twity.FilterTrack::GetKey()
extern void FilterTrack_GetKey_m7290961ABC90B064A38F1E6DCEB4B750897C63DA (void);
// 0x00000067 System.String Twity.FilterTrack::GetValue()
extern void FilterTrack_GetValue_mE2FD32BFDBE4CB0B593C28D27BB43096452B636F (void);
// 0x00000068 System.Void Twity.FilterLocations::.ctor()
extern void FilterLocations__ctor_mC46EE071249EC0FEB399A09130EA393FB4D19080 (void);
// 0x00000069 System.Void Twity.FilterLocations::.ctor(Twity.Coordinate,Twity.Coordinate)
extern void FilterLocations__ctor_m9D8D5F0843408CF581156A3183DF376FFB547866 (void);
// 0x0000006A System.Void Twity.FilterLocations::AddLocation(Twity.Coordinate,Twity.Coordinate)
extern void FilterLocations_AddLocation_m9EB4686B26CD9487A748598A965C358775A9ECFC (void);
// 0x0000006B System.String Twity.FilterLocations::GetKey()
extern void FilterLocations_GetKey_mC817046371BACB8A0B844390E620EFDB970AB705 (void);
// 0x0000006C System.String Twity.FilterLocations::GetValue()
extern void FilterLocations_GetValue_m21BC5FCA1C233FF7055F47A661DEB4C9F1BED8FF (void);
// 0x0000006D System.Single Twity.Coordinate::get_lng()
extern void Coordinate_get_lng_mF64590776C038C0F76B4F8841B68AB4FAE4D12B1 (void);
// 0x0000006E System.Void Twity.Coordinate::set_lng(System.Single)
extern void Coordinate_set_lng_m1AC64DDC84AF216A2C942EBA1BDA9B27FCDF37D4 (void);
// 0x0000006F System.Single Twity.Coordinate::get_lat()
extern void Coordinate_get_lat_mB5DE0E2CB95F7C3B389B4706D717657C931FEA1E (void);
// 0x00000070 System.Void Twity.Coordinate::set_lat(System.Single)
extern void Coordinate_set_lat_m40DBEE59941924384B6FDF3A2B1F85A7FB0387F9 (void);
// 0x00000071 System.Void Twity.Coordinate::.ctor(System.Single,System.Single)
extern void Coordinate__ctor_m927B6CA22565CED2FD0A0AF96BAA17EDBE3DB427 (void);
// 0x00000072 System.Void Twity.FilterFollow::.ctor(System.Collections.Generic.List`1<System.String>)
extern void FilterFollow__ctor_m90CDA3046FBF79A453D5BDD8A75739887328A906 (void);
// 0x00000073 System.Void Twity.FilterFollow::.ctor(System.Collections.Generic.List`1<System.Int64>)
extern void FilterFollow__ctor_m7BAD053CBD078F3063393DE783E83EBA681D9328 (void);
// 0x00000074 System.Void Twity.FilterFollow::.ctor(System.Int64)
extern void FilterFollow__ctor_m8097A33C38D83CFCF2B76278316EEEDC9AE4691D (void);
// 0x00000075 System.Void Twity.FilterFollow::AddId(System.Int64)
extern void FilterFollow_AddId_mB9622584B22CC61F4B040C825E070EF57ECD95F4 (void);
// 0x00000076 System.Void Twity.FilterFollow::AddIds(System.Collections.Generic.List`1<System.Int64>)
extern void FilterFollow_AddIds_m1E28FA2355758BF166B5E8AEFC625D438FA81707 (void);
// 0x00000077 System.String Twity.FilterFollow::GetKey()
extern void FilterFollow_GetKey_mD933D1748FEA2047F3043BCC81AF30EEEF80D706 (void);
// 0x00000078 System.String Twity.FilterFollow::GetValue()
extern void FilterFollow_GetValue_m2084B82D6504B6DB1E300B81180E58BFBEA3E008 (void);
// 0x00000079 System.String Twity.Helpers.JsonHelper::ArrayToObject(System.String)
extern void JsonHelper_ArrayToObject_mF586D27C70A931072BFE91A73AF1C8130F77CDEF (void);
// 0x0000007A System.Void Twity.Helpers.JsonHelper::.ctor()
extern void JsonHelper__ctor_mBEF1019EF4999E6CE1E26CAF95C351A745EA9D7D (void);
// 0x0000007B System.Collections.Generic.SortedDictionary`2<System.String,System.String> Twity.Helpers.Helper::ConvertToSortedDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Helper_ConvertToSortedDictionary_mAC4C021FA3168393304709E0B8A751C6E5E054EA (void);
// 0x0000007C System.String Twity.Helpers.Helper::GenerateRequestparams(System.Collections.Generic.SortedDictionary`2<System.String,System.String>)
extern void Helper_GenerateRequestparams_mCF77DDD50B4E775D09EB81B264101CA673572695 (void);
// 0x0000007D System.String Twity.Helpers.Helper::UrlEncode(System.String)
extern void Helper_UrlEncode_mBE391C51AC67BAABFC5D976520F252F16BBE0068 (void);
// 0x0000007E System.Void Twity.Helpers.Helper::.ctor()
extern void Helper__ctor_m3FBEED53B8B43AE919B6B2EAC2B396E5507270A4 (void);
// 0x0000007F System.Void Twity.DataModels.Trends.Trend::.ctor()
extern void Trend__ctor_m83B62C6B833E7291C2C001D0F85072131797A4E5 (void);
// 0x00000080 System.Void Twity.DataModels.Trends.TrendsPlace::.ctor()
extern void TrendsPlace__ctor_m844F73CF87EA05913F11DEE9E197785D4F6CC7D5 (void);
// 0x00000081 System.Void Twity.DataModels.Trends.Woeid_location::.ctor()
extern void Woeid_location__ctor_m1E8E409856B545ACFCD8C80138D6206F5837ED99 (void);
// 0x00000082 System.Void Twity.DataModels.StreamMessages.DeletedStatus::.ctor()
extern void DeletedStatus__ctor_mA5E9B4F7D4B03750F8CBBC86988894A08CC28498 (void);
// 0x00000083 System.Void Twity.DataModels.StreamMessages.Disconnect::.ctor()
extern void Disconnect__ctor_m04527128E01524B37C5CE3117BC5D5BD9F9CE082 (void);
// 0x00000084 System.Void Twity.DataModels.StreamMessages.DisconnectMessage::.ctor()
extern void DisconnectMessage__ctor_mC9AF71906C391389CA55B571176DA12FA8ECAEB4 (void);
// 0x00000085 System.Void Twity.DataModels.StreamMessages.FriendsList::.ctor()
extern void FriendsList__ctor_mE49DA4F12D3BA217AC918B71FE13E93C9CE313B8 (void);
// 0x00000086 System.Void Twity.DataModels.StreamMessages.LimitNotice::.ctor()
extern void LimitNotice__ctor_mEF033092C986B32306EC1659B2EAEB60111CBDDD (void);
// 0x00000087 System.Void Twity.DataModels.StreamMessages.LocationDeletionNotice::.ctor()
extern void LocationDeletionNotice__ctor_mA2D9B3D87CAE3A4D1B184AE7F73FC3EDA75400CA (void);
// 0x00000088 System.Void Twity.DataModels.StreamMessages.ScrubGeo::.ctor()
extern void ScrubGeo__ctor_m72ED85217B7B5F607FA5D10A805EE9E4BDD517FC (void);
// 0x00000089 System.Void Twity.DataModels.StreamMessages.StallWarning::.ctor()
extern void StallWarning__ctor_m888711235F9A1E096342F6C8A7D8AAC0D3301B00 (void);
// 0x0000008A System.Void Twity.DataModels.StreamMessages.StatusDelete::.ctor()
extern void StatusDelete__ctor_mA86542DC095E0DA6F460696BCAFB32F561395406 (void);
// 0x0000008B System.Void Twity.DataModels.StreamMessages.StatusDeletionNotice::.ctor()
extern void StatusDeletionNotice__ctor_m93112A549A96582FDE2180E26313272B994A2954 (void);
// 0x0000008C System.Void Twity.DataModels.StreamMessages.StatusWithheld::.ctor()
extern void StatusWithheld__ctor_m10FA4A0F1E8057749C082103847CA161D9D1B377 (void);
// 0x0000008D System.Void Twity.DataModels.StreamMessages.StreamEvent::.ctor()
extern void StreamEvent__ctor_m485DC27A9074B0863FC89FAC09F622E6EB9A17DC (void);
// 0x0000008E System.Void Twity.DataModels.StreamMessages.StringifyFriendsList::.ctor()
extern void StringifyFriendsList__ctor_mB682329CFA714DA2BC36F91E57993BEDF0E20996 (void);
// 0x0000008F System.Void Twity.DataModels.StreamMessages.TrackLimit::.ctor()
extern void TrackLimit__ctor_mF65EF7882BA081D9BBDA99F02BAAD185501E12F8 (void);
// 0x00000090 System.Void Twity.DataModels.StreamMessages.UserWithheld::.ctor()
extern void UserWithheld__ctor_m29AC86AD83906CBBAD30A1EBAF0163EFA1407115 (void);
// 0x00000091 System.Void Twity.DataModels.StreamMessages.Warning::.ctor()
extern void Warning__ctor_mB4053FB4F23D81AA2F947DDA1366B2425F14E990 (void);
// 0x00000092 System.Void Twity.DataModels.StreamMessages.WithheldContentNotice::.ctor()
extern void WithheldContentNotice__ctor_m50D6F9A5319A045B0F4087595F6414FD02D581F5 (void);
// 0x00000093 System.Void Twity.DataModels.Responses.ErrorResponse::.ctor()
extern void ErrorResponse__ctor_mDABD1C9D27F734385DC037D753182CBC34CD77D3 (void);
// 0x00000094 System.Void Twity.DataModels.Responses.FollowersListResponse::.ctor()
extern void FollowersListResponse__ctor_m5EBE568798E97075F98B8DE34551BFE0C4A8BA93 (void);
// 0x00000095 System.Void Twity.DataModels.Responses.FriendsidsResponse::.ctor()
extern void FriendsidsResponse__ctor_mB154514E8DC7A89FCE34C88C5810A85D61EFC33F (void);
// 0x00000096 System.Void Twity.DataModels.Responses.FriendsListResponse::.ctor()
extern void FriendsListResponse__ctor_m4755F959B702F6916E76BE531E24AE9432C1A52A (void);
// 0x00000097 System.Void Twity.DataModels.Responses.SearchTweetsResponse::.ctor()
extern void SearchTweetsResponse__ctor_m5735BFBA100A7B8C1FB2C994064A4B2C4D0E8000 (void);
// 0x00000098 System.Void Twity.DataModels.Responses.StatusesHomeTimelineResponse::.ctor()
extern void StatusesHomeTimelineResponse__ctor_m4B879A7185D6463F2DF10C414391C39E1A4A5958 (void);
// 0x00000099 System.Void Twity.DataModels.Responses.StatusesUserTimelineResponse::.ctor()
extern void StatusesUserTimelineResponse__ctor_mF4B8B7E48EE8A4BC8485AF4DDF19C72012127C31 (void);
// 0x0000009A System.Void Twity.DataModels.Responses.TrendsPlaceResponse::.ctor()
extern void TrendsPlaceResponse__ctor_m64D5AE63CFC87156A86347CC0D4F570D3DFF9FD9 (void);
// 0x0000009B System.Void Twity.DataModels.Responses.Tweets::.ctor()
extern void Tweets__ctor_m9E42EF6B92FAE067695C85F8116B6742F71FEDCA (void);
// 0x0000009C System.Void Twity.DataModels.Oauth.AccessToken::.ctor()
extern void AccessToken__ctor_m19C90672826B2D46124DC9DA631AA04571844EE1 (void);
// 0x0000009D System.Void Twity.DataModels.Oauth.BearerToken::.ctor()
extern void BearerToken__ctor_m48B0FA0063BCA121AD25182D73858FE9BAE4B6B8 (void);
// 0x0000009E System.Void Twity.DataModels.Errors.Error::.ctor()
extern void Error__ctor_mDD14010598F0D1723617D44A9B8EAAB79A82BA76 (void);
// 0x0000009F System.Void Twity.DataModels.Entities.Entities::.ctor()
extern void Entities__ctor_mCAE65BE8EC1FC097BD4B0474B964C7EBC75C3396 (void);
// 0x000000A0 System.Void Twity.DataModels.Entities.Extended_Entities::.ctor()
extern void Extended_Entities__ctor_mAD7111FA833BB0C90DFE9AF62437553AA042A373 (void);
// 0x000000A1 System.Void Twity.DataModels.Entities.HashTag::.ctor()
extern void HashTag__ctor_m048D83890F1B382BF8C5D585D523C6ABBBF64232 (void);
// 0x000000A2 System.Void Twity.DataModels.Entities.Media::.ctor()
extern void Media__ctor_m747ED4B87AF9C85B09EEE34B0038E234C20868CB (void);
// 0x000000A3 System.Void Twity.DataModels.Entities.Symbol::.ctor()
extern void Symbol__ctor_mD49AD4AA125CBA180C23DFDAA776AB9CD53B12E5 (void);
// 0x000000A4 System.Void Twity.DataModels.Entities.Url::.ctor()
extern void Url__ctor_mBC1DFB17FCCE927680042EA57BDA74A2C66489D6 (void);
// 0x000000A5 System.Void Twity.DataModels.Entities.UserMention::.ctor()
extern void UserMention__ctor_m9386949F63F8EB3D3DD6FE28BAFDA60462673595 (void);
// 0x000000A6 System.Void Twity.DataModels.Entities.Variant::.ctor()
extern void Variant__ctor_m9F0CD0AB25ED9AAE6CB73281B7E7B7747D47DE94 (void);
// 0x000000A7 System.Void Twity.DataModels.Entities.Video_Info::.ctor()
extern void Video_Info__ctor_mFBB493CC412CB8D7DF47E378672DCCAD43A2C5A4 (void);
// 0x000000A8 System.Void Twity.DataModels.DirectMessages.DirectMessage::.ctor()
extern void DirectMessage__ctor_m6DC76F60D3D30CEFB9CA95D7DBA8904073C35C47 (void);
// 0x000000A9 System.Void Twity.DataModels.DirectMessages.DirectMessages::.ctor()
extern void DirectMessages__ctor_m0EA6D78C7924D47F84C5C6BBD935516DAD41C154 (void);
// 0x000000AA System.Void Twity.DataModels.Core.List::.ctor()
extern void List__ctor_m9728090A3859D3655976B97EEFA579A1DB7E0A7C (void);
// 0x000000AB System.Void Twity.DataModels.Core.Tweet::.ctor()
extern void Tweet__ctor_m8C1CD928089ECED12DCBC939C9F7BC3DB4185FEB (void);
// 0x000000AC System.Void Twity.DataModels.Core.TweetObject::.ctor()
extern void TweetObject__ctor_m86F1F19158973C9BE2063481417B0F61CFB78487 (void);
// 0x000000AD System.Void Twity.DataModels.Core.TweetObjectWithUser::.ctor()
extern void TweetObjectWithUser__ctor_mC4D018937E5E0A4B6AF6C0295AEC6DC996C3DB7C (void);
// 0x000000AE System.Void Twity.DataModels.Core.TweetUser::.ctor()
extern void TweetUser__ctor_m739F1FB3A2CD1DA9768207FA770DCE5FF38123F1 (void);
// 0x000000AF System.Void Twity.DataModels.Core.UploadMedia::.ctor()
extern void UploadMedia__ctor_mFD5E95A63CEAA4AA8F28B62D031606980331FAB6 (void);
// 0x000000B0 System.Void Twity.DataModels.Core.UploadMediaImage::.ctor()
extern void UploadMediaImage__ctor_m35AD0B6648F79495F21A23BBAA7B59951E507105 (void);
static Il2CppMethodPointer s_methodPointers[176] = 
{
	Oauth_get_consumerKey_m61A846BA517257F029E8E139FEEC56E9921E1346,
	Oauth_set_consumerKey_mD68460CC3E5F8367190F2B42C275154D3AFEC0D9,
	Oauth_get_consumerSecret_m5A65F7AD328F0A61DF0B0A4B613D2ABA976FCF9B,
	Oauth_set_consumerSecret_m3B1C7C14A340A37E4B185158889DB758A293606C,
	Oauth_get_accessToken_m2EBC8E59004D1269320CB6F6A7B7FFCA7426B6FB,
	Oauth_set_accessToken_mF41BD5D9A21D4E2B0A4CAAF65495E3015701E93B,
	Oauth_get_accessTokenSecret_m999E61ABEA354C54661982635EFCF8B3BBC03D5B,
	Oauth_set_accessTokenSecret_m19D3AF5A70D4FB192E4CF09643D61765755E543B,
	Oauth_get_bearerToken_m674E6FD041A0B9FEA694ED51542B4317B53A380B,
	Oauth_set_bearerToken_mD5CEE7AC4541D7AA548D84D3461A9ADF1BA1F64A,
	Oauth_get_requestToken_mF01F8A8422811D62CE2AC37C8D6B28099DE3771D,
	Oauth_set_requestToken_mEEE71E1BB5D6DFC4679152D0E91811B5F6DD1E11,
	Oauth_get_requestTokenSecret_mB3347333C65AD21267525A9013E0E3D5E9BE499A,
	Oauth_set_requestTokenSecret_mD8B0944DC530A61E5B83F083BED3478AB4DBB011,
	Oauth_get_authorizeURL_m30207CBCB785E42EDA3C021E506226F97DDC2318,
	Oauth_set_authorizeURL_mA77A9BC6F7F5E8EFCAB3613642C0929770B1C340,
	Oauth_GenerateHeaderWithAccessToken_m9BE322169C1A12B41519032A0E5862E5B87B855F,
	Oauth_GenerateSignature_mBA4A1C46E37CE49D4D6C2FCE64985E55B002370F,
	Oauth_AddDefaultOauthParams_m13702BAB523CF3B342124875DEFC595538927EA5,
	Oauth_GenerateTimeStamp_m56AF846A0BE7117EF3AFF19F825EC081C9A5AC6B,
	Oauth_GenerateNonce_m0A45A228B37EB6052FF8410CF5E32589F89A703E,
	Oauth__ctor_mDDEED119A948B5DB1E678BE70A7CC53266F3D045,
	TwitterCallback__ctor_mF0E71E7438D170681D1C750E60974610A99BC3F9,
	TwitterCallback_Invoke_mC6989AD60A35A57F9807075055EFBEB926243868,
	TwitterCallback_BeginInvoke_m40F5B4BA8E1288AD6F2255278A88F0261E2760ED,
	TwitterCallback_EndInvoke_m207CF462953108317879C66174B26399F57BCB87,
	TwitterAuthenticationCallback__ctor_m9C4A19703E6495A85176C121D730CE097CA0A36C,
	TwitterAuthenticationCallback_Invoke_m1AD31EA8277D265CAF1124AD5A462C5AFD33C357,
	TwitterAuthenticationCallback_BeginInvoke_m15E81CDC4CBA5688EC8381DD5752A3FDE678FB23,
	TwitterAuthenticationCallback_EndInvoke_m1969361091C93EE9AAB3C4ADC13410C61406EA1D,
	Client_Get_m77B7CBC5231C803CE813F811390E332E13CF8C0A,
	Client_Post_m8F11395F0F9BD57C15725531E2F278D72039D6E0,
	Client_GetOauth2BearerToken_m7D63D358CF5F1D48DBC6F97229F1F0712F78CD3F,
	Client_GenerateRequestToken_m89EEF80AAB6C7CA299E44C28B17A47B2377253F1,
	Client_GenerateRequestToken_m76B83A0EF0246B6367DD49C8130BD1376BA7A099,
	Client_GenerateAccessToken_mC7057AF92BD4801271958D8B78B4A69CBF8C86BD,
	Client_SendRequest_m085BA40712294B20881B87FCA3A42F84763EFBE5,
	Client_ClearTokens_m1AFE2D0EEFC30BF006147F603E6455BA6745227B,
	Client__ctor_m1AA10B63F860CB77F0AF1E9CCB3F5C0D77C96144,
	U3CGetU3Ed__1__ctor_mB45A7109B8F2001A6DAD50231CC61CEC9DA4DBF6,
	U3CGetU3Ed__1_System_IDisposable_Dispose_m82749FC73D1782662E0CB7AA51E1D5112D2E8C56,
	U3CGetU3Ed__1_MoveNext_m545037CB597E77B024B6A800CD7E735A104655E6,
	U3CGetU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF57169BBB97FCB0C899391F8EE7E8C748B7138EC,
	U3CGetU3Ed__1_System_Collections_IEnumerator_Reset_mC92BD8C812B48EFA022304D9DA27D60E3956A31D,
	U3CGetU3Ed__1_System_Collections_IEnumerator_get_Current_m3DD2C851AD024F742A7D8889FF634ED1F5FFF58A,
	U3CPostU3Ed__2__ctor_mDB5C1B699A7427961D426CB512A13718F8DFE12D,
	U3CPostU3Ed__2_System_IDisposable_Dispose_mCF015EB8019B34A8EFFD4A08094283FBBAA4D31B,
	U3CPostU3Ed__2_MoveNext_mEA88825B5895BF700FAEC2BD713F2CC96DD866FC,
	U3CPostU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m604530C78E6599983B29DFF9654F3E7A92413F8B,
	U3CPostU3Ed__2_System_Collections_IEnumerator_Reset_m99E157F2D8CC526FF82DE83A7F0CA0B0C620934B,
	U3CPostU3Ed__2_System_Collections_IEnumerator_get_Current_m16AEE6D7393D5E3561C9591DD2B76033CC4EE94E,
	U3CGetOauth2BearerTokenU3Ed__3__ctor_mE9E732178D0D4E66F25C8B901562714A949E0235,
	U3CGetOauth2BearerTokenU3Ed__3_System_IDisposable_Dispose_m82CA1B1A5E08A65F773BDAACB4156204F87EB898,
	U3CGetOauth2BearerTokenU3Ed__3_MoveNext_mF3AF72C038C5A8A8D67D777224AE233660EC4AD9,
	U3CGetOauth2BearerTokenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBB9AD093BD70B71DD9B1D82D20CA64A238282B1,
	U3CGetOauth2BearerTokenU3Ed__3_System_Collections_IEnumerator_Reset_m482F5B7AB1ACFA8A62F3B40F8D6203B83F3D55A9,
	U3CGetOauth2BearerTokenU3Ed__3_System_Collections_IEnumerator_get_Current_mC2E3116949C07DB5A2E2D6480F3ED10257FBEAF6,
	U3CGenerateRequestTokenU3Ed__4__ctor_m0422279F9383699B382B6469BD518A51E0F7BEA3,
	U3CGenerateRequestTokenU3Ed__4_System_IDisposable_Dispose_m02B18DBC777D20C5D04CE27A211277F2596D98E1,
	U3CGenerateRequestTokenU3Ed__4_MoveNext_m6C0BE6471ED709A9B36FEFEF482A875816471820,
	U3CGenerateRequestTokenU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF62D7C04F45AC2DB56CBCFEEA0D1AB86125D1793,
	U3CGenerateRequestTokenU3Ed__4_System_Collections_IEnumerator_Reset_m683EC7A3C8DBF8E9FFB8BB98DCFE420E20C8AD88,
	U3CGenerateRequestTokenU3Ed__4_System_Collections_IEnumerator_get_Current_m84A58B5D7276B5325A7CC31CD0E32BBCB34B2BE6,
	U3CGenerateRequestTokenU3Ed__5__ctor_mD7560F442C9228D7C47C7C4BC3C986F9AB47F668,
	U3CGenerateRequestTokenU3Ed__5_System_IDisposable_Dispose_m4F2164B4395E383978E0AF04D40A814CF79B1C28,
	U3CGenerateRequestTokenU3Ed__5_MoveNext_mEF38A9EBFC2993781E48AD0B847A7B4607CD7A3E,
	U3CGenerateRequestTokenU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DFF339A4E7ACBB748A5A9C42D5851CD6526C825,
	U3CGenerateRequestTokenU3Ed__5_System_Collections_IEnumerator_Reset_m6FB87E0E0771B7FEFDAB8B5641F1271D5020243F,
	U3CGenerateRequestTokenU3Ed__5_System_Collections_IEnumerator_get_Current_mC02B4264E85634FC6121CEE55E67805EEA10A075,
	U3CGenerateAccessTokenU3Ed__6__ctor_mE0C0AA3B3A9B3B812D28CABDBB5A727C08515558,
	U3CGenerateAccessTokenU3Ed__6_System_IDisposable_Dispose_m090EB9398BE78F117F97FA80FBF75CF49476C64A,
	U3CGenerateAccessTokenU3Ed__6_MoveNext_mF5BC6A7E4F558AF6F4AD3FBF2DD906F7FBB4D7B7,
	U3CGenerateAccessTokenU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m509D69C93AEC3D19110C55479A0940EB8804847F,
	U3CGenerateAccessTokenU3Ed__6_System_Collections_IEnumerator_Reset_m3109BDF2C6D338A0A85432CF233C98DD3AEED3F0,
	U3CGenerateAccessTokenU3Ed__6_System_Collections_IEnumerator_get_Current_m59F08CE41356D443A6E36E4206D9D03823201ED1,
	U3CSendRequestU3Ed__7__ctor_mAB75823F6837F7A47C988536B29EB9427C584A81,
	U3CSendRequestU3Ed__7_System_IDisposable_Dispose_mE83E95BA8164FF65D029CF51FB4FA0B90D85FC66,
	U3CSendRequestU3Ed__7_MoveNext_mB10C8969FC7E1E77004CBEFBF563B05AAE1897D8,
	U3CSendRequestU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA26F8D60F411AD29F8B569391C15C71DD956FEDB,
	U3CSendRequestU3Ed__7_System_Collections_IEnumerator_Reset_mF831DEAE4619D7D93A4EA074713DFFDBB19F415C,
	U3CSendRequestU3Ed__7_System_Collections_IEnumerator_get_Current_mC22DB8D4C9DBD6593535B63EB24054BE839816B8,
	TwitterStreamCallback__ctor_m66CE22E00700643F8AF0F6B1EC28A6419A2C4A8C,
	TwitterStreamCallback_Invoke_mA293742C1200156136C550D3C886719CD28765BB,
	TwitterStreamCallback_BeginInvoke_mAAD892163BFDA19A210E212E0BB96D024C2410C0,
	TwitterStreamCallback_EndInvoke_m3C18F358015937307B423E132F65AA459B47C387,
	Stream__ctor_mF780F7F0CBF90AAACEF2B620DBB3FD2207A3E3C5,
	Stream_On_m60761E1797368DDA68CE77B47B4D2C27C1C276BF,
	Stream_Off_m670D876D3F3EAEE2A524308113B867CA589C00B1,
	U3COnU3Ed__3__ctor_m94C9C2B061FC7AE723E3A1490B935E96BD40049C,
	U3COnU3Ed__3_System_IDisposable_Dispose_m76DB636B5714C3DC6DBE9C340B0B8CC9A06EDD26,
	U3COnU3Ed__3_MoveNext_m9BE84548231EF8ACE554AF5730AD38F2A24ABA63,
	U3COnU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3815F036CBBC0EF9420AA22C725B9AB2F8A1DF16,
	U3COnU3Ed__3_System_Collections_IEnumerator_Reset_m855F120E76C13BD3915D992778CD4F3CE0BD46E0,
	U3COnU3Ed__3_System_Collections_IEnumerator_get_Current_mDD08942A991CEDC8F5DBFE94852702F5A6B378CE,
	StreamingDownloadHandler__ctor_m5FA6E1765A52CB2D7C0F2115E67361989796B21B,
	StreamingDownloadHandler_ReceiveData_mC5D4F4A32CF0182E0E543B643F6C50614E10D328,
	StreamingDownloadHandler_CheckMessageType_mBA7BCA9A8EFBB47D276A3F9A2EF948B40864EE10,
	FilterTrack__ctor_mBDF698C4A6639D440FD517A8A494F6947441BE7D,
	FilterTrack__ctor_m2CC99562495898F5B2A1815EBB6634C70976FD51,
	FilterTrack_AddTrack_mFCCF5BCA3E0A1DBE5AA08D0E7010E64D0C993CFF,
	FilterTrack_AddTracks_m1FF5F03C45CCA1C52DB49595F0789CD75B2CEC8A,
	FilterTrack_GetKey_m7290961ABC90B064A38F1E6DCEB4B750897C63DA,
	FilterTrack_GetValue_mE2FD32BFDBE4CB0B593C28D27BB43096452B636F,
	FilterLocations__ctor_mC46EE071249EC0FEB399A09130EA393FB4D19080,
	FilterLocations__ctor_m9D8D5F0843408CF581156A3183DF376FFB547866,
	FilterLocations_AddLocation_m9EB4686B26CD9487A748598A965C358775A9ECFC,
	FilterLocations_GetKey_mC817046371BACB8A0B844390E620EFDB970AB705,
	FilterLocations_GetValue_m21BC5FCA1C233FF7055F47A661DEB4C9F1BED8FF,
	Coordinate_get_lng_mF64590776C038C0F76B4F8841B68AB4FAE4D12B1,
	Coordinate_set_lng_m1AC64DDC84AF216A2C942EBA1BDA9B27FCDF37D4,
	Coordinate_get_lat_mB5DE0E2CB95F7C3B389B4706D717657C931FEA1E,
	Coordinate_set_lat_m40DBEE59941924384B6FDF3A2B1F85A7FB0387F9,
	Coordinate__ctor_m927B6CA22565CED2FD0A0AF96BAA17EDBE3DB427,
	FilterFollow__ctor_m90CDA3046FBF79A453D5BDD8A75739887328A906,
	FilterFollow__ctor_m7BAD053CBD078F3063393DE783E83EBA681D9328,
	FilterFollow__ctor_m8097A33C38D83CFCF2B76278316EEEDC9AE4691D,
	FilterFollow_AddId_mB9622584B22CC61F4B040C825E070EF57ECD95F4,
	FilterFollow_AddIds_m1E28FA2355758BF166B5E8AEFC625D438FA81707,
	FilterFollow_GetKey_mD933D1748FEA2047F3043BCC81AF30EEEF80D706,
	FilterFollow_GetValue_m2084B82D6504B6DB1E300B81180E58BFBEA3E008,
	JsonHelper_ArrayToObject_mF586D27C70A931072BFE91A73AF1C8130F77CDEF,
	JsonHelper__ctor_mBEF1019EF4999E6CE1E26CAF95C351A745EA9D7D,
	Helper_ConvertToSortedDictionary_mAC4C021FA3168393304709E0B8A751C6E5E054EA,
	Helper_GenerateRequestparams_mCF77DDD50B4E775D09EB81B264101CA673572695,
	Helper_UrlEncode_mBE391C51AC67BAABFC5D976520F252F16BBE0068,
	Helper__ctor_m3FBEED53B8B43AE919B6B2EAC2B396E5507270A4,
	Trend__ctor_m83B62C6B833E7291C2C001D0F85072131797A4E5,
	TrendsPlace__ctor_m844F73CF87EA05913F11DEE9E197785D4F6CC7D5,
	Woeid_location__ctor_m1E8E409856B545ACFCD8C80138D6206F5837ED99,
	DeletedStatus__ctor_mA5E9B4F7D4B03750F8CBBC86988894A08CC28498,
	Disconnect__ctor_m04527128E01524B37C5CE3117BC5D5BD9F9CE082,
	DisconnectMessage__ctor_mC9AF71906C391389CA55B571176DA12FA8ECAEB4,
	FriendsList__ctor_mE49DA4F12D3BA217AC918B71FE13E93C9CE313B8,
	LimitNotice__ctor_mEF033092C986B32306EC1659B2EAEB60111CBDDD,
	LocationDeletionNotice__ctor_mA2D9B3D87CAE3A4D1B184AE7F73FC3EDA75400CA,
	ScrubGeo__ctor_m72ED85217B7B5F607FA5D10A805EE9E4BDD517FC,
	StallWarning__ctor_m888711235F9A1E096342F6C8A7D8AAC0D3301B00,
	StatusDelete__ctor_mA86542DC095E0DA6F460696BCAFB32F561395406,
	StatusDeletionNotice__ctor_m93112A549A96582FDE2180E26313272B994A2954,
	StatusWithheld__ctor_m10FA4A0F1E8057749C082103847CA161D9D1B377,
	StreamEvent__ctor_m485DC27A9074B0863FC89FAC09F622E6EB9A17DC,
	StringifyFriendsList__ctor_mB682329CFA714DA2BC36F91E57993BEDF0E20996,
	TrackLimit__ctor_mF65EF7882BA081D9BBDA99F02BAAD185501E12F8,
	UserWithheld__ctor_m29AC86AD83906CBBAD30A1EBAF0163EFA1407115,
	Warning__ctor_mB4053FB4F23D81AA2F947DDA1366B2425F14E990,
	WithheldContentNotice__ctor_m50D6F9A5319A045B0F4087595F6414FD02D581F5,
	ErrorResponse__ctor_mDABD1C9D27F734385DC037D753182CBC34CD77D3,
	FollowersListResponse__ctor_m5EBE568798E97075F98B8DE34551BFE0C4A8BA93,
	FriendsidsResponse__ctor_mB154514E8DC7A89FCE34C88C5810A85D61EFC33F,
	FriendsListResponse__ctor_m4755F959B702F6916E76BE531E24AE9432C1A52A,
	SearchTweetsResponse__ctor_m5735BFBA100A7B8C1FB2C994064A4B2C4D0E8000,
	StatusesHomeTimelineResponse__ctor_m4B879A7185D6463F2DF10C414391C39E1A4A5958,
	StatusesUserTimelineResponse__ctor_mF4B8B7E48EE8A4BC8485AF4DDF19C72012127C31,
	TrendsPlaceResponse__ctor_m64D5AE63CFC87156A86347CC0D4F570D3DFF9FD9,
	Tweets__ctor_m9E42EF6B92FAE067695C85F8116B6742F71FEDCA,
	AccessToken__ctor_m19C90672826B2D46124DC9DA631AA04571844EE1,
	BearerToken__ctor_m48B0FA0063BCA121AD25182D73858FE9BAE4B6B8,
	Error__ctor_mDD14010598F0D1723617D44A9B8EAAB79A82BA76,
	Entities__ctor_mCAE65BE8EC1FC097BD4B0474B964C7EBC75C3396,
	Extended_Entities__ctor_mAD7111FA833BB0C90DFE9AF62437553AA042A373,
	HashTag__ctor_m048D83890F1B382BF8C5D585D523C6ABBBF64232,
	Media__ctor_m747ED4B87AF9C85B09EEE34B0038E234C20868CB,
	Symbol__ctor_mD49AD4AA125CBA180C23DFDAA776AB9CD53B12E5,
	Url__ctor_mBC1DFB17FCCE927680042EA57BDA74A2C66489D6,
	UserMention__ctor_m9386949F63F8EB3D3DD6FE28BAFDA60462673595,
	Variant__ctor_m9F0CD0AB25ED9AAE6CB73281B7E7B7747D47DE94,
	Video_Info__ctor_mFBB493CC412CB8D7DF47E378672DCCAD43A2C5A4,
	DirectMessage__ctor_m6DC76F60D3D30CEFB9CA95D7DBA8904073C35C47,
	DirectMessages__ctor_m0EA6D78C7924D47F84C5C6BBD935516DAD41C154,
	List__ctor_m9728090A3859D3655976B97EEFA579A1DB7E0A7C,
	Tweet__ctor_m8C1CD928089ECED12DCBC939C9F7BC3DB4185FEB,
	TweetObject__ctor_m86F1F19158973C9BE2063481417B0F61CFB78487,
	TweetObjectWithUser__ctor_mC4D018937E5E0A4B6AF6C0295AEC6DC996C3DB7C,
	TweetUser__ctor_m739F1FB3A2CD1DA9768207FA770DCE5FF38123F1,
	UploadMedia__ctor_mFD5E95A63CEAA4AA8F28B62D031606980331FAB6,
	UploadMediaImage__ctor_m35AD0B6648F79495F21A23BBAA7B59951E507105,
};
static const int32_t s_InvokerIndices[176] = 
{
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	7076,
	6974,
	5588,
	5588,
	6521,
	7076,
	7076,
	4526,
	1932,
	1971,
	582,
	3636,
	1932,
	3668,
	799,
	3636,
	5588,
	5588,
	6849,
	6849,
	6065,
	6065,
	4891,
	7102,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	1932,
	1930,
	568,
	3636,
	3613,
	1277,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3636,
	1372,
	3636,
	3636,
	3636,
	3636,
	3636,
	4427,
	4427,
	4526,
	1935,
	1935,
	4427,
	4427,
	4470,
	3673,
	4470,
	3673,
	1979,
	3636,
	3636,
	3614,
	3614,
	3636,
	4427,
	4427,
	6849,
	4526,
	6849,
	6849,
	6849,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Twity_CodeGenModule;
const Il2CppCodeGenModule g_Twity_CodeGenModule = 
{
	"Twity.dll",
	176,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
