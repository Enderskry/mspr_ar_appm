﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.GameObject AnchorCreator::get_AnchorPrefab()
extern void AnchorCreator_get_AnchorPrefab_mBBAF7805E9D9F8D79408EE642D153BC76306B25A (void);
// 0x00000002 System.Void AnchorCreator::set_AnchorPrefab(UnityEngine.GameObject)
extern void AnchorCreator_set_AnchorPrefab_m53CC9CC3022C713826B44536B48B462C8A1FACFF (void);
// 0x00000003 System.Void AnchorCreator::RemoveAllAnchors()
extern void AnchorCreator_RemoveAllAnchors_m6BA302DD9EECA47969FD3F6E5397441DCB09ED21 (void);
// 0x00000004 System.Void AnchorCreator::Awake()
extern void AnchorCreator_Awake_m8820A3F157354D7EDED7B2D7CE2BFA42844F1F8E (void);
// 0x00000005 System.Void AnchorCreator::Update()
extern void AnchorCreator_Update_mA0A8BCCBAB0AE50DB087524E6273D11F1D6456D0 (void);
// 0x00000006 System.Void AnchorCreator::.ctor()
extern void AnchorCreator__ctor_m702B01425680D7FD1C9272A423AAF8A913E5476E (void);
// 0x00000007 System.Void AnchorCreator::.cctor()
extern void AnchorCreator__cctor_mF5AE519F086E52A01EA091115062D84D9F094F34 (void);
// 0x00000008 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m14D3A8BE3E9A745E6FD525B19ADDC892B8399B4D (void);
// 0x00000009 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD616A09A3B426EA5DE1FA37334DD194E43EEC110 (void);
// 0x0000000A System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_mC5DB0414A2514BF4851266C25141C903F0AC57BA (void);
// 0x0000000B System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m8781C85CFED871C8A81A5B88DB1031856E0FC9F0 (void);
// 0x0000000C System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m2343B05B1A8F14BAD4DD516C584281B66FE6A4E8 (void);
// 0x0000000D System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mB3D9BBD14EA1FE3ECDBACC2DB89C1B110B8B6B5F (void);
// 0x0000000E System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mF756D3C1F7925A69CD8C7C8CCE56209AB321FEF5 (void);
// 0x0000000F System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_m9A77651BCAE58AA0B994FFF6C6B63B1CFF2729F6 (void);
// 0x00000010 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mF4BA6DDB611A3FD966C8B9494AE6B3EB8647CEAD (void);
// 0x00000011 System.Void ImageRecogniation::Awake()
extern void ImageRecogniation_Awake_mB0269DF668FD0F078E016FAD515885074723E23C (void);
// 0x00000012 System.Void ImageRecogniation::OnEnable()
extern void ImageRecogniation_OnEnable_m63FFBA6793ADADDFF7AACF5F5A4EC3D8BA847D06 (void);
// 0x00000013 System.Void ImageRecogniation::OnDisable()
extern void ImageRecogniation_OnDisable_mA3713DAA6303E01A7FDB2AF1E694DD95996103D1 (void);
// 0x00000014 System.Void ImageRecogniation::OnImageChange(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void ImageRecogniation_OnImageChange_m7EC2C8145E66AA6F63595322096E965AE694FD6E (void);
// 0x00000015 System.Void ImageRecogniation::Start()
extern void ImageRecogniation_Start_m87E02686A714B70AF7861FF453FF3E013161198C (void);
// 0x00000016 System.Void ImageRecogniation::Update()
extern void ImageRecogniation_Update_mC603C86AA7168C62C5FC924FB75124200E3F98E2 (void);
// 0x00000017 System.Void ImageRecogniation::.ctor()
extern void ImageRecogniation__ctor_mAC16FAE6FE84689C7FCECAE685493C5537F5371F (void);
// 0x00000018 System.Void RecogniationMultiImage::Awake()
extern void RecogniationMultiImage_Awake_m50D762D252CF11E2B9F816BFAE5BD0D3DFB251A1 (void);
// 0x00000019 System.Void RecogniationMultiImage::OnEnable()
extern void RecogniationMultiImage_OnEnable_m602457113F07D4311256B10BC093409AEFAC5E1C (void);
// 0x0000001A System.Void RecogniationMultiImage::OnDisable()
extern void RecogniationMultiImage_OnDisable_m5A68756429F0DC48127305484A76545B63F9C034 (void);
// 0x0000001B System.Void RecogniationMultiImage::ImageChange(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void RecogniationMultiImage_ImageChange_mE2D7FF27C05DF4BAB5606EED461801F8770A880D (void);
// 0x0000001C System.Void RecogniationMultiImage::UpdateImage(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void RecogniationMultiImage_UpdateImage_mB905BF84425394756587219742A7C355635640A6 (void);
// 0x0000001D System.Void RecogniationMultiImage::.ctor()
extern void RecogniationMultiImage__ctor_m37142BAC3CC05A08EC2C71FA9593904473264DE4 (void);
// 0x0000001E System.Void MainScript::LogMessage(System.String,System.String)
extern void MainScript_LogMessage_m91BF7567C53593AFD0358BB3EA724F8D0C0DD8A2 (void);
// 0x0000001F System.Void MainScript::Get()
extern void MainScript_Get_m661CD9D8F56F2D75B4667F3A65540388C423108D (void);
// 0x00000020 System.Void MainScript::Post()
extern void MainScript_Post_mB79191CF05B82594D5B7A862A8EA5E20424BFBA8 (void);
// 0x00000021 System.Void MainScript::Put()
extern void MainScript_Put_m0635AC6846FC16BB0099BAA2FF4A11DAF2814083 (void);
// 0x00000022 System.Void MainScript::Delete()
extern void MainScript_Delete_m74CD4E75650A5858532A58E90A22FEB1227F151E (void);
// 0x00000023 System.Void MainScript::AbortRequest()
extern void MainScript_AbortRequest_mA98E7D9BBF1C175455B423430DB5C71FB67D8A1A (void);
// 0x00000024 System.Void MainScript::DownloadFile()
extern void MainScript_DownloadFile_mBBFA99D95E3E778AE410029D328A4891197ADF6B (void);
// 0x00000025 System.Void MainScript::.ctor()
extern void MainScript__ctor_m815FA088F921DB1343C256EA721C073FADB5D713 (void);
// 0x00000026 System.Void MainScript::<Post>b__4_0(Models.Post)
extern void MainScript_U3CPostU3Eb__4_0_m5CC5AE9AF5CD587FD301B42E09049EF8A253337F (void);
// 0x00000027 System.Void MainScript::<Post>b__4_1(System.Exception)
extern void MainScript_U3CPostU3Eb__4_1_m2F61C9F5A0B2D97B258B884C4A1B54B72538D968 (void);
// 0x00000028 System.Void MainScript::<Put>b__5_0(Proyecto26.RequestException,Proyecto26.ResponseHelper,Models.Post)
extern void MainScript_U3CPutU3Eb__5_0_m1BF18A7090ADB303E08E4C48EE4A6A34C0F18210 (void);
// 0x00000029 System.Void MainScript::<Delete>b__6_0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
extern void MainScript_U3CDeleteU3Eb__6_0_mE673244BD58CDAD546C09353881D692320ABDBD4 (void);
// 0x0000002A System.Void MainScript::<DownloadFile>b__8_0(Proyecto26.ResponseHelper)
extern void MainScript_U3CDownloadFileU3Eb__8_0_mA61E7597BE5C84115D06D54EAB62AA4181EFC42E (void);
// 0x0000002B System.Void MainScript::<DownloadFile>b__8_1(System.Exception)
extern void MainScript_U3CDownloadFileU3Eb__8_1_m09ECF502288EF0FC9F10DE661017318AD287F5E1 (void);
// 0x0000002C System.Void MainScript/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m09129313A9B160F21FAD9BC073200549EE68FB04 (void);
// 0x0000002D RSG.IPromise`1<Models.Todo[]> MainScript/<>c__DisplayClass3_0::<Get>b__0(Models.Post[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m60D54003CE39A2F4CA0424D27878ECF1D99C0180 (void);
// 0x0000002E RSG.IPromise`1<Models.User[]> MainScript/<>c__DisplayClass3_0::<Get>b__1(Models.Todo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_m129236EAD89BCDF394C44F28D4ECCFC10761E0B5 (void);
// 0x0000002F RSG.IPromise`1<Models.Photo[]> MainScript/<>c__DisplayClass3_0::<Get>b__2(Models.User[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mFBEE16C4C04D41290B63A23EEFF5BCA71BCEA6C6 (void);
// 0x00000030 System.Void MainScript/<>c__DisplayClass3_0::<Get>b__3(Models.Photo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_mE6B7237074F01FCC65E6EDF84D8FD41AB91EBF8E (void);
// 0x00000031 System.Void MainScript/<>c__DisplayClass3_0::<Get>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m802E0FE01483720E19629583659C8FC03112286F (void);
// 0x00000032 System.Void MainScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m2425F6CE9904CE8CFEFE96F253D878AB4EA7E4F3 (void);
// 0x00000033 System.Void MainScript/<>c::.ctor()
extern void U3CU3Ec__ctor_mAFA5E702511BDFE7294BCCF5713C6DF2D5CA74EB (void);
// 0x00000034 System.Void MainScript/<>c::<Put>b__5_1(Proyecto26.RequestException,System.Int32)
extern void U3CU3Ec_U3CPutU3Eb__5_1_m99752A12CB0D3B95CEB0B34F6724923370F00450 (void);
// 0x00000035 System.Void ReadInput::ReadStringInputName(System.String)
extern void ReadInput_ReadStringInputName_mF703ADA03B30A3115E608F8B0D19DA55B2E8BB4A (void);
// 0x00000036 System.Void ReadInput::ReadStringInputEmail(System.String)
extern void ReadInput_ReadStringInputEmail_mAA380240AED021929F049A54326CED07EF1E7141 (void);
// 0x00000037 System.Void ReadInput::UploadUser()
extern void ReadInput_UploadUser_m3E02DD2E6E2D51F5E67C3B0C8698B6A44090B8FC (void);
// 0x00000038 System.Void ReadInput::.ctor()
extern void ReadInput__ctor_mA0DFAAC66A5A241CBE1A198C3C430775540B828E (void);
// 0x00000039 System.Void ReadInput/<>c::.cctor()
extern void U3CU3Ec__cctor_m2EA29933503826CB7030B0EFCA9CAB5D124FBF45 (void);
// 0x0000003A System.Void ReadInput/<>c::.ctor()
extern void U3CU3Ec__ctor_m6F0D3B950AB4B7F9FF21614EE38442FF8E2E82A6 (void);
// 0x0000003B System.Void ReadInput/<>c::<UploadUser>b__5_0(Proyecto26.ResponseHelper)
extern void U3CU3Ec_U3CUploadUserU3Eb__5_0_mF474DEF828A0635268F89F936AA9CD3789AB7B0D (void);
// 0x0000003C System.Void ReadInput/<>c::<UploadUser>b__5_1(System.Exception)
extern void U3CU3Ec_U3CUploadUserU3Eb__5_1_m00A9087CCA2B186927398F3794CF232593739F29 (void);
// 0x0000003D System.Void ToggleChange::Start()
extern void ToggleChange_Start_mB50D6EA26D38D03CF715FAD07925A3F99CAA159D (void);
// 0x0000003E System.Void ToggleChange::ChangeColor(UnityEngine.UI.Toggle)
extern void ToggleChange_ChangeColor_mDEC392A30E5FEE24818952C586B9AFEB4622E3C1 (void);
// 0x0000003F System.Void ToggleChange::.ctor()
extern void ToggleChange__ctor_mED2E986F18942B94F28B438500AAD34072704039 (void);
// 0x00000040 System.Void ToggleChange/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m863305539EE289B3009B8AD4D4AFA1C916E348BE (void);
// 0x00000041 System.Boolean ToggleChange/<>c__DisplayClass5_0::<ChangeColor>b__0(System.String)
extern void U3CU3Ec__DisplayClass5_0_U3CChangeColorU3Eb__0_m430832C8610693DCCB09442D41C30F9018A664DF (void);
// 0x00000042 System.Void ToolTip::Update()
extern void ToolTip_Update_m628B4F8BF8F7A4EA6901421D1E4BEA2A91A0B6D5 (void);
// 0x00000043 System.Void ToolTip::.ctor()
extern void ToolTip__ctor_mC3F7C85CFD5491C4CBC41A3D102A0F2917A2AE01 (void);
// 0x00000044 System.Void UIElementDragger::Update()
extern void UIElementDragger_Update_m587434C83391B288CCD8F41C59DAD6B41414730C (void);
// 0x00000045 UnityEngine.GameObject UIElementDragger::GetObjectUnderMouse()
extern void UIElementDragger_GetObjectUnderMouse_m0FB27B48409EAE19F2D506980CAC8B97B357E418 (void);
// 0x00000046 UnityEngine.Transform UIElementDragger::GetDraggableTransformUnderMouse()
extern void UIElementDragger_GetDraggableTransformUnderMouse_m9814FED1C8BE991F8FE1822C74952642370C8A67 (void);
// 0x00000047 System.Void UIElementDragger::.ctor()
extern void UIElementDragger__ctor_mB04A805B65F7AF39BA1B4A903EF27073AB471C8A (void);
// 0x00000048 System.Void UIManagerTech::MoveToFront(UnityEngine.GameObject)
extern void UIManagerTech_MoveToFront_mEE23FCC6DE9F01151C7CEBC7D10F3B17B30A575B (void);
// 0x00000049 System.Void UIManagerTech::Start()
extern void UIManagerTech_Start_m5A0F82FD0BC1FC656CA43EEE205139EE160E1991 (void);
// 0x0000004A System.Void UIManagerTech::IncreaseIndex(System.Int32)
extern void UIManagerTech_IncreaseIndex_m0E93CD1CCB292B534BA608B3B0637E2D97476056 (void);
// 0x0000004B System.Void UIManagerTech::DecreaseIndex(System.Int32)
extern void UIManagerTech_DecreaseIndex_m0AD3FFA916DF3E561AABC52D00171903E9F4644B (void);
// 0x0000004C System.Void UIManagerTech::SetTint()
extern void UIManagerTech_SetTint_m59863BD69914E5546B2FB80B5D2362153EA3AD54 (void);
// 0x0000004D System.Void UIManagerTech::Update()
extern void UIManagerTech_Update_mFA0D4BB5230AC61BB54EA3DA110294C9D3B1B829 (void);
// 0x0000004E System.Void UIManagerTech::MessageDisplayDatabase(System.String,UnityEngine.Color)
extern void UIManagerTech_MessageDisplayDatabase_m1F9A477A37CC52AF2F087E543428620DD55B8299 (void);
// 0x0000004F System.Collections.IEnumerator UIManagerTech::MessageDisplay(System.String,UnityEngine.Color)
extern void UIManagerTech_MessageDisplay_m2FE0649475E11298C5705D14D722472ADA9F119C (void);
// 0x00000050 System.Void UIManagerTech::UIScaler()
extern void UIManagerTech_UIScaler_mE46A87763B58705F4A7E9FA0B0DC538D61036223 (void);
// 0x00000051 System.Void UIManagerTech::CheckSettings()
extern void UIManagerTech_CheckSettings_m98EA092B7A6C84953E2DF236968602CC46B76ACD (void);
// 0x00000052 System.String UIManagerTech::ResToString(UnityEngine.Resolution)
extern void UIManagerTech_ResToString_mB84898AFE4127CBC16B389CA63AB67B52E42AEE8 (void);
// 0x00000053 System.Void UIManagerTech::AudioSlider()
extern void UIManagerTech_AudioSlider_mC4C7E0002E6CB17D308682095CA71ACCC4041B4E (void);
// 0x00000054 System.Void UIManagerTech::Quit()
extern void UIManagerTech_Quit_m21FA5F4FCCD0414F620CEDCDE1B833F36134E0F9 (void);
// 0x00000055 System.Void UIManagerTech::QualityChange(System.Int32)
extern void UIManagerTech_QualityChange_m241D90D950C12B6C5E6D721B655197A44086ECA3 (void);
// 0x00000056 System.Void UIManagerTech::LoadNewLevel()
extern void UIManagerTech_LoadNewLevel_mF14996D103A88A75D3BC8353335BF6ECA0AD6BCA (void);
// 0x00000057 System.Void UIManagerTech::LoadSavedLevel()
extern void UIManagerTech_LoadSavedLevel_m0965D837D58421A0B2FAE4778F9C2430FFA3AA92 (void);
// 0x00000058 System.Collections.IEnumerator UIManagerTech::LoadAsynchronously(System.String)
extern void UIManagerTech_LoadAsynchronously_m4E2F0C72836E5A7E6C981763CF8C990017CA0615 (void);
// 0x00000059 System.Void UIManagerTech::UpdateAccountValues()
extern void UIManagerTech_UpdateAccountValues_m29ADF42692DDE287B54985A0F5E3352250786CE5 (void);
// 0x0000005A System.Void UIManagerTech::ConfirmNewAccount()
extern void UIManagerTech_ConfirmNewAccount_m23071D4B32696D2DF65B3DD6BB3FCE20F21F1418 (void);
// 0x0000005B System.Void UIManagerTech::LoginButton()
extern void UIManagerTech_LoginButton_mC03DC4E4DAED07FF65323479C835B2F90FF70809 (void);
// 0x0000005C System.Void UIManagerTech::ConfirmDeleteAccount()
extern void UIManagerTech_ConfirmDeleteAccount_m3664DC9B1FCE822301E79BB8B8AA69E31E962FFB (void);
// 0x0000005D System.Void UIManagerTech::.ctor()
extern void UIManagerTech__ctor_m6D9895B068C3DACE44F72B70FDDE0FDD55F7C9BF (void);
// 0x0000005E System.Void UIManagerTech::<Start>b__79_0(System.Int32)
extern void UIManagerTech_U3CStartU3Eb__79_0_m2E693E6640086FCE3031F40FD1FB1FCEF38BCF51 (void);
// 0x0000005F System.Void UIManagerTech/<MessageDisplay>d__85::.ctor(System.Int32)
extern void U3CMessageDisplayU3Ed__85__ctor_mE07CFD5E22109645739388DB3BC61269F5641371 (void);
// 0x00000060 System.Void UIManagerTech/<MessageDisplay>d__85::System.IDisposable.Dispose()
extern void U3CMessageDisplayU3Ed__85_System_IDisposable_Dispose_m08CC2163590D5A388FAC20B7EC1F5D77FB04ED5A (void);
// 0x00000061 System.Boolean UIManagerTech/<MessageDisplay>d__85::MoveNext()
extern void U3CMessageDisplayU3Ed__85_MoveNext_m4CADA2F4A735DADB555D4BA190C586BB918123EF (void);
// 0x00000062 System.Object UIManagerTech/<MessageDisplay>d__85::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMessageDisplayU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5B2686F856089B29C597614D0E1A7F5FE52FB57 (void);
// 0x00000063 System.Void UIManagerTech/<MessageDisplay>d__85::System.Collections.IEnumerator.Reset()
extern void U3CMessageDisplayU3Ed__85_System_Collections_IEnumerator_Reset_mF7CE8F8D4159EA5AE4A460A60198198AE0920CDE (void);
// 0x00000064 System.Object UIManagerTech/<MessageDisplay>d__85::System.Collections.IEnumerator.get_Current()
extern void U3CMessageDisplayU3Ed__85_System_Collections_IEnumerator_get_Current_m396D622BA06A59BBB63DC0338877CC116109CDD0 (void);
// 0x00000065 System.Void UIManagerTech/<LoadAsynchronously>d__94::.ctor(System.Int32)
extern void U3CLoadAsynchronouslyU3Ed__94__ctor_m1BAA59970D13AB336CEA8CF507E8755AB8E4311D (void);
// 0x00000066 System.Void UIManagerTech/<LoadAsynchronously>d__94::System.IDisposable.Dispose()
extern void U3CLoadAsynchronouslyU3Ed__94_System_IDisposable_Dispose_m387A6DE049CA9C23EADDEBB72C366DAC2C9846EF (void);
// 0x00000067 System.Boolean UIManagerTech/<LoadAsynchronously>d__94::MoveNext()
extern void U3CLoadAsynchronouslyU3Ed__94_MoveNext_m695FD6533ED82D935DE549ECCE5704B92BBB6438 (void);
// 0x00000068 System.Object UIManagerTech/<LoadAsynchronously>d__94::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__94_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA687C08CC8F6BC039B78ABFAA544DC604467D9A (void);
// 0x00000069 System.Void UIManagerTech/<LoadAsynchronously>d__94::System.Collections.IEnumerator.Reset()
extern void U3CLoadAsynchronouslyU3Ed__94_System_Collections_IEnumerator_Reset_mD267C3EFAD02A00392DB7AD6033A41398D6547E2 (void);
// 0x0000006A System.Object UIManagerTech/<LoadAsynchronously>d__94::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__94_System_Collections_IEnumerator_get_Current_m690AE884567FC740FC975A761AAFE8052719717E (void);
// 0x0000006B System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x0000006C System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x0000006D System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x0000006E System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x0000006F System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x00000070 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x00000071 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x00000072 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x00000073 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x00000074 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x00000075 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x00000076 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x00000077 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x00000078 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x00000079 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x0000007A System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x0000007B System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x0000007C System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x0000007D System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x0000007E TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x0000007F System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x00000080 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x00000081 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x00000082 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x00000083 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x00000084 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x00000085 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x00000086 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x00000087 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x00000088 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x00000089 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x0000008A System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x0000008B System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x0000008C System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x0000008D System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x0000008E System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x0000008F System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x00000090 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x00000091 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x00000092 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x00000093 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x00000094 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x00000095 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x00000096 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x00000097 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x00000098 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x00000099 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x0000009A System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x0000009B System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x0000009C System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x0000009D System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x0000009E System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x0000009F System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x000000A0 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x000000A1 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x000000A2 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x000000A3 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x000000A4 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x000000A5 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x000000A6 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x000000A7 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x000000A8 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x000000A9 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x000000AA System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x000000AB System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x000000AC System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x000000AD System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x000000AE System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x000000AF System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x000000B0 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x000000B1 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x000000B2 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x000000B3 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x000000B4 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x000000B5 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x000000B6 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x000000B7 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x000000B8 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x000000B9 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x000000BA System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x000000BB System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x000000BC System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x000000BD System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x000000BE System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x000000BF System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x000000C0 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000000C1 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000000C2 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000000C3 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000000C4 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000000C5 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000000C6 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000000C7 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000000C8 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000000C9 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000000CA System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000000CB System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000000CC System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x000000CD System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000000CE System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000000CF System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000000D0 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000000D1 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000000D2 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000000D3 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000000D4 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000000D5 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x000000D6 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000000D7 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000000D8 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000000D9 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000000DA System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000000DB System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000000DC System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000000DD System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000000DE System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000000DF System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000000E0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000000E1 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000000E2 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000000E3 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x000000E4 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000000E5 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000000E6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000000E7 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000000E8 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x000000E9 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x000000EA System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x000000EB System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x000000EC System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x000000ED System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x000000EE System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x000000EF System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x000000F0 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m352D68463FC9600F9139AD78F0176368562F63C6 (void);
// 0x000000F1 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x000000F2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x000000F3 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x000000F4 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x000000F5 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797 (void);
// 0x000000F6 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x000000F7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x000000F8 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x000000F9 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x000000FA System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x000000FB System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1 (void);
// 0x000000FC System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x000000FD System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x000000FE System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x000000FF System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000100 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x00000101 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x00000102 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x00000103 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x00000104 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x00000105 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x00000106 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x00000107 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x00000108 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x00000109 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x0000010A System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x0000010B System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x0000010C System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x0000010D System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x0000010E System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x0000010F System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x00000110 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x00000111 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x00000112 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x00000113 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x00000114 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x00000115 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x00000116 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x00000117 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x00000118 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x00000119 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x0000011A System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x0000011B System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x0000011C System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x0000011D System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x0000011E System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x0000011F System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000120 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x00000121 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x00000122 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x00000123 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x00000124 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x00000125 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x00000126 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x00000127 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x00000128 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x00000129 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x0000012A System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x0000012B System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x0000012C System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x0000012D System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x0000012E System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x0000012F System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x00000130 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x00000131 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x00000132 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x00000133 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x00000134 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x00000135 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x00000136 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x00000137 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x00000138 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x00000139 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x0000013A System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x0000013B System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x0000013C System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x0000013D System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x0000013E System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x0000013F System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x00000140 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x00000141 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x00000142 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x00000143 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x00000144 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x00000145 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x00000146 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x00000147 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x00000148 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x00000149 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x0000014A System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x0000014B System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x0000014C System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x0000014D System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x0000014E System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x0000014F System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000150 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x00000151 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x00000152 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x00000155 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x00000156 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x00000157 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x00000158 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x00000159 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x0000015A System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x0000015B System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x0000015C System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x0000015D System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x0000015E System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x0000015F System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x00000160 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x00000161 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x00000162 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x00000163 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x00000164 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x00000165 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x00000166 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x00000167 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x00000168 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x00000169 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x0000016A System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x0000016B UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x0000016C System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x0000016D System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x0000016E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x0000016F System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000170 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x00000171 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x00000172 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x00000173 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x00000174 System.Void Script.Twitter.ShareButton::Share()
extern void ShareButton_Share_m03F465A5C7BDC061CF1E60950A19C37E7BFDC6E0 (void);
// 0x00000175 System.Void Script.Twitter.ShareButton::.ctor()
extern void ShareButton__ctor_mCCBB7E13A22B57B1DE31377177AC0C35AFEEFC0E (void);
// 0x00000176 System.Void Script.Twitter.ShareButton/<>c::.cctor()
extern void U3CU3Ec__cctor_m928ED25AE006A9860F5039513FEBFB988B1BB666 (void);
// 0x00000177 System.Void Script.Twitter.ShareButton/<>c::.ctor()
extern void U3CU3Ec__ctor_m53F9C64973736DF514D755B119DAE1FE1DCC1B96 (void);
// 0x00000178 System.Void Script.Twitter.ShareButton/<>c::<Share>b__0_0(Proyecto26.ResponseHelper)
extern void U3CU3Ec_U3CShareU3Eb__0_0_m60D5F2861EC9E4B4B64A68754FED63C9465580F3 (void);
// 0x00000179 System.Void Script.Twitter.ShareButton/<>c::<Share>b__0_1(System.Exception)
extern void U3CU3Ec_U3CShareU3Eb__0_1_m98B67B7F0C8CFC600D60B956AEA2021E3D513257 (void);
// 0x0000017A System.Void Script.Twitter.TwitterPost::Start()
extern void TwitterPost_Start_m9E15C10146EA697F7A025DB735111E3852B00713 (void);
// 0x0000017B System.Void Script.Twitter.TwitterPost::Upload()
extern void TwitterPost_Upload_m909285353B8CC8C2B23C35991127C885278ABB5C (void);
// 0x0000017C System.Void Script.Twitter.TwitterPost::MediaUploadCallback(System.Boolean,System.String)
extern void TwitterPost_MediaUploadCallback_mBDF91F0EEF18B926BFDD29B9FC55CD2F73AA9D24 (void);
// 0x0000017D System.Void Script.Twitter.TwitterPost::StatusesUpdateCallback(System.Boolean,System.String)
extern void TwitterPost_StatusesUpdateCallback_mC0D27B4824E869EF5772300727D825166454B1E7 (void);
// 0x0000017E System.Void Script.Twitter.TwitterPost::Callback(System.Boolean)
extern void TwitterPost_Callback_m7308346CCD285A2112B8F3BE182076295045DE13 (void);
// 0x0000017F System.Void Script.Twitter.TwitterPost::.ctor()
extern void TwitterPost__ctor_m77129030A3364462525BCCAD7866DDC8422FD2E5 (void);
// 0x00000180 System.Void Script.Twitter.TwitterPost/<>c::.cctor()
extern void U3CU3Ec__cctor_m544860AFC2B2770DDD8AEE73E7F067F22FD6E875 (void);
// 0x00000181 System.Void Script.Twitter.TwitterPost/<>c::.ctor()
extern void U3CU3Ec__ctor_mBAB23ADA139BD6EF82CA5B41B6E350A0FCEAEDAA (void);
// 0x00000182 System.Void Script.Twitter.TwitterPost/<>c::<Start>b__0_0(System.Boolean,System.String)
extern void U3CU3Ec_U3CStartU3Eb__0_0_m141ACEC84A19590D35DFABA5C39324A75E99F265 (void);
// 0x00000183 System.Void Scenes.ViewChange::LoadScene(System.String)
extern void ViewChange_LoadScene_mBA90A55289A6501B95A4DDEABC49FA8EA7024344 (void);
// 0x00000184 System.Void Scenes.ViewChange::BlankARView()
extern void ViewChange_BlankARView_mB7993B5323E9A22493817A8488A82FEC27293C4C (void);
// 0x00000185 System.Void Scenes.ViewChange::MenuView()
extern void ViewChange_MenuView_m390FBDD33756AC837BBB12199A9DF4E087A8CD4E (void);
// 0x00000186 System.Void Scenes.ViewChange::FormView()
extern void ViewChange_FormView_m844FC62335B1539D65BD088224FDAF2254F5E240 (void);
// 0x00000187 System.Void Scenes.ViewChange::Start()
extern void ViewChange_Start_m6A1FE182105FC16398D449A5C43CD1276E97A9C7 (void);
// 0x00000188 System.Void Scenes.ViewChange::Update()
extern void ViewChange_Update_m219FE23FD2D7528D3D3E7252B96EE7A62D6BD025 (void);
// 0x00000189 System.Void Scenes.ViewChange::.ctor()
extern void ViewChange__ctor_m51CEC5C50602B6D428C7165D2C7BE16B02489219 (void);
// 0x0000018A System.Int32 RSG.IPromise`1::get_Id()
// 0x0000018B RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x0000018C System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000018D System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x0000018E System.Void RSG.IPromise`1::Done()
// 0x0000018F RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x00000190 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000191 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000192 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x00000193 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x00000194 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000195 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000196 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000197 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000198 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000199 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000019A RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x0000019B RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000019C RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000019D RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000019E RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000019F RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x000001A0 RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000001A1 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000001A2 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x000001A3 System.Void RSG.IRejectable::Reject(System.Exception)
// 0x000001A4 System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x000001A5 System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x000001A6 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x000001A7 System.Int32 RSG.Promise`1::get_Id()
// 0x000001A8 System.String RSG.Promise`1::get_Name()
// 0x000001A9 System.Void RSG.Promise`1::set_Name(System.String)
// 0x000001AA RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x000001AB System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x000001AC System.Void RSG.Promise`1::.ctor()
// 0x000001AD System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x000001AE System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x000001AF System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x000001B0 System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x000001B1 System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x000001B2 System.Void RSG.Promise`1::ClearHandlers()
// 0x000001B3 System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x000001B4 System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x000001B5 System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x000001B6 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x000001B7 System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x000001B8 System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x000001B9 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000001BA System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x000001BB System.Void RSG.Promise`1::Done()
// 0x000001BC RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x000001BD RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x000001BE RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000001BF RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000001C0 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000001C1 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x000001C2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000001C3 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000001C4 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000001C5 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000001C6 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001C7 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001C8 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000001C9 System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000001CA System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x000001CB RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001CC RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001CD RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x000001CE RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000001CF RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001D0 RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001D1 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x000001D2 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000001D3 RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x000001D4 RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x000001D5 RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x000001D6 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000001D7 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000001D8 RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x000001D9 System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x000001DA System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x000001DB System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x000001DC System.Void RSG.Promise`1/<>c__DisplayClass24_0::.ctor()
// 0x000001DD System.Void RSG.Promise`1/<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x000001DE System.Void RSG.Promise`1/<>c__DisplayClass26_0::.ctor()
// 0x000001DF System.Void RSG.Promise`1/<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x000001E0 System.Void RSG.Promise`1/<>c__DisplayClass34_0::.ctor()
// 0x000001E1 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x000001E2 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x000001E3 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x000001E4 System.Void RSG.Promise`1/<>c__DisplayClass35_0::.ctor()
// 0x000001E5 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x000001E6 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x000001E7 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x000001E8 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::.ctor()
// 0x000001E9 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x000001EA System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x000001EB System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x000001EC System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x000001ED System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x000001EE System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x000001EF System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x000001F0 System.Void RSG.Promise`1/<>c__DisplayClass43_0::.ctor()
// 0x000001F1 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x000001F2 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x000001F3 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__3()
// 0x000001F4 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x000001F5 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x000001F6 System.Void RSG.Promise`1/<>c__DisplayClass44_0::.ctor()
// 0x000001F7 System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x000001F8 System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x000001F9 System.Void RSG.Promise`1/<>c__DisplayClass45_0`1::.ctor()
// 0x000001FA RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x000001FB System.Void RSG.Promise`1/<>c__DisplayClass48_0`1::.ctor()
// 0x000001FC RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1/<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x000001FD System.Void RSG.Promise`1/<>c__DisplayClass49_0::.ctor()
// 0x000001FE RSG.IPromise RSG.Promise`1/<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x000001FF System.Void RSG.Promise`1/<>c__DisplayClass51_0::.ctor()
// 0x00000200 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000201 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x00000202 System.Void RSG.Promise`1/<>c__DisplayClass51_1::.ctor()
// 0x00000203 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x00000204 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x00000205 System.Void RSG.Promise`1/<>c__DisplayClass52_0`1::.ctor()
// 0x00000206 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x00000207 System.Void RSG.Promise`1/<>c__DisplayClass53_0::.ctor()
// 0x00000208 RSG.IPromise RSG.Promise`1/<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x00000209 System.Void RSG.Promise`1/<>c__DisplayClass55_0::.ctor()
// 0x0000020A System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x0000020B System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x0000020C System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x0000020D System.Void RSG.Promise`1/<>c__DisplayClass55_1::.ctor()
// 0x0000020E System.Void RSG.Promise`1/<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x0000020F System.Void RSG.Promise`1/<>c__DisplayClass58_0::.ctor()
// 0x00000210 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x00000211 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x00000212 PromisedT RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x00000213 System.Void RSG.Promise`1/<>c__DisplayClass59_0::.ctor()
// 0x00000214 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000215 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000216 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::.ctor()
// 0x00000217 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x00000218 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000219 System.Int32 RSG.IPromise::get_Id()
// 0x0000021A RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x0000021B System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x0000021C System.Void RSG.IPromise::Done(System.Action)
// 0x0000021D System.Void RSG.IPromise::Done()
// 0x0000021E RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x0000021F RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000220 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x00000221 RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x00000222 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000223 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000224 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x00000225 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000226 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000227 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000228 RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000229 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000022A RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x0000022B RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000022C RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000022D RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x0000022E RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000022F RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000230 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x00000231 System.Int32 RSG.IPendingPromise::get_Id()
// 0x00000232 System.Void RSG.IPendingPromise::Resolve()
// 0x00000233 System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x00000234 System.Int32 RSG.IPromiseInfo::get_Id()
// 0x00000235 System.String RSG.IPromiseInfo::get_Name()
// 0x00000236 System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m631D6AC7892A4BD309DB2DB36B71AE7617EF20AB (void);
// 0x00000237 System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_mE1653B0B40B662C17CC5CF7A734EE3F0BA7620E1 (void);
// 0x00000238 System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_mA50BA260007949DDB15BC3CEFDE69D27B1A93922 (void);
// 0x00000239 System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m60F13449991E7C2683FD8981F8740687D8A34A87 (void);
// 0x0000023A System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m39A222899B7C1A0C85802F0CD92EC415DC388527 (void);
// 0x0000023B System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_m1ECA9EDDD1F7CB200019B69456F8B85DA6EF4E82 (void);
// 0x0000023C System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m5AFDBD1A671F2EBC2C1521E9A3786657D0E1AE34 (void);
// 0x0000023D System.String RSG.Promise::get_Name()
extern void Promise_get_Name_mFA2E670F57075D333E4DDD53D981FACCBD12A496 (void);
// 0x0000023E System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m348EA57D5FEDA7710E4496EE3F36894920420B9A (void);
// 0x0000023F RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_mD0512D15E08EC733965BA299239B02AFAB772490 (void);
// 0x00000240 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_m458DB5B838CCA6E10D835E8006DC0839C06386D9 (void);
// 0x00000241 System.Void RSG.Promise::.ctor()
extern void Promise__ctor_mF3C706B6D756F3C2CF35C36889D5CB055CFFC4A8 (void);
// 0x00000242 System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_mD5DE022EE907B5AB7F870CCBA000B91835C491C4 (void);
// 0x00000243 System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_mF232BC29933B7A8D316E9F7DF310FC2F79BD0668 (void);
// 0x00000244 System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m052FF9343C09C567EA852CBB0317A5EDDD41C32B (void);
// 0x00000245 System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_mF61971AD66877D8919FDDE1E48256C7AEAD0FFA0 (void);
// 0x00000246 System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_m042CA8C9C68D98D919BE433F5BFBA8D1E26E39AD (void);
// 0x00000247 System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_m49B2D5AAEB34EFCC768224F0AA8BF2F02C040991 (void);
// 0x00000248 System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_m3A25CEBDAE49C882965AFCD67FEBF033F956C4D7 (void);
// 0x00000249 System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m83372C7CDE89BE8C915155346E4F216B87389B9E (void);
// 0x0000024A System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_mCDBAFB8325F73B3EF7F307B5D1D2CC32849BDAA7 (void);
// 0x0000024B System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m845F6E61117E5677D3EBFF04A3A2AEA73E85D200 (void);
// 0x0000024C System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m3538EC5D55D5784EBCC3E29ABB9EEA42D02F398E (void);
// 0x0000024D System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_mB126FC44C4CA670E0A40A19DFF51AEF5914BC788 (void);
// 0x0000024E System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mBBDFB4EEB1AA6DBECC97FC732840CE6194693A85 (void);
// 0x0000024F System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_mF554905157A7FCB6711F4524BEF276FEAF0442F3 (void);
// 0x00000250 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m6D4C8B13F0FDC54826EC382F834B90F3A37B1E97 (void);
// 0x00000251 System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_m64BF9B435F15E2D45D3AF60EBF78D20BB11BAE1F (void);
// 0x00000252 System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_m1D623420C2FD6C2417F4473DDB8AAB1DEFA47BE3 (void);
// 0x00000253 System.Void RSG.Promise::Done()
extern void Promise_Done_m2643251B83BDEA0EB3540BA9F558358FDB4EFF5D (void);
// 0x00000254 RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m135C00B72AFA5F951F36AFBD6D8C1ACF16A9C66C (void);
// 0x00000255 RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_m6513FEF948071ECE9FDF2402EE982472812AC636 (void);
// 0x00000256 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000257 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_mC640A94175B17F3BEB17C11B8D24902FAB69C839 (void);
// 0x00000258 RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_m29445A358CA6F7C6772419A8137663D85C5003E7 (void);
// 0x00000259 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000025A RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m5735DE787B26B5FEE2E7ACA794E1B9D4AD613F39 (void);
// 0x0000025B RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m920087B7407E07F9197BA7D5BDC8233654DFD35E (void);
// 0x0000025C RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000025D RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m8B6ABDBA30471930B8EED5DB012A17113A16650D (void);
// 0x0000025E RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_mEBD03F33E77489EC764FDAA28320F03E6B29DB78 (void);
// 0x0000025F System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m32008551CED6FFF161BCB9D17DE5A98CE1582C6F (void);
// 0x00000260 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m6D4D4DF372D317FAEF05664BC81BFC7FB4DEEA00 (void);
// 0x00000261 RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m28D1BCA29899781AEE05A340B45A57E4F379998F (void);
// 0x00000262 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000263 RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m48EEF822915D8B5A27D5A7F81EA6231003441B91 (void);
// 0x00000264 RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_m05E09E982475B18694193676BA61D06517749F0B (void);
// 0x00000265 RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_mA5DBE3AD740FE9CC7945E9C139C2AD7124AA53D3 (void);
// 0x00000266 RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_m585CD03D201AF7FC30FE7A85B9CCF58818897386 (void);
// 0x00000267 RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_mAAD31E5050811A7D864DAF60B51AAF804B3BFEF4 (void);
// 0x00000268 RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_mA71DE342E882BBB84D1E081B7E18BC2515D8F714 (void);
// 0x00000269 RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000026A RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m448804C9E2934111CBF554A64AC620C30193B84A (void);
// 0x0000026B RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_mD86AAB4E03EE3A0B816C363F56DF6705A5FFF0D5 (void);
// 0x0000026C RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m7846F8C76B74E75E7E850780F997C588EC9C1202 (void);
// 0x0000026D RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_m3F751DE74C6B3A76B91EA52FD27C6B9511B653DA (void);
// 0x0000026E RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_m4DFC5161F59CED77D92A4960B9BB3E7DB530DA3E (void);
// 0x0000026F RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_mDE80BEDE819C0E1E5D81D0671FB4E37A2A3DB3B3 (void);
// 0x00000270 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000271 RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_mA53E5CA4F1AFE6330D0216F1037176906BD7C085 (void);
// 0x00000272 System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_m89ACA988FF12BCBC7414ED335D6C3C0853A72FDC (void);
// 0x00000273 System.Void RSG.Promise::.cctor()
extern void Promise__cctor_m080830C73ADD25E51657D67032A4C2141E1F4180 (void);
// 0x00000274 System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mEDB06C835F866E6683364F5A0D367673B22EBC7C (void);
// 0x00000275 System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m35431750443DD94020F64C8D82171A5097D1FE57 (void);
// 0x00000276 System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mDDA9E97AE88980725F9713DA8C832E7FFDD58025 (void);
// 0x00000277 System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m00B92505423E6143AEA25A174D3A2871667F73AC (void);
// 0x00000278 System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m897DC60DCFF9720781FDDCF89BAF8AE80A641586 (void);
// 0x00000279 System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mB4D3508FB53C5380CF3DA70DFA540EF228F52481 (void);
// 0x0000027A System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m14342AEB656B3C6E043CB9C328024C9498A709DB (void);
// 0x0000027B System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mD181A5DD642FE3E0D38EC53325C7E45430715A5F (void);
// 0x0000027C System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mF6D43FC6CD19DE6AFF0B9656924C222BFAEA6C0E (void);
// 0x0000027D System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mB3B9D6EB885D400E307A3BB7B4148ECC9BEED195 (void);
// 0x0000027E System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m7D1C74900668244ED2925D69FFEB0353189765B1 (void);
// 0x0000027F System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m20246593163120F7F446B076B1F4EC4E6C90A2CA (void);
// 0x00000280 System.Void RSG.Promise/<>c__DisplayClass51_0`1::.ctor()
// 0x00000281 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__0()
// 0x00000282 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x00000283 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x00000284 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x00000285 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x00000286 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x00000287 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x00000288 System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mC731BB36D8ADD6B4079B3BC339BBF9E0C4EFB4F8 (void);
// 0x00000289 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m91C10E82E96D2D909CE9F075FBE73C2D7E278192 (void);
// 0x0000028A System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m0DF0E0F3E4563CE5C7427F7405ADEB03DB8A5F83 (void);
// 0x0000028B System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m7501B93112EC99EC8EE3EEE1B56A7F3C3DEFC685 (void);
// 0x0000028C System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m194278F8D66D227B7EB1FAFCF826F331113265D1 (void);
// 0x0000028D System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m2DDEA0BFAE75634CEB55BB1D746BBD330F4869C0 (void);
// 0x0000028E System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_mFACADFB9DBD1920C191A332AF7300F7E8E024ED3 (void);
// 0x0000028F System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_mF7C4944C5F8BE6513BDDCA8B0FDBB6E14DC2C5E3 (void);
// 0x00000290 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m979DC9DB8F68E5C79ABF476468906D4B87D01D9C (void);
// 0x00000291 System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_m612F1A6EDC882C7B7B04ACCEA10F78FFB99D71A7 (void);
// 0x00000292 RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m7BBD1735E2BD1AECDEA2A20CB9E29A311E048C50 (void);
// 0x00000293 System.Void RSG.Promise/<>c__DisplayClass57_0`1::.ctor()
// 0x00000294 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise/<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x00000295 System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m516408BBD1540BB479CACA4215789A61E588D810 (void);
// 0x00000296 System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m87565D3C5EA3DFEB778B00A54EF744307E7FFD01 (void);
// 0x00000297 System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_mE7882075EE24DC9055D3224D33DD3E7FE5C3649A (void);
// 0x00000298 System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m4B8FB61B59089C4DEA648A75D409E836CEE092FF (void);
// 0x00000299 System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m43EED26E0FBA8DE6083A06EF6B646BF78872E14D (void);
// 0x0000029A System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m9B45224464BC3BFF1C41548F8B2A37BD35A8F29C (void);
// 0x0000029B System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m2E7C2B7DDFE3EEB3871DF5303188E2430F55AA74 (void);
// 0x0000029C RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mB394CFDCCB95FD085A8736AD8E482A50F44D4A49 (void);
// 0x0000029D System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m9CE32458E8BABAD882B5CA483D36D5EB2994C570 (void);
// 0x0000029E RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m7D3A4FFC66539686B3257F32DF09A8EFC93D7AED (void);
// 0x0000029F System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mB3DB8505EF43580EE4356A320BF4FB3842CFFA50 (void);
// 0x000002A0 System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_mF6802236B37785089ED7E9A3F5DA5558ED85951D (void);
// 0x000002A1 RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m9F213538E12F1333EAADF3690503584E98B82237 (void);
// 0x000002A2 System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m25D42314D26736A13DD29D1331B0BA160532D3AA (void);
// 0x000002A3 System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_m05206B1BC0E0943BD3F41312AA406BF6190D2521 (void);
// 0x000002A4 RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m57BC5E3BB67D69BCA61F5E8BF4AC755B38295222 (void);
// 0x000002A5 System.Void RSG.Promise/<>c__DisplayClass64_0`1::.ctor()
// 0x000002A6 RSG.IPromise`1<ConvertedT> RSG.Promise/<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x000002A7 System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m0532938FA47506CFA8732885E283AD9F21C72ADD (void);
// 0x000002A8 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m930B260B50CC2D259DD4E73FC7EF574B6ED6BEA0 (void);
// 0x000002A9 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m4EC2810EE4C20AE7FB3AF79489717ADE34ED665A (void);
// 0x000002AA System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mC808C98C3D85BA35CB856A3E3739FB79AA5D4D2F (void);
// 0x000002AB System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m9DBFC26D5CBB1FBB640770336F18C142C8E93405 (void);
// 0x000002AC System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_m25CC0AF8BFAC0DDB555ECDEA6F7B4068CF1052EE (void);
// 0x000002AD System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m78E8AD1705A8C3F3CA12430C2E97120BF28F5C36 (void);
// 0x000002AE System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mB98B7DDB295AFE5A93D21C9EA74C15A70B620BD4 (void);
// 0x000002AF System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mA1F009415ADC80C8DF29CADD62E6CE6E861BCDE2 (void);
// 0x000002B0 System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m2335F4081798B9CA126758D5B38D58578ACD02B4 (void);
// 0x000002B1 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m5ED5B955EBC2657600378AEA3741D2096B294467 (void);
// 0x000002B2 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m7C6285AA36261DCEE12D834674167AF1F8418D44 (void);
// 0x000002B3 System.Void RSG.Promise/<>c__DisplayClass71_0`1::.ctor()
// 0x000002B4 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x000002B5 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x000002B6 System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_mDF49A69B00B47D8E30C59A1EFE8CDCCCED2175BD (void);
// 0x000002B7 System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_m327B4108B2935F10E2B77A95E7B6D3CD7574DA63 (void);
// 0x000002B8 System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_mB2C16AE66329DF694119F386C897F586F72F96F1 (void);
// 0x000002B9 System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m5AF42A933A440D43FE8A735DAAEFC5B59D439A25 (void);
// 0x000002BA System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mF4411B73A303831214435467F21ABB529A5C3CA9 (void);
// 0x000002BB System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m6D6DEBEE15613C770037CF803694BFEA0167B311 (void);
// 0x000002BC System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_m35DC167FD43166522E5A0077DD1D0BE53F2F9744 (void);
// 0x000002BD UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_mC1F2A506707D8B14B961E2373324474282877873 (void);
// 0x000002BE Proyecto26.RequestException Proyecto26.HttpBase::CreateException(Proyecto26.RequestHelper,UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_mED2910E823C82872433781C9A0C666EC38A4F727 (void);
// 0x000002BF System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_mE7CC8B1218860CF7322DEBAA4C9B1FD788B19E6D (void);
// 0x000002C0 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_mDCDE1A9E1732408B0988222C9F5B729B99DEC210 (void);
// 0x000002C1 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x000002C2 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x000002C3 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__1__ctor_mB1EB168AD8F16B094B7B642D236ED8CFD4B21D93 (void);
// 0x000002C4 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__1_System_IDisposable_Dispose_m5EDA8599DA335CB6537EC9926059B2B1F2E61396 (void);
// 0x000002C5 System.Boolean Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__1_MoveNext_m339EB2C40A99750F58696026014E81E2A1926A40 (void);
// 0x000002C6 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__1_U3CU3Em__Finally1_m67AA93F9FE785A41D82008F53D548A3484C6C0B3 (void);
// 0x000002C7 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97E66B8BAD956883BCC8131DF9A54557FADB15AF (void);
// 0x000002C8 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__1_System_Collections_IEnumerator_Reset_m13FA4CC32B13471431FC86EC0FB1FCA2E8BD6AA4 (void);
// 0x000002C9 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__1_System_Collections_IEnumerator_get_Current_m46D17F003FC5CD9211994B23675A58EB54AB3F1F (void);
// 0x000002CA System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::.ctor()
// 0x000002CB System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x000002CC System.Void Proyecto26.HttpBase/<>c__DisplayClass7_0`1::.ctor()
// 0x000002CD System.Void Proyecto26.HttpBase/<>c__DisplayClass7_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x000002CE T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x000002CF T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x000002D0 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x000002D1 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x000002D2 System.Void Proyecto26.JsonHelper/Wrapper`1::.ctor()
// 0x000002D3 Proyecto26.RequestHelper Proyecto26.RequestException::get_Request()
extern void RequestException_get_Request_m3E619CA5E6DF922C28E73E0B52AFA0A42DD527A1 (void);
// 0x000002D4 System.Void Proyecto26.RequestException::set_Request(Proyecto26.RequestHelper)
extern void RequestException_set_Request_m4A39C732359927451AF59D654313E73727FAA113 (void);
// 0x000002D5 System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_m2FBBBB7756A22FF573189DCD20590F773436AF38 (void);
// 0x000002D6 System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_m0BAA2B0BE9794C73D99DE699E643BF53E9074A9B (void);
// 0x000002D7 System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_m1E99469DB868249A682360D6812AF364F69B43B8 (void);
// 0x000002D8 System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_mD03F7650C1DEF980ED2EAD76E91D660D34654995 (void);
// 0x000002D9 System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m095758F8685082FDE8835A82E94BF4EE1E9D391F (void);
// 0x000002DA System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mC6924445A9BFA983A0C44D9F90D029760CEA2542 (void);
// 0x000002DB System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_m495637BFD1FA10496F20C6AD90931AB70BE9DDE5 (void);
// 0x000002DC System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m046F5AF5B54CF7BB2A627F056133571B7964F933 (void);
// 0x000002DD System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_m7AA3200ADA93B624DCC0768D324204F5D7171D92 (void);
// 0x000002DE System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_m3650ABABE07676D1D97D845AA42F681113549F04 (void);
// 0x000002DF System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_mC77265431C17D4AC0450CEA340469690794E9F2C (void);
// 0x000002E0 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m56382201F8D03DC7AF75DC54776681CEAC3639BA (void);
// 0x000002E1 System.Void Proyecto26.RequestException::.ctor(Proyecto26.RequestHelper,System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_mEE6C35547B7E1F32B9E8A4A1270323E36ECDE869 (void);
// 0x000002E2 System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_m4BBB86E86D0B704C7EE5DBE2D36EC7D1D925D245 (void);
// 0x000002E3 System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_m95F799EA43CDE070FB94050EB32739594DA824CE (void);
// 0x000002E4 System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_mD86FE6C056CAA5BFA4C768D5CFD6B1AC2D924719 (void);
// 0x000002E5 System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_m6112760F386FB3160A999EE7E505E7B7B78CFAD5 (void);
// 0x000002E6 System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_m727A2AEE626E7FC01DF0A978441C001EB34EBCBB (void);
// 0x000002E7 System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m2CCEC8C91A32A69CA7B68E9906326DDA156CE4C8 (void);
// 0x000002E8 System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m9F94287AC2B538F2A7DAEC148696D217EFBF8E53 (void);
// 0x000002E9 System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_m49A6D60FDBDACE28CFD43E4EDFB3A67CC3B681EB (void);
// 0x000002EA System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m819005CD0C1E7634FCF67CA97B1F8D5D06A1EAFD (void);
// 0x000002EB System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_mF989309E4B72DC5FB7AA0B0918F902CDCFC6C56D (void);
// 0x000002EC System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_m2B636E856FDDFD91989839B407A414D9932312F0 (void);
// 0x000002ED System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m9F4273E021F533CE48552400B15F603D8E4C50CE (void);
// 0x000002EE System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_m1DE79D1DB56E8D9E796B0E9E60ED993AB94715AB (void);
// 0x000002EF System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_m2ECE34056F84BBE96CD18F18A7B1CFD03942A66E (void);
// 0x000002F0 System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_m9BF4A1F6FA0020CB3CCF729FE41650C13769E83E (void);
// 0x000002F1 System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m890B434A994EFEABB0CD0680B8E0377C5B5B5318 (void);
// 0x000002F2 System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_m234E28296D60688F538185FD3E11466B67668EB7 (void);
// 0x000002F3 System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_m17A1172B216D0985B7D63832F54C0B8AF37A2090 (void);
// 0x000002F4 System.Boolean Proyecto26.RequestHelper::get_RetryCallbackOnlyOnNetworkErrors()
extern void RequestHelper_get_RetryCallbackOnlyOnNetworkErrors_m345837ED1A7C7DA8C84C363522418B704965423A (void);
// 0x000002F5 System.Void Proyecto26.RequestHelper::set_RetryCallbackOnlyOnNetworkErrors(System.Boolean)
extern void RequestHelper_set_RetryCallbackOnlyOnNetworkErrors_mDEF28C9504B4F402EB0ECCBC74DA6E568997D85A (void);
// 0x000002F6 System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_m8958FBD9B8E6A0852AFC98537FF1A2AB40273461 (void);
// 0x000002F7 System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_mB09B3A1DED93C0565A309D708FE77A84085F89DD (void);
// 0x000002F8 System.Action`1<System.Single> Proyecto26.RequestHelper::get_ProgressCallback()
extern void RequestHelper_get_ProgressCallback_m914F4F08FFB614CF83D64A5FF4F0164539A11CB2 (void);
// 0x000002F9 System.Void Proyecto26.RequestHelper::set_ProgressCallback(System.Action`1<System.Single>)
extern void RequestHelper_set_ProgressCallback_m58CF4252FDE248CE2F7862B345B6C55F002B50C3 (void);
// 0x000002FA System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_mDB14F675CDD96C48A77B3E1322020207030547C1 (void);
// 0x000002FB System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_mCB357C37BA62BF8103C6508A6B1743F2D83A9C3C (void);
// 0x000002FC System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_m3637AB717925A6979C5B34080D032E2FFDC5FD2F (void);
// 0x000002FD System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_mB4AF296D392D92304FA264EED61600095C9E73EE (void);
// 0x000002FE System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_m12FA845010695EEE4A09DD9A6F76DE8471A862D4 (void);
// 0x000002FF System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_m86B5BBC876F738C1BE292CD062077D24C00AEA9F (void);
// 0x00000300 System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m62F4E6F0A29DA302D61746DD6AAF49EB657ACF50 (void);
// 0x00000301 System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_mE9395D343CF98123B065E9ACF06C7FA1100AC075 (void);
// 0x00000302 UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_mFCABF1F3476D7580A9AAB63C6B010C1114DD6947 (void);
// 0x00000303 System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_m548CAB9872FFD2A8027994B8D95506B79F06115B (void);
// 0x00000304 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_m15B81EB567521859E32A255F893E86F798C8B164 (void);
// 0x00000305 System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_m9FF72C01F325AF2A39944732B2F7B5D5B8B8C7B1 (void);
// 0x00000306 System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_m43721DC6C2EBCC4BB13FAE074739AD26C8583F97 (void);
// 0x00000307 System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_mD874EEC922A94AC08D0FAC879652033B54C9424D (void);
// 0x00000308 UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_m321AC0450D35604BA128C3288D044FE6884ED265 (void);
// 0x00000309 System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m2D81B9BEECF8BFA4F18047F08EE7C256E1B21117 (void);
// 0x0000030A UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_m67989620874FA461E24832C70D639EC15147AE3F (void);
// 0x0000030B System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mCFF33186F8DCA2178A4AA54EC97B6823DB6350A4 (void);
// 0x0000030C UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m576BE2E2C2E06E5B1831D089223FDD2C399D08E4 (void);
// 0x0000030D System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_mF3F1FAFD4D003B5F5718979222F871F32DC636C6 (void);
// 0x0000030E System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_mF06E05AD5EE832BFE56704B7A546B0C7E25D4B79 (void);
// 0x0000030F System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_mC80E0F973C211BD73B04AECB46205766BA9F6409 (void);
// 0x00000310 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Params()
extern void RequestHelper_get_Params_m52A62A6900B4D659B401879E1B9AAE4A7E6D1123 (void);
// 0x00000311 System.Void Proyecto26.RequestHelper::set_Params(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Params_m3F4126B55624310028A0C42D490E10F54A4750AF (void);
// 0x00000312 System.Boolean Proyecto26.RequestHelper::get_ParseResponseBody()
extern void RequestHelper_get_ParseResponseBody_m87AE3F014C05690D1CC4C6D5204DCA44D386D591 (void);
// 0x00000313 System.Void Proyecto26.RequestHelper::set_ParseResponseBody(System.Boolean)
extern void RequestHelper_set_ParseResponseBody_m021500316923EA402EDE2E525753AA265389C14E (void);
// 0x00000314 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_m6E6296880FD1879EAEDE88034709C17D68F0643E (void);
// 0x00000315 System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_mA1C3D38E6CBDABDCF58527D9A28070653A1C1979 (void);
// 0x00000316 System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m981820F2EEF397803961FC90BBBD36B6CC6D574A (void);
// 0x00000317 System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m9E7B04CE766C97D243CEDE9DB42E4CEA35A8D6F7 (void);
// 0x00000318 System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_m5158223FDF91676452FA27A330E661EE8C9D9506 (void);
// 0x00000319 System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_m7D8E2CC4748631F399B91549A2E7A597E2316841 (void);
// 0x0000031A System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_m522CB3C8DEAA6725CC66F293E84BF09B7DB26683 (void);
// 0x0000031B System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_m5B21E1C18BD58CCDE57D63D2BD61537C40735129 (void);
// 0x0000031C System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_m94327332571DC63115E2C45BC4BF07D15C8F3694 (void);
// 0x0000031D System.Boolean Proyecto26.RequestHelper::get_DefaultContentType()
extern void RequestHelper_get_DefaultContentType_m91687E0E1878F17A2047E3E7741EF6CA42A1458B (void);
// 0x0000031E System.Void Proyecto26.RequestHelper::set_DefaultContentType(System.Boolean)
extern void RequestHelper_set_DefaultContentType_m58E76D9215877D62BA0BDE96356D961D70184203 (void);
// 0x0000031F System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_m8A66AFBDDFC59B629764F7242D284DA1D71503EF (void);
// 0x00000320 System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_m7C53F61C41E1B440A83BB8761275696AF36AF7DC (void);
// 0x00000321 UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_m7BC4EF21DC9F461CDFCBC86DCF0AA8F2C06D50B0 (void);
// 0x00000322 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_m9734C03F625D17D16F2BD1696ADED283AE295194 (void);
// 0x00000323 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m3CCCE641244AC12357CC70E8C05AA39285A9C9A0 (void);
// 0x00000324 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_m217BF73AEA4E2259D9A19D797E9C4CCFB3819633 (void);
// 0x00000325 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_m27FFFFF8A67EC8C3F7793072A4D094CEA7C09336 (void);
// 0x00000326 System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_m4529907A41F3E760A73564C9C189AC97F320F822 (void);
// 0x00000327 System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mBD25C2495C9336770708687BDB2A08B8AB09E7C9 (void);
// 0x00000328 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_m2A5A136C757A50B353C80E0323FAF95D5970A4E5 (void);
// 0x00000329 System.String Proyecto26.ResponseHelper::GetHeader(System.String)
extern void ResponseHelper_GetHeader_m479626D4E493E1CEC980E312D85BE27A20AA3501 (void);
// 0x0000032A System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_m5DDBBE98399DDA4D2B534767C539573FAE8F33EC (void);
// 0x0000032B Proyecto26.StaticCoroutine/CoroutineHolder Proyecto26.StaticCoroutine::get_Runner()
extern void StaticCoroutine_get_Runner_m99CB27E232AC6F00A6EF64A2924391E2CDC8A8B3 (void);
// 0x0000032C UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_m96DBFAB1D1218585286F517D000CD8E57D81050D (void);
// 0x0000032D System.Void Proyecto26.StaticCoroutine/CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_m44080B42ED079EFD8E60751B760C0210006C9EEC (void);
// 0x0000032E System.Version Proyecto26.RestClient::get_Version()
extern void RestClient_get_Version_mE00E27D9A87081E91634BCA3DFFB399BF2B83720 (void);
// 0x0000032F System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestParams()
extern void RestClient_get_DefaultRequestParams_m3C946CC3E2AAE867138DEB40AB422D73DF735651 (void);
// 0x00000330 System.Void Proyecto26.RestClient::set_DefaultRequestParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestParams_mCA379D4AC4714C731395D2ED049F2218636F5412 (void);
// 0x00000331 System.Void Proyecto26.RestClient::ClearDefaultParams()
extern void RestClient_ClearDefaultParams_mD98BAF7050FE16E46B53D1682742A20187EFF054 (void);
// 0x00000332 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_mD15628D2B50489DCA34C24AFB5918B1275D3BD06 (void);
// 0x00000333 System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_m9193F5869FA022E88210D5F12372C28D0EBE3586 (void);
// 0x00000334 System.Void Proyecto26.RestClient::ClearDefaultHeaders()
extern void RestClient_ClearDefaultHeaders_m372635168791F3CA28B5197913E70705BC754E5E (void);
// 0x00000335 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_mDBEBA7EE6F1986FABA1487374F8CC56556457E6D (void);
// 0x00000336 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000337 System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m35A190076C4706104A3C159C898E59B427DBA1A2 (void);
// 0x00000338 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m36C3A32A743F62ADAB89F5193065ECD989D45683 (void);
// 0x00000339 System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000033A System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000033B System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x0000033C System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x0000033D System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m83D2FCE9E88982247122AE568559F5AE97D34F26 (void);
// 0x0000033E System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m801E218330870755BA217ED52C44A6B1FECCCE71 (void);
// 0x0000033F System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m868B1F256565734EF2E771C3A4D17A8BB4FFD583 (void);
// 0x00000340 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000341 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000342 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000343 System.Void Proyecto26.RestClient::PostArray(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000344 System.Void Proyecto26.RestClient::PostArray(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000345 System.Void Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000346 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_mE378F5382982882EAE68147E1025173BBEECE6E6 (void);
// 0x00000347 System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_mD062595425F1307D4A2FE2D405B94E51F69DA243 (void);
// 0x00000348 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m6BCCA98E28999F5A658FDEF9DFCEC6A08EB9FFC5 (void);
// 0x00000349 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000034A System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000034B System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000034C System.Void Proyecto26.RestClient::Patch(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Patch_mFE4C7BC8297762069561515E3166376B1B04BF16 (void);
// 0x0000034D System.Void Proyecto26.RestClient::Patch(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Patch_mC74278923877E14BB7DBAAD12183B84EB07718EC (void);
// 0x0000034E System.Void Proyecto26.RestClient::Patch(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Patch_mDCA173A6B4AC83F2C40CAB7668255CC13C5463C4 (void);
// 0x0000034F System.Void Proyecto26.RestClient::Patch(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000350 System.Void Proyecto26.RestClient::Patch(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000351 System.Void Proyecto26.RestClient::Patch(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000352 System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_mADE13524F212CC2FF5DC65C218EE6108222BD3C7 (void);
// 0x00000353 System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_mF86A2079CDF0A4972258FC8FAE75FE97CFD0FA26 (void);
// 0x00000354 System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m563C993B2CEB8166279A8A5842AEF64E88FC7018 (void);
// 0x00000355 System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m596A590AD91B3D91A2554702C84AFD40DAA466B3 (void);
// 0x00000356 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_mBBC58641FED9656F4B03DA443705F7752954A05F (void);
// 0x00000357 RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x00000358 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_mDD3EB818C782291807A153842D1D9612F95CEAC4 (void);
// 0x00000359 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_m21CD13976DB446B991932616AC1093045D510580 (void);
// 0x0000035A RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x0000035B RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x0000035C RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x0000035D RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x0000035E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_m623AE9FBAB3576D548E9E5397F1CDC9CE198978C (void);
// 0x0000035F RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_m007A792E1C5ED725DFFF980D277B60293B8072E7 (void);
// 0x00000360 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_m161D873EA6D79B3F86C5A5357B0610E7BEDDA7F5 (void);
// 0x00000361 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x00000362 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x00000363 RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x00000364 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.Object)
// 0x00000365 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.String)
// 0x00000366 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper)
// 0x00000367 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_mB7D8FD402D48C7C69B4503F031185596A8C343EB (void);
// 0x00000368 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m2C1AB7B6B4E66B34A6B17B72EE86158595A24195 (void);
// 0x00000369 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_m4B4BBC4DC8031BD79359BD4E1DC7FB421DB20996 (void);
// 0x0000036A RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x0000036B RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x0000036C RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x0000036D RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Patch(System.String,System.Object)
extern void RestClient_Patch_mE3283A5E7DBBD115E1013A2F78CD4BDF44888958 (void);
// 0x0000036E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Patch(System.String,System.String)
extern void RestClient_Patch_m2723DD0A18C09E7650CDA02626018F5F64CF94AB (void);
// 0x0000036F RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Patch(Proyecto26.RequestHelper)
extern void RestClient_Patch_m69B97FB06DE7A0F9A7A952F6AABDC1C94D56BC2D (void);
// 0x00000370 RSG.IPromise`1<T> Proyecto26.RestClient::Patch(System.String,System.Object)
// 0x00000371 RSG.IPromise`1<T> Proyecto26.RestClient::Patch(System.String,System.String)
// 0x00000372 RSG.IPromise`1<T> Proyecto26.RestClient::Patch(Proyecto26.RequestHelper)
// 0x00000373 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m3CB4F4FBEC461A9D21A4A423BD9763FDD0F6AAC1 (void);
// 0x00000374 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m2FCE9AF1698612DA9F23F87C30AE30D556604B5F (void);
// 0x00000375 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_m371CDEA0CB9C5DC3A4E671A3274D40B276C7DF03 (void);
// 0x00000376 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_m2458809288DB81B6B90DB21F6C53D4DDA091AB5C (void);
// 0x00000377 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x00000378 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x00000379 Proyecto26.Helper.ExecuteOnMainThread Proyecto26.Helper.ExecuteOnMainThread::get_Instance()
extern void ExecuteOnMainThread_get_Instance_m57A7DD0347FA71E927B3FA57457DEE07BFC368E9 (void);
// 0x0000037A System.Void Proyecto26.Helper.ExecuteOnMainThread::Awake()
extern void ExecuteOnMainThread_Awake_m96AA2D23BE882B821722FB53B9B5ACA35DF3B0F2 (void);
// 0x0000037B System.Void Proyecto26.Helper.ExecuteOnMainThread::.ctor()
extern void ExecuteOnMainThread__ctor_m9396E93259FED5C79003E08CC1D95F3C4C9D1814 (void);
// 0x0000037C System.String Proyecto26.Common.Common::GetFormSectionsContentType(System.Byte[]&,Proyecto26.RequestHelper)
extern void Common_GetFormSectionsContentType_mE07A67FFE9476D2887EF3A914C9F907CEE354F6D (void);
// 0x0000037D System.Void Proyecto26.Common.Common::ConfigureWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,System.Byte[],System.String,Proyecto26.RequestHelper)
extern void Common_ConfigureWebRequestWithOptions_mAEC964E81E8520EF8FBA784E20845EB4DDDCDC89 (void);
// 0x0000037E UnityEngine.AsyncOperation Proyecto26.Common.Common::SendWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Common_SendWebRequestWithOptions_m83473BAB8B86A9A6D2207AC212560D1AE61FFD44 (void);
// 0x0000037F Proyecto26.ResponseHelper Proyecto26.Common.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_m60EE5304206BA3F871F798B9B67FD0360E3BD8C6 (void);
// 0x00000380 System.Boolean Proyecto26.Common.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_m0830F9B95E46476522CF1338B3B94499BEA7BE81 (void);
// 0x00000381 System.String Proyecto26.Common.Extensions::EscapeURL(System.String)
extern void Extensions_EscapeURL_m58CE4576E50E439B8070117EC1581EC3A160A188 (void);
// 0x00000382 System.String Proyecto26.Common.Extensions::BuildUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Extensions_BuildUrl_m1D73D79077F26F2C0A0737CA287DEA5A30D855AA (void);
// 0x00000383 System.Void Proyecto26.Common.Extensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m39F8F1DFCE5887655F9E6168820F86EA4D3DB29F (void);
// 0x00000384 System.Boolean Proyecto26.Common.Extensions/<>c__DisplayClass3_0::<BuildUrl>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_m85934B7498E0B2549B7CD1ADA69103F9A50C236F (void);
// 0x00000385 System.Void Proyecto26.Common.Extensions/<>c::.cctor()
extern void U3CU3Ec__cctor_m1F7DA2D6F4C08CD2ABB695DA98B2831C889BA942 (void);
// 0x00000386 System.Void Proyecto26.Common.Extensions/<>c::.ctor()
extern void U3CU3Ec__ctor_mAB9972DF599999D1767BC5FA12A2C547E525FF79 (void);
// 0x00000387 System.String Proyecto26.Common.Extensions/<>c::<BuildUrl>b__3_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CBuildUrlU3Eb__3_1_mF7A7B262A28BC96810538E15E76941C13C1ED08F (void);
// 0x00000388 System.String Models.Photo::ToString()
extern void Photo_ToString_mD6A22636275A6B35FAC2E3CB8574231785438D8C (void);
// 0x00000389 System.Void Models.Photo::.ctor()
extern void Photo__ctor_mC6E90DF93DF6EE443FE41F931F45C95EB31C0A31 (void);
// 0x0000038A System.String Models.Post::ToString()
extern void Post_ToString_m29DD92DEF3102ED3D3B8A6342DF7E6B681DC8AC5 (void);
// 0x0000038B System.Void Models.Post::.ctor()
extern void Post__ctor_mA6934B0F1762428A4C4A9B709EFA7DADDDD7B5D5 (void);
// 0x0000038C System.String Models.Todo::ToString()
extern void Todo_ToString_m2717C1F1DF64663621435EF9C35CE95C8C7F51B9 (void);
// 0x0000038D System.Void Models.Todo::.ctor()
extern void Todo__ctor_mBFA41A1DC43949DAAA32DDE4D705863D4F469C13 (void);
// 0x0000038E System.String Models.User::ToString()
extern void User_ToString_m4DEC5DBD2A99CE340A211F2C66BA6D86D01C12CF (void);
// 0x0000038F System.Void Models.User::.ctor()
extern void User__ctor_m7F7F3F420853C6F4A54B9270C6DD870EB878C993 (void);
static Il2CppMethodPointer s_methodPointers[911] = 
{
	AnchorCreator_get_AnchorPrefab_mBBAF7805E9D9F8D79408EE642D153BC76306B25A,
	AnchorCreator_set_AnchorPrefab_m53CC9CC3022C713826B44536B48B462C8A1FACFF,
	AnchorCreator_RemoveAllAnchors_m6BA302DD9EECA47969FD3F6E5397441DCB09ED21,
	AnchorCreator_Awake_m8820A3F157354D7EDED7B2D7CE2BFA42844F1F8E,
	AnchorCreator_Update_mA0A8BCCBAB0AE50DB087524E6273D11F1D6456D0,
	AnchorCreator__ctor_m702B01425680D7FD1C9272A423AAF8A913E5476E,
	AnchorCreator__cctor_mF5AE519F086E52A01EA091115062D84D9F094F34,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m14D3A8BE3E9A745E6FD525B19ADDC892B8399B4D,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD616A09A3B426EA5DE1FA37334DD194E43EEC110,
	ARFeatheredPlaneMeshVisualizer_Awake_mC5DB0414A2514BF4851266C25141C903F0AC57BA,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m8781C85CFED871C8A81A5B88DB1031856E0FC9F0,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m2343B05B1A8F14BAD4DD516C584281B66FE6A4E8,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mB3D9BBD14EA1FE3ECDBACC2DB89C1B110B8B6B5F,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mF756D3C1F7925A69CD8C7C8CCE56209AB321FEF5,
	ARFeatheredPlaneMeshVisualizer__ctor_m9A77651BCAE58AA0B994FFF6C6B63B1CFF2729F6,
	ARFeatheredPlaneMeshVisualizer__cctor_mF4BA6DDB611A3FD966C8B9494AE6B3EB8647CEAD,
	ImageRecogniation_Awake_mB0269DF668FD0F078E016FAD515885074723E23C,
	ImageRecogniation_OnEnable_m63FFBA6793ADADDFF7AACF5F5A4EC3D8BA847D06,
	ImageRecogniation_OnDisable_mA3713DAA6303E01A7FDB2AF1E694DD95996103D1,
	ImageRecogniation_OnImageChange_m7EC2C8145E66AA6F63595322096E965AE694FD6E,
	ImageRecogniation_Start_m87E02686A714B70AF7861FF453FF3E013161198C,
	ImageRecogniation_Update_mC603C86AA7168C62C5FC924FB75124200E3F98E2,
	ImageRecogniation__ctor_mAC16FAE6FE84689C7FCECAE685493C5537F5371F,
	RecogniationMultiImage_Awake_m50D762D252CF11E2B9F816BFAE5BD0D3DFB251A1,
	RecogniationMultiImage_OnEnable_m602457113F07D4311256B10BC093409AEFAC5E1C,
	RecogniationMultiImage_OnDisable_m5A68756429F0DC48127305484A76545B63F9C034,
	RecogniationMultiImage_ImageChange_mE2D7FF27C05DF4BAB5606EED461801F8770A880D,
	RecogniationMultiImage_UpdateImage_mB905BF84425394756587219742A7C355635640A6,
	RecogniationMultiImage__ctor_m37142BAC3CC05A08EC2C71FA9593904473264DE4,
	MainScript_LogMessage_m91BF7567C53593AFD0358BB3EA724F8D0C0DD8A2,
	MainScript_Get_m661CD9D8F56F2D75B4667F3A65540388C423108D,
	MainScript_Post_mB79191CF05B82594D5B7A862A8EA5E20424BFBA8,
	MainScript_Put_m0635AC6846FC16BB0099BAA2FF4A11DAF2814083,
	MainScript_Delete_m74CD4E75650A5858532A58E90A22FEB1227F151E,
	MainScript_AbortRequest_mA98E7D9BBF1C175455B423430DB5C71FB67D8A1A,
	MainScript_DownloadFile_mBBFA99D95E3E778AE410029D328A4891197ADF6B,
	MainScript__ctor_m815FA088F921DB1343C256EA721C073FADB5D713,
	MainScript_U3CPostU3Eb__4_0_m5CC5AE9AF5CD587FD301B42E09049EF8A253337F,
	MainScript_U3CPostU3Eb__4_1_m2F61C9F5A0B2D97B258B884C4A1B54B72538D968,
	MainScript_U3CPutU3Eb__5_0_m1BF18A7090ADB303E08E4C48EE4A6A34C0F18210,
	MainScript_U3CDeleteU3Eb__6_0_mE673244BD58CDAD546C09353881D692320ABDBD4,
	MainScript_U3CDownloadFileU3Eb__8_0_mA61E7597BE5C84115D06D54EAB62AA4181EFC42E,
	MainScript_U3CDownloadFileU3Eb__8_1_m09ECF502288EF0FC9F10DE661017318AD287F5E1,
	U3CU3Ec__DisplayClass3_0__ctor_m09129313A9B160F21FAD9BC073200549EE68FB04,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m60D54003CE39A2F4CA0424D27878ECF1D99C0180,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_m129236EAD89BCDF394C44F28D4ECCFC10761E0B5,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mFBEE16C4C04D41290B63A23EEFF5BCA71BCEA6C6,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_mE6B7237074F01FCC65E6EDF84D8FD41AB91EBF8E,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m802E0FE01483720E19629583659C8FC03112286F,
	U3CU3Ec__cctor_m2425F6CE9904CE8CFEFE96F253D878AB4EA7E4F3,
	U3CU3Ec__ctor_mAFA5E702511BDFE7294BCCF5713C6DF2D5CA74EB,
	U3CU3Ec_U3CPutU3Eb__5_1_m99752A12CB0D3B95CEB0B34F6724923370F00450,
	ReadInput_ReadStringInputName_mF703ADA03B30A3115E608F8B0D19DA55B2E8BB4A,
	ReadInput_ReadStringInputEmail_mAA380240AED021929F049A54326CED07EF1E7141,
	ReadInput_UploadUser_m3E02DD2E6E2D51F5E67C3B0C8698B6A44090B8FC,
	ReadInput__ctor_mA0DFAAC66A5A241CBE1A198C3C430775540B828E,
	U3CU3Ec__cctor_m2EA29933503826CB7030B0EFCA9CAB5D124FBF45,
	U3CU3Ec__ctor_m6F0D3B950AB4B7F9FF21614EE38442FF8E2E82A6,
	U3CU3Ec_U3CUploadUserU3Eb__5_0_mF474DEF828A0635268F89F936AA9CD3789AB7B0D,
	U3CU3Ec_U3CUploadUserU3Eb__5_1_m00A9087CCA2B186927398F3794CF232593739F29,
	ToggleChange_Start_mB50D6EA26D38D03CF715FAD07925A3F99CAA159D,
	ToggleChange_ChangeColor_mDEC392A30E5FEE24818952C586B9AFEB4622E3C1,
	ToggleChange__ctor_mED2E986F18942B94F28B438500AAD34072704039,
	U3CU3Ec__DisplayClass5_0__ctor_m863305539EE289B3009B8AD4D4AFA1C916E348BE,
	U3CU3Ec__DisplayClass5_0_U3CChangeColorU3Eb__0_m430832C8610693DCCB09442D41C30F9018A664DF,
	ToolTip_Update_m628B4F8BF8F7A4EA6901421D1E4BEA2A91A0B6D5,
	ToolTip__ctor_mC3F7C85CFD5491C4CBC41A3D102A0F2917A2AE01,
	UIElementDragger_Update_m587434C83391B288CCD8F41C59DAD6B41414730C,
	UIElementDragger_GetObjectUnderMouse_m0FB27B48409EAE19F2D506980CAC8B97B357E418,
	UIElementDragger_GetDraggableTransformUnderMouse_m9814FED1C8BE991F8FE1822C74952642370C8A67,
	UIElementDragger__ctor_mB04A805B65F7AF39BA1B4A903EF27073AB471C8A,
	UIManagerTech_MoveToFront_mEE23FCC6DE9F01151C7CEBC7D10F3B17B30A575B,
	UIManagerTech_Start_m5A0F82FD0BC1FC656CA43EEE205139EE160E1991,
	UIManagerTech_IncreaseIndex_m0E93CD1CCB292B534BA608B3B0637E2D97476056,
	UIManagerTech_DecreaseIndex_m0AD3FFA916DF3E561AABC52D00171903E9F4644B,
	UIManagerTech_SetTint_m59863BD69914E5546B2FB80B5D2362153EA3AD54,
	UIManagerTech_Update_mFA0D4BB5230AC61BB54EA3DA110294C9D3B1B829,
	UIManagerTech_MessageDisplayDatabase_m1F9A477A37CC52AF2F087E543428620DD55B8299,
	UIManagerTech_MessageDisplay_m2FE0649475E11298C5705D14D722472ADA9F119C,
	UIManagerTech_UIScaler_mE46A87763B58705F4A7E9FA0B0DC538D61036223,
	UIManagerTech_CheckSettings_m98EA092B7A6C84953E2DF236968602CC46B76ACD,
	UIManagerTech_ResToString_mB84898AFE4127CBC16B389CA63AB67B52E42AEE8,
	UIManagerTech_AudioSlider_mC4C7E0002E6CB17D308682095CA71ACCC4041B4E,
	UIManagerTech_Quit_m21FA5F4FCCD0414F620CEDCDE1B833F36134E0F9,
	UIManagerTech_QualityChange_m241D90D950C12B6C5E6D721B655197A44086ECA3,
	UIManagerTech_LoadNewLevel_mF14996D103A88A75D3BC8353335BF6ECA0AD6BCA,
	UIManagerTech_LoadSavedLevel_m0965D837D58421A0B2FAE4778F9C2430FFA3AA92,
	UIManagerTech_LoadAsynchronously_m4E2F0C72836E5A7E6C981763CF8C990017CA0615,
	UIManagerTech_UpdateAccountValues_m29ADF42692DDE287B54985A0F5E3352250786CE5,
	UIManagerTech_ConfirmNewAccount_m23071D4B32696D2DF65B3DD6BB3FCE20F21F1418,
	UIManagerTech_LoginButton_mC03DC4E4DAED07FF65323479C835B2F90FF70809,
	UIManagerTech_ConfirmDeleteAccount_m3664DC9B1FCE822301E79BB8B8AA69E31E962FFB,
	UIManagerTech__ctor_m6D9895B068C3DACE44F72B70FDDE0FDD55F7C9BF,
	UIManagerTech_U3CStartU3Eb__79_0_m2E693E6640086FCE3031F40FD1FB1FCEF38BCF51,
	U3CMessageDisplayU3Ed__85__ctor_mE07CFD5E22109645739388DB3BC61269F5641371,
	U3CMessageDisplayU3Ed__85_System_IDisposable_Dispose_m08CC2163590D5A388FAC20B7EC1F5D77FB04ED5A,
	U3CMessageDisplayU3Ed__85_MoveNext_m4CADA2F4A735DADB555D4BA190C586BB918123EF,
	U3CMessageDisplayU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5B2686F856089B29C597614D0E1A7F5FE52FB57,
	U3CMessageDisplayU3Ed__85_System_Collections_IEnumerator_Reset_mF7CE8F8D4159EA5AE4A460A60198198AE0920CDE,
	U3CMessageDisplayU3Ed__85_System_Collections_IEnumerator_get_Current_m396D622BA06A59BBB63DC0338877CC116109CDD0,
	U3CLoadAsynchronouslyU3Ed__94__ctor_m1BAA59970D13AB336CEA8CF507E8755AB8E4311D,
	U3CLoadAsynchronouslyU3Ed__94_System_IDisposable_Dispose_m387A6DE049CA9C23EADDEBB72C366DAC2C9846EF,
	U3CLoadAsynchronouslyU3Ed__94_MoveNext_m695FD6533ED82D935DE549ECCE5704B92BBB6438,
	U3CLoadAsynchronouslyU3Ed__94_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA687C08CC8F6BC039B78ABFAA544DC604467D9A,
	U3CLoadAsynchronouslyU3Ed__94_System_Collections_IEnumerator_Reset_mD267C3EFAD02A00392DB7AD6033A41398D6547E2,
	U3CLoadAsynchronouslyU3Ed__94_System_Collections_IEnumerator_get_Current_m690AE884567FC740FC975A761AAFE8052719717E,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m352D68463FC9600F9139AD78F0176368562F63C6,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	ShareButton_Share_m03F465A5C7BDC061CF1E60950A19C37E7BFDC6E0,
	ShareButton__ctor_mCCBB7E13A22B57B1DE31377177AC0C35AFEEFC0E,
	U3CU3Ec__cctor_m928ED25AE006A9860F5039513FEBFB988B1BB666,
	U3CU3Ec__ctor_m53F9C64973736DF514D755B119DAE1FE1DCC1B96,
	U3CU3Ec_U3CShareU3Eb__0_0_m60D5F2861EC9E4B4B64A68754FED63C9465580F3,
	U3CU3Ec_U3CShareU3Eb__0_1_m98B67B7F0C8CFC600D60B956AEA2021E3D513257,
	TwitterPost_Start_m9E15C10146EA697F7A025DB735111E3852B00713,
	TwitterPost_Upload_m909285353B8CC8C2B23C35991127C885278ABB5C,
	TwitterPost_MediaUploadCallback_mBDF91F0EEF18B926BFDD29B9FC55CD2F73AA9D24,
	TwitterPost_StatusesUpdateCallback_mC0D27B4824E869EF5772300727D825166454B1E7,
	TwitterPost_Callback_m7308346CCD285A2112B8F3BE182076295045DE13,
	TwitterPost__ctor_m77129030A3364462525BCCAD7866DDC8422FD2E5,
	U3CU3Ec__cctor_m544860AFC2B2770DDD8AEE73E7F067F22FD6E875,
	U3CU3Ec__ctor_mBAB23ADA139BD6EF82CA5B41B6E350A0FCEAEDAA,
	U3CU3Ec_U3CStartU3Eb__0_0_m141ACEC84A19590D35DFABA5C39324A75E99F265,
	ViewChange_LoadScene_mBA90A55289A6501B95A4DDEABC49FA8EA7024344,
	ViewChange_BlankARView_mB7993B5323E9A22493817A8488A82FEC27293C4C,
	ViewChange_MenuView_m390FBDD33756AC837BBB12199A9DF4E087A8CD4E,
	ViewChange_FormView_m844FC62335B1539D65BD088224FDAF2254F5E240,
	ViewChange_Start_m6A1FE182105FC16398D449A5C43CD1276E97A9C7,
	ViewChange_Update_m219FE23FD2D7528D3D3E7252B96EE7A62D6BD025,
	ViewChange__ctor_m51CEC5C50602B6D428C7165D2C7BE16B02489219,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m631D6AC7892A4BD309DB2DB36B71AE7617EF20AB,
	ExceptionEventArgs_get_Exception_mE1653B0B40B662C17CC5CF7A734EE3F0BA7620E1,
	ExceptionEventArgs_set_Exception_mA50BA260007949DDB15BC3CEFDE69D27B1A93922,
	Promise_add_UnhandledException_m60F13449991E7C2683FD8981F8740687D8A34A87,
	Promise_remove_UnhandledException_m39A222899B7C1A0C85802F0CD92EC415DC388527,
	Promise_GetPendingPromises_m1ECA9EDDD1F7CB200019B69456F8B85DA6EF4E82,
	Promise_get_Id_m5AFDBD1A671F2EBC2C1521E9A3786657D0E1AE34,
	Promise_get_Name_mFA2E670F57075D333E4DDD53D981FACCBD12A496,
	Promise_set_Name_m348EA57D5FEDA7710E4496EE3F36894920420B9A,
	Promise_get_CurState_mD0512D15E08EC733965BA299239B02AFAB772490,
	Promise_set_CurState_m458DB5B838CCA6E10D835E8006DC0839C06386D9,
	Promise__ctor_mF3C706B6D756F3C2CF35C36889D5CB055CFFC4A8,
	Promise__ctor_mD5DE022EE907B5AB7F870CCBA000B91835C491C4,
	Promise_NextId_mF232BC29933B7A8D316E9F7DF310FC2F79BD0668,
	Promise_AddRejectHandler_m052FF9343C09C567EA852CBB0317A5EDDD41C32B,
	Promise_AddResolveHandler_mF61971AD66877D8919FDDE1E48256C7AEAD0FFA0,
	Promise_AddProgressHandler_m042CA8C9C68D98D919BE433F5BFBA8D1E26E39AD,
	Promise_InvokeRejectHandler_m49B2D5AAEB34EFCC768224F0AA8BF2F02C040991,
	Promise_InvokeResolveHandler_m3A25CEBDAE49C882965AFCD67FEBF033F956C4D7,
	Promise_InvokeProgressHandler_m83372C7CDE89BE8C915155346E4F216B87389B9E,
	Promise_ClearHandlers_mCDBAFB8325F73B3EF7F307B5D1D2CC32849BDAA7,
	Promise_InvokeRejectHandlers_m845F6E61117E5677D3EBFF04A3A2AEA73E85D200,
	Promise_InvokeResolveHandlers_m3538EC5D55D5784EBCC3E29ABB9EEA42D02F398E,
	Promise_InvokeProgressHandlers_mB126FC44C4CA670E0A40A19DFF51AEF5914BC788,
	Promise_Reject_mBBDFB4EEB1AA6DBECC97FC732840CE6194693A85,
	Promise_Resolve_mF554905157A7FCB6711F4524BEF276FEAF0442F3,
	Promise_ReportProgress_m6D4C8B13F0FDC54826EC382F834B90F3A37B1E97,
	Promise_Done_m64BF9B435F15E2D45D3AF60EBF78D20BB11BAE1F,
	Promise_Done_m1D623420C2FD6C2417F4473DDB8AAB1DEFA47BE3,
	Promise_Done_m2643251B83BDEA0EB3540BA9F558358FDB4EFF5D,
	Promise_WithName_m135C00B72AFA5F951F36AFBD6D8C1ACF16A9C66C,
	Promise_Catch_m6513FEF948071ECE9FDF2402EE982472812AC636,
	NULL,
	Promise_Then_mC640A94175B17F3BEB17C11B8D24902FAB69C839,
	Promise_Then_m29445A358CA6F7C6772419A8137663D85C5003E7,
	NULL,
	Promise_Then_m5735DE787B26B5FEE2E7ACA794E1B9D4AD613F39,
	Promise_Then_m920087B7407E07F9197BA7D5BDC8233654DFD35E,
	NULL,
	Promise_Then_m8B6ABDBA30471930B8EED5DB012A17113A16650D,
	Promise_Then_mEBD03F33E77489EC764FDAA28320F03E6B29DB78,
	Promise_ActionHandlers_m32008551CED6FFF161BCB9D17DE5A98CE1582C6F,
	Promise_ProgressHandlers_m6D4D4DF372D317FAEF05664BC81BFC7FB4DEEA00,
	Promise_ThenAll_m28D1BCA29899781AEE05A340B45A57E4F379998F,
	NULL,
	Promise_All_m48EEF822915D8B5A27D5A7F81EA6231003441B91,
	Promise_All_m05E09E982475B18694193676BA61D06517749F0B,
	Promise_ThenSequence_mA5DBE3AD740FE9CC7945E9C139C2AD7124AA53D3,
	Promise_Sequence_m585CD03D201AF7FC30FE7A85B9CCF58818897386,
	Promise_Sequence_mAAD31E5050811A7D864DAF60B51AAF804B3BFEF4,
	Promise_ThenRace_mA71DE342E882BBB84D1E081B7E18BC2515D8F714,
	NULL,
	Promise_Race_m448804C9E2934111CBF554A64AC620C30193B84A,
	Promise_Race_mD86AAB4E03EE3A0B816C363F56DF6705A5FFF0D5,
	Promise_Resolved_m7846F8C76B74E75E7E850780F997C588EC9C1202,
	Promise_Rejected_m3F751DE74C6B3A76B91EA52FD27C6B9511B653DA,
	Promise_Finally_m4DFC5161F59CED77D92A4960B9BB3E7DB530DA3E,
	Promise_ContinueWith_mDE80BEDE819C0E1E5D81D0671FB4E37A2A3DB3B3,
	NULL,
	Promise_Progress_mA53E5CA4F1AFE6330D0216F1037176906BD7C085,
	Promise_PropagateUnhandledException_m89ACA988FF12BCBC7414ED335D6C3C0853A72FDC,
	Promise__cctor_m080830C73ADD25E51657D67032A4C2141E1F4180,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mEDB06C835F866E6683364F5A0D367673B22EBC7C,
	Promise_U3CDoneU3Eb__40_0_m35431750443DD94020F64C8D82171A5097D1FE57,
	Promise_U3CDoneU3Eb__41_0_mDDA9E97AE88980725F9713DA8C832E7FFDD58025,
	Promise_U3CDoneU3Eb__42_0_m00B92505423E6143AEA25A174D3A2871667F73AC,
	U3CU3Ec__DisplayClass34_0__ctor_m897DC60DCFF9720781FDDCF89BAF8AE80A641586,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mB4D3508FB53C5380CF3DA70DFA540EF228F52481,
	U3CU3Ec__DisplayClass36_0__ctor_m14342AEB656B3C6E043CB9C328024C9498A709DB,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mD181A5DD642FE3E0D38EC53325C7E45430715A5F,
	U3CU3Ec__DisplayClass44_0__ctor_mF6D43FC6CD19DE6AFF0B9656924C222BFAEA6C0E,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mB3B9D6EB885D400E307A3BB7B4148ECC9BEED195,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m7D1C74900668244ED2925D69FFEB0353189765B1,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m20246593163120F7F446B076B1F4EC4E6C90A2CA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mC731BB36D8ADD6B4079B3BC339BBF9E0C4EFB4F8,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m91C10E82E96D2D909CE9F075FBE73C2D7E278192,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m0DF0E0F3E4563CE5C7427F7405ADEB03DB8A5F83,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m7501B93112EC99EC8EE3EEE1B56A7F3C3DEFC685,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m194278F8D66D227B7EB1FAFCF826F331113265D1,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m2DDEA0BFAE75634CEB55BB1D746BBD330F4869C0,
	U3CU3Ec__DisplayClass53_0__ctor_mFACADFB9DBD1920C191A332AF7300F7E8E024ED3,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_mF7C4944C5F8BE6513BDDCA8B0FDBB6E14DC2C5E3,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m979DC9DB8F68E5C79ABF476468906D4B87D01D9C,
	U3CU3Ec__DisplayClass56_0__ctor_m612F1A6EDC882C7B7B04ACCEA10F78FFB99D71A7,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m7BBD1735E2BD1AECDEA2A20CB9E29A311E048C50,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_m516408BBD1540BB479CACA4215789A61E588D810,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m87565D3C5EA3DFEB778B00A54EF744307E7FFD01,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_mE7882075EE24DC9055D3224D33DD3E7FE5C3649A,
	U3CU3Ec__DisplayClass59_1__ctor_m4B8FB61B59089C4DEA648A75D409E836CEE092FF,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m43EED26E0FBA8DE6083A06EF6B646BF78872E14D,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m9B45224464BC3BFF1C41548F8B2A37BD35A8F29C,
	U3CU3Ec__DisplayClass60_0__ctor_m2E7C2B7DDFE3EEB3871DF5303188E2430F55AA74,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mB394CFDCCB95FD085A8736AD8E482A50F44D4A49,
	U3CU3Ec__DisplayClass62_0__ctor_m9CE32458E8BABAD882B5CA483D36D5EB2994C570,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m7D3A4FFC66539686B3257F32DF09A8EFC93D7AED,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mB3DB8505EF43580EE4356A320BF4FB3842CFFA50,
	U3CU3Ec__DisplayClass62_1__ctor_mF6802236B37785089ED7E9A3F5DA5558ED85951D,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m9F213538E12F1333EAADF3690503584E98B82237,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m25D42314D26736A13DD29D1331B0BA160532D3AA,
	U3CU3Ec__DisplayClass63_0__ctor_m05206B1BC0E0943BD3F41312AA406BF6190D2521,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m57BC5E3BB67D69BCA61F5E8BF4AC755B38295222,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m0532938FA47506CFA8732885E283AD9F21C72ADD,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m930B260B50CC2D259DD4E73FC7EF574B6ED6BEA0,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m4EC2810EE4C20AE7FB3AF79489717ADE34ED665A,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mC808C98C3D85BA35CB856A3E3739FB79AA5D4D2F,
	U3CU3Ec__DisplayClass66_1__ctor_m9DBFC26D5CBB1FBB640770336F18C142C8E93405,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_m25CC0AF8BFAC0DDB555ECDEA6F7B4068CF1052EE,
	U3CU3Ec__DisplayClass69_0__ctor_m78E8AD1705A8C3F3CA12430C2E97120BF28F5C36,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mB98B7DDB295AFE5A93D21C9EA74C15A70B620BD4,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mA1F009415ADC80C8DF29CADD62E6CE6E861BCDE2,
	U3CU3Ec__DisplayClass70_0__ctor_m2335F4081798B9CA126758D5B38D58578ACD02B4,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m5ED5B955EBC2657600378AEA3741D2096B294467,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m7C6285AA36261DCEE12D834674167AF1F8418D44,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_mDF49A69B00B47D8E30C59A1EFE8CDCCCED2175BD,
	PromiseException__ctor_m327B4108B2935F10E2B77A95E7B6D3CD7574DA63,
	PromiseException__ctor_mB2C16AE66329DF694119F386C897F586F72F96F1,
	PromiseStateException__ctor_m5AF42A933A440D43FE8A735DAAEFC5B59D439A25,
	PromiseStateException__ctor_mF4411B73A303831214435467F21ABB529A5C3CA9,
	PromiseStateException__ctor_m6D6DEBEE15613C770037CF803694BFEA0167B311,
	HttpBase_CreateRequestAndRetry_m35DC167FD43166522E5A0077DD1D0BE53F2F9744,
	HttpBase_CreateRequest_mC1F2A506707D8B14B961E2373324474282877873,
	HttpBase_CreateException_mED2910E823C82872433781C9A0C666EC38A4F727,
	HttpBase_DebugLog_mE7CC8B1218860CF7322DEBAA4C9B1FD788B19E6D,
	HttpBase_DefaultUnityWebRequest_mDCDE1A9E1732408B0988222C9F5B729B99DEC210,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__1__ctor_mB1EB168AD8F16B094B7B642D236ED8CFD4B21D93,
	U3CCreateRequestAndRetryU3Ed__1_System_IDisposable_Dispose_m5EDA8599DA335CB6537EC9926059B2B1F2E61396,
	U3CCreateRequestAndRetryU3Ed__1_MoveNext_m339EB2C40A99750F58696026014E81E2A1926A40,
	U3CCreateRequestAndRetryU3Ed__1_U3CU3Em__Finally1_m67AA93F9FE785A41D82008F53D548A3484C6C0B3,
	U3CCreateRequestAndRetryU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97E66B8BAD956883BCC8131DF9A54557FADB15AF,
	U3CCreateRequestAndRetryU3Ed__1_System_Collections_IEnumerator_Reset_m13FA4CC32B13471431FC86EC0FB1FCA2E8BD6AA4,
	U3CCreateRequestAndRetryU3Ed__1_System_Collections_IEnumerator_get_Current_m46D17F003FC5CD9211994B23675A58EB54AB3F1F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_Request_m3E619CA5E6DF922C28E73E0B52AFA0A42DD527A1,
	RequestException_set_Request_m4A39C732359927451AF59D654313E73727FAA113,
	RequestException_get_IsHttpError_m2FBBBB7756A22FF573189DCD20590F773436AF38,
	RequestException_set_IsHttpError_m0BAA2B0BE9794C73D99DE699E643BF53E9074A9B,
	RequestException_get_IsNetworkError_m1E99469DB868249A682360D6812AF364F69B43B8,
	RequestException_set_IsNetworkError_mD03F7650C1DEF980ED2EAD76E91D660D34654995,
	RequestException_get_StatusCode_m095758F8685082FDE8835A82E94BF4EE1E9D391F,
	RequestException_set_StatusCode_mC6924445A9BFA983A0C44D9F90D029760CEA2542,
	RequestException_get_ServerMessage_m495637BFD1FA10496F20C6AD90931AB70BE9DDE5,
	RequestException_set_ServerMessage_m046F5AF5B54CF7BB2A627F056133571B7964F933,
	RequestException_get_Response_m7AA3200ADA93B624DCC0768D324204F5D7171D92,
	RequestException_set_Response_m3650ABABE07676D1D97D845AA42F681113549F04,
	RequestException__ctor_mC77265431C17D4AC0450CEA340469690794E9F2C,
	RequestException__ctor_m56382201F8D03DC7AF75DC54776681CEAC3639BA,
	RequestException__ctor_mEE6C35547B7E1F32B9E8A4A1270323E36ECDE869,
	RequestHelper_get_Uri_m4BBB86E86D0B704C7EE5DBE2D36EC7D1D925D245,
	RequestHelper_set_Uri_m95F799EA43CDE070FB94050EB32739594DA824CE,
	RequestHelper_get_Method_mD86FE6C056CAA5BFA4C768D5CFD6B1AC2D924719,
	RequestHelper_set_Method_m6112760F386FB3160A999EE7E505E7B7B78CFAD5,
	RequestHelper_get_Body_m727A2AEE626E7FC01DF0A978441C001EB34EBCBB,
	RequestHelper_set_Body_m2CCEC8C91A32A69CA7B68E9906326DDA156CE4C8,
	RequestHelper_get_BodyString_m9F94287AC2B538F2A7DAEC148696D217EFBF8E53,
	RequestHelper_set_BodyString_m49A6D60FDBDACE28CFD43E4EDFB3A67CC3B681EB,
	RequestHelper_get_BodyRaw_m819005CD0C1E7634FCF67CA97B1F8D5D06A1EAFD,
	RequestHelper_set_BodyRaw_mF989309E4B72DC5FB7AA0B0918F902CDCFC6C56D,
	RequestHelper_get_Timeout_m2B636E856FDDFD91989839B407A414D9932312F0,
	RequestHelper_set_Timeout_m9F4273E021F533CE48552400B15F603D8E4C50CE,
	RequestHelper_get_ContentType_m1DE79D1DB56E8D9E796B0E9E60ED993AB94715AB,
	RequestHelper_set_ContentType_m2ECE34056F84BBE96CD18F18A7B1CFD03942A66E,
	RequestHelper_get_Retries_m9BF4A1F6FA0020CB3CCF729FE41650C13769E83E,
	RequestHelper_set_Retries_m890B434A994EFEABB0CD0680B8E0377C5B5B5318,
	RequestHelper_get_RetrySecondsDelay_m234E28296D60688F538185FD3E11466B67668EB7,
	RequestHelper_set_RetrySecondsDelay_m17A1172B216D0985B7D63832F54C0B8AF37A2090,
	RequestHelper_get_RetryCallbackOnlyOnNetworkErrors_m345837ED1A7C7DA8C84C363522418B704965423A,
	RequestHelper_set_RetryCallbackOnlyOnNetworkErrors_mDEF28C9504B4F402EB0ECCBC74DA6E568997D85A,
	RequestHelper_get_RetryCallback_m8958FBD9B8E6A0852AFC98537FF1A2AB40273461,
	RequestHelper_set_RetryCallback_mB09B3A1DED93C0565A309D708FE77A84085F89DD,
	RequestHelper_get_ProgressCallback_m914F4F08FFB614CF83D64A5FF4F0164539A11CB2,
	RequestHelper_set_ProgressCallback_m58CF4252FDE248CE2F7862B345B6C55F002B50C3,
	RequestHelper_get_EnableDebug_mDB14F675CDD96C48A77B3E1322020207030547C1,
	RequestHelper_set_EnableDebug_mCB357C37BA62BF8103C6508A6B1743F2D83A9C3C,
	RequestHelper_get_UseHttpContinue_m3637AB717925A6979C5B34080D032E2FFDC5FD2F,
	RequestHelper_set_UseHttpContinue_mB4AF296D392D92304FA264EED61600095C9E73EE,
	RequestHelper_get_RedirectLimit_m12FA845010695EEE4A09DD9A6F76DE8471A862D4,
	RequestHelper_set_RedirectLimit_m86B5BBC876F738C1BE292CD062077D24C00AEA9F,
	RequestHelper_get_IgnoreHttpException_m62F4E6F0A29DA302D61746DD6AAF49EB657ACF50,
	RequestHelper_set_IgnoreHttpException_mE9395D343CF98123B065E9ACF06C7FA1100AC075,
	RequestHelper_get_FormData_mFCABF1F3476D7580A9AAB63C6B010C1114DD6947,
	RequestHelper_set_FormData_m548CAB9872FFD2A8027994B8D95506B79F06115B,
	RequestHelper_get_SimpleForm_m15B81EB567521859E32A255F893E86F798C8B164,
	RequestHelper_set_SimpleForm_m9FF72C01F325AF2A39944732B2F7B5D5B8B8C7B1,
	RequestHelper_get_FormSections_m43721DC6C2EBCC4BB13FAE074739AD26C8583F97,
	RequestHelper_set_FormSections_mD874EEC922A94AC08D0FAC879652033B54C9424D,
	RequestHelper_get_CertificateHandler_m321AC0450D35604BA128C3288D044FE6884ED265,
	RequestHelper_set_CertificateHandler_m2D81B9BEECF8BFA4F18047F08EE7C256E1B21117,
	RequestHelper_get_UploadHandler_m67989620874FA461E24832C70D639EC15147AE3F,
	RequestHelper_set_UploadHandler_mCFF33186F8DCA2178A4AA54EC97B6823DB6350A4,
	RequestHelper_get_DownloadHandler_m576BE2E2C2E06E5B1831D089223FDD2C399D08E4,
	RequestHelper_set_DownloadHandler_mF3F1FAFD4D003B5F5718979222F871F32DC636C6,
	RequestHelper_get_Headers_mF06E05AD5EE832BFE56704B7A546B0C7E25D4B79,
	RequestHelper_set_Headers_mC80E0F973C211BD73B04AECB46205766BA9F6409,
	RequestHelper_get_Params_m52A62A6900B4D659B401879E1B9AAE4A7E6D1123,
	RequestHelper_set_Params_m3F4126B55624310028A0C42D490E10F54A4750AF,
	RequestHelper_get_ParseResponseBody_m87AE3F014C05690D1CC4C6D5204DCA44D386D591,
	RequestHelper_set_ParseResponseBody_m021500316923EA402EDE2E525753AA265389C14E,
	RequestHelper_get_Request_m6E6296880FD1879EAEDE88034709C17D68F0643E,
	RequestHelper_set_Request_mA1C3D38E6CBDABDCF58527D9A28070653A1C1979,
	RequestHelper_get_UploadProgress_m981820F2EEF397803961FC90BBBD36B6CC6D574A,
	RequestHelper_get_UploadedBytes_m9E7B04CE766C97D243CEDE9DB42E4CEA35A8D6F7,
	RequestHelper_get_DownloadProgress_m5158223FDF91676452FA27A330E661EE8C9D9506,
	RequestHelper_get_DownloadedBytes_m7D8E2CC4748631F399B91549A2E7A597E2316841,
	RequestHelper_GetHeader_m522CB3C8DEAA6725CC66F293E84BF09B7DB26683,
	RequestHelper_get_IsAborted_m5B21E1C18BD58CCDE57D63D2BD61537C40735129,
	RequestHelper_set_IsAborted_m94327332571DC63115E2C45BC4BF07D15C8F3694,
	RequestHelper_get_DefaultContentType_m91687E0E1878F17A2047E3E7741EF6CA42A1458B,
	RequestHelper_set_DefaultContentType_m58E76D9215877D62BA0BDE96356D961D70184203,
	RequestHelper_Abort_m8A66AFBDDFC59B629764F7242D284DA1D71503EF,
	RequestHelper__ctor_m7C53F61C41E1B440A83BB8761275696AF36AF7DC,
	ResponseHelper_get_Request_m7BC4EF21DC9F461CDFCBC86DCF0AA8F2C06D50B0,
	ResponseHelper_set_Request_m9734C03F625D17D16F2BD1696ADED283AE295194,
	ResponseHelper__ctor_m3CCCE641244AC12357CC70E8C05AA39285A9C9A0,
	ResponseHelper_get_StatusCode_m217BF73AEA4E2259D9A19D797E9C4CCFB3819633,
	ResponseHelper_get_Data_m27FFFFF8A67EC8C3F7793072A4D094CEA7C09336,
	ResponseHelper_get_Text_m4529907A41F3E760A73564C9C189AC97F320F822,
	ResponseHelper_get_Error_mBD25C2495C9336770708687BDB2A08B8AB09E7C9,
	ResponseHelper_get_Headers_m2A5A136C757A50B353C80E0323FAF95D5970A4E5,
	ResponseHelper_GetHeader_m479626D4E493E1CEC980E312D85BE27A20AA3501,
	ResponseHelper_ToString_m5DDBBE98399DDA4D2B534767C539573FAE8F33EC,
	StaticCoroutine_get_Runner_m99CB27E232AC6F00A6EF64A2924391E2CDC8A8B3,
	StaticCoroutine_StartCoroutine_m96DBFAB1D1218585286F517D000CD8E57D81050D,
	CoroutineHolder__ctor_m44080B42ED079EFD8E60751B760C0210006C9EEC,
	RestClient_get_Version_mE00E27D9A87081E91634BCA3DFFB399BF2B83720,
	RestClient_get_DefaultRequestParams_m3C946CC3E2AAE867138DEB40AB422D73DF735651,
	RestClient_set_DefaultRequestParams_mCA379D4AC4714C731395D2ED049F2218636F5412,
	RestClient_ClearDefaultParams_mD98BAF7050FE16E46B53D1682742A20187EFF054,
	RestClient_get_DefaultRequestHeaders_mD15628D2B50489DCA34C24AFB5918B1275D3BD06,
	RestClient_set_DefaultRequestHeaders_m9193F5869FA022E88210D5F12372C28D0EBE3586,
	RestClient_ClearDefaultHeaders_m372635168791F3CA28B5197913E70705BC754E5E,
	RestClient_Request_mDBEBA7EE6F1986FABA1487374F8CC56556457E6D,
	NULL,
	RestClient_Get_m35A190076C4706104A3C159C898E59B427DBA1A2,
	RestClient_Get_m36C3A32A743F62ADAB89F5193065ECD989D45683,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m83D2FCE9E88982247122AE568559F5AE97D34F26,
	RestClient_Post_m801E218330870755BA217ED52C44A6B1FECCCE71,
	RestClient_Post_m868B1F256565734EF2E771C3A4D17A8BB4FFD583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mE378F5382982882EAE68147E1025173BBEECE6E6,
	RestClient_Put_mD062595425F1307D4A2FE2D405B94E51F69DA243,
	RestClient_Put_m6BCCA98E28999F5A658FDEF9DFCEC6A08EB9FFC5,
	NULL,
	NULL,
	NULL,
	RestClient_Patch_mFE4C7BC8297762069561515E3166376B1B04BF16,
	RestClient_Patch_mC74278923877E14BB7DBAAD12183B84EB07718EC,
	RestClient_Patch_mDCA173A6B4AC83F2C40CAB7668255CC13C5463C4,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_mADE13524F212CC2FF5DC65C218EE6108222BD3C7,
	RestClient_Delete_mF86A2079CDF0A4972258FC8FAE75FE97CFD0FA26,
	RestClient_Head_m563C993B2CEB8166279A8A5842AEF64E88FC7018,
	RestClient_Head_m596A590AD91B3D91A2554702C84AFD40DAA466B3,
	RestClient_Request_mBBC58641FED9656F4B03DA443705F7752954A05F,
	NULL,
	RestClient_Get_mDD3EB818C782291807A153842D1D9612F95CEAC4,
	RestClient_Get_m21CD13976DB446B991932616AC1093045D510580,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m623AE9FBAB3576D548E9E5397F1CDC9CE198978C,
	RestClient_Post_m007A792E1C5ED725DFFF980D277B60293B8072E7,
	RestClient_Post_m161D873EA6D79B3F86C5A5357B0610E7BEDDA7F5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mB7D8FD402D48C7C69B4503F031185596A8C343EB,
	RestClient_Put_m2C1AB7B6B4E66B34A6B17B72EE86158595A24195,
	RestClient_Put_m4B4BBC4DC8031BD79359BD4E1DC7FB421DB20996,
	NULL,
	NULL,
	NULL,
	RestClient_Patch_mE3283A5E7DBBD115E1013A2F78CD4BDF44888958,
	RestClient_Patch_m2723DD0A18C09E7650CDA02626018F5F64CF94AB,
	RestClient_Patch_m69B97FB06DE7A0F9A7A952F6AABDC1C94D56BC2D,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m3CB4F4FBEC461A9D21A4A423BD9763FDD0F6AAC1,
	RestClient_Delete_m2FCE9AF1698612DA9F23F87C30AE30D556604B5F,
	RestClient_Head_m371CDEA0CB9C5DC3A4E671A3274D40B276C7DF03,
	RestClient_Head_m2458809288DB81B6B90DB21F6C53D4DDA091AB5C,
	NULL,
	NULL,
	ExecuteOnMainThread_get_Instance_m57A7DD0347FA71E927B3FA57457DEE07BFC368E9,
	ExecuteOnMainThread_Awake_m96AA2D23BE882B821722FB53B9B5ACA35DF3B0F2,
	ExecuteOnMainThread__ctor_m9396E93259FED5C79003E08CC1D95F3C4C9D1814,
	Common_GetFormSectionsContentType_mE07A67FFE9476D2887EF3A914C9F907CEE354F6D,
	Common_ConfigureWebRequestWithOptions_mAEC964E81E8520EF8FBA784E20845EB4DDDCDC89,
	Common_SendWebRequestWithOptions_m83473BAB8B86A9A6D2207AC212560D1AE61FFD44,
	Extensions_CreateWebResponse_m60EE5304206BA3F871F798B9B67FD0360E3BD8C6,
	Extensions_IsValidRequest_m0830F9B95E46476522CF1338B3B94499BEA7BE81,
	Extensions_EscapeURL_m58CE4576E50E439B8070117EC1581EC3A160A188,
	Extensions_BuildUrl_m1D73D79077F26F2C0A0737CA287DEA5A30D855AA,
	U3CU3Ec__DisplayClass3_0__ctor_m39F8F1DFCE5887655F9E6168820F86EA4D3DB29F,
	U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_m85934B7498E0B2549B7CD1ADA69103F9A50C236F,
	U3CU3Ec__cctor_m1F7DA2D6F4C08CD2ABB695DA98B2831C889BA942,
	U3CU3Ec__ctor_mAB9972DF599999D1767BC5FA12A2C547E525FF79,
	U3CU3Ec_U3CBuildUrlU3Eb__3_1_mF7A7B262A28BC96810538E15E76941C13C1ED08F,
	Photo_ToString_mD6A22636275A6B35FAC2E3CB8574231785438D8C,
	Photo__ctor_mC6E90DF93DF6EE443FE41F931F45C95EB31C0A31,
	Post_ToString_m29DD92DEF3102ED3D3B8A6342DF7E6B681DC8AC5,
	Post__ctor_mA6934B0F1762428A4C4A9B709EFA7DADDDD7B5D5,
	Todo_ToString_m2717C1F1DF64663621435EF9C35CE95C8C7F51B9,
	Todo__ctor_mBFA41A1DC43949DAAA32DDE4D705863D4F469C13,
	User_ToString_m4DEC5DBD2A99CE340A211F2C66BA6D86D01C12CF,
	User__ctor_m7F7F3F420853C6F4A54B9270C6DD870EB878C993,
};
static const int32_t s_InvokerIndices[911] = 
{
	4427,
	3636,
	4526,
	4526,
	4526,
	4526,
	7102,
	4470,
	3673,
	4526,
	4526,
	4526,
	3541,
	3636,
	4526,
	7102,
	4526,
	4526,
	4526,
	3551,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	3551,
	3636,
	4526,
	1935,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	3636,
	3636,
	1013,
	1935,
	3636,
	3636,
	4526,
	2615,
	2615,
	2615,
	3636,
	3636,
	7102,
	4526,
	1930,
	3636,
	3636,
	4526,
	4526,
	7102,
	4526,
	3636,
	3636,
	4526,
	3636,
	4526,
	4526,
	3018,
	4526,
	4526,
	4526,
	4427,
	4427,
	4526,
	3636,
	4526,
	3613,
	3613,
	4526,
	4526,
	1925,
	1274,
	4526,
	4526,
	2618,
	4526,
	4526,
	3613,
	4526,
	4526,
	2615,
	4526,
	4526,
	4526,
	4526,
	4526,
	3613,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	3636,
	4526,
	4526,
	4526,
	4526,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	744,
	4526,
	744,
	4526,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4526,
	4526,
	3636,
	3636,
	1586,
	1586,
	994,
	994,
	1012,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	4526,
	2615,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3636,
	2615,
	2615,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4427,
	4427,
	4526,
	7102,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3613,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	3613,
	4526,
	4526,
	4526,
	1586,
	1586,
	994,
	994,
	1012,
	4526,
	4526,
	4526,
	4526,
	3636,
	3636,
	4526,
	4526,
	4526,
	4526,
	3636,
	4526,
	3636,
	3636,
	3636,
	3636,
	3613,
	4526,
	4526,
	4526,
	4526,
	3613,
	4526,
	4526,
	4526,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3636,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3636,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3636,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	4526,
	4526,
	3636,
	4427,
	4526,
	4526,
	1125,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	2615,
	4427,
	4526,
	3613,
	4526,
	4465,
	4427,
	4526,
	4427,
	4526,
	4526,
	7102,
	4526,
	3636,
	3636,
	4526,
	4526,
	1971,
	1971,
	3668,
	4526,
	7102,
	4526,
	1971,
	3636,
	4526,
	4526,
	4526,
	4526,
	4526,
	4526,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3636,
	4427,
	3636,
	6974,
	6974,
	7076,
	4402,
	4427,
	3636,
	4402,
	3613,
	4526,
	3636,
	7069,
	1935,
	1935,
	1935,
	1013,
	1935,
	1015,
	4526,
	3636,
	4526,
	3673,
	3636,
	4526,
	3673,
	1935,
	3636,
	4526,
	2615,
	2615,
	0,
	2615,
	2615,
	0,
	1277,
	1277,
	0,
	788,
	788,
	1013,
	1935,
	2615,
	0,
	6849,
	6849,
	2615,
	6849,
	6849,
	2615,
	0,
	6849,
	6849,
	7076,
	6849,
	2615,
	2615,
	0,
	2615,
	6521,
	7102,
	3777,
	3636,
	3636,
	3636,
	4526,
	3658,
	4526,
	3647,
	4526,
	4526,
	3636,
	3673,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4526,
	4526,
	3673,
	4526,
	3636,
	3636,
	4526,
	4526,
	3636,
	4526,
	4427,
	0,
	0,
	4526,
	1930,
	3636,
	4526,
	3673,
	4526,
	4526,
	4427,
	4526,
	1277,
	4526,
	4526,
	4427,
	3673,
	4526,
	4427,
	0,
	0,
	4526,
	1930,
	3636,
	4526,
	4526,
	3673,
	4526,
	4526,
	3636,
	4526,
	4526,
	3636,
	0,
	0,
	0,
	4526,
	3636,
	1935,
	4526,
	3636,
	1935,
	6065,
	6849,
	6065,
	5901,
	6065,
	0,
	0,
	3613,
	4526,
	4465,
	4526,
	4427,
	4526,
	4427,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4427,
	3636,
	4465,
	3668,
	4465,
	3668,
	4403,
	3614,
	4427,
	3636,
	4427,
	3636,
	4526,
	3636,
	194,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4277,
	3447,
	4427,
	3636,
	4402,
	3613,
	4470,
	3673,
	4465,
	3668,
	4427,
	3636,
	4427,
	3636,
	4465,
	3668,
	4273,
	3443,
	4277,
	3447,
	4465,
	3668,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4427,
	3636,
	4465,
	3668,
	4427,
	3636,
	4470,
	4403,
	4470,
	4403,
	2615,
	4465,
	3668,
	4465,
	3668,
	4526,
	4526,
	4427,
	3636,
	3636,
	4403,
	4427,
	4427,
	4427,
	4427,
	2615,
	4427,
	7076,
	6849,
	4526,
	7076,
	7076,
	6974,
	7102,
	7076,
	6974,
	7102,
	6521,
	0,
	6521,
	6521,
	0,
	0,
	0,
	0,
	5889,
	5889,
	6521,
	0,
	0,
	0,
	0,
	0,
	0,
	5889,
	5889,
	6521,
	0,
	0,
	0,
	5889,
	5889,
	6521,
	0,
	0,
	0,
	6521,
	6521,
	6521,
	6521,
	6849,
	0,
	6849,
	6849,
	0,
	0,
	0,
	0,
	6065,
	6065,
	6849,
	0,
	0,
	0,
	0,
	0,
	0,
	6065,
	6065,
	6849,
	0,
	0,
	0,
	6065,
	6065,
	6849,
	0,
	0,
	0,
	6849,
	6849,
	6849,
	6849,
	0,
	0,
	7076,
	4526,
	4526,
	6045,
	5401,
	6065,
	6849,
	6227,
	6849,
	6065,
	4526,
	2767,
	7102,
	4526,
	2603,
	4427,
	4526,
	4427,
	4526,
	4427,
	4526,
	4427,
	4526,
};
static const Il2CppTokenRangePair s_rgctxIndices[78] = 
{
	{ 0x0200005B, { 0, 102 } },
	{ 0x0200005C, { 136, 1 } },
	{ 0x0200005D, { 137, 1 } },
	{ 0x0200005F, { 138, 6 } },
	{ 0x02000060, { 144, 18 } },
	{ 0x02000061, { 162, 5 } },
	{ 0x02000062, { 167, 2 } },
	{ 0x02000063, { 169, 4 } },
	{ 0x02000064, { 173, 4 } },
	{ 0x02000065, { 177, 2 } },
	{ 0x02000066, { 179, 13 } },
	{ 0x02000067, { 192, 4 } },
	{ 0x02000068, { 196, 4 } },
	{ 0x02000069, { 200, 2 } },
	{ 0x0200006A, { 202, 14 } },
	{ 0x0200006B, { 216, 3 } },
	{ 0x0200006C, { 219, 3 } },
	{ 0x0200007A, { 248, 18 } },
	{ 0x0200007E, { 266, 4 } },
	{ 0x02000085, { 270, 4 } },
	{ 0x0200008F, { 280, 3 } },
	{ 0x02000090, { 283, 3 } },
	{ 0x060001B1, { 102, 2 } },
	{ 0x060001BF, { 104, 1 } },
	{ 0x060001C2, { 105, 1 } },
	{ 0x060001C5, { 106, 7 } },
	{ 0x060001C8, { 113, 6 } },
	{ 0x060001CB, { 119, 6 } },
	{ 0x060001CF, { 125, 6 } },
	{ 0x060001D7, { 131, 5 } },
	{ 0x06000256, { 222, 1 } },
	{ 0x06000259, { 223, 1 } },
	{ 0x0600025C, { 224, 7 } },
	{ 0x06000262, { 231, 6 } },
	{ 0x06000269, { 237, 6 } },
	{ 0x06000270, { 243, 5 } },
	{ 0x060002C1, { 274, 3 } },
	{ 0x060002C2, { 277, 3 } },
	{ 0x060002CE, { 286, 1 } },
	{ 0x060002CF, { 287, 1 } },
	{ 0x060002D0, { 288, 2 } },
	{ 0x060002D1, { 290, 2 } },
	{ 0x06000336, { 292, 1 } },
	{ 0x06000339, { 293, 1 } },
	{ 0x0600033A, { 294, 1 } },
	{ 0x0600033B, { 295, 1 } },
	{ 0x0600033C, { 296, 1 } },
	{ 0x06000340, { 297, 1 } },
	{ 0x06000341, { 298, 1 } },
	{ 0x06000342, { 299, 1 } },
	{ 0x06000343, { 300, 1 } },
	{ 0x06000344, { 301, 1 } },
	{ 0x06000345, { 302, 1 } },
	{ 0x06000349, { 303, 1 } },
	{ 0x0600034A, { 304, 1 } },
	{ 0x0600034B, { 305, 1 } },
	{ 0x0600034F, { 306, 1 } },
	{ 0x06000350, { 307, 1 } },
	{ 0x06000351, { 308, 1 } },
	{ 0x06000357, { 309, 6 } },
	{ 0x0600035A, { 315, 1 } },
	{ 0x0600035B, { 316, 6 } },
	{ 0x0600035C, { 322, 1 } },
	{ 0x0600035D, { 323, 6 } },
	{ 0x06000361, { 329, 1 } },
	{ 0x06000362, { 330, 1 } },
	{ 0x06000363, { 331, 6 } },
	{ 0x06000364, { 337, 1 } },
	{ 0x06000365, { 338, 1 } },
	{ 0x06000366, { 339, 6 } },
	{ 0x0600036A, { 345, 1 } },
	{ 0x0600036B, { 346, 1 } },
	{ 0x0600036C, { 347, 6 } },
	{ 0x06000370, { 353, 1 } },
	{ 0x06000371, { 354, 1 } },
	{ 0x06000372, { 355, 6 } },
	{ 0x06000377, { 361, 3 } },
	{ 0x06000378, { 364, 1 } },
};
extern const uint32_t g_rgctx_Promise_1_set_CurState_m7E1BE6636D58589840B5EF9301691C2F0BDAE702;
extern const uint32_t g_rgctx_Promise_1_Resolve_m9D5548FC044C1528673C45043C86D46C005EDF21;
extern const uint32_t g_rgctx_Action_1_t8B16E288481B5BEB8453DE3471F117CD87D5ED49;
extern const uint32_t g_rgctx_Action_1__ctor_m084088C1209422CA94DCC02738DB81A8ED8AC117;
extern const uint32_t g_rgctx_Promise_1_Reject_mCD34FBC8039B8AA806DADBD5D0EA04E6A29998E9;
extern const uint32_t g_rgctx_Action_2_t9892BBA1839B44CB122080480B48272B6EC46C8B;
extern const uint32_t g_rgctx_Action_2_Invoke_m0A916A2D7A7F32B765DC5C7F320EB0C85E25871B;
extern const uint32_t g_rgctx_List_1_t11D44EA446595843AA679F5DD864508C73415E38;
extern const uint32_t g_rgctx_List_1__ctor_m02BB972F281B0889AB7EF614C1FF679EA12B68B1;
extern const uint32_t g_rgctx_List_1_Add_m4296F3D4A22A85575AECF05891393897DF8C985E;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0_t1779B966B4B038CDE58E5DA298C212ACC61797B1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0__ctor_m6B3483D47BC0F46EED8C1BF9AE280D02982E0504;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0_U3CInvokeRejectHandlersU3Eb__0_m3C80C0B112E8769E4AA5573408BBA672971C8612;
extern const uint32_t g_rgctx_Promise_1_ClearHandlers_mD69BC795406B6C3FB69E880AEABC48A10640A35E;
extern const uint32_t g_rgctx_List_1_get_Count_mDD52305D342E5CCB4E02663565381197E2C97A48;
extern const uint32_t g_rgctx_List_1_get_Item_m0545E30E4F578B9B71CCDDFEBD503C22BECECC4E;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_m2B995DA9B2D442B57814C9F953CEE2CEED959832;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0_t67ED753D6602C2B68726D01B5784CA0CF8808735;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0__ctor_mEF448D58E8621ECB300BF4BB3ADE5BDD7566B9FC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0_U3CInvokeProgressHandlersU3Eb__0_m14F755229DAEADD38EAA73AFF884E05885AA2FCE;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m8E4120C1066AB1ED7C49F5CE7C13BC58FA1982EA;
extern const uint32_t g_rgctx_Promise_1_InvokeRejectHandlers_mEDBF900AAE39AAC955086AA1FF660BFA8CE1B4D7;
extern const uint32_t g_rgctx_Promise_1_InvokeResolveHandlers_m8037BA107DC396528C003BE97D48D7BD404DF91E;
extern const uint32_t g_rgctx_Promise_1_InvokeProgressHandlers_m24DCAD8C34BA6FAD43AE9CD0153666BB29BAF59A;
extern const uint32_t g_rgctx_Promise_1_Then_mC5EB2D9F687CD425B67635DCB2BCFF63823B932A;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__30_0_m3C158D1FB79DEB503B2013423106852BDBAE4646;
extern const uint32_t g_rgctx_Promise_1_Then_m351D23121005C2D9BFD709EC49C65C3D994633AE;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__31_0_mA350EACCBDABD27A7260E91626F1C315C08599B8;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__32_0_m847EABCFB0BEEEC1624BA819CE52767462839E7A;
extern const uint32_t g_rgctx_Promise_1_Catch_mB259A6353D0E07A38E3A73DE95B57CBDD6B8333A;
extern const uint32_t g_rgctx_Promise_1_set_Name_mE05FE843995858A11E5C1E8BBF0C8322434B638A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_t8E426B106214B772E8A3541EF9EC9C6A5680EA31;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0__ctor_mD98A09D78F198B5556D40602F93C72D5FAF30204;
extern const uint32_t g_rgctx_Promise_1_get_Name_m1153DFC356E7D70BA4DBD4242F4C47049DAECC2B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__0_mBA206773F224E1AAA25ADA3749E7337D948EBD02;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__1_mA3909674F1A3FC7A55D33F6E7AD59E3877BDE640;
extern const uint32_t g_rgctx_Promise_1_ActionHandlers_m31284B2CCFC54C1B31E69F477BF1525E5442F71E;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__2_m6652B53044142F56A632224595882698DEBF69F7;
extern const uint32_t g_rgctx_Promise_1_ProgressHandlers_m73394BAF1D801B02BE6B771C128C54ADC598D2D4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_tF7AC0AADFAB885F5959E97A1B4B689D245862334;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0__ctor_mB9C7ECFF111031C1C49620792B30A8A39F00D217;
extern const uint32_t g_rgctx_Promise_1_t56E54FB8F556BA04FFCC8A406CE3E101700BC2E8;
extern const uint32_t g_rgctx_Promise_1__ctor_mCBF25BCB5F97D95FF955CE0B098ABC3F1F1FE13A;
extern const uint32_t g_rgctx_Promise_1_WithName_m5D45951D94596073B13A391CA7045697551B8A4A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__0_m45DEABB8DA64FF84840D27073C8CAC13AAB48594;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__1_m4125381DA464C5860B640144D34BC3012ACA74C1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__2_m50412EB5F09625053F1EDDE3DB5734EED9720362;
extern const uint32_t g_rgctx_Promise_1_Then_mA803252B5AD6DC573944E9D4C4AACCB7ADB6A35F;
extern const uint32_t g_rgctx_Promise_1_Then_m57ED7D60245EFFF2472BAD8EE827DF17FF8CD30B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_tC4BCE0874C3B6C80BBDD2972910D2F293BDEA565;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0__ctor_mF0C3B923395589B62C8132D17B369F0CC51F2727;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__0_mE24F66100D6B99A003C7F3F1602E9D0415AB3EB9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__1_m204A37108F3DE0FFA00EBE97B330818BAFBAAC0B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_t9EAC0603D2A289D8E19963B1E9D5311871E89CD3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m5E4D95DF597418E463F1E69B0FCCE7DBAE4590BD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__0_mEEEBCA8C69119AAD814EC6DEC48E0DF1E1D8CB4F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__1_mF458E25F706D196D0EFF31C1043DAA2E3D869ED9;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisException_t_m4D67C96D8455C7E7B7B3129676BD7B29BE7DC7A6;
extern const uint32_t g_rgctx_Promise_1_AddResolveHandler_m705919ECE972168EEE7E5CEE4AE73E60AA6BBA4F;
extern const uint32_t g_rgctx_Promise_1_AddRejectHandler_mDD2C5E3BD6ED3A21CB80C24DD739BA41600F89B0;
extern const uint32_t g_rgctx_Promise_1_AddProgressHandler_mC8B31A4B274528DA2D5C4FC9BBABC642E0573CA4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_t77289E059759A9DA1CDB59849D4163C50AFA627F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0__ctor_mD6F44450B7B11FB4D627456244700EACDE68FE2B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_U3CThenAllU3Eb__0_m6B85513FF99F8F08248F000B9173FABA4CF31569;
extern const uint32_t g_rgctx_Func_2_tD64F65719E163C592931EF84BC5BFDEC6E67549B;
extern const uint32_t g_rgctx_Func_2__ctor_m89B454074F8D9B805CBD887131EC1358AFA92C68;
extern const uint32_t g_rgctx_Promise_1_Then_mF64E8E8D7707A25A6E242AA637E55E954AA5691B;
extern const uint32_t g_rgctx_Promise_1_All_m0554DFBF6738A96E8E179E346704CDF8A8595002;
extern const uint32_t g_rgctx_Promise_1_t56E54FB8F556BA04FFCC8A406CE3E101700BC2E8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_t509DD7979E7FC1D3047F3D60872BC99C4612ECBE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0__ctor_mBDC80CC72675FC662C3331352D20AD433F487AA4;
extern const uint32_t g_rgctx_Enumerable_ToArray_TisIPromise_1_t04DD58177BD18C606F7035FA4373C19422242A39_m514F488DBF041D63EC4744EB8B2F7C2DE10EA19E;
extern const uint32_t g_rgctx_Enumerable_Empty_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_mD4334AC5D4350FF8082FF9C5253E360F7522244C;
extern const uint32_t g_rgctx_Promise_1_Resolved_mFC2C89D6006C84DA9B2D96A5BC38A07921984708;
extern const uint32_t g_rgctx_Promise_1_t15C5A001A293F5A05FEB00815FB7884F3E8B2311;
extern const uint32_t g_rgctx_PromisedTU5BU5D_t014E4B7596216061C37AED87215DE9365E6D994D;
extern const uint32_t g_rgctx_Promise_1_t15C5A001A293F5A05FEB00815FB7884F3E8B2311;
extern const uint32_t g_rgctx_Promise_1__ctor_m3B2C6E47FB4577323F38376F319337BEEDAC489D;
extern const uint32_t g_rgctx_Promise_1_WithName_m2D66C3B02F4D27AD61B8F572315421149694700D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__0_m79FA5A2911DB80BB7E9985A5DA4329C7C22EE465;
extern const uint32_t g_rgctx_Action_2_tC1D7CED234A37A6696526E459CE83A4A6F2E9F6C;
extern const uint32_t g_rgctx_Action_2__ctor_m8938F46DB6AC3845CD30E5E072AC30714FE50EBD;
extern const uint32_t g_rgctx_EnumerableExt_Each_TisIPromise_1_t04DD58177BD18C606F7035FA4373C19422242A39_m318F5C658CA153DFBC7FDD9FD48D0D76B69F9397;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_t9030E8A19DF2B1B236F17575B743327B851C24F5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_mD50158B986EDCA182933DC7CE5D5406E3DD7E3B5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_U3CThenRaceU3Eb__0_m330D20712D8E11316A7D371994AAC526EBAA88E4;
extern const uint32_t g_rgctx_Promise_1_Race_m7862B9B5EE6815EB2EB754B55FA16B9B9FD6A3B6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_t0D50D883F5B808B7EC814BF9F895C60C48FEDB67;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0__ctor_m4318FC299367F3560CF9588290A10F09D6910318;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__0_m915E2CD1CE4234A81DAB104435EF4DED33ABAB57;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_t82BA7D6314012526ACB0CD8F52D288FFC405F76D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0__ctor_mE5A6B551CB508382D2B4B951761F0B5EFB3DE2B3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__0_m435C57261DD0C448718BEA5805C1C492453FC12A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__1_mBE74DD9794F68839C7ABE1802F4D2A28AA2E831F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__2_m9466ADE2593700280DC306793E1B20C231EFDFA3;
extern const uint32_t g_rgctx_Func_2_t4AEC34429EB32588A2FC5F27725A020D87772CB6;
extern const uint32_t g_rgctx_Func_2__ctor_m072F09A4A5BDB958B09EC49E12C4814B4CE7100B;
extern const uint32_t g_rgctx_Promise_1_Then_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_m1F0736B7410975D7F21854F97F3F6719012608EC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_tAAB9B6020FECD3BC21B0941896BA6A20FB087F91;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0__ctor_mFFC1EBC80D6CA14EE701BDFA98522251F78611A8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__0_m3A02F375858137C471ACE1F06B454C48881B68E5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__1_m1B21FB1D2BD3739B28D2C47A628517B2F20817D3;
extern const uint32_t g_rgctx_Action_1_t63896218AC9092F717493E83FD66E68E0024B3B8;
extern const uint32_t g_rgctx_Action_1_Invoke_mB5390D8AA578A89568A1F1DAFDC6E3876E7C4E06;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_t34BAB60F53DC587FBCDB2A30B79F54C23D7F83E7_m925A00ECCEDE3EAD6EC13C8610F3FE157DB6465C;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tA6D16B1D6E321124B78C0F7AA397583C27BED00A_m2C9DE26D6DE5E6F3E317AB99A9307045C26B1443;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_tF86423FC8EE6694CE27620BCB6B2818BEB6BFFA9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1__ctor_m7D063B713F12B475E1139BBFAB5992B24EBA710D;
extern const uint32_t g_rgctx_Promise_1_t5255F0E15D1E0CEB90BD5C7AE4EAA854417F7C0B;
extern const uint32_t g_rgctx_Promise_1__ctor_m6ECBEC288C2D18736729F79C701583F98EC0411A;
extern const uint32_t g_rgctx_Promise_1_WithName_mD1914415CCFD087A74AB5FF2C9835B02EA2C1095;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__0_m8308079D6E4E8931B82A2D0307CB2833B7A3D8AD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__1_m9EA9D1E199E92A56F7E38C3F084C0058203D7DEE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1_t5CF996FF708D9DE726D4DCE032A934B306ECBB39;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1__ctor_m5B6FCACF8F27B52BA10BBB58F9F9ED91B9B37179;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1_U3CThenU3Eb__0_m5EFF28ABEC6A04E3FFCD70AF8BAD1CF9DD3630CC;
extern const uint32_t g_rgctx_Func_2_tF9C7D29B88C7038AF683DC2CA7FF7507F15EF718;
extern const uint32_t g_rgctx_Func_2__ctor_mA29ABF1F8CB21D8BE78BF45A8FCBDDE2CABB0E1E;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tE144C479DA3777F795811B5452F199702CCA7691_m287D9AB00CA6E0A79B8868A7CE2FFC5CC295BAD8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1_t2F2EE9AB6CABF4489615C3051AAF1952B2BCF0FE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1__ctor_m6BD859B4E5D6423CAB621C6D8B5BFFF0E36919DE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1_U3CThenAllU3Eb__0_m59F3E3FC16056E5D3A50872B114E72C074028AEA;
extern const uint32_t g_rgctx_Func_2_tFB524A62E008BE1167DE908FD63D4BB401719991;
extern const uint32_t g_rgctx_Func_2__ctor_m6D8FBADB188267CEB3587BC2DCAF9568E50CB910;
extern const uint32_t g_rgctx_Promise_1_Then_TisIEnumerable_1_tD15C7AD5C07EEA60C2CE1D04FC23C7BF04A4BA97_mBFE51F76AF0E1C0AD185DF50E9769C6F7869B7E8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1_tA1F86245D7DCB37F27D7A01995D10E7EA25F0D97;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1__ctor_m9D3427A50412AF4A872257597BFFFED9BCB19E4D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1_U3CThenRaceU3Eb__0_mF1B7C16A00850A6570FCCC7703C52951EF84D638;
extern const uint32_t g_rgctx_Func_2_t9E0E3968305092F8BB813680B8C969A3505CD12F;
extern const uint32_t g_rgctx_Func_2__ctor_m6FF25AD2BE82929A2DC13CA5A1E14320B531035B;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_t8AD84B13E874A5A747588B109768637B7D31333B_m1D25EF64FD15532E356855D9A9382FAD417F655D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_t038FE7B2EF68DCE39F45562BBDD88510BB7414E2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1__ctor_mF10C4E080767C4708448F4A60F78BEA5EC71FF8F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__0_m97693BB9B63AAD3D3DC6567237E9074C2A8F79FD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__1_m910ECBAFB49BEFFFCA709B659DFBD8DD11EC475B;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t82E73B815327B7AEF0E946E40CF7A5B7696B25B4_m87B0871CDDCAD65FBD49C432E93457774CE1942C;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisException_t_m3EE6FC70D838573130C5708DF3EDAF2512276BDA;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m3C3503B1D0EE62363DB6DE07B418C2F1470D508B;
extern const uint32_t g_rgctx_Promise_1_t60B7A1F293BF46FF9BD7402AC8AA0BF40BCF06D0;
extern const uint32_t g_rgctx_Promise_1_Resolve_m0B79CD309271E0E6AEE86E17A3DF70635A2765D9;
extern const uint32_t g_rgctx_Func_2_t1965BAD3D6C398471ACD39B801B81B3CEB9BD802;
extern const uint32_t g_rgctx_Func_2_Invoke_mDE0EF600722100868610D7A39BC74DA6A526A48F;
extern const uint32_t g_rgctx_Promise_1_Reject_m23878EEB98D918DD499A910242DF2EF84933A10B;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m8D4BF61D293D9C53B2BFFA94B63924ACF2C209C6;
extern const uint32_t g_rgctx_Func_2_t2BA9541290971BB435A69F4FD2F15C7977E3021D;
extern const uint32_t g_rgctx_Func_2_Invoke_m8A090964F7BE7E8A17D7B19AAA12CDF789258A90;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__2_mC88E63024B265B17EE5422A7513FB806D9FDD7B9;
extern const uint32_t g_rgctx_IPromise_1_tC35C8EFF46E3D1A9A6C312A971D8EAB5CAFD0B14;
extern const uint32_t g_rgctx_IPromise_1_Progress_mAC6EBAAEAFA919583FD711FFEFAF031AE3226F2C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__3_mF362CE4A2E397E0F4D357D8BB8C83FF2290A8E1F;
extern const uint32_t g_rgctx_Action_1_t3864777002109E4647D9C1A4246C916EC2D1D5F1;
extern const uint32_t g_rgctx_Action_1__ctor_m4C1041105590B2A4DF6FD3376AA29E20797C3869;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__4_m4DEDD8E453CE66C717A191D813022D90252CE0E7;
extern const uint32_t g_rgctx_IPromise_1_Then_mC7D65767132CA254450122D1C376773EAEF58AB8;
extern const uint32_t g_rgctx_Promise_1_tA64B52BFB094F9291FDC9CC0D88308105D859276;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m2D0AE792A9B6188B5F5FF0184A88664B165F1364;
extern const uint32_t g_rgctx_Promise_1_Resolve_m694CB7F4DD1C616398373052C21FDA5A9B272E64;
extern const uint32_t g_rgctx_Promise_1_Reject_mF553BFCCE61205F7D544ED6BC3C9C04C1521F919;
extern const uint32_t g_rgctx_Func_2_t8203D3DCC331520D5F653153F383D7043D95EA26;
extern const uint32_t g_rgctx_Func_2_Invoke_m0C4E2EB33D5B7AAB2BB53D13D88C37CE5BBCC75F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__5_m53DD25776F6AF4131B7D835A1A9FE8DCD261D155;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__6_m4A727A850BB9E9335395163D5AF43763DF615395;
extern const uint32_t g_rgctx_Func_2_t17D725907D8AEBB16BF95B2046F5EB7D7C8BCFA5;
extern const uint32_t g_rgctx_Func_2_Invoke_m95570D123A7B7F31F0EAE917F2E7297CEACF35A3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__2_mB38C6B4547CE8906D7851809310C6E4835B815BF;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__3_m67552772743B58CAB0D30B1FC4CBD61E4BA7822E;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__4_m5C2908A45336203CAD96402E410785709953774E;
extern const uint32_t g_rgctx_Action_1_t30F2346E6013ECFF366EB5E1427C985CD1B00282;
extern const uint32_t g_rgctx_Action_1_Invoke_m2C53E36CBF534B02D1B4795DB2F788E42762AE22;
extern const uint32_t g_rgctx_Func_2_t6125922899DCB1C45BF0BC6DB6F20A8DC6DADAF0;
extern const uint32_t g_rgctx_Func_2_Invoke_m28E04CBD3A575A076A227E1C1C7D640929934B3A;
extern const uint32_t g_rgctx_Promise_1_Resolved_m254A7ADEB97DCA73ACC71D77049A5B91B07BB412;
extern const uint32_t g_rgctx_Promise_1_t7FADE30B306331272C2AB24A4BE553EF72C386F9;
extern const uint32_t g_rgctx_Func_2_tAD049E07DA4410007492A4ECFA63104A0F6FF05A;
extern const uint32_t g_rgctx_Func_2_Invoke_m632DA3FB1A5D271BF639E69050F60556588C9B88;
extern const uint32_t g_rgctx_Promise_1_All_m1CC851AC51F482D4D74330BB37C1CB8A763AFA8A;
extern const uint32_t g_rgctx_Promise_1_t6F11A74E7D5BECB1280AE4E8FE2464B1E9010B88;
extern const uint32_t g_rgctx_Func_2_t8B85DB329F21FDC6C049529A3143E38063F85220;
extern const uint32_t g_rgctx_Func_2_Invoke_m061521CE7CA9C290F007057E02929879C7FD72B5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_tD8569019861D72A592A05BC507FAE15D8EFB452C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1__ctor_mA9B8E5CEF33264EF28CDD1BF367F29EDF384E30A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__1_mCBE594F63CDAD922CBEC7FB422952A81924B4F95;
extern const uint32_t g_rgctx_IPromise_1_t9B6B47B73C0984FF1F8B1A36B727776105DF3FAB;
extern const uint32_t g_rgctx_IPromise_1_Progress_mE6929C26A78BF4A34B5026261CD2ED46FD8AD181;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__2_mAF1A72B3D77069929B366A067BA1EC23FE547EA1;
extern const uint32_t g_rgctx_Action_1_t229FF9D31B37A798A740020F44B5FC8A7FACB8E5;
extern const uint32_t g_rgctx_Action_1__ctor_m63B095E707A31F6A8F54A63785431A90D74B36FD;
extern const uint32_t g_rgctx_IPromise_1_Then_mF7BA63F497D3C0CC94F9DF5C86730B4C6B5AF58B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__3_m50C7C250C080C09D129B3818FC7197485961D2C0;
extern const uint32_t g_rgctx_Promise_1_tF50B4C2AD0E16D7D16D80E77A401E9CF2540C338;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m560E111D1D2F3BECE40D99574BFBD892E29D9EE7;
extern const uint32_t g_rgctx_Promise_1_Reject_m294179109D3F485238BD735AA715F67D08B24066;
extern const uint32_t g_rgctx_Promise_1_tF7594D8D4C06B149386BBFE3F1B1F1E7CD705872;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m274C24AB0B30BFCF94A02D6F9318CFC021436911;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m6B874313296E8C906D880D80B34EB01D6B4D65A4;
extern const uint32_t g_rgctx_Promise_1_Resolve_mCA96FE39CD9E6C364485E42BB421D8F113B2EFA5;
extern const uint32_t g_rgctx_Func_2_tBA2E054C20919DE3AC8D663D68CD123DE42FC1ED;
extern const uint32_t g_rgctx_Func_2_Invoke_m20E0306F69A39CE04643E077590FA0FC3310A9E6;
extern const uint32_t g_rgctx_Promise_1_Race_mBA973C818F63618C9410522F207E060BEDCC0718;
extern const uint32_t g_rgctx_Promise_1_tE772739ECE3C1C7EABE7B71F57693434BEEABF5A;
extern const uint32_t g_rgctx_Func_2_t9C719C6A419791326B88DF3E81CCDFC639455A5E;
extern const uint32_t g_rgctx_Func_2_Invoke_m2A78A052305BC5E95AD3983A519A0CC49B853B11;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1_t042B6173BC68A2B84FCEA48913270FF12108637A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1__ctor_m4C285BB45ECC3838AF1BF48A6C84C9F6E2670902;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1_U3CRaceU3Eb__1_mA97DDD75AC67F1B6AD6390E607A00BA7D358C810;
extern const uint32_t g_rgctx_IPromise_1_t2E4D8C1904F5D5369D8B2E68816457659F38A969;
extern const uint32_t g_rgctx_IPromise_1_Progress_mC2DA7467091B83EB0BC51B5CF3445C094E9AEB00;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__2_m59C97D6EF0E7F2781FE2A1B5D1FDE5B26D73504E;
extern const uint32_t g_rgctx_Action_1_t1336D68871B6159C21DB7404F172C600709F8282;
extern const uint32_t g_rgctx_Action_1__ctor_mCBAE60593322D2F7C2BBD4ECDC422B4A36F8B9ED;
extern const uint32_t g_rgctx_IPromise_1_Then_m61305AEE9F223A8CB6E704A1C690A7D681A41E4A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__3_m233E69EDA2E5C391C26E4C32A35AEA9F041B3259;
extern const uint32_t g_rgctx_Promise_1_tAD71F27C8C7E593ECF8B592F443EEE626CCFD776;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m025BB0C5E4D42609690AEFBDE4C1827C4E9AE6D8;
extern const uint32_t g_rgctx_Promise_1_Resolve_m49DCA646171DDE1229C54492F26AC8AC93E3EA05;
extern const uint32_t g_rgctx_Promise_1_Reject_mBBC2AF89BF5D8720CF44BACC883F0E05537C3F10;
extern const uint32_t g_rgctx_Promise_1_t35CFCF5AD6C704380843B4FBBD4D881C8C1361D8;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m3B45BE6B57FC50A18D3D3CB7D593F556553CE422;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m2CBE2740D0A073B322A9ECAA12FAF80E785FE4F3;
extern const uint32_t g_rgctx_Promise_1_t28053214A79EBE6D664670826D8978B5219C0251;
extern const uint32_t g_rgctx_Promise_1_Resolve_m6784969C94ED98B49B5AA889EB7CFF599DA7E174;
extern const uint32_t g_rgctx_Promise_1_Reject_m1402961BFCD84347378498F27743441BADE249AA;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_tA5DC4F913BA7B289659CAF845184EFC7EAE3FBF2_m666BF22058423E4813570A3794A8970886F5EC1F;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_tF68B073F7C56CFA6BF53F8971E2EF0C6C010B29C_m025DDE30BC9D16B4289C143B2A45114FB4396EA7;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_t15DCC4EE58459B2FE455DCA330EA57A1E4473992;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_m3505C1B585D3D66198C0787FD6D67F44CBA6C782;
extern const uint32_t g_rgctx_Promise_1_t4E57D7B7B959613474B3A7E04BBFB8CAC5107AC4;
extern const uint32_t g_rgctx_Promise_1__ctor_m027A90E6360D1D0CF305B1D92942AB66B828AC46;
extern const uint32_t g_rgctx_Promise_1_WithName_m3D6BB0CE85965D38E2F81B12916E33C4E6995FC9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__0_m73F579FE4D837D0EAB1A6A853E6503FB102C1A91;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__1_m26C12B76F155276090F6AD1490AD8CE754BBCCB4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1_tA84878B6538427D3BFC73C2A98AB737479AF0B50;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1__ctor_m7E7CF12B27EE8C53B2310EE4D7B4E65F79B5BDA9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1_U3CThenAllU3Eb__0_m19FBE320A63C6ED651AE866F4E5900107AB9FEA8;
extern const uint32_t g_rgctx_Func_1_t7C4408B8C34C3F6B6808DA99CE44716E5FA19CBC;
extern const uint32_t g_rgctx_Func_1__ctor_m91DBE9D0176EC2DA1F78E6ADEE3E24573042F102;
extern const uint32_t g_rgctx_Promise_Then_TisIEnumerable_1_t5DF2349BABF448A9B240164D0DC069E4E8DC452D_mE2362615A8A049B96609138329F95329C5AAFA27;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1_t1E99DD52A17E7F593ADBC5A3D37E1A036DC34278;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1__ctor_mD0344CA5ED4D8B81C7183E79669EB565DA4C4695;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1_U3CThenRaceU3Eb__0_m2448B1751F69D3CC0825B24C1D185CC75051EBF0;
extern const uint32_t g_rgctx_Func_1_t5A8D42340E329984DDE142BBDF96F3035BF74B7A;
extern const uint32_t g_rgctx_Func_1__ctor_mB032AD7606E33AD56F1D44F0F558E384C1866EAA;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_tBFF1C3C5256CED24364FF8675BB99BAFF02714A9_mDD500132BB441A51BDE6A98C141786C9CE63DACD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_t70D8216C44443D31C5A516A181256A10E41AA379;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1__ctor_m3C407913426C389B0CCAD56F5C2609A243227D7E;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__0_mB1F018729FF2137EE5E03481E6E7269FCFE91818;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__1_mA0138E1844F8BA07534BB26306FFBFB4EB3A2146;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t5B5D4DEBA88921BC24922A0F2983621D9B91BF62_m31417B7E0C6A3B1BDA42620061A3CD3D9A323F5F;
extern const uint32_t g_rgctx_Func_1_t00BF45EB0E9DE0CE17D90A2D223435D7209AC6FD;
extern const uint32_t g_rgctx_Func_1_Invoke_m06BE6E5BBFD4998418CEBF00E6C23D102C165392;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__2_m4B41960A0088CA03E6889DE9B50219D02A088109;
extern const uint32_t g_rgctx_IPromise_1_t5D35A12CB4E16457C54B58D1BEF4B75403191979;
extern const uint32_t g_rgctx_IPromise_1_Progress_m858E0658301EA7603E1C0FE5E444CA364237123C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__3_m4102838CBDFAB10959EC95CA9FD791DCF925BB01;
extern const uint32_t g_rgctx_Action_1_t83FD1BAB4E4097843CDA6464877A2445085180D0;
extern const uint32_t g_rgctx_Action_1__ctor_m02B654E1E0CEED5C7D3CB896393327DA40906C59;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__4_m2EE64CB91DE0AC57E3CD5A5CA6C41427EC7C64E4;
extern const uint32_t g_rgctx_IPromise_1_Then_mBFB7BC603CB3887FA17F0BA446EAD3C2075DBBA1;
extern const uint32_t g_rgctx_Promise_1_t525078C4D837F926FDB426442F1224A8634D3ABD;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_mED99D1972DD8B070EA6A4F6D513DD365EFFEFABF;
extern const uint32_t g_rgctx_Promise_1_Resolve_mC6F6BEED72A4F05EA35EB05B8A878ED9613341D6;
extern const uint32_t g_rgctx_Promise_1_Reject_mF752D76DB5886A93ADAF6916F9FB8F6B25FC7706;
extern const uint32_t g_rgctx_Func_2_t184BDCE450B765EBE1F9DD22F5CBE983D1E33430;
extern const uint32_t g_rgctx_Func_2_Invoke_mC041FD9C0E91BEAAFDB6557DC3B0295351AA9844;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__5_mE8BE0C5FAF7A28FC75D5F5C5A307A52B754D95F8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__6_m8584B0D0E7315C22806B36F71E676D95F0DE879A;
extern const uint32_t g_rgctx_Func_1_t68401B4DB6835D1065CA4A696B0D7D5E19FA71E3;
extern const uint32_t g_rgctx_Func_1_Invoke_m677C8663C6C364AA0A082046AA848F8A4D873E27;
extern const uint32_t g_rgctx_Promise_1_All_m2ED17A7BB304F2913BBE86BBA3C9CECA9E6FBD3E;
extern const uint32_t g_rgctx_Promise_1_tC43AB0F3CA9267473A98C507CB47DC8D61683090;
extern const uint32_t g_rgctx_Func_1_tEF77E7A193B4E162F8069297E2530C3DE2C5B5E5;
extern const uint32_t g_rgctx_Func_1_Invoke_m77F5CF8326911ED0F8A549F90ECD6709F62436ED;
extern const uint32_t g_rgctx_Promise_1_Race_m08418CD68DA27F19B584DA12D879CDD45E80E636;
extern const uint32_t g_rgctx_Promise_1_t99E8026181408EEAA0C6E7E88F79788362583CDA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1_tE1FF409FF32D2AC27EB23F8A5AB043530AAE581F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1__ctor_m31B5AAFBACEBFC25E756ABC12A020FC08EEB9ABB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_1_U3CDefaultUnityWebRequestU3Eb__0_m2FD52FCDB37243E5A9BDE3E7327895E5AB4293BC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1_t0B78C8EDA2742AD3DD860A4B4AFB7AF30D9B630F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1__ctor_m0B5B1F4D629DD071DD78F882B28731BB5C194F27;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_1_U3CDefaultUnityWebRequestU3Eb__0_m50C8AEDD37C3945C45591BD17DC25B712657FE80;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisTResponse_t6C41777F102D933C7F1DF524510EEF4CD68A00FB_mC155C8F1686494C341055827A4B5AA2C7D03F7C6;
extern const uint32_t g_rgctx_Action_3_t3CDF48C251C5EA97062E701FA2672DE2E20C766F;
extern const uint32_t g_rgctx_Action_3_Invoke_mB2B8FABABCD5E0B0AAE6DCBC6E9C712792BB2222;
extern const uint32_t g_rgctx_JsonHelper_ArrayFromJson_TisTResponse_t53B25A53786EA118912D0F036DBEEEE13118D8CB_mD6596516BF54895ABD9A965E48B9864B391CAAB9;
extern const uint32_t g_rgctx_Action_3_tA4E3BAFED40CD84EDF1EC29EA884C13188C8BC27;
extern const uint32_t g_rgctx_Action_3_Invoke_m428AD86046F848E56B4E32BFCCD9B110B93CA265;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisWrapper_1_tB5A88805CEB6B27C6010A0FD729ADC0C84974C5E_m2B068EE44A8206C9FF568DB0272A47C6CFE20D95;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisWrapper_1_t1ACCAB4C7C5F8802465A6258C211D055DB2905A1_m0B1CBEA5A9BBFCE2C7320E786801523EF09F1C02;
extern const uint32_t g_rgctx_Wrapper_1_tB7A173F6F821010BC04DE9C99571F42669AA9EAC;
extern const uint32_t g_rgctx_Wrapper_1__ctor_m01A44CB7915084AE9851868D516EAB2C7443898E;
extern const uint32_t g_rgctx_Wrapper_1_tB6A5EDDF775ABAFDDAD805FAA669E7E1D8219098;
extern const uint32_t g_rgctx_Wrapper_1__ctor_m8A8450346B9CDFBB4F39C178323EA2822C88FC59;
extern const uint32_t g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_tD117B4A8ECD6BBA8F4E4C69B4BB01C15DC047A0E_mD79D2E7990F1EDD6EE791D9932D8182BD0AE4A37;
extern const uint32_t g_rgctx_RestClient_Get_TisT_t83867E9875F3DF048E0A67465AC3942CE3447990_m17825CC4AE9AA8AE2AFF4FE618E165B8BA9FB1C8;
extern const uint32_t g_rgctx_RestClient_Request_TisT_t886A7903AE708C78F0A6FA63699344C0162421A6_mD46070E8872F782137A601D6DD870F47D385FCE0;
extern const uint32_t g_rgctx_RestClient_GetArray_TisT_t634BDE1A0290F8C7529053DC6D32F6D79028D554_mA85C96523073284D5D47438BFC8E7AF31CE45469;
extern const uint32_t g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_tFE3EDFA8DC4B5268DAA2D1EB30AA49A6A1C4543F_m805718C9C8675E2BC8C7E88CE7F19E72D1057D8F;
extern const uint32_t g_rgctx_RestClient_Post_TisT_t08F22C87BE1C4818613ABEF113436BCF5C850767_m65D71BEA0C1D7BDC9DF009BB91BDEC2CDB982B52;
extern const uint32_t g_rgctx_RestClient_Post_TisT_t128CFB107C0EB6D9FC21B202286E6882316B98EE_m3BA7A684BB6DA7126E21EC23A75B74D720697FB4;
extern const uint32_t g_rgctx_RestClient_Request_TisT_t124257CB75A94512115D1543B1C94112755F1D1D_mB596C5E2704B8B801E2D6B80E1A0D351E8ED1BA4;
extern const uint32_t g_rgctx_RestClient_PostArray_TisT_t1A02A9BC30840725729D98FFD99035FABF7E6CF5_m6B5533400EA9655C24044367BE24F84AB2E9F38D;
extern const uint32_t g_rgctx_RestClient_PostArray_TisT_t602D8D6576954DACA96808F9B2B1E84C98EDD098_m944E5821A83BA156DECB97727CC2E8EEA6FCC8FD;
extern const uint32_t g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_t504B3C0151E363A5D03DB1CEC8152895A62479B1_m018C4493E808939605ED0764E316ABFAEB02E3F4;
extern const uint32_t g_rgctx_RestClient_Put_TisT_t4D27DA095511CDDC56BD3EC72C499428ECB5DFE5_mE6F5EC854C85F993A7C3282DE13D93331CDD0245;
extern const uint32_t g_rgctx_RestClient_Put_TisT_t52910655884241C13B96DF4E8F202F4111F3FAA6_mC1D263D925AF3F4670135D5B2A2D858B6B8C0151;
extern const uint32_t g_rgctx_RestClient_Request_TisT_t88C3194A1D53169B4A79A46C74B5C930E4E2E53D_m35D31AB8CBAECF8FAAABB0267EE301819D79094E;
extern const uint32_t g_rgctx_RestClient_Patch_TisT_tEDE46512BF858D7CB7BE1F5B1EDF4C5E7E47D945_mEB0061CDE11F00F9C6131C52B86EC2B9186C290C;
extern const uint32_t g_rgctx_RestClient_Patch_TisT_tAE0C049D764B2E3C603F30267FBB5631A73AAE70_mFC3878A90700293BD59BB6EA8AF169758902F58A;
extern const uint32_t g_rgctx_RestClient_Request_TisT_tB2649DD63D7F01088CD62F61DF6F1E3C3C44CAFC_mC4ECE2C0D39D0C956BC54162BA9DFEB2C735E003;
extern const uint32_t g_rgctx_Promise_1_tA4E39A9F53C23634EB4C0E526ED9EC98C61F3683;
extern const uint32_t g_rgctx_Promise_1__ctor_mF738E2689DAB1FC7495C61E28AB4174783804747;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_t913737CEF85EBB264A13153F34F0B0A664A6C9E3_m674C9BF6CC30CFB8A5F0C0E18FF721E3EBB3F938;
extern const uint32_t g_rgctx_Action_3_t5C6EB3D49B13267E3011CC926CE37A7B4B29A45F;
extern const uint32_t g_rgctx_Action_3__ctor_m1801BC4A951C1B09CA10F1E23C688283C90AA493;
extern const uint32_t g_rgctx_RestClient_Request_TisT_t913737CEF85EBB264A13153F34F0B0A664A6C9E3_mC80DDEDF1437BDB53C0A1954D99819A7D9307417;
extern const uint32_t g_rgctx_RestClient_Get_TisT_tC93B0D8C6B668FC459794F878286B9C7E6DC43E8_m54D9E42B6183DF5353CE6E80258FC9AA0537A98B;
extern const uint32_t g_rgctx_Promise_1_tD1970B50BE83D54E3A42860D750034AE526A2152;
extern const uint32_t g_rgctx_Promise_1__ctor_mE5696C67A7F6CD9D0BB550C6E86F053F26058114;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_t150C6C63169BD78D8C25F931706841F84075062E_m05AE171FED89D2C42E7B5155B2DD3B46ECA20E8B;
extern const uint32_t g_rgctx_Action_3_t700F7D20BFF3DD7C8F859B5C53BF5969A5EB8E97;
extern const uint32_t g_rgctx_Action_3__ctor_mA5BCEB450F2D1AE843CB769602813B75B34999D9;
extern const uint32_t g_rgctx_RestClient_Get_TisT_t150C6C63169BD78D8C25F931706841F84075062E_mD30DCF07EEBC5BFE07BC7ECE831D591FDDD0280C;
extern const uint32_t g_rgctx_RestClient_GetArray_TisT_tF1C93AF8F35F07B08AEE40BF59300BBC3007370A_m9AEFCE65CDC7A31EEFEDB1DA823EADCC859437CB;
extern const uint32_t g_rgctx_Promise_1_t71E3D6719C508581C2E361428EFB4D2178A9961B;
extern const uint32_t g_rgctx_Promise_1__ctor_m9DE3F05AAD40BEC6467CFD476F970747E5A7D8DE;
extern const uint32_t g_rgctx_RestClient_Promisify_TisTU5BU5D_t99EF92CB84B68CBBB46B79F55EF3A42AA7A8AEF8_m1676B20B638C40E5C780D2810D9F705B02DD9671;
extern const uint32_t g_rgctx_Action_3_t4CE18541EDBCDA2990A5527943074DE871E12321;
extern const uint32_t g_rgctx_Action_3__ctor_mF11E59F4A67E04BB6B1241A425BE27A25A724AD3;
extern const uint32_t g_rgctx_RestClient_GetArray_TisT_tC4CFD3277336A9153938A2B25F4D147A556D4716_mB265F39C3A6706B39D60774B679CA6BD9200F2EC;
extern const uint32_t g_rgctx_RestClient_Post_TisT_tBCAAF676D75733A088629A5360DA6DE6E02C39EC_m1571E9D10A5A26E49F45AEF0673BA4CE68B5AF4A;
extern const uint32_t g_rgctx_RestClient_Post_TisT_tFDB9EE274A55F769EB86068702ED462E9309C292_mF1CA9C2A2754D61BAEECF1CEF9A10D714F8E887A;
extern const uint32_t g_rgctx_Promise_1_t7E701B2883A9394339A3189971F920D091FC4E69;
extern const uint32_t g_rgctx_Promise_1__ctor_mFF371CD7AD0852387407EE3C1BEA0F617565FF2E;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_t784E9562E468EE1A86103C30A61F693DEFC0BB11_m0911B4197183F9D10A3CF557CD743E5761ABBD25;
extern const uint32_t g_rgctx_Action_3_tCE1FEFD63C57F2D3D63587C71980D63310F25C11;
extern const uint32_t g_rgctx_Action_3__ctor_m954EE0BDE52363686DBA738EC893A5BD7E6ABCE1;
extern const uint32_t g_rgctx_RestClient_Post_TisT_t784E9562E468EE1A86103C30A61F693DEFC0BB11_mB2F9CF15E7DC37C61225BCF35E2CE7F0AC0E1906;
extern const uint32_t g_rgctx_RestClient_PostArray_TisT_t72120E9822B25A6F139E776671046FC43D9B1292_m8E1B922FFF9FF8E2F38E71635AA5CE93B20219CE;
extern const uint32_t g_rgctx_RestClient_PostArray_TisT_t3448E55C1F62481700E36C8567FEBD383814B2C9_m7C17D45151FAB396F9461C1E04588BF5F74B7A6B;
extern const uint32_t g_rgctx_Promise_1_t427E76746704B851FBEE3013A0BAB354D3F51239;
extern const uint32_t g_rgctx_Promise_1__ctor_m674540B8910F618253498CC5C2BF9ECBE8C1AE16;
extern const uint32_t g_rgctx_RestClient_Promisify_TisTU5BU5D_t58093F002DC7993D22B92841E0F73666C9A8E2A9_mB0EA6FCFEF33A7EBFB19DDF490419C92AC039BC7;
extern const uint32_t g_rgctx_Action_3_t3DC78AF8FBFC3C76DAEA6A04C926C594C8833CF7;
extern const uint32_t g_rgctx_Action_3__ctor_mBB8D4D53BD3465ABAC0B6B9509CAE03C8C8AD5F3;
extern const uint32_t g_rgctx_RestClient_PostArray_TisT_t29E3B231F1968F195836DA003DF1E2B982DE6EB9_mF7AE51EB8352DCB114D2E7075EE57B1C0CFBCABB;
extern const uint32_t g_rgctx_RestClient_Put_TisT_t4390DC7B1DB61EE8C7DEED85355FF79101A65803_m534DE450FD51168478712DDCE2F534F6B0610540;
extern const uint32_t g_rgctx_RestClient_Put_TisT_tBC3AC6990E9D908D053850D2C431BF68AAE9F2F8_m44CA3E7DF19768266013B890722575B2643F5658;
extern const uint32_t g_rgctx_Promise_1_tF9B00EAB3E9F7DF83E95CC8423850F2BC823DAB5;
extern const uint32_t g_rgctx_Promise_1__ctor_m80E6C8395930F29BF5B560CC10B7AD15CBE7BCE4;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_t3F2C3B0D59553D1E97B89ABC12533A568CF6DCF6_mE90CDF54D28B9BD79AE6CAA3BC5915758A79912A;
extern const uint32_t g_rgctx_Action_3_t08C7302D7A4D7A04C036BCCF199C86BCDDA0259C;
extern const uint32_t g_rgctx_Action_3__ctor_m5B29FB5D1863E55B91F2F5509D3BE20982FE55FC;
extern const uint32_t g_rgctx_RestClient_Put_TisT_t3F2C3B0D59553D1E97B89ABC12533A568CF6DCF6_mD308F8DCC6E0FB33506522935BD489D34A618457;
extern const uint32_t g_rgctx_RestClient_Patch_TisT_t29ECB8B08A7D9721744075CADBDCCD34EA53CBD0_m846A63A4F3F8CC938DA80DAFCC01879CA453361C;
extern const uint32_t g_rgctx_RestClient_Patch_TisT_t4CAA10D51B75057BA4E6714574B12DB27D51EB9E_mB7CE2919FC5CE70E2E590BB5C3E921AE0E45D9A2;
extern const uint32_t g_rgctx_Promise_1_t64658469516DF0CC31E705DE0EDE1140C5EE36CC;
extern const uint32_t g_rgctx_Promise_1__ctor_mBA6590A2E05ED03E30F6F0F3BF83516A0C80AEDF;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_tD6235DCD8CA7C0B177480A1E23DBA1665C0DB266_m40A638C5C6D800B815C3A74DDCB9232C6BDD47ED;
extern const uint32_t g_rgctx_Action_3_t86277884127C5A70F713D80D9AAA122A756085FA;
extern const uint32_t g_rgctx_Action_3__ctor_mFC24507F516841A89944DD9D69318DFF037F9E5D;
extern const uint32_t g_rgctx_RestClient_Patch_TisT_tD6235DCD8CA7C0B177480A1E23DBA1665C0DB266_mA1807590CED60ECEEEFC36B078E925D125A56456;
extern const uint32_t g_rgctx_Promise_1_tC1E77D9C2661BCEA873BCA3D20591513BEDC7A57;
extern const uint32_t g_rgctx_Promise_1_Reject_mE94C583E133561D296255DA6CC2BBBFC9F7CA1E0;
extern const uint32_t g_rgctx_Promise_1_Resolve_m26AFD09E25CA1EF74CD724E59BD78D71DFFAA826;
extern const uint32_t g_rgctx_RestClient_Promisify_TisT_tF028780809602AAEE5613612A20639BA635A67DF_m66AFFABE041A223DD28BB963880E34D00F2B1A4C;
static const Il2CppRGCTXDefinition s_rgctxValues[365] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_set_CurState_m7E1BE6636D58589840B5EF9301691C2F0BDAE702 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m9D5548FC044C1528673C45043C86D46C005EDF21 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t8B16E288481B5BEB8453DE3471F117CD87D5ED49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m084088C1209422CA94DCC02738DB81A8ED8AC117 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mCD34FBC8039B8AA806DADBD5D0EA04E6A29998E9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t9892BBA1839B44CB122080480B48272B6EC46C8B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m0A916A2D7A7F32B765DC5C7F320EB0C85E25871B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t11D44EA446595843AA679F5DD864508C73415E38 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m02BB972F281B0889AB7EF614C1FF679EA12B68B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m4296F3D4A22A85575AECF05891393897DF8C985E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0_t1779B966B4B038CDE58E5DA298C212ACC61797B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0__ctor_m6B3483D47BC0F46EED8C1BF9AE280D02982E0504 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0_U3CInvokeRejectHandlersU3Eb__0_m3C80C0B112E8769E4AA5573408BBA672971C8612 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ClearHandlers_mD69BC795406B6C3FB69E880AEABC48A10640A35E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mDD52305D342E5CCB4E02663565381197E2C97A48 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m0545E30E4F578B9B71CCDDFEBD503C22BECECC4E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_m2B995DA9B2D442B57814C9F953CEE2CEED959832 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0_t67ED753D6602C2B68726D01B5784CA0CF8808735 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0__ctor_mEF448D58E8621ECB300BF4BB3ADE5BDD7566B9FC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0_U3CInvokeProgressHandlersU3Eb__0_m14F755229DAEADD38EAA73AFF884E05885AA2FCE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m8E4120C1066AB1ED7C49F5CE7C13BC58FA1982EA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeRejectHandlers_mEDBF900AAE39AAC955086AA1FF660BFA8CE1B4D7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeResolveHandlers_m8037BA107DC396528C003BE97D48D7BD404DF91E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeProgressHandlers_m24DCAD8C34BA6FAD43AE9CD0153666BB29BAF59A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_mC5EB2D9F687CD425B67635DCB2BCFF63823B932A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__30_0_m3C158D1FB79DEB503B2013423106852BDBAE4646 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m351D23121005C2D9BFD709EC49C65C3D994633AE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__31_0_mA350EACCBDABD27A7260E91626F1C315C08599B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__32_0_m847EABCFB0BEEEC1624BA819CE52767462839E7A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Catch_mB259A6353D0E07A38E3A73DE95B57CBDD6B8333A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_set_Name_mE05FE843995858A11E5C1E8BBF0C8322434B638A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_t8E426B106214B772E8A3541EF9EC9C6A5680EA31 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0__ctor_mD98A09D78F198B5556D40602F93C72D5FAF30204 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_Name_m1153DFC356E7D70BA4DBD4242F4C47049DAECC2B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__0_mBA206773F224E1AAA25ADA3749E7337D948EBD02 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__1_mA3909674F1A3FC7A55D33F6E7AD59E3877BDE640 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ActionHandlers_m31284B2CCFC54C1B31E69F477BF1525E5442F71E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__2_m6652B53044142F56A632224595882698DEBF69F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ProgressHandlers_m73394BAF1D801B02BE6B771C128C54ADC598D2D4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_tF7AC0AADFAB885F5959E97A1B4B689D245862334 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0__ctor_mB9C7ECFF111031C1C49620792B30A8A39F00D217 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t56E54FB8F556BA04FFCC8A406CE3E101700BC2E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mCBF25BCB5F97D95FF955CE0B098ABC3F1F1FE13A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m5D45951D94596073B13A391CA7045697551B8A4A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__0_m45DEABB8DA64FF84840D27073C8CAC13AAB48594 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__1_m4125381DA464C5860B640144D34BC3012ACA74C1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__2_m50412EB5F09625053F1EDDE3DB5734EED9720362 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_mA803252B5AD6DC573944E9D4C4AACCB7ADB6A35F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m57ED7D60245EFFF2472BAD8EE827DF17FF8CD30B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_tC4BCE0874C3B6C80BBDD2972910D2F293BDEA565 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0__ctor_mF0C3B923395589B62C8132D17B369F0CC51F2727 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__0_mE24F66100D6B99A003C7F3F1602E9D0415AB3EB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__1_m204A37108F3DE0FFA00EBE97B330818BAFBAAC0B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_t9EAC0603D2A289D8E19963B1E9D5311871E89CD3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m5E4D95DF597418E463F1E69B0FCCE7DBAE4590BD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__0_mEEEBCA8C69119AAD814EC6DEC48E0DF1E1D8CB4F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__1_mF458E25F706D196D0EFF31C1043DAA2E3D869ED9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisException_t_m4D67C96D8455C7E7B7B3129676BD7B29BE7DC7A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddResolveHandler_m705919ECE972168EEE7E5CEE4AE73E60AA6BBA4F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddRejectHandler_mDD2C5E3BD6ED3A21CB80C24DD739BA41600F89B0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddProgressHandler_mC8B31A4B274528DA2D5C4FC9BBABC642E0573CA4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_t77289E059759A9DA1CDB59849D4163C50AFA627F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0__ctor_mD6F44450B7B11FB4D627456244700EACDE68FE2B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_U3CThenAllU3Eb__0_m6B85513FF99F8F08248F000B9173FABA4CF31569 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tD64F65719E163C592931EF84BC5BFDEC6E67549B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m89B454074F8D9B805CBD887131EC1358AFA92C68 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_mF64E8E8D7707A25A6E242AA637E55E954AA5691B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_m0554DFBF6738A96E8E179E346704CDF8A8595002 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t56E54FB8F556BA04FFCC8A406CE3E101700BC2E8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_t509DD7979E7FC1D3047F3D60872BC99C4612ECBE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0__ctor_mBDC80CC72675FC662C3331352D20AD433F487AA4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_ToArray_TisIPromise_1_t04DD58177BD18C606F7035FA4373C19422242A39_m514F488DBF041D63EC4744EB8B2F7C2DE10EA19E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_Empty_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_mD4334AC5D4350FF8082FF9C5253E360F7522244C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolved_mFC2C89D6006C84DA9B2D96A5BC38A07921984708 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t15C5A001A293F5A05FEB00815FB7884F3E8B2311 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_PromisedTU5BU5D_t014E4B7596216061C37AED87215DE9365E6D994D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t15C5A001A293F5A05FEB00815FB7884F3E8B2311 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m3B2C6E47FB4577323F38376F319337BEEDAC489D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m2D66C3B02F4D27AD61B8F572315421149694700D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__0_m79FA5A2911DB80BB7E9985A5DA4329C7C22EE465 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tC1D7CED234A37A6696526E459CE83A4A6F2E9F6C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2__ctor_m8938F46DB6AC3845CD30E5E072AC30714FE50EBD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumerableExt_Each_TisIPromise_1_t04DD58177BD18C606F7035FA4373C19422242A39_m318F5C658CA153DFBC7FDD9FD48D0D76B69F9397 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_t9030E8A19DF2B1B236F17575B743327B851C24F5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_mD50158B986EDCA182933DC7CE5D5406E3DD7E3B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_U3CThenRaceU3Eb__0_m330D20712D8E11316A7D371994AAC526EBAA88E4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_m7862B9B5EE6815EB2EB754B55FA16B9B9FD6A3B6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_t0D50D883F5B808B7EC814BF9F895C60C48FEDB67 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0__ctor_m4318FC299367F3560CF9588290A10F09D6910318 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__0_m915E2CD1CE4234A81DAB104435EF4DED33ABAB57 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_t82BA7D6314012526ACB0CD8F52D288FFC405F76D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0__ctor_mE5A6B551CB508382D2B4B951761F0B5EFB3DE2B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__0_m435C57261DD0C448718BEA5805C1C492453FC12A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__1_mBE74DD9794F68839C7ABE1802F4D2A28AA2E831F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__2_m9466ADE2593700280DC306793E1B20C231EFDFA3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t4AEC34429EB32588A2FC5F27725A020D87772CB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m072F09A4A5BDB958B09EC49E12C4814B4CE7100B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisPromisedT_t84C1215714897928D2E7BF8E9C80B4764A82DD7E_m1F0736B7410975D7F21854F97F3F6719012608EC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_tAAB9B6020FECD3BC21B0941896BA6A20FB087F91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0__ctor_mFFC1EBC80D6CA14EE701BDFA98522251F78611A8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__0_m3A02F375858137C471ACE1F06B454C48881B68E5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__1_m1B21FB1D2BD3739B28D2C47A628517B2F20817D3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t63896218AC9092F717493E83FD66E68E0024B3B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_mB5390D8AA578A89568A1F1DAFDC6E3876E7C4E06 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_t34BAB60F53DC587FBCDB2A30B79F54C23D7F83E7_m925A00ECCEDE3EAD6EC13C8610F3FE157DB6465C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tA6D16B1D6E321124B78C0F7AA397583C27BED00A_m2C9DE26D6DE5E6F3E317AB99A9307045C26B1443 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_tF86423FC8EE6694CE27620BCB6B2818BEB6BFFA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1__ctor_m7D063B713F12B475E1139BBFAB5992B24EBA710D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t5255F0E15D1E0CEB90BD5C7AE4EAA854417F7C0B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m6ECBEC288C2D18736729F79C701583F98EC0411A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_mD1914415CCFD087A74AB5FF2C9835B02EA2C1095 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__0_m8308079D6E4E8931B82A2D0307CB2833B7A3D8AD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__1_m9EA9D1E199E92A56F7E38C3F084C0058203D7DEE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1_t5CF996FF708D9DE726D4DCE032A934B306ECBB39 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1__ctor_m5B6FCACF8F27B52BA10BBB58F9F9ED91B9B37179 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1_U3CThenU3Eb__0_m5EFF28ABEC6A04E3FFCD70AF8BAD1CF9DD3630CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tF9C7D29B88C7038AF683DC2CA7FF7507F15EF718 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_mA29ABF1F8CB21D8BE78BF45A8FCBDDE2CABB0E1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tE144C479DA3777F795811B5452F199702CCA7691_m287D9AB00CA6E0A79B8868A7CE2FFC5CC295BAD8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1_t2F2EE9AB6CABF4489615C3051AAF1952B2BCF0FE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1__ctor_m6BD859B4E5D6423CAB621C6D8B5BFFF0E36919DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1_U3CThenAllU3Eb__0_m59F3E3FC16056E5D3A50872B114E72C074028AEA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tFB524A62E008BE1167DE908FD63D4BB401719991 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m6D8FBADB188267CEB3587BC2DCAF9568E50CB910 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisIEnumerable_1_tD15C7AD5C07EEA60C2CE1D04FC23C7BF04A4BA97_mBFE51F76AF0E1C0AD185DF50E9769C6F7869B7E8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1_tA1F86245D7DCB37F27D7A01995D10E7EA25F0D97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1__ctor_m9D3427A50412AF4A872257597BFFFED9BCB19E4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1_U3CThenRaceU3Eb__0_mF1B7C16A00850A6570FCCC7703C52951EF84D638 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t9E0E3968305092F8BB813680B8C969A3505CD12F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m6FF25AD2BE82929A2DC13CA5A1E14320B531035B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_t8AD84B13E874A5A747588B109768637B7D31333B_m1D25EF64FD15532E356855D9A9382FAD417F655D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_t038FE7B2EF68DCE39F45562BBDD88510BB7414E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1__ctor_mF10C4E080767C4708448F4A60F78BEA5EC71FF8F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__0_m97693BB9B63AAD3D3DC6567237E9074C2A8F79FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__1_m910ECBAFB49BEFFFCA709B659DFBD8DD11EC475B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t82E73B815327B7AEF0E946E40CF7A5B7696B25B4_m87B0871CDDCAD65FBD49C432E93457774CE1942C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisException_t_m3EE6FC70D838573130C5708DF3EDAF2512276BDA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m3C3503B1D0EE62363DB6DE07B418C2F1470D508B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t60B7A1F293BF46FF9BD7402AC8AA0BF40BCF06D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m0B79CD309271E0E6AEE86E17A3DF70635A2765D9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t1965BAD3D6C398471ACD39B801B81B3CEB9BD802 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mDE0EF600722100868610D7A39BC74DA6A526A48F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m23878EEB98D918DD499A910242DF2EF84933A10B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m8D4BF61D293D9C53B2BFFA94B63924ACF2C209C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t2BA9541290971BB435A69F4FD2F15C7977E3021D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m8A090964F7BE7E8A17D7B19AAA12CDF789258A90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__2_mC88E63024B265B17EE5422A7513FB806D9FDD7B9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tC35C8EFF46E3D1A9A6C312A971D8EAB5CAFD0B14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mAC6EBAAEAFA919583FD711FFEFAF031AE3226F2C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__3_mF362CE4A2E397E0F4D357D8BB8C83FF2290A8E1F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t3864777002109E4647D9C1A4246C916EC2D1D5F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m4C1041105590B2A4DF6FD3376AA29E20797C3869 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__4_m4DEDD8E453CE66C717A191D813022D90252CE0E7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mC7D65767132CA254450122D1C376773EAEF58AB8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tA64B52BFB094F9291FDC9CC0D88308105D859276 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m2D0AE792A9B6188B5F5FF0184A88664B165F1364 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m694CB7F4DD1C616398373052C21FDA5A9B272E64 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mF553BFCCE61205F7D544ED6BC3C9C04C1521F919 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t8203D3DCC331520D5F653153F383D7043D95EA26 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m0C4E2EB33D5B7AAB2BB53D13D88C37CE5BBCC75F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__5_m53DD25776F6AF4131B7D835A1A9FE8DCD261D155 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__6_m4A727A850BB9E9335395163D5AF43763DF615395 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t17D725907D8AEBB16BF95B2046F5EB7D7C8BCFA5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m95570D123A7B7F31F0EAE917F2E7297CEACF35A3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__2_mB38C6B4547CE8906D7851809310C6E4835B815BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__3_m67552772743B58CAB0D30B1FC4CBD61E4BA7822E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__4_m5C2908A45336203CAD96402E410785709953774E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t30F2346E6013ECFF366EB5E1427C985CD1B00282 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m2C53E36CBF534B02D1B4795DB2F788E42762AE22 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t6125922899DCB1C45BF0BC6DB6F20A8DC6DADAF0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m28E04CBD3A575A076A227E1C1C7D640929934B3A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolved_m254A7ADEB97DCA73ACC71D77049A5B91B07BB412 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t7FADE30B306331272C2AB24A4BE553EF72C386F9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tAD049E07DA4410007492A4ECFA63104A0F6FF05A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m632DA3FB1A5D271BF639E69050F60556588C9B88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_m1CC851AC51F482D4D74330BB37C1CB8A763AFA8A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t6F11A74E7D5BECB1280AE4E8FE2464B1E9010B88 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t8B85DB329F21FDC6C049529A3143E38063F85220 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m061521CE7CA9C290F007057E02929879C7FD72B5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_tD8569019861D72A592A05BC507FAE15D8EFB452C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1__ctor_mA9B8E5CEF33264EF28CDD1BF367F29EDF384E30A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__1_mCBE594F63CDAD922CBEC7FB422952A81924B4F95 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t9B6B47B73C0984FF1F8B1A36B727776105DF3FAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mE6929C26A78BF4A34B5026261CD2ED46FD8AD181 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__2_mAF1A72B3D77069929B366A067BA1EC23FE547EA1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t229FF9D31B37A798A740020F44B5FC8A7FACB8E5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m63B095E707A31F6A8F54A63785431A90D74B36FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mF7BA63F497D3C0CC94F9DF5C86730B4C6B5AF58B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__3_m50C7C250C080C09D129B3818FC7197485961D2C0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tF50B4C2AD0E16D7D16D80E77A401E9CF2540C338 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m560E111D1D2F3BECE40D99574BFBD892E29D9EE7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m294179109D3F485238BD735AA715F67D08B24066 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tF7594D8D4C06B149386BBFE3F1B1F1E7CD705872 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m274C24AB0B30BFCF94A02D6F9318CFC021436911 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m6B874313296E8C906D880D80B34EB01D6B4D65A4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_mCA96FE39CD9E6C364485E42BB421D8F113B2EFA5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tBA2E054C20919DE3AC8D663D68CD123DE42FC1ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m20E0306F69A39CE04643E077590FA0FC3310A9E6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_mBA973C818F63618C9410522F207E060BEDCC0718 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tE772739ECE3C1C7EABE7B71F57693434BEEABF5A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t9C719C6A419791326B88DF3E81CCDFC639455A5E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m2A78A052305BC5E95AD3983A519A0CC49B853B11 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1_t042B6173BC68A2B84FCEA48913270FF12108637A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1__ctor_m4C285BB45ECC3838AF1BF48A6C84C9F6E2670902 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1_U3CRaceU3Eb__1_mA97DDD75AC67F1B6AD6390E607A00BA7D358C810 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t2E4D8C1904F5D5369D8B2E68816457659F38A969 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mC2DA7467091B83EB0BC51B5CF3445C094E9AEB00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__2_m59C97D6EF0E7F2781FE2A1B5D1FDE5B26D73504E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t1336D68871B6159C21DB7404F172C600709F8282 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_mCBAE60593322D2F7C2BBD4ECDC422B4A36F8B9ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_m61305AEE9F223A8CB6E704A1C690A7D681A41E4A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__3_m233E69EDA2E5C391C26E4C32A35AEA9F041B3259 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tAD71F27C8C7E593ECF8B592F443EEE626CCFD776 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m025BB0C5E4D42609690AEFBDE4C1827C4E9AE6D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m49DCA646171DDE1229C54492F26AC8AC93E3EA05 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mBBC2AF89BF5D8720CF44BACC883F0E05537C3F10 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t35CFCF5AD6C704380843B4FBBD4D881C8C1361D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m3B45BE6B57FC50A18D3D3CB7D593F556553CE422 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m2CBE2740D0A073B322A9ECAA12FAF80E785FE4F3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t28053214A79EBE6D664670826D8978B5219C0251 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m6784969C94ED98B49B5AA889EB7CFF599DA7E174 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m1402961BFCD84347378498F27743441BADE249AA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_tA5DC4F913BA7B289659CAF845184EFC7EAE3FBF2_m666BF22058423E4813570A3794A8970886F5EC1F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_tF68B073F7C56CFA6BF53F8971E2EF0C6C010B29C_m025DDE30BC9D16B4289C143B2A45114FB4396EA7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_t15DCC4EE58459B2FE455DCA330EA57A1E4473992 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_m3505C1B585D3D66198C0787FD6D67F44CBA6C782 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t4E57D7B7B959613474B3A7E04BBFB8CAC5107AC4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m027A90E6360D1D0CF305B1D92942AB66B828AC46 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m3D6BB0CE85965D38E2F81B12916E33C4E6995FC9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__0_m73F579FE4D837D0EAB1A6A853E6503FB102C1A91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__1_m26C12B76F155276090F6AD1490AD8CE754BBCCB4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1_tA84878B6538427D3BFC73C2A98AB737479AF0B50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1__ctor_m7E7CF12B27EE8C53B2310EE4D7B4E65F79B5BDA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1_U3CThenAllU3Eb__0_m19FBE320A63C6ED651AE866F4E5900107AB9FEA8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t7C4408B8C34C3F6B6808DA99CE44716E5FA19CBC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_m91DBE9D0176EC2DA1F78E6ADEE3E24573042F102 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisIEnumerable_1_t5DF2349BABF448A9B240164D0DC069E4E8DC452D_mE2362615A8A049B96609138329F95329C5AAFA27 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1_t1E99DD52A17E7F593ADBC5A3D37E1A036DC34278 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1__ctor_mD0344CA5ED4D8B81C7183E79669EB565DA4C4695 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1_U3CThenRaceU3Eb__0_m2448B1751F69D3CC0825B24C1D185CC75051EBF0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t5A8D42340E329984DDE142BBDF96F3035BF74B7A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_mB032AD7606E33AD56F1D44F0F558E384C1866EAA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_tBFF1C3C5256CED24364FF8675BB99BAFF02714A9_mDD500132BB441A51BDE6A98C141786C9CE63DACD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_t70D8216C44443D31C5A516A181256A10E41AA379 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1__ctor_m3C407913426C389B0CCAD56F5C2609A243227D7E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__0_mB1F018729FF2137EE5E03481E6E7269FCFE91818 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__1_mA0138E1844F8BA07534BB26306FFBFB4EB3A2146 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t5B5D4DEBA88921BC24922A0F2983621D9B91BF62_m31417B7E0C6A3B1BDA42620061A3CD3D9A323F5F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t00BF45EB0E9DE0CE17D90A2D223435D7209AC6FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m06BE6E5BBFD4998418CEBF00E6C23D102C165392 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__2_m4B41960A0088CA03E6889DE9B50219D02A088109 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t5D35A12CB4E16457C54B58D1BEF4B75403191979 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_m858E0658301EA7603E1C0FE5E444CA364237123C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__3_m4102838CBDFAB10959EC95CA9FD791DCF925BB01 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t83FD1BAB4E4097843CDA6464877A2445085180D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m02B654E1E0CEED5C7D3CB896393327DA40906C59 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__4_m2EE64CB91DE0AC57E3CD5A5CA6C41427EC7C64E4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mBFB7BC603CB3887FA17F0BA446EAD3C2075DBBA1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t525078C4D837F926FDB426442F1224A8634D3ABD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_mED99D1972DD8B070EA6A4F6D513DD365EFFEFABF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_mC6F6BEED72A4F05EA35EB05B8A878ED9613341D6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mF752D76DB5886A93ADAF6916F9FB8F6B25FC7706 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t184BDCE450B765EBE1F9DD22F5CBE983D1E33430 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mC041FD9C0E91BEAAFDB6557DC3B0295351AA9844 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__5_mE8BE0C5FAF7A28FC75D5F5C5A307A52B754D95F8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__6_m8584B0D0E7315C22806B36F71E676D95F0DE879A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t68401B4DB6835D1065CA4A696B0D7D5E19FA71E3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m677C8663C6C364AA0A082046AA848F8A4D873E27 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_m2ED17A7BB304F2913BBE86BBA3C9CECA9E6FBD3E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tC43AB0F3CA9267473A98C507CB47DC8D61683090 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tEF77E7A193B4E162F8069297E2530C3DE2C5B5E5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m77F5CF8326911ED0F8A549F90ECD6709F62436ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_m08418CD68DA27F19B584DA12D879CDD45E80E636 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t99E8026181408EEAA0C6E7E88F79788362583CDA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1_tE1FF409FF32D2AC27EB23F8A5AB043530AAE581F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1__ctor_m31B5AAFBACEBFC25E756ABC12A020FC08EEB9ABB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_1_U3CDefaultUnityWebRequestU3Eb__0_m2FD52FCDB37243E5A9BDE3E7327895E5AB4293BC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1_t0B78C8EDA2742AD3DD860A4B4AFB7AF30D9B630F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1__ctor_m0B5B1F4D629DD071DD78F882B28731BB5C194F27 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_1_U3CDefaultUnityWebRequestU3Eb__0_m50C8AEDD37C3945C45591BD17DC25B712657FE80 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisTResponse_t6C41777F102D933C7F1DF524510EEF4CD68A00FB_mC155C8F1686494C341055827A4B5AA2C7D03F7C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t3CDF48C251C5EA97062E701FA2672DE2E20C766F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3_Invoke_mB2B8FABABCD5E0B0AAE6DCBC6E9C712792BB2222 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonHelper_ArrayFromJson_TisTResponse_t53B25A53786EA118912D0F036DBEEEE13118D8CB_mD6596516BF54895ABD9A965E48B9864B391CAAB9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_tA4E3BAFED40CD84EDF1EC29EA884C13188C8BC27 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3_Invoke_m428AD86046F848E56B4E32BFCCD9B110B93CA265 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisWrapper_1_tB5A88805CEB6B27C6010A0FD729ADC0C84974C5E_m2B068EE44A8206C9FF568DB0272A47C6CFE20D95 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisWrapper_1_t1ACCAB4C7C5F8802465A6258C211D055DB2905A1_m0B1CBEA5A9BBFCE2C7320E786801523EF09F1C02 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Wrapper_1_tB7A173F6F821010BC04DE9C99571F42669AA9EAC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Wrapper_1__ctor_m01A44CB7915084AE9851868D516EAB2C7443898E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Wrapper_1_tB6A5EDDF775ABAFDDAD805FAA669E7E1D8219098 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Wrapper_1__ctor_m8A8450346B9CDFBB4F39C178323EA2822C88FC59 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_tD117B4A8ECD6BBA8F4E4C69B4BB01C15DC047A0E_mD79D2E7990F1EDD6EE791D9932D8182BD0AE4A37 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Get_TisT_t83867E9875F3DF048E0A67465AC3942CE3447990_m17825CC4AE9AA8AE2AFF4FE618E165B8BA9FB1C8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Request_TisT_t886A7903AE708C78F0A6FA63699344C0162421A6_mD46070E8872F782137A601D6DD870F47D385FCE0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_GetArray_TisT_t634BDE1A0290F8C7529053DC6D32F6D79028D554_mA85C96523073284D5D47438BFC8E7AF31CE45469 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_tFE3EDFA8DC4B5268DAA2D1EB30AA49A6A1C4543F_m805718C9C8675E2BC8C7E88CE7F19E72D1057D8F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Post_TisT_t08F22C87BE1C4818613ABEF113436BCF5C850767_m65D71BEA0C1D7BDC9DF009BB91BDEC2CDB982B52 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Post_TisT_t128CFB107C0EB6D9FC21B202286E6882316B98EE_m3BA7A684BB6DA7126E21EC23A75B74D720697FB4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Request_TisT_t124257CB75A94512115D1543B1C94112755F1D1D_mB596C5E2704B8B801E2D6B80E1A0D351E8ED1BA4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_PostArray_TisT_t1A02A9BC30840725729D98FFD99035FABF7E6CF5_m6B5533400EA9655C24044367BE24F84AB2E9F38D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_PostArray_TisT_t602D8D6576954DACA96808F9B2B1E84C98EDD098_m944E5821A83BA156DECB97727CC2E8EEA6FCC8FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HttpBase_DefaultUnityWebRequest_TisT_t504B3C0151E363A5D03DB1CEC8152895A62479B1_m018C4493E808939605ED0764E316ABFAEB02E3F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Put_TisT_t4D27DA095511CDDC56BD3EC72C499428ECB5DFE5_mE6F5EC854C85F993A7C3282DE13D93331CDD0245 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Put_TisT_t52910655884241C13B96DF4E8F202F4111F3FAA6_mC1D263D925AF3F4670135D5B2A2D858B6B8C0151 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Request_TisT_t88C3194A1D53169B4A79A46C74B5C930E4E2E53D_m35D31AB8CBAECF8FAAABB0267EE301819D79094E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Patch_TisT_tEDE46512BF858D7CB7BE1F5B1EDF4C5E7E47D945_mEB0061CDE11F00F9C6131C52B86EC2B9186C290C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Patch_TisT_tAE0C049D764B2E3C603F30267FBB5631A73AAE70_mFC3878A90700293BD59BB6EA8AF169758902F58A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Request_TisT_tB2649DD63D7F01088CD62F61DF6F1E3C3C44CAFC_mC4ECE2C0D39D0C956BC54162BA9DFEB2C735E003 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tA4E39A9F53C23634EB4C0E526ED9EC98C61F3683 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mF738E2689DAB1FC7495C61E28AB4174783804747 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_t913737CEF85EBB264A13153F34F0B0A664A6C9E3_m674C9BF6CC30CFB8A5F0C0E18FF721E3EBB3F938 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t5C6EB3D49B13267E3011CC926CE37A7B4B29A45F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_m1801BC4A951C1B09CA10F1E23C688283C90AA493 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Request_TisT_t913737CEF85EBB264A13153F34F0B0A664A6C9E3_mC80DDEDF1437BDB53C0A1954D99819A7D9307417 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Get_TisT_tC93B0D8C6B668FC459794F878286B9C7E6DC43E8_m54D9E42B6183DF5353CE6E80258FC9AA0537A98B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tD1970B50BE83D54E3A42860D750034AE526A2152 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mE5696C67A7F6CD9D0BB550C6E86F053F26058114 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_t150C6C63169BD78D8C25F931706841F84075062E_m05AE171FED89D2C42E7B5155B2DD3B46ECA20E8B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t700F7D20BFF3DD7C8F859B5C53BF5969A5EB8E97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_mA5BCEB450F2D1AE843CB769602813B75B34999D9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Get_TisT_t150C6C63169BD78D8C25F931706841F84075062E_mD30DCF07EEBC5BFE07BC7ECE831D591FDDD0280C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_GetArray_TisT_tF1C93AF8F35F07B08AEE40BF59300BBC3007370A_m9AEFCE65CDC7A31EEFEDB1DA823EADCC859437CB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t71E3D6719C508581C2E361428EFB4D2178A9961B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m9DE3F05AAD40BEC6467CFD476F970747E5A7D8DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisTU5BU5D_t99EF92CB84B68CBBB46B79F55EF3A42AA7A8AEF8_m1676B20B638C40E5C780D2810D9F705B02DD9671 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t4CE18541EDBCDA2990A5527943074DE871E12321 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_mF11E59F4A67E04BB6B1241A425BE27A25A724AD3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_GetArray_TisT_tC4CFD3277336A9153938A2B25F4D147A556D4716_mB265F39C3A6706B39D60774B679CA6BD9200F2EC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Post_TisT_tBCAAF676D75733A088629A5360DA6DE6E02C39EC_m1571E9D10A5A26E49F45AEF0673BA4CE68B5AF4A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Post_TisT_tFDB9EE274A55F769EB86068702ED462E9309C292_mF1CA9C2A2754D61BAEECF1CEF9A10D714F8E887A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t7E701B2883A9394339A3189971F920D091FC4E69 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mFF371CD7AD0852387407EE3C1BEA0F617565FF2E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_t784E9562E468EE1A86103C30A61F693DEFC0BB11_m0911B4197183F9D10A3CF557CD743E5761ABBD25 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_tCE1FEFD63C57F2D3D63587C71980D63310F25C11 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_m954EE0BDE52363686DBA738EC893A5BD7E6ABCE1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Post_TisT_t784E9562E468EE1A86103C30A61F693DEFC0BB11_mB2F9CF15E7DC37C61225BCF35E2CE7F0AC0E1906 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_PostArray_TisT_t72120E9822B25A6F139E776671046FC43D9B1292_m8E1B922FFF9FF8E2F38E71635AA5CE93B20219CE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_PostArray_TisT_t3448E55C1F62481700E36C8567FEBD383814B2C9_m7C17D45151FAB396F9461C1E04588BF5F74B7A6B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t427E76746704B851FBEE3013A0BAB354D3F51239 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m674540B8910F618253498CC5C2BF9ECBE8C1AE16 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisTU5BU5D_t58093F002DC7993D22B92841E0F73666C9A8E2A9_mB0EA6FCFEF33A7EBFB19DDF490419C92AC039BC7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t3DC78AF8FBFC3C76DAEA6A04C926C594C8833CF7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_mBB8D4D53BD3465ABAC0B6B9509CAE03C8C8AD5F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_PostArray_TisT_t29E3B231F1968F195836DA003DF1E2B982DE6EB9_mF7AE51EB8352DCB114D2E7075EE57B1C0CFBCABB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Put_TisT_t4390DC7B1DB61EE8C7DEED85355FF79101A65803_m534DE450FD51168478712DDCE2F534F6B0610540 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Put_TisT_tBC3AC6990E9D908D053850D2C431BF68AAE9F2F8_m44CA3E7DF19768266013B890722575B2643F5658 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tF9B00EAB3E9F7DF83E95CC8423850F2BC823DAB5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m80E6C8395930F29BF5B560CC10B7AD15CBE7BCE4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_t3F2C3B0D59553D1E97B89ABC12533A568CF6DCF6_mE90CDF54D28B9BD79AE6CAA3BC5915758A79912A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t08C7302D7A4D7A04C036BCCF199C86BCDDA0259C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_m5B29FB5D1863E55B91F2F5509D3BE20982FE55FC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Put_TisT_t3F2C3B0D59553D1E97B89ABC12533A568CF6DCF6_mD308F8DCC6E0FB33506522935BD489D34A618457 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Patch_TisT_t29ECB8B08A7D9721744075CADBDCCD34EA53CBD0_m846A63A4F3F8CC938DA80DAFCC01879CA453361C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Patch_TisT_t4CAA10D51B75057BA4E6714574B12DB27D51EB9E_mB7CE2919FC5CE70E2E590BB5C3E921AE0E45D9A2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t64658469516DF0CC31E705DE0EDE1140C5EE36CC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mBA6590A2E05ED03E30F6F0F3BF83516A0C80AEDF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_tD6235DCD8CA7C0B177480A1E23DBA1665C0DB266_m40A638C5C6D800B815C3A74DDCB9232C6BDD47ED },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t86277884127C5A70F713D80D9AAA122A756085FA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_mFC24507F516841A89944DD9D69318DFF037F9E5D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Patch_TisT_tD6235DCD8CA7C0B177480A1E23DBA1665C0DB266_mA1807590CED60ECEEEFC36B078E925D125A56456 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tC1E77D9C2661BCEA873BCA3D20591513BEDC7A57 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mE94C583E133561D296255DA6CC2BBBFC9F7CA1E0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m26AFD09E25CA1EF74CD724E59BD78D71DFFAA826 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_RestClient_Promisify_TisT_tF028780809602AAEE5613612A20639BA635A67DF_m66AFFABE041A223DD28BB963880E34D00F2B1A4C },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	911,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	78,
	s_rgctxIndices,
	365,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
