﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 RSG.IPromise`1::get_Id()
// 0x00000002 RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x00000003 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000004 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x00000005 System.Void RSG.IPromise`1::Done()
// 0x00000006 RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x00000007 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000008 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000009 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x0000000A RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x0000000B RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000000C RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x0000000D RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000000E RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000000F RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000010 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000011 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x00000012 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000013 RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000014 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000015 RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000016 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x00000017 RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000018 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000019 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x0000001A System.Void RSG.IRejectable::Reject(System.Exception)
// 0x0000001B System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x0000001C System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x0000001D System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x0000001E System.Int32 RSG.Promise`1::get_Id()
// 0x0000001F System.String RSG.Promise`1::get_Name()
// 0x00000020 System.Void RSG.Promise`1::set_Name(System.String)
// 0x00000021 RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x00000022 System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x00000023 System.Void RSG.Promise`1::.ctor()
// 0x00000024 System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x00000025 System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x00000026 System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x00000027 System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x00000028 System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x00000029 System.Void RSG.Promise`1::ClearHandlers()
// 0x0000002A System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x0000002B System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x0000002C System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x0000002D System.Void RSG.Promise`1::Reject(System.Exception)
// 0x0000002E System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x0000002F System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x00000030 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000031 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x00000032 System.Void RSG.Promise`1::Done()
// 0x00000033 RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x00000034 RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x00000035 RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000036 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000037 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x00000038 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x00000039 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000003A RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x0000003B RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000003C RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000003D RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000003E RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000003F RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x00000040 System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000041 System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x00000042 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000043 RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000044 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x00000045 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000046 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000047 RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000048 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x00000049 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x0000004A RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x0000004B RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x0000004C RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x0000004D RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000004E RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000004F RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x00000050 System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x00000051 System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x00000052 System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x00000053 System.Void RSG.Promise`1/<>c__DisplayClass24_0::.ctor()
// 0x00000054 System.Void RSG.Promise`1/<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x00000055 System.Void RSG.Promise`1/<>c__DisplayClass26_0::.ctor()
// 0x00000056 System.Void RSG.Promise`1/<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x00000057 System.Void RSG.Promise`1/<>c__DisplayClass34_0::.ctor()
// 0x00000058 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x00000059 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x0000005A System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x0000005B System.Void RSG.Promise`1/<>c__DisplayClass35_0::.ctor()
// 0x0000005C System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x0000005D System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x0000005E System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x0000005F System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::.ctor()
// 0x00000060 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x00000061 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x00000062 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x00000063 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x00000064 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x00000065 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x00000066 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x00000067 System.Void RSG.Promise`1/<>c__DisplayClass43_0::.ctor()
// 0x00000068 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x00000069 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x0000006A System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__3()
// 0x0000006B System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x0000006C System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x0000006D System.Void RSG.Promise`1/<>c__DisplayClass44_0::.ctor()
// 0x0000006E System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x0000006F System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x00000070 System.Void RSG.Promise`1/<>c__DisplayClass45_0`1::.ctor()
// 0x00000071 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x00000072 System.Void RSG.Promise`1/<>c__DisplayClass48_0`1::.ctor()
// 0x00000073 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1/<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x00000074 System.Void RSG.Promise`1/<>c__DisplayClass49_0::.ctor()
// 0x00000075 RSG.IPromise RSG.Promise`1/<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x00000076 System.Void RSG.Promise`1/<>c__DisplayClass51_0::.ctor()
// 0x00000077 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000078 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x00000079 System.Void RSG.Promise`1/<>c__DisplayClass51_1::.ctor()
// 0x0000007A System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x0000007B System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x0000007C System.Void RSG.Promise`1/<>c__DisplayClass52_0`1::.ctor()
// 0x0000007D RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x0000007E System.Void RSG.Promise`1/<>c__DisplayClass53_0::.ctor()
// 0x0000007F RSG.IPromise RSG.Promise`1/<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x00000080 System.Void RSG.Promise`1/<>c__DisplayClass55_0::.ctor()
// 0x00000081 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000082 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x00000083 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x00000084 System.Void RSG.Promise`1/<>c__DisplayClass55_1::.ctor()
// 0x00000085 System.Void RSG.Promise`1/<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x00000086 System.Void RSG.Promise`1/<>c__DisplayClass58_0::.ctor()
// 0x00000087 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x00000088 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x00000089 PromisedT RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x0000008A System.Void RSG.Promise`1/<>c__DisplayClass59_0::.ctor()
// 0x0000008B System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x0000008C System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x0000008D System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::.ctor()
// 0x0000008E System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x0000008F System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000090 RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x00000091 RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x00000092 RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x00000093 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::.ctor()
// 0x00000094 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x00000095 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x00000096 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x00000097 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x00000098 System.Void RSG.PromiseHelpers/<>c__1`3::.cctor()
// 0x00000099 System.Void RSG.PromiseHelpers/<>c__1`3::.ctor()
// 0x0000009A RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers/<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x0000009B System.Void RSG.PromiseHelpers/<>c__2`4::.cctor()
// 0x0000009C System.Void RSG.PromiseHelpers/<>c__2`4::.ctor()
// 0x0000009D RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers/<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x0000009E System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_m44309A22F64391524EDCB4DB860D41D28DD0AA34 (void);
// 0x0000009F System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m0361C3DB96557B1C1DD6749C5951B276ADF7AEB6 (void);
// 0x000000A0 System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_m412EFDE9D40AEEC03FFF90B533F73FBBB5E7DBC4 (void);
// 0x000000A1 RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x000000A2 RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x000000A3 RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x000000A4 System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x000000A5 System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x000000A6 RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_mFBADA409CEBBCA185DF032B513BA1F9B55AF8D0B (void);
// 0x000000A7 RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m31CC9654EF84376642C87603F4506322F7AEAC41 (void);
// 0x000000A8 RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE (void);
// 0x000000A9 System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_m470F6FFA6B830E6F7D97D2A68C6B71C26DCD6C57 (void);
// 0x000000AA System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_m5EB9C79697DF112F8377897420659AEB19DA8CF6 (void);
// 0x000000AB System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_mF954CE03B679A20DAF5DD99B33A5CB419E841013 (void);
// 0x000000AC System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F (void);
// 0x000000AD System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_m57B7BDB6A53486AFE172E95E855F0D37EF1F3341 (void);
// 0x000000AE System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m3C9E276C0B8D1955883B8B927CEC3C7AF5A6593C (void);
// 0x000000AF System.Boolean RSG.PromiseTimer/<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E (void);
// 0x000000B0 System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m1D3457A043DF6BA5F5391DA47B8DA35673635FAB (void);
// 0x000000B1 System.Boolean RSG.PromiseTimer/<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4 (void);
// 0x000000B2 System.Int32 RSG.IPromise::get_Id()
// 0x000000B3 RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x000000B4 System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x000000B5 System.Void RSG.IPromise::Done(System.Action)
// 0x000000B6 System.Void RSG.IPromise::Done()
// 0x000000B7 RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x000000B8 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000000B9 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x000000BA RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x000000BB RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000000BC RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000000BD RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x000000BE RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000000BF RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000000C0 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000000C1 RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000000C2 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000000C3 RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x000000C4 RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000000C5 RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000000C6 RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x000000C7 RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000000C8 RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000000C9 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x000000CA System.Int32 RSG.IPendingPromise::get_Id()
// 0x000000CB System.Void RSG.IPendingPromise::Resolve()
// 0x000000CC System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x000000CD System.Int32 RSG.IPromiseInfo::get_Id()
// 0x000000CE System.String RSG.IPromiseInfo::get_Name()
// 0x000000CF System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m1D932BFCF3873476A40B88F8AA75EFB400812224 (void);
// 0x000000D0 System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_mA8561281DD20ADE854ED9177B6B116BD5301FCEF (void);
// 0x000000D1 System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970 (void);
// 0x000000D2 System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m0FBD6C63F54715BEF0B694A2D41408FD229E8DBC (void);
// 0x000000D3 System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m578D0DFBFEAEE169BEDD5EF654E0CC150ACC3E4F (void);
// 0x000000D4 System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_mC8A8CF26DF363FB48A483EEA030A25EF5CC61F72 (void);
// 0x000000D5 System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m8A3E1CBF297D2437C988C4DE730EE80CF360058F (void);
// 0x000000D6 System.String RSG.Promise::get_Name()
extern void Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205 (void);
// 0x000000D7 System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059 (void);
// 0x000000D8 RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD (void);
// 0x000000D9 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE (void);
// 0x000000DA System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF (void);
// 0x000000DB System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_mD6C7CBA18CF33E760A9A853224E3C31C5C102DC3 (void);
// 0x000000DC System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D (void);
// 0x000000DD System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_mE5BC8F2F50C3C7B1A2AC8CA7FCBF933FECDA0550 (void);
// 0x000000DE System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m99E30F6C68B539076C0E8D07FF05B4E6BE2A56E6 (void);
// 0x000000DF System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_m0898CAB05023C68BD94E91C3496CA3B30C20471E (void);
// 0x000000E0 System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54 (void);
// 0x000000E1 System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E (void);
// 0x000000E2 System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m3E4DD72481BD449FA36A24A6324DD23FA8367589 (void);
// 0x000000E3 System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F (void);
// 0x000000E4 System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m20A1B3583BF638CC7C2F0EA5B739FE7E1327BDFF (void);
// 0x000000E5 System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m5DC60EAD55ADE888A16689C026BBF0FD72CE4354 (void);
// 0x000000E6 System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_mDEDCF0A1858DF08B90C273FAD03AA33B15FC247F (void);
// 0x000000E7 System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF (void);
// 0x000000E8 System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808 (void);
// 0x000000E9 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0 (void);
// 0x000000EA System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_m54CBCA8B3EB300A7C4C2534ADE5AF53D71D4FBB4 (void);
// 0x000000EB System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_m66CDC87BCAB708C6F999D1CB838EE4B27FBE7CA5 (void);
// 0x000000EC System.Void RSG.Promise::Done()
extern void Promise_Done_m218F80C8F8959377CB7BCB637AE12150BEE3D979 (void);
// 0x000000ED RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58 (void);
// 0x000000EE RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088 (void);
// 0x000000EF RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000000F0 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05 (void);
// 0x000000F1 RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D (void);
// 0x000000F2 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000000F3 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m87C269B2CA1E15DAAF0BBB2A7F389575F86AE4BE (void);
// 0x000000F4 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m91F1EE136222A59062D07FA0A2857F6F439B74A7 (void);
// 0x000000F5 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000000F6 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93 (void);
// 0x000000F7 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7 (void);
// 0x000000F8 System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888 (void);
// 0x000000F9 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD (void);
// 0x000000FA RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_mAFAA6572E4C049D282DEF096CF8A3662816C2DCE (void);
// 0x000000FB RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000000FC RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m4E3CD9398A6FE307C1F7B30E39E15AF6D028D277 (void);
// 0x000000FD RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2 (void);
// 0x000000FE RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_mEA544C9E1A6ECAF979AFB7072BAF1E497B0888FB (void);
// 0x000000FF RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_mA9C41F9AA1B15AE32B13AAEB918C9C39C9CB0A93 (void);
// 0x00000100 RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F (void);
// 0x00000101 RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_mBEBF13A1B643D5BD8C8AA3C8C1E34F46033E2C37 (void);
// 0x00000102 RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000103 RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m174412F14AF9AF5E72ACC54AFE424CBAFCB0FCE7 (void);
// 0x00000104 RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E (void);
// 0x00000105 RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C (void);
// 0x00000106 RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_m75966446ECB8170A186463C054F5E40CCC479B25 (void);
// 0x00000107 RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_mB06879F20FE6D9871B1B0677771662B65531FB7F (void);
// 0x00000108 RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_m05367014E6744D377DEBA484EDE08F5A6A7E72F6 (void);
// 0x00000109 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000010A RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_mC151B05499CAD04419AE9C3A30F7A5036332DCC6 (void);
// 0x0000010B System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48 (void);
// 0x0000010C System.Void RSG.Promise::.cctor()
extern void Promise__cctor_m49F60296CB19D2264E020FBF75CC32C42657A778 (void);
// 0x0000010D System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF (void);
// 0x0000010E System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD (void);
// 0x0000010F System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11 (void);
// 0x00000110 System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF (void);
// 0x00000111 System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m777DA7CE17AD10D18C6A866CC996D64D966176C2 (void);
// 0x00000112 System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61 (void);
// 0x00000113 System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m1CC4BF0264C238FF06EBD6D51182AD9D9F00A44C (void);
// 0x00000114 System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506 (void);
// 0x00000115 System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m1489BE8776752DD4703EBFDAA409ABA61E6CC649 (void);
// 0x00000116 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68 (void);
// 0x00000117 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47 (void);
// 0x00000118 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19 (void);
// 0x00000119 System.Void RSG.Promise/<>c__DisplayClass51_0`1::.ctor()
// 0x0000011A System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__0()
// 0x0000011B System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x0000011C System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x0000011D System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x0000011E System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x0000011F System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x00000120 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x00000121 System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mE8F288904D95E3DC0025D1FBE2C15EEB625049AB (void);
// 0x00000122 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B (void);
// 0x00000123 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668 (void);
// 0x00000124 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328 (void);
// 0x00000125 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3 (void);
// 0x00000126 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792 (void);
// 0x00000127 System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m92B2808BB6DFC101EDB128ADCE074E66706EA045 (void);
// 0x00000128 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13 (void);
// 0x00000129 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5 (void);
// 0x0000012A System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mB81C3730446AE8C66A7D3EAFE2F0D416873CD51C (void);
// 0x0000012B RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A (void);
// 0x0000012C System.Void RSG.Promise/<>c__DisplayClass57_0`1::.ctor()
// 0x0000012D RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise/<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x0000012E System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m2D14CCC0D09F29E28A2479BD4966A6EEAE384627 (void);
// 0x0000012F System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF (void);
// 0x00000130 System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971 (void);
// 0x00000131 System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_mBB0688A58C068F7B05F67132C0437CA689E3540E (void);
// 0x00000132 System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149 (void);
// 0x00000133 System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095 (void);
// 0x00000134 System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m658D101B81BC36F252DED37541EFDB385BA526E4 (void);
// 0x00000135 RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92 (void);
// 0x00000136 System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_mD8148B137ABC682149B7AC033FFC943A4ED35CD1 (void);
// 0x00000137 RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC (void);
// 0x00000138 System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7 (void);
// 0x00000139 System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_m378111927982C2FF6C8AAA87AC31A3F631D36495 (void);
// 0x0000013A RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92 (void);
// 0x0000013B System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223 (void);
// 0x0000013C System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_mF531911D8BEFC44215B51FEBB012E039F51266C8 (void);
// 0x0000013D RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1 (void);
// 0x0000013E System.Void RSG.Promise/<>c__DisplayClass64_0`1::.ctor()
// 0x0000013F RSG.IPromise`1<ConvertedT> RSG.Promise/<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x00000140 System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m2B345ACF4133F168C6638758CDE9864531934908 (void);
// 0x00000141 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632 (void);
// 0x00000142 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6 (void);
// 0x00000143 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98 (void);
// 0x00000144 System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_mF9A6583E6F209770FC8B76F1E90E1AE9BE64653A (void);
// 0x00000145 System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253 (void);
// 0x00000146 System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m46CE508BDC0469757590835B30D4A0169716ABF3 (void);
// 0x00000147 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E (void);
// 0x00000148 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED (void);
// 0x00000149 System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mE72A5373F806CE548BA3484362F2F7FE433EEBFE (void);
// 0x0000014A System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928 (void);
// 0x0000014B System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273 (void);
// 0x0000014C System.Void RSG.Promise/<>c__DisplayClass71_0`1::.ctor()
// 0x0000014D System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x0000014E System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x0000014F RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x00000150 RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x00000151 RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x00000152 System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_mEE4C2AB59B8C048706F3AC40529D78301FFA74A8 (void);
// 0x00000153 System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x00000154 T1 RSG.Tuple`2::get_Item1()
// 0x00000155 System.Void RSG.Tuple`2::set_Item1(T1)
// 0x00000156 T2 RSG.Tuple`2::get_Item2()
// 0x00000157 System.Void RSG.Tuple`2::set_Item2(T2)
// 0x00000158 System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x00000159 T1 RSG.Tuple`3::get_Item1()
// 0x0000015A System.Void RSG.Tuple`3::set_Item1(T1)
// 0x0000015B T2 RSG.Tuple`3::get_Item2()
// 0x0000015C System.Void RSG.Tuple`3::set_Item2(T2)
// 0x0000015D T3 RSG.Tuple`3::get_Item3()
// 0x0000015E System.Void RSG.Tuple`3::set_Item3(T3)
// 0x0000015F System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x00000160 T1 RSG.Tuple`4::get_Item1()
// 0x00000161 System.Void RSG.Tuple`4::set_Item1(T1)
// 0x00000162 T2 RSG.Tuple`4::get_Item2()
// 0x00000163 System.Void RSG.Tuple`4::set_Item2(T2)
// 0x00000164 T3 RSG.Tuple`4::get_Item3()
// 0x00000165 System.Void RSG.Tuple`4::set_Item3(T3)
// 0x00000166 T4 RSG.Tuple`4::get_Item4()
// 0x00000167 System.Void RSG.Tuple`4::set_Item4(T4)
// 0x00000168 System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_m555AAF5903923E345DE642D648F00143A5442218 (void);
// 0x00000169 System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_mA3A95CC09C60213917698EA0D1F4E49ADE76AB20 (void);
// 0x0000016A System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m39A3D4D0F18B6923549CB458A5FB67BD56524EA0 (void);
// 0x0000016B System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m5705CBFF638E07568E352F95697FEC35697938F5 (void);
// 0x0000016C System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24 (void);
// 0x0000016D System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m9286CD3BDADD656A97C9398D21666B1CCDAF82DB (void);
// 0x0000016E System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000016F System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x00000170 System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x00000171 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::.ctor(System.Int32)
// 0x00000172 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x00000173 System.Boolean RSG.Promises.EnumerableExt/<FromItems>d__2`1::MoveNext()
// 0x00000174 T RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000175 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000176 System.Object RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000177 System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000178 System.Collections.IEnumerator RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
static Il2CppMethodPointer s_methodPointers[376] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_m44309A22F64391524EDCB4DB860D41D28DD0AA34,
	PromiseCancelledException__ctor_m0361C3DB96557B1C1DD6749C5951B276ADF7AEB6,
	PredicateWait__ctor_m412EFDE9D40AEEC03FFF90B533F73FBBB5E7DBC4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_mFBADA409CEBBCA185DF032B513BA1F9B55AF8D0B,
	PromiseTimer_WaitWhile_m31CC9654EF84376642C87603F4506322F7AEAC41,
	PromiseTimer_WaitUntil_m19635D82985F4D8CE5BDF26F33C9002DB6C9FEBE,
	PromiseTimer_Cancel_m470F6FFA6B830E6F7D97D2A68C6B71C26DCD6C57,
	PromiseTimer_FindInWaiting_m5EB9C79697DF112F8377897420659AEB19DA8CF6,
	PromiseTimer_Update_mF954CE03B679A20DAF5DD99B33A5CB419E841013,
	PromiseTimer_RemoveNode_m109335DED742F0018CB66750FCF232A36B3DCB2F,
	PromiseTimer__ctor_m57B7BDB6A53486AFE172E95E855F0D37EF1F3341,
	U3CU3Ec__DisplayClass3_0__ctor_m3C9E276C0B8D1955883B8B927CEC3C7AF5A6593C,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mD88E711C99339B4481E08377FFDF423F992F529E,
	U3CU3Ec__DisplayClass4_0__ctor_m1D3457A043DF6BA5F5391DA47B8DA35673635FAB,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m6F6E6D0B1FE9363A98E7BEE11DDCA2535C23C1D4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m1D932BFCF3873476A40B88F8AA75EFB400812224,
	ExceptionEventArgs_get_Exception_mA8561281DD20ADE854ED9177B6B116BD5301FCEF,
	ExceptionEventArgs_set_Exception_mF834389C9111463083374DB45BDD6AC776A13970,
	Promise_add_UnhandledException_m0FBD6C63F54715BEF0B694A2D41408FD229E8DBC,
	Promise_remove_UnhandledException_m578D0DFBFEAEE169BEDD5EF654E0CC150ACC3E4F,
	Promise_GetPendingPromises_mC8A8CF26DF363FB48A483EEA030A25EF5CC61F72,
	Promise_get_Id_m8A3E1CBF297D2437C988C4DE730EE80CF360058F,
	Promise_get_Name_m7B29A81A6FCADC296195887B39DB74185DCCC205,
	Promise_set_Name_m2982C2676FECF23005F1A8A0E6092354FD30B059,
	Promise_get_CurState_mDE97B7AFA168C297267DDF0BCBD65511A321C4BD,
	Promise_set_CurState_mA3B0BB2B36252909843EDAB3D1ED7BD5622881BE,
	Promise__ctor_m19934364A27E9819B4B61653CD640DFDA975E7CF,
	Promise__ctor_mD6C7CBA18CF33E760A9A853224E3C31C5C102DC3,
	Promise_NextId_mA94D46DA2BB15235434E581B5E05EF4174712A8D,
	Promise_AddRejectHandler_mE5BC8F2F50C3C7B1A2AC8CA7FCBF933FECDA0550,
	Promise_AddResolveHandler_m99E30F6C68B539076C0E8D07FF05B4E6BE2A56E6,
	Promise_AddProgressHandler_m0898CAB05023C68BD94E91C3496CA3B30C20471E,
	Promise_InvokeRejectHandler_mCE7E007166E353DDB9F7380DF2A2D0B3A1218E54,
	Promise_InvokeResolveHandler_mC47A43925C91333DC4440A06214A29B38D69B86E,
	Promise_InvokeProgressHandler_m3E4DD72481BD449FA36A24A6324DD23FA8367589,
	Promise_ClearHandlers_m59623ED64BFC3BD3165DF3825ED1A2FA6D96DB4F,
	Promise_InvokeRejectHandlers_m20A1B3583BF638CC7C2F0EA5B739FE7E1327BDFF,
	Promise_InvokeResolveHandlers_m5DC60EAD55ADE888A16689C026BBF0FD72CE4354,
	Promise_InvokeProgressHandlers_mDEDCF0A1858DF08B90C273FAD03AA33B15FC247F,
	Promise_Reject_mDBCD2A4736D520CAD1DC6F96E676D142A73AB4EF,
	Promise_Resolve_mA08DEA69FB7D010870415D93B51ED3E491C76808,
	Promise_ReportProgress_m8A0FDAABEAAE4D13ACA1EC241E6C0BC5018E5DF0,
	Promise_Done_m54CBCA8B3EB300A7C4C2534ADE5AF53D71D4FBB4,
	Promise_Done_m66CDC87BCAB708C6F999D1CB838EE4B27FBE7CA5,
	Promise_Done_m218F80C8F8959377CB7BCB637AE12150BEE3D979,
	Promise_WithName_m075528A0721A1509DDD76BF2A8760C9396C9BD58,
	Promise_Catch_m677962CA884227097B0E075053B92F53B05DA088,
	NULL,
	Promise_Then_mDBCE5FDDAA21FE052DB7FE3B466A81F379527F05,
	Promise_Then_mAE7E500FD924DA984CCDB3C826ED3778DF9E9E3D,
	NULL,
	Promise_Then_m87C269B2CA1E15DAAF0BBB2A7F389575F86AE4BE,
	Promise_Then_m91F1EE136222A59062D07FA0A2857F6F439B74A7,
	NULL,
	Promise_Then_mF00B20F77D6F0E2929D3E76DB35E531F1971CA93,
	Promise_Then_m90D1817089E5606A19E7E753784E4DE041E41AE7,
	Promise_ActionHandlers_m387CE73E09C007D40A1834486E56647BDBB1B888,
	Promise_ProgressHandlers_m0E30265AA5CE06B7D1407ABC86C12D791E1976CD,
	Promise_ThenAll_mAFAA6572E4C049D282DEF096CF8A3662816C2DCE,
	NULL,
	Promise_All_m4E3CD9398A6FE307C1F7B30E39E15AF6D028D277,
	Promise_All_m9430E75057AD6C834DDC6229E90EE2FF573CCCB2,
	Promise_ThenSequence_mEA544C9E1A6ECAF979AFB7072BAF1E497B0888FB,
	Promise_Sequence_mA9C41F9AA1B15AE32B13AAEB918C9C39C9CB0A93,
	Promise_Sequence_mD16057F97B23D91662D10D06F41B10360579AA7F,
	Promise_ThenRace_mBEBF13A1B643D5BD8C8AA3C8C1E34F46033E2C37,
	NULL,
	Promise_Race_m174412F14AF9AF5E72ACC54AFE424CBAFCB0FCE7,
	Promise_Race_mFB4C431EB7CC2638C26FA53D300365FD2ACA665E,
	Promise_Resolved_m7B7E203821264B26667A5173C02671FFA015D82C,
	Promise_Rejected_m75966446ECB8170A186463C054F5E40CCC479B25,
	Promise_Finally_mB06879F20FE6D9871B1B0677771662B65531FB7F,
	Promise_ContinueWith_m05367014E6744D377DEBA484EDE08F5A6A7E72F6,
	NULL,
	Promise_Progress_mC151B05499CAD04419AE9C3A30F7A5036332DCC6,
	Promise_PropagateUnhandledException_mD99B69EF5DE154AEE4E6450A6C005A08C5475B48,
	Promise__cctor_m49F60296CB19D2264E020FBF75CC32C42657A778,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mB148A4D6A46099E6B3343DDA6475E84BFC364BFF,
	Promise_U3CDoneU3Eb__40_0_m932AE49F04525E045C22BB917346EE4A0944F0FD,
	Promise_U3CDoneU3Eb__41_0_mE8AD0236B701A605BB0C8295ACD7A83F4BFADD11,
	Promise_U3CDoneU3Eb__42_0_m37DEBD8B4313D069BBB25BBB3E059331D9B4B3EF,
	U3CU3Ec__DisplayClass34_0__ctor_m777DA7CE17AD10D18C6A866CC996D64D966176C2,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mF09313B04384B86BBA78ACF4DE06FF2EAD413C61,
	U3CU3Ec__DisplayClass36_0__ctor_m1CC4BF0264C238FF06EBD6D51182AD9D9F00A44C,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m76F10866EBE95165442D9E120F5CC07DA3E6D506,
	U3CU3Ec__DisplayClass44_0__ctor_m1489BE8776752DD4703EBFDAA409ABA61E6CC649,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m08259A10A6DB4A803814CBD70B73BA501319EF68,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mF883E6C60CD8EE094BC63E1C9611855827E2CD47,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m5693532D16BF71D5506487B7A2E5C3673233EB19,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mE8F288904D95E3DC0025D1FBE2C15EEB625049AB,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m1456E18C2ED840A3FF945BBB843E07E353530B3B,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mF297954342ACFB8A71D38539141F2030D11E0668,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m40F017116B111DE7012978E6888EA133B9FB0328,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m7723E48D9F75C23B04239EFA2515B599776977E3,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m53ACC4AB8C53AFFB24DE07F3CE94FA58AC440792,
	U3CU3Ec__DisplayClass53_0__ctor_m92B2808BB6DFC101EDB128ADCE074E66706EA045,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m6E99B367DC7C957FBCE6B7E96E2BF031C5F97D13,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_mFFE18A98BD4509E971B7BEB779F6DCD187462FD5,
	U3CU3Ec__DisplayClass56_0__ctor_mB81C3730446AE8C66A7D3EAFE2F0D416873CD51C,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m9571758A00138E0C44C7A536F024544EB4EF2A5A,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_m2D14CCC0D09F29E28A2479BD4966A6EEAE384627,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m3EB91721413FD6AA4EC737807CFE4B63F9F674CF,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m404D188D0D900DF7F4BB07CD595F42537FA8F971,
	U3CU3Ec__DisplayClass59_1__ctor_mBB0688A58C068F7B05F67132C0437CA689E3540E,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m34575EAE3C7F4FE3D5749E1CAB86A63FFF38F149,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m72B3AC861C4DE8B1A5238B05D10977E5B2C76095,
	U3CU3Ec__DisplayClass60_0__ctor_m658D101B81BC36F252DED37541EFDB385BA526E4,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m119B4F1552C365688C9477DF521FE2A8E0369E92,
	U3CU3Ec__DisplayClass62_0__ctor_mD8148B137ABC682149B7AC033FFC943A4ED35CD1,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m71133B21BFC3CF61DA6557103EE0F542E2EB49AC,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m4C27A0729B96AFE4F25E08420F092F426341F0B7,
	U3CU3Ec__DisplayClass62_1__ctor_m378111927982C2FF6C8AAA87AC31A3F631D36495,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_mABFD70E61461CC8EE3F65BEC04B70048ACD23F92,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m623E301061C2E0DCC99A1F3A1472F772B111E223,
	U3CU3Ec__DisplayClass63_0__ctor_mF531911D8BEFC44215B51FEBB012E039F51266C8,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m89660D2ECA8124FCCCA46F3152655469D93B96C1,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m2B345ACF4133F168C6638758CDE9864531934908,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m623F29FF25D7E11D889754D496DDDB424906F632,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m07673F2BA8C669530194F530784716847AAE47B6,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mF77BED0CBB6803F697FFD880CDAB53ACDFE57E98,
	U3CU3Ec__DisplayClass66_1__ctor_mF9A6583E6F209770FC8B76F1E90E1AE9BE64653A,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mE3722854A8F83AA2670CABA1CA8B8FC6D7AD9253,
	U3CU3Ec__DisplayClass69_0__ctor_m46CE508BDC0469757590835B30D4A0169716ABF3,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_mFE2705C5DBAE2598D8095EA3AE23276163A8237E,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mE76BC4631E34855C4B5ADA53358122749E379CED,
	U3CU3Ec__DisplayClass70_0__ctor_mE72A5373F806CE548BA3484362F2F7FE433EEBFE,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m91E4D353282A9917F303204FD00B9A0E01016928,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mF85F2BC1F1980528C921081E57F269102ACC4273,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_mEE4C2AB59B8C048706F3AC40529D78301FFA74A8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_m555AAF5903923E345DE642D648F00143A5442218,
	PromiseException__ctor_mA3A95CC09C60213917698EA0D1F4E49ADE76AB20,
	PromiseException__ctor_m39A3D4D0F18B6923549CB458A5FB67BD56524EA0,
	PromiseStateException__ctor_m5705CBFF638E07568E352F95697FEC35697938F5,
	PromiseStateException__ctor_mA0FE8FCDFF1D932407121A2079B616CFC7D3CE24,
	PromiseStateException__ctor_m9286CD3BDADD656A97C9398D21666B1CCDAF82DB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[376] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4526,
	3636,
	4526,
	0,
	0,
	0,
	0,
	0,
	2620,
	2615,
	2615,
	3018,
	2615,
	3673,
	2615,
	4526,
	4526,
	3094,
	4526,
	3094,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3636,
	4427,
	3636,
	6974,
	6974,
	7076,
	4402,
	4427,
	3636,
	4402,
	3613,
	4526,
	3636,
	7069,
	1935,
	1935,
	1935,
	1013,
	1935,
	1015,
	4526,
	3636,
	4526,
	3673,
	3636,
	4526,
	3673,
	1935,
	3636,
	4526,
	2615,
	2615,
	0,
	2615,
	2615,
	0,
	1277,
	1277,
	0,
	788,
	788,
	1013,
	1935,
	2615,
	0,
	6849,
	6849,
	2615,
	6849,
	6849,
	2615,
	0,
	6849,
	6849,
	7076,
	6849,
	2615,
	2615,
	0,
	2615,
	6521,
	7102,
	3778,
	3636,
	3636,
	3636,
	4526,
	3659,
	4526,
	3648,
	4526,
	4526,
	3636,
	3673,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4526,
	4526,
	3673,
	4526,
	3636,
	3636,
	4526,
	4526,
	3636,
	4526,
	4427,
	0,
	0,
	4526,
	1930,
	3636,
	4526,
	3673,
	4526,
	4526,
	4427,
	4526,
	1277,
	4526,
	4526,
	4427,
	3673,
	4526,
	4427,
	0,
	0,
	4526,
	1930,
	3636,
	4526,
	4526,
	3673,
	4526,
	4526,
	3636,
	4526,
	4526,
	3636,
	0,
	0,
	0,
	0,
	0,
	0,
	4526,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4526,
	3636,
	1935,
	4526,
	3636,
	1935,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
static const Il2CppTokenRangePair s_rgctxIndices[50] = 
{
	{ 0x02000006, { 0, 102 } },
	{ 0x02000007, { 136, 1 } },
	{ 0x02000008, { 137, 1 } },
	{ 0x0200000A, { 138, 6 } },
	{ 0x0200000B, { 144, 18 } },
	{ 0x0200000C, { 162, 5 } },
	{ 0x0200000D, { 167, 2 } },
	{ 0x0200000E, { 169, 4 } },
	{ 0x0200000F, { 173, 4 } },
	{ 0x02000010, { 177, 2 } },
	{ 0x02000011, { 179, 13 } },
	{ 0x02000012, { 192, 4 } },
	{ 0x02000013, { 196, 4 } },
	{ 0x02000014, { 200, 2 } },
	{ 0x02000015, { 202, 14 } },
	{ 0x02000016, { 216, 3 } },
	{ 0x02000017, { 219, 3 } },
	{ 0x0200001B, { 255, 4 } },
	{ 0x0200001C, { 259, 10 } },
	{ 0x0200001D, { 269, 13 } },
	{ 0x02000030, { 308, 18 } },
	{ 0x02000034, { 326, 4 } },
	{ 0x0200003B, { 330, 4 } },
	{ 0x02000042, { 340, 2 } },
	{ 0x02000043, { 342, 3 } },
	{ 0x02000044, { 345, 4 } },
	{ 0x02000048, { 363, 4 } },
	{ 0x06000028, { 102, 2 } },
	{ 0x06000036, { 104, 1 } },
	{ 0x06000039, { 105, 1 } },
	{ 0x0600003C, { 106, 7 } },
	{ 0x0600003F, { 113, 6 } },
	{ 0x06000042, { 119, 6 } },
	{ 0x06000046, { 125, 6 } },
	{ 0x0600004E, { 131, 5 } },
	{ 0x06000090, { 222, 16 } },
	{ 0x06000091, { 238, 8 } },
	{ 0x06000092, { 246, 9 } },
	{ 0x060000EF, { 282, 1 } },
	{ 0x060000F2, { 283, 1 } },
	{ 0x060000F5, { 284, 7 } },
	{ 0x060000FB, { 291, 6 } },
	{ 0x06000102, { 297, 6 } },
	{ 0x06000109, { 303, 5 } },
	{ 0x0600014F, { 334, 2 } },
	{ 0x06000150, { 336, 2 } },
	{ 0x06000151, { 338, 2 } },
	{ 0x0600016E, { 349, 6 } },
	{ 0x0600016F, { 355, 6 } },
	{ 0x06000170, { 361, 2 } },
};
extern const uint32_t g_rgctx_Promise_1_set_CurState_m1ECC4DA9DDAC7C144555188547B848A353CA38FA;
extern const uint32_t g_rgctx_Promise_1_Resolve_m551CAE50A348D4FFAB47531918371231E54B767C;
extern const uint32_t g_rgctx_Action_1_t5DAD6343390BA1BEC48FA7CE128ED71DD107E73E;
extern const uint32_t g_rgctx_Action_1__ctor_m333E13DC7F9B7F4FA9D1E93E4A97A495A96A84DD;
extern const uint32_t g_rgctx_Promise_1_Reject_m1D6CA1596D12BC66D859E0A8918396BA571CD878;
extern const uint32_t g_rgctx_Action_2_tAB52B1E476C79BA9A2A540D5A878A2297CB0FA31;
extern const uint32_t g_rgctx_Action_2_Invoke_m6FF739143A5E08480FFAACAB49F88FB28EE84272;
extern const uint32_t g_rgctx_List_1_t23C374B92708CA0FD06A289D3260B7A22E08C225;
extern const uint32_t g_rgctx_List_1__ctor_m92F8157FA372D4A1D9E76EB1251292918003B2CE;
extern const uint32_t g_rgctx_List_1_Add_mDABD2E29AED77C0DDB92C1DDAF739D3176342B8D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0_tFFE9E8C2C62863D1111ADD8C585EF46E17F30258;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0__ctor_m0E231234A008B0E9B5255AA492F7F5FB2621E34C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass24_0_U3CInvokeRejectHandlersU3Eb__0_m83EA165D555686178A057EE01A164883A7AD812D;
extern const uint32_t g_rgctx_Promise_1_ClearHandlers_m71C9A7DD13A3682D2B209CC6D9F13FCE97A6B820;
extern const uint32_t g_rgctx_List_1_get_Count_m8C91FAB173DCA0E6BDDC6973E634CE7AA925801B;
extern const uint32_t g_rgctx_List_1_get_Item_m1B1DA5F2134419F72BC32358DF9B3F296B2F42DE;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m47C3DF1CFA87FFB4DD1AEB51C9EA72468BB4B9FA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0_t2CC56A63BCA633B790526BAD1EF2B8CA3D21F068;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0__ctor_m161E60AAACB6D562158552D230DE9A90C5AC307F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass26_0_U3CInvokeProgressHandlersU3Eb__0_mB59B49314A6E1FD1401685CFF6E736D11A4AB2A8;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m31674D3F00C6EA599B90E26376893B01D9179E1D;
extern const uint32_t g_rgctx_Promise_1_InvokeRejectHandlers_mC1475D7580A623428F7D113B0FE69989AFF8BD88;
extern const uint32_t g_rgctx_Promise_1_InvokeResolveHandlers_m5E1BBB5635954D23C6D89D3F9BE57592FF219853;
extern const uint32_t g_rgctx_Promise_1_InvokeProgressHandlers_m6475BD188ABB1B047B43D9ECA031C4FE0BC2E7B7;
extern const uint32_t g_rgctx_Promise_1_Then_m6532FE8FF9D26DDEDAAF3C0AF19342D85AE9F5B5;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__30_0_mE577C7A1FDC15FB0506C0EAAEA3199EE25EDCB8A;
extern const uint32_t g_rgctx_Promise_1_Then_m24742A8AD20665A61952BE253C7E619193858951;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__31_0_m1B2DE1912D01449384CE9B8347CF5FCFBBF87FBE;
extern const uint32_t g_rgctx_Promise_1_U3CDoneU3Eb__32_0_mDA7B78FC49CB323DED16EA3C45CA78D3F92A09B3;
extern const uint32_t g_rgctx_Promise_1_Catch_m019FDF43960F19D2DE4A95BCD97BAB77E6DBA154;
extern const uint32_t g_rgctx_Promise_1_set_Name_m8101F208A6DDF270EB58E85E43403E9F0644C292;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_t4A8D7788647A4ADBA9C15A7AB009E9B0842F5E30;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0__ctor_m0D1228E8A290FCF5D8ADCCC7D34987C88E535985;
extern const uint32_t g_rgctx_Promise_1_get_Name_mB7022A97842AD6897CBD39A07076B9709BC932D2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__0_mD349F0C97FA9BBAA6B39BACF9D5BC9DC12CC1BE6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__1_mDF504ADC9B7E8C56205F2EC0EA4A53458F61DEAA;
extern const uint32_t g_rgctx_Promise_1_ActionHandlers_mB88B8AAE2631FC55E6D88FE13CC09BB3495C92D9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__2_mA5E358DDE136F59B53D6E6C1CA82CDC2C33678F9;
extern const uint32_t g_rgctx_Promise_1_ProgressHandlers_mA06EE8CA23243082444F6D8590A4035643A2DFC6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_t7AF10B1676D27145D0A9D2C2E5B248F1F46DAB88;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0__ctor_mFB3F58FB58766E02B3F577C7C23271B04BD4809B;
extern const uint32_t g_rgctx_Promise_1_t32535C14922BBCD90C28AAB1F7BB7739CB521290;
extern const uint32_t g_rgctx_Promise_1__ctor_mAAE84E6DD38C426765AC31101F6B99498D7EED3C;
extern const uint32_t g_rgctx_Promise_1_WithName_m272D2728F74A2D044D5DD6C1698144143F7A2617;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__0_mB84F423B114E4CBFD46FE6CC94FA52F13CDDFC2E;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__1_m4E289E73E3666916951E8FE686CED1D982F64C9A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__2_m1E6375059FEA78A1634D95DAA52C90696D1F32EF;
extern const uint32_t g_rgctx_Promise_1_Then_mA3C0E02AC7EF55A12C685CD48CCDF3D146F2D220;
extern const uint32_t g_rgctx_Promise_1_Then_m5D3186C059D80AECBE58A3F86B9617B6DCCE1028;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_t4E358CD10BF3FC450EEC9FA4AD66D0E501957FBA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0__ctor_m20F8401E529081ACA38849490BA135489AF755E9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__0_mBCA2732A1FF9BB3D930D2CF2A693B17B64B3C172;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__1_m84033AC3740C222D269D62A3C0C4E448FE00E272;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_t999B963569B9872768870AC7FBF59E3C866466E3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m8B7AC57A4CFCFB8C2D84924E864E5A636D06BE85;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__0_mAE61247EBE814A8B3203A02BCE557B1E2829A244;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__1_mB5664BCA87878A881618C63F865295DB584D3926;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisException_t_m7397F49BBD27E6A139CBE451396AE99A8C8D8EC0;
extern const uint32_t g_rgctx_Promise_1_AddResolveHandler_m662F84012F87F3A80165AEBEC24D33836956485C;
extern const uint32_t g_rgctx_Promise_1_AddRejectHandler_mB8A743972FCC93C42426F8581E9E142550BCAED0;
extern const uint32_t g_rgctx_Promise_1_AddProgressHandler_mFC35B5113CF6FD67E118309B44B8B26BE5884A6D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_tC10CB95DAF2962270E3399964CBFC7302A6DB39B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0__ctor_mEB5A4A973837FE33E44677DF36EF7ECCDA0579D2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_U3CThenAllU3Eb__0_mC7BE3DEAD505B3C1D4F2077AF1C28DDBAF815CC8;
extern const uint32_t g_rgctx_Func_2_tEDA1EA93C2CFC33D7DD4BC9AE525EFE7366E64D1;
extern const uint32_t g_rgctx_Func_2__ctor_m89BD68AE3DCBD6D81A76109665BDEF060E1A180B;
extern const uint32_t g_rgctx_Promise_1_Then_m011DDB4B943D5AE21B34B3E3F4F5C1032CA3EF94;
extern const uint32_t g_rgctx_Promise_1_All_m9AD532795FF8619671B3D779616B24FD5611C696;
extern const uint32_t g_rgctx_Promise_1_t32535C14922BBCD90C28AAB1F7BB7739CB521290;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_t8AE16AC3629E58E92E125453FB4605C507519DC1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0__ctor_mC50BCD41762EAA20AC84974F6CEA42BED0E5082D;
extern const uint32_t g_rgctx_Enumerable_ToArray_TisIPromise_1_t14FB7E1915396FFFA349B9F2362B0FC7866D875B_m9258A611AB8E40620EC4EEC379DE923EF08E23D4;
extern const uint32_t g_rgctx_Enumerable_Empty_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m4F06CF6CB23C3370CB70022D289E44B76A99B86B;
extern const uint32_t g_rgctx_Promise_1_Resolved_m242EFD75B29E50A13A31D1541D4497DD1F179268;
extern const uint32_t g_rgctx_Promise_1_tF12886B86A88C394BFDBD0D1642A524A7419CDD3;
extern const uint32_t g_rgctx_PromisedTU5BU5D_t46868C854795A0F4561AD974EC0F164A932AE473;
extern const uint32_t g_rgctx_Promise_1_tF12886B86A88C394BFDBD0D1642A524A7419CDD3;
extern const uint32_t g_rgctx_Promise_1__ctor_m3BBD511C370F3FDB79E4AF0B84022473B23A3D9D;
extern const uint32_t g_rgctx_Promise_1_WithName_m6AAC51D6A2C0140255FE62D5BEEABF76D45516F4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__0_m8E01AEDEE27F841785A16DDCC03427ACB0F7B880;
extern const uint32_t g_rgctx_Action_2_tEAC1CC3F869BC13CCD0F257A1B244B264108F96B;
extern const uint32_t g_rgctx_Action_2__ctor_mF8609ADFA4E2DC059B026AB4480124F796C0F020;
extern const uint32_t g_rgctx_EnumerableExt_Each_TisIPromise_1_t14FB7E1915396FFFA349B9F2362B0FC7866D875B_m5361FBC4E8FE2082A3AA4FBA3293AD2A83A05E20;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_tB1105E5F66607441474BE76AE37D98E35F048E49;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_m7BD38B48A83252863A83F053044DB5BBA70B0932;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_U3CThenRaceU3Eb__0_m9BBB9910D45D866C930555D9587EA4116B447919;
extern const uint32_t g_rgctx_Promise_1_Race_m5BCE8F0A6961C79810E3C2B22C44381C3A701A41;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_tAAD2E6EFA637299CF606A97963F2389C02370F91;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0__ctor_m00B0F7D26665FFB20A2C64BD038D73323F9D83F1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__0_m9519AEE270351C9C7FD7B33D15576FC16447ED2B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_t457AA62A14C0B96BDA8D924210D0F20BC96941DB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0__ctor_m193EE2957C1CFBD7ABB2D600F8C42185DA202DCC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__0_m435BE6CA5E4579BACFA78911E59F5DEB209C614F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__1_m3FA5C0D463A1EE480D80DCDC6571C37B4B291EAA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__2_mFB8A48E22DC5DEF5B303BD0F26DC5407027A63D7;
extern const uint32_t g_rgctx_Func_2_tC7874C1A3C73AE847ED697A2219397F94114A2D1;
extern const uint32_t g_rgctx_Func_2__ctor_mB37796A2E7081921F4D3DC04E2F7523190C3C225;
extern const uint32_t g_rgctx_Promise_1_Then_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m132DC8C4C4B5E2C273B022CFDC46A29561DB76B9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_tE5DBC6C70D34F730E0C47B5F4BCC3BA62AA3E90F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0__ctor_m9AF73931D8D49C4027774DDEB9B0A49423CB1BF5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__0_mB57051FF218B76DD8AD498251C8233824A50ED46;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__1_mA5B85FDE18B70AB1E164A6EAB241F2EDE9568950;
extern const uint32_t g_rgctx_Action_1_tAA6308B02A094EFCEAAB1E3477A9FCC4D43F4C2F;
extern const uint32_t g_rgctx_Action_1_Invoke_m108989F040524694EFB0410B54F7156F7F756500;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tDA1DB186BC1FE24A5A476C29746F7E9BF9F16E71_m1EAB238ADAA3389BEC64037D4A73578DAEC8C723;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tF33414E571F136C8F3DD1F2CB2B7489A52C8D97D_m019108891408E70861991F0F3055045FE5856AC5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_tCC052122159A272047A3ECE3E98B9084ADDE0EFB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1__ctor_mAFD26CA291BF15990588740100FF741325572391;
extern const uint32_t g_rgctx_Promise_1_t15B6C371ACE956FE5E6E4899F67CF79D8E7E6294;
extern const uint32_t g_rgctx_Promise_1__ctor_m3B3988C14AD033056434AC0CFAB5D54DAE618615;
extern const uint32_t g_rgctx_Promise_1_WithName_mD90D1C8234320CBC4442C74DBDEDCD66025136DF;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__0_mE8D76974110A84E63AAE4E78EE215545A47BAFFD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__1_m46B9229E725E322221134724648AAD8BC82B0BD9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1_t6EAF6A35F00541D419C5BBC78E38251FE08BD9A2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1__ctor_m701E3AD38193BC2114FAAEC4DA4B0FEFA262C9D0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass45_0_1_U3CThenU3Eb__0_m242084279110A0B6833534E6D5FE6B59551DC9AE;
extern const uint32_t g_rgctx_Func_2_tD396911C5A60E06C7293556B00F55B0A5875E96C;
extern const uint32_t g_rgctx_Func_2__ctor_mAE0F9F400834F094B73BEC773EA4AA19D46F8B15;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tA936501C914C100BED94FEF3182303FAC6C09D09_m2F3CD4711FD39D79AA9E457D4B8638F51B874D3F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1_t56938B10C1DF13D3F6CC3C24A88943BABB12A4B4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1__ctor_m4CBDF18ED8D5239F7D34D6ADFA50D5B4037CA577;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_1_U3CThenAllU3Eb__0_mB00F4A0274BFD4E2D1C5F21BF27EA6D7CC45BFDE;
extern const uint32_t g_rgctx_Func_2_t719153F92288F6FE3D7BAE5A7C80C483EF7D6413;
extern const uint32_t g_rgctx_Func_2__ctor_m8FE370FD898FBA99816D7DA59125789A6C4EEE71;
extern const uint32_t g_rgctx_Promise_1_Then_TisIEnumerable_1_tD8F37ABF388FB04F725ECA7C7801766862110696_mFA05F43478590CF656818D054AAB74193256ACAE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1_t41FF20E8B147763CD51FEBFB35A2A578BBAD1B54;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1__ctor_m3D7FE74F80245CE514D98E176AA02418DD182DA9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_1_U3CThenRaceU3Eb__0_mA7E853170574F65EB8B4C094E5E5DA8DC501BF6D;
extern const uint32_t g_rgctx_Func_2_tF7FC0A7B2E4DF3CB84256A03943FA78A725A062C;
extern const uint32_t g_rgctx_Func_2__ctor_m106FFFA6BD8FAC8B3FDD50633E336E0537852A98;
extern const uint32_t g_rgctx_Promise_1_Then_TisConvertedT_tB0948638940780B05F28CEE8DF5BF5B7AD0C1166_mE001E3603532E117A49C3A0585B7CB557B03ED3B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_t9919A179F1813465AEC5DD632A84B6E4DB97D7F8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1__ctor_m01F47B36657581BB35E2E9E020B4114564173B4D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__0_m3D7CC1933A88FF0036B149EA697AC4B8B5B188F0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__1_mDEA55704C2A950AAAFD046A616AEC3B1F2C11158;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t30F433FD5F31795F0BAEB469843816EDE7482D2C_mBEAE40836384A6BBC133557B678982E6191129B1;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisException_t_mBB131E2175703E293D5A35ABFEA9F50F1217CC0B;
extern const uint32_t g_rgctx_Promise_1_InvokeHandler_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m4C96BF63DD9544D4150688F4883FB230A6791161;
extern const uint32_t g_rgctx_Promise_1_t823D794659A7F02BE8C335078BB018EEA4EF6B70;
extern const uint32_t g_rgctx_Promise_1_Resolve_m1F58A4ECEDB5D22B7B107D91D464C8B168017A06;
extern const uint32_t g_rgctx_Func_2_tEF6280DA606A24DA6475DC79AC9D1EEAA6E7F6BF;
extern const uint32_t g_rgctx_Func_2_Invoke_m203B6D2B53E81D2879AC4E69C8144F6E074E8CE6;
extern const uint32_t g_rgctx_Promise_1_Reject_m982B8F085378727146644F8B9863E0DE2B5DEFBF;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m9E1A94C0F2ECF456FDBAACA28D858DBE95FEB72B;
extern const uint32_t g_rgctx_Func_2_t81976C4605E78A8522F6405A11E9F3816C680A49;
extern const uint32_t g_rgctx_Func_2_Invoke_m6E40D86BFF4592DFB01AACFA612D3CDE6496F3B2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__2_mC094CFB8264AA72491F289BCFB7C00B4008CE8DF;
extern const uint32_t g_rgctx_IPromise_1_tF13114AE41252CEE248399B4DA7C87677B907BEA;
extern const uint32_t g_rgctx_IPromise_1_Progress_mEE9F4A2E2277B38A600EA53895431FE382002F05;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__3_m59333925F038CA94373AF55024E7014A3DD2D0BD;
extern const uint32_t g_rgctx_Action_1_t872F223DDD41D211C14C8C7FA933DD2376947A2D;
extern const uint32_t g_rgctx_Action_1__ctor_m7369EFD91AEE8038B12A049156214BC027CADCA3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__4_m20C1787C794F91C0275514160832AF4E864DE77E;
extern const uint32_t g_rgctx_IPromise_1_Then_mE44CCA66EB922D28FDCDA87C6DC29B7D5E0328D7;
extern const uint32_t g_rgctx_Promise_1_tE7929371F2B3D6157988CA1EECF5CC93BF0F590A;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m6FF42E8F768F514D859E5A9F211BDFDC07540A64;
extern const uint32_t g_rgctx_Promise_1_Resolve_m1DD42C287F8FC60ABB81051FC71E9C49ABAE3CA9;
extern const uint32_t g_rgctx_Promise_1_Reject_m78FB456326D92321AF15A97B59C86367F03C350D;
extern const uint32_t g_rgctx_Func_2_t2A030CB01D6DA9F85495E57D630260CF640C666A;
extern const uint32_t g_rgctx_Func_2_Invoke_m6CE0030BFB8A094E31558A782C619248C4815ABD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__5_m8CDAFC49A9196C746CF3F69DEEAE617BED58CCEA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__6_m6A1DCE6F8FAC706E0701BC9D7371780640673678;
extern const uint32_t g_rgctx_Func_2_tCE3CC19A13708FC62B4F527A5394AAE4E06D1753;
extern const uint32_t g_rgctx_Func_2_Invoke_mF77C95736EACCBC355228B9F8292C4BED40631EB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__2_mA63412F5C5C22ACB5067265F5E53545A88167161;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__3_m00E8D2C69A15A3FDEE040F1F7DD71BA5E297B9A0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__4_m5C087C8DF1F4F664AF910E1E61E5445556941BAD;
extern const uint32_t g_rgctx_Action_1_t6BD105FD8A987C0711D7A32EACDE10A5A5C4F058;
extern const uint32_t g_rgctx_Action_1_Invoke_m7F57391B1182A0DD2F4BB9608ADC4439BB6E8FCF;
extern const uint32_t g_rgctx_Func_2_tCAC09CFF6FAA9BA78D96EEFDCD69A9654D664D06;
extern const uint32_t g_rgctx_Func_2_Invoke_m613E59C231CD90D4D716EB8370A45A7F6E3D3A9C;
extern const uint32_t g_rgctx_Promise_1_Resolved_m7BBB00614F981EEAD6E97B6748FB88332F632CF0;
extern const uint32_t g_rgctx_Promise_1_t014AF287823AD8BB8B9195809E7460B611E3340F;
extern const uint32_t g_rgctx_Func_2_t460272C3F9E304087541015622B27EAB4322AF91;
extern const uint32_t g_rgctx_Func_2_Invoke_mC76CF4C9A6566D3C860BD7F799F0A77FD03ADF96;
extern const uint32_t g_rgctx_Promise_1_All_m90BAE12FEDA93F3EFF0DD3658CEEF48E87BADBE7;
extern const uint32_t g_rgctx_Promise_1_tDAAC28CAC20436377EA7C4938BD2263436E13BE0;
extern const uint32_t g_rgctx_Func_2_tB2E366C2AD72F7922DF992D75D1970F71013888C;
extern const uint32_t g_rgctx_Func_2_Invoke_mA84BBEAC6A67D3A0404A639D14B8C14055BE91CB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_tAA01817D325C959176CE9F47120D655E118778EF;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1__ctor_m041B845383B265929CB36617B1B58B49BF6D2FF0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__1_m896B49A2322CD784214AEF7A7944FF73B30DE44B;
extern const uint32_t g_rgctx_IPromise_1_tFAA402A327FA216A77CBFA3E1A4E543B88A3DE14;
extern const uint32_t g_rgctx_IPromise_1_Progress_mADE43C0601C7B7677E3B54047841731AF3C58169;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__2_m6B360CFD1ADAF7B4DC422FC4F08D6ABA8AFEEC6A;
extern const uint32_t g_rgctx_Action_1_tB1B45185AE5D5C64BCB1FCFEF0D123C56AE5D339;
extern const uint32_t g_rgctx_Action_1__ctor_m4B73E16A284E60E92950B1DCC96497F47FEA0954;
extern const uint32_t g_rgctx_IPromise_1_Then_mF25E668228DDE89F184C3A75DE034DC244D50261;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__3_m56BAA2A3322F50869A9B090193ACC5424466FF9C;
extern const uint32_t g_rgctx_Promise_1_t382861ECD22CF28A007D83C9B523014ED85B7F47;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m4E52CA34ED797D11BDDD8CF650703FFE09F47ADE;
extern const uint32_t g_rgctx_Promise_1_Reject_mEBAE8942D93D01A98DE73B8B79A6EA53EE92215C;
extern const uint32_t g_rgctx_Promise_1_t27C87830E36212B8537B81460D6DC71A5BD21FA3;
extern const uint32_t g_rgctx_Promise_1_get_CurState_m61923F12BB092F1BE456C215DA50626504F36EBD;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m8AE027F155B4A4591D655B446831C2AE94BA7F8A;
extern const uint32_t g_rgctx_Promise_1_Resolve_m72F955CAF58D5355A578ABD7CA1BDCBF5703CAF3;
extern const uint32_t g_rgctx_Func_2_tEEE6428F194A864E84247C895EFE317048960596;
extern const uint32_t g_rgctx_Func_2_Invoke_mCFC985C5703484EF81FBD93BA3DC45075F309BE1;
extern const uint32_t g_rgctx_Promise_1_Race_m9F11E64ABEC46703FBCBEECFAE6894E62E0663C3;
extern const uint32_t g_rgctx_Promise_1_tE93E161E98885D3D50CBB2C0E8A3D62D19991DB3;
extern const uint32_t g_rgctx_Func_2_t00F60B4EAC6EF7BAADECB32EBBF7997FF2C385A5;
extern const uint32_t g_rgctx_Func_2_Invoke_mC31BC808A10C4E0060B6A7268545DC47E8FD2FE2;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1_tE9C105F21A82573AAC78EE9135E8808117A3BB60;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1__ctor_mFE2B873402D826C6A5EE441995B197E6E96F366A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_1_U3CRaceU3Eb__1_mA0969CD6E2B5549ACB56A8A7F15D22131878D1AF;
extern const uint32_t g_rgctx_IPromise_1_t2DD5DA0ACA6A947FBAC37745C6D0A9F38FDB43FF;
extern const uint32_t g_rgctx_IPromise_1_Progress_mCF4D3E801A4536DB9955DE3CCA982CF744C8A010;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__2_m9F4AA93FD0A4CDC7602384828BC7576AB68D4A96;
extern const uint32_t g_rgctx_Action_1_t5BD48C61D0FDF349342E0019CD985126329A8CDA;
extern const uint32_t g_rgctx_Action_1__ctor_m2081D05CED069C5D304969DC2192EA0B83191462;
extern const uint32_t g_rgctx_IPromise_1_Then_m5D56CF9F8C538CBF178C9DA75D99D0CCDB774DAB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__3_mE211A73253EDA549696C9CBB48C3203A4EAEDE49;
extern const uint32_t g_rgctx_Promise_1_tB8F88D448F6435994178ACC3AFC340FC361F3ADC;
extern const uint32_t g_rgctx_Promise_1_get_CurState_mF1FFA924A66693CE2EC0486F670521D61D89413B;
extern const uint32_t g_rgctx_Promise_1_Resolve_m5C61E73256A0CC81DF9EEEA095E7D5E5BA2A82D9;
extern const uint32_t g_rgctx_Promise_1_Reject_m40B9A74361B3924237903AC29F298C649A012DD4;
extern const uint32_t g_rgctx_Promise_1_t7B2922EEF2D6E1C0BCE661ED4F4C73D8F9367107;
extern const uint32_t g_rgctx_Promise_1_get_CurState_mC91339BAF748606216BB033CB0B588D8BAC5C26D;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_mF67B1B7E2B60A6A863A573E4BAAD146714976703;
extern const uint32_t g_rgctx_Promise_1_tEC0063F8255DED72478E7DD3B20996EA0F2C7BAD;
extern const uint32_t g_rgctx_Promise_1_Resolve_mC3816CB90584C3C4776169EB90EEE524108BE65D;
extern const uint32_t g_rgctx_Promise_1_Reject_mA09A16E0C79AB585E0522CD54422C01ACCACA3CB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2_tFD23EF7EF995DCFEABDA991D0CDE43500D59105D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2__ctor_m1448B1271E6F6BB53CAEDBEE9830D7B1FE4C9B61;
extern const uint32_t g_rgctx_Promise_1_t5050B6B6D6A95AF941E45A1F0B2CC8714B9F4108;
extern const uint32_t g_rgctx_Promise_1__ctor_mFC089C920318F18F32B6109EEEF97983F8FC2D14;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__0_m6AF52D88E0E50F01F7BEAB98E263D4A834B2B7B6;
extern const uint32_t g_rgctx_Action_1_t6DAEEF20045C6F70A793DB88316C4112101C087E;
extern const uint32_t g_rgctx_Action_1__ctor_m00BCC4C103F536F3E37A6F23B462AE68415A4D6E;
extern const uint32_t g_rgctx_IPromise_1_t69ED5B602125D0C74D5D090A230150DA9BDA0117;
extern const uint32_t g_rgctx_IPromise_1_Then_m9D499AC6B5F8FBB85C4DAA76BF8DB36E35088BBC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__1_m9A6CACF56A4439F2B3CEE4D1864DF6E058771EC6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__2_m05D1B670808D9C654A2A9A02639FD757E93B582C;
extern const uint32_t g_rgctx_Action_1_t27BF23E1152F8D021333BCD7F024A4F3495136B9;
extern const uint32_t g_rgctx_Action_1__ctor_m67958F0D5251B0F64B175B485BE6C6ED4BFB39F3;
extern const uint32_t g_rgctx_IPromise_1_tC3FB9910D484BA7A41B69307FBCD3AFF183410ED;
extern const uint32_t g_rgctx_IPromise_1_Then_mBA4DEBF9FE3EC69B91D5C9235B522629958A0076;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__3_m639DE3408E9ED93A7E14950A44935238E1EF8FC0;
extern const uint32_t g_rgctx_PromiseHelpers_All_TisT1_tDEC7DF9B191C75AE6E0D7E58C56954C325DAED4F_TisT2_t63DF01EF720CB123EE38972778F1981398A5CDB8_m65A7B7AD5097DD1386C45362D572E4A292AFB7D0;
extern const uint32_t g_rgctx_PromiseHelpers_All_TisTuple_2_tBC53FC8D43038543914E56555211C8FFA8367D0F_TisT3_t0E524C042673E05526FF2B21380CE0E56E3D65A7_m2416A77A744B31DAA6E1ED312433C2A7C9B46A30;
extern const uint32_t g_rgctx_U3CU3Ec__1_3_t6437F11E017087BA97988FC0725AF97F3B53C7A1;
extern const uint32_t g_rgctx_U3CU3Ec__1_3_U3CAllU3Eb__1_0_mA547613BFC67124E0568D0D2B20894F0A6345C78;
extern const uint32_t g_rgctx_Func_2_tF2BB9F60AB7228CEE7E71AD0395C05089FF0B85D;
extern const uint32_t g_rgctx_Func_2__ctor_mA026BA06D682754DC44BFB224624318211E8472C;
extern const uint32_t g_rgctx_IPromise_1_tEB477363BECB34EEDD8AC237C2A8E44D3F8D69A2;
extern const uint32_t g_rgctx_IPromise_1_Then_TisTuple_3_tF42F666C710E5B42B262C8400275B682EE44161C_mCD12CE7581ADA643AAA53ADDE53BEB77C178B07C;
extern const uint32_t g_rgctx_PromiseHelpers_All_TisT1_tF623FA9068A6235E2FA85633431CA8B629A13C02_TisT2_t9B439CCD835A795F915591CC7AD987968B9BE4F9_mE2597588BD302DD4EE43D89F5536D4A36710FA97;
extern const uint32_t g_rgctx_PromiseHelpers_All_TisT3_tACB2F41F2DC4B4F550689242EC879D7B096D568E_TisT4_t527A9C88ECF5D76F280B71B8DACEFE40A151D81F_mFF4E94BA62609CA96367D9C655922F713935B072;
extern const uint32_t g_rgctx_PromiseHelpers_All_TisTuple_2_t731B82FBB52D32710FB5EA011228D01763E7A648_TisTuple_2_t492188A09D7126E3C481F8DE1C29DC69CFE58EBD_m868141569046D5F325D54AC2F44E6194DD565FB3;
extern const uint32_t g_rgctx_U3CU3Ec__2_4_t52F9D7BE83AB9A676E615D059CE21E4742788371;
extern const uint32_t g_rgctx_U3CU3Ec__2_4_U3CAllU3Eb__2_0_mD5CE2F69D95A5F10000FFB1B3DAFD9F3E8C95DD1;
extern const uint32_t g_rgctx_Func_2_t86CB8F80042B8E6B1657161EADA5C8CFC3782044;
extern const uint32_t g_rgctx_Func_2__ctor_m76C18CFB92A48A05E48894A10C41B0CD3B7C7599;
extern const uint32_t g_rgctx_IPromise_1_tC25D24B1E5550C1E7C30F3AD8C3D8972539313A1;
extern const uint32_t g_rgctx_IPromise_1_Then_TisTuple_4_t4353CA157EDBCFF686E84C7726D0D83ED7C991E0_m46C9FC7BF773171350E263175AF7D00F6FE07A76;
extern const uint32_t g_rgctx_Tuple_Create_TisT1_t946325D96409E9073D21393B0B21682562E5EFA9_TisT2_t3700F925F31E37BDF386A3091E765E194FFE0B10_mF3DABB26D5BFADDDE04F1918172B0E12D60BEDD4;
extern const uint32_t g_rgctx_Promise_1_t800D4B7C9E388ECD1634C8F387DCF695F5A09995;
extern const uint32_t g_rgctx_Promise_1_Resolve_mDE6361C4D62899188524D333BB6499E98CBA96DE;
extern const uint32_t g_rgctx_Promise_1_Reject_mE77552F982CB5DDCE7FC69DD49529F5456C04C90;
extern const uint32_t g_rgctx_U3CU3Ec__1_3_tF8F8BF2E4103D40A75F9A16AE41C62E72A198717;
extern const uint32_t g_rgctx_U3CU3Ec__1_3__ctor_m2AA3C097AA2232FB879F88F7438FAB87D42C85B2;
extern const uint32_t g_rgctx_U3CU3Ec__1_3_tF8F8BF2E4103D40A75F9A16AE41C62E72A198717;
extern const uint32_t g_rgctx_Tuple_2_tCAAE3B87F4B41382F8E77E59715848116DED9506;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_m8A7B45EB2E032C0DEA47014656F28AD413828583;
extern const uint32_t g_rgctx_Tuple_2_t94B0BC55859F7FDDF45B67D7A0C17BA4627E744B;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_mD303449F6BA87C4DBDED46C31E451EF6A2F0D39B;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_m2ED0FC2A9109AD6708814F0860331DF3E9905C91;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_m35164E626317248921ECAD872978340D0C215A66;
extern const uint32_t g_rgctx_Tuple_Create_TisT1_tD47B7D606D199E477FCB82860A34D7CA5DC67AD6_TisT2_t87D1D7B5AD08C9291ABB2996B1235A63D650C737_TisT3_t92DFCB9B719F7D02311CAD7CEEF0878F5FF41924_m96AF1271184D9C393AA187F901164BDCCC139E1A;
extern const uint32_t g_rgctx_U3CU3Ec__2_4_tC8D674EE5D3561900D8E59F0458AE17647FFE96F;
extern const uint32_t g_rgctx_U3CU3Ec__2_4__ctor_m5986F6A49DA30FB0B5F58EC7818FF0A797E9A009;
extern const uint32_t g_rgctx_U3CU3Ec__2_4_tC8D674EE5D3561900D8E59F0458AE17647FFE96F;
extern const uint32_t g_rgctx_Tuple_2_tBA319E477525859DE715C1E024B25750E4F46538;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_m969E9DF4C065109B55B49221D1FD2913D7E40131;
extern const uint32_t g_rgctx_Tuple_2_t024F99E564B731BFB74960651FC2F129AA0D151E;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_mE84FEF0B48524C9C8EDB0BA5E5DC9FED3BCE5D17;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_mFC61E64E8D4DB3985C1B2DFA043FEE4E9DF92205;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_mC75EC2D534C56039C96E4E69A10AD066D99245B7;
extern const uint32_t g_rgctx_Tuple_2_t0CA673C0F74A7627522C7AAB56C6FA86969C9F69;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_m0B0C523BE5B9CDD30645A4FCD09196A88A376756;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_m79AD18035AF520015438207E6AC279FCA20D0EDD;
extern const uint32_t g_rgctx_Tuple_Create_TisT1_tF91049BCFEC64966F3A9FBD9D899270164791918_TisT2_t89127E27C14EEDDA8DF91ADB69546DE9DC544EF2_TisT3_t6B787F7DB3CDF66947E3901547245B3B08E1FEE1_TisT4_t552596B8E84211C9786CD315A4C0173DE08C7E66_m42B72BC1FA7A5990FA928461A92526FDD0A7709A;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t562C77C656E4AE2CEF25CBE8E849C1F3948DAC71_m3D66B9997751D097FE6A665C95E345E0C2BA8F0C;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_tE4FB303F1DA331D2620B92EEEEA3DA71D589D788_m553826AFF55AAFFE27140A55ABAD64828CE0B908;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_tF687161371CCF86E51C8702D511DE7DBA8EBBB51;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_mD61D74AEF8A8E57971C412B794C7BAAA49BF5BAD;
extern const uint32_t g_rgctx_Promise_1_t553430A46EDB9BDA5599FCDC986B21D164DEC1E7;
extern const uint32_t g_rgctx_Promise_1__ctor_mB2EB49DA59CC0F1ACDF7984D3296B04F33EF15C4;
extern const uint32_t g_rgctx_Promise_1_WithName_m1BF042E6B77A54A05D48F7108EFC3DAB78F0BDC4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__0_mFAC039FDB6AD9A8D1E1A11276A034BFF7B154404;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__1_mFEA05BA0B90F5ECFD5BECD639EAEACA237FB0845;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1_tB39077A83141BABC2C5EFD9F6060F221361809DE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1__ctor_m1C183923FE4388F471A67BDD0C98CFAE52CCFD1F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass57_0_1_U3CThenAllU3Eb__0_mD21E3768650A1B44C4C5B64A2369347DB7C90C84;
extern const uint32_t g_rgctx_Func_1_t642E17C15D67DFD3B9C6ED38038AF75ED101C476;
extern const uint32_t g_rgctx_Func_1__ctor_m6A671146485DEAB830E8A7D9283D6843B1A9B4BB;
extern const uint32_t g_rgctx_Promise_Then_TisIEnumerable_1_tEF6B72C3E0782763FC243239A2B9D3504543F356_mF7CFD1E093DB992B29478BDB56AA5F1A535E8EDA;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1_t76A141A9DCFB78BC6612A45BDF742393A0CEFA79;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1__ctor_m58259DA0307AF326099B1D1116F48E5D55833E5B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass64_0_1_U3CThenRaceU3Eb__0_m9A2F836C07A85D17EE426CE52E83061C7EF1208E;
extern const uint32_t g_rgctx_Func_1_tF2FE2816C581D2937EE8167924E96E6533D602F0;
extern const uint32_t g_rgctx_Func_1__ctor_m272EA3AFF671BF0759CFFACE23F2797FE8C8FFF2;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t20F10CBA271586144917AE10F6A2686CF4104134_m310F04E4290ABDFD05A99786215A3065E73B1EE3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_t31C86448732111AEF79BA64CB3825F99F142255B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1__ctor_m4DF0068183F68F39C53D23550323338C5E90EB46;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__0_m5CE41BB47C385AE700B06985929B9868BEACC0FD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__1_m8716013A3CBD0D099AB2B22D137BB3705E9AED03;
extern const uint32_t g_rgctx_Promise_Then_TisConvertedT_t19A72C91376F413FAADEF64191E2528F218286F7_m3CC698140D152D56C09A57DFF3935AE2D30D5165;
extern const uint32_t g_rgctx_Func_1_tB46CE9ABC0B4D0246AC7EE98C729267D0C87B5B9;
extern const uint32_t g_rgctx_Func_1_Invoke_m34DF8DB558EC136F160E41E956ED5A5A90F3AB14;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__2_m4F991939A4042953EC6AB3C0E60156691E17E322;
extern const uint32_t g_rgctx_IPromise_1_t289BA40924C19D1285039B91B1C9B72A38E26842;
extern const uint32_t g_rgctx_IPromise_1_Progress_m124FD05986AF249679CBC07BA7DE7875F4255D0B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__3_mFD8226C2096B95A476FC913A8529C87E550F5A97;
extern const uint32_t g_rgctx_Action_1_t3E4C1EE3A9E5D61297A0DAE72607AA3E0F02FB7A;
extern const uint32_t g_rgctx_Action_1__ctor_mFD7E2E627BD07B5D55B0057C9303DE0B4BA58826;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__4_mF518B202C2A6E352D435C22A7E6ACFA71E8C8D1E;
extern const uint32_t g_rgctx_IPromise_1_Then_mE38A4073E63325C14716A52DF4CF1E9A3E4AABE4;
extern const uint32_t g_rgctx_Promise_1_t83E2C1F04504C0CE0B37D9812B04905A14C7E492;
extern const uint32_t g_rgctx_Promise_1_ReportProgress_m4AFD35D5632D5279BA9CDD611950C56D667EDA1F;
extern const uint32_t g_rgctx_Promise_1_Resolve_mDD99CF0BC024EA1279E407EBE3FEE9F6BB8093C7;
extern const uint32_t g_rgctx_Promise_1_Reject_mB46B82875D95FEF7BD7B4DCE9D2621E0E5E8FE88;
extern const uint32_t g_rgctx_Func_2_tE71870839576C06CAFE85FA313ECFD14ED2A0F9B;
extern const uint32_t g_rgctx_Func_2_Invoke_m86009A6DB1987858E00BB5D38FC933CED2A761E6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__5_m2C626CD9F48580227DCD926D200375910A2C7186;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__6_mA2F8FF6AED77CF11E8C0BEB967A867FCEC767467;
extern const uint32_t g_rgctx_Func_1_tCD49995BC71E9DABD30BE72490E3642A78C0FD8B;
extern const uint32_t g_rgctx_Func_1_Invoke_mB0C018F2A2F6CC19757C4E9F4AC779884DAA0351;
extern const uint32_t g_rgctx_Promise_1_All_mAFD902AABD0043BD15D9B754D3721471DC186B79;
extern const uint32_t g_rgctx_Promise_1_t7920E010701EEF3EC85D2AF7D921B6389E8463DC;
extern const uint32_t g_rgctx_Func_1_t6CAA0DE4A9CDE0BD875C71004A3B41AA78D1BE5C;
extern const uint32_t g_rgctx_Func_1_Invoke_m681D38F7A1E7867D61669095004B2B4B8E30EA41;
extern const uint32_t g_rgctx_Promise_1_Race_m3ED11384E340051D43A5CDC58FF77FDAFDF1C227;
extern const uint32_t g_rgctx_Promise_1_tD3E6006A8341AA63B01E4AC4B11F3C82E0EB5053;
extern const uint32_t g_rgctx_Tuple_2_t521350C44AE103E8D6822E8A6A5B9B060C01AE3B;
extern const uint32_t g_rgctx_Tuple_2__ctor_m5A8CE3F8933BB422D01A9ED229D10FC8083D7D6C;
extern const uint32_t g_rgctx_Tuple_3_t4FBA50C71573D5B2E0DE754B3ED077EED21FB440;
extern const uint32_t g_rgctx_Tuple_3__ctor_mD981EFFD6B445CD09AE3518F87818C94E79A3C81;
extern const uint32_t g_rgctx_Tuple_4_tD7E09361614FF69F41AFF4AEF80A3C938C5427CF;
extern const uint32_t g_rgctx_Tuple_4__ctor_mF76B71862FF460AECE7201DD83197592B8A3273A;
extern const uint32_t g_rgctx_Tuple_2_set_Item1_mAAF86A4803DA85CC47054EA3BB2534DDC315B0D2;
extern const uint32_t g_rgctx_Tuple_2_set_Item2_m6CE5D847F4901A58C006EBEECBD10B3F772572AB;
extern const uint32_t g_rgctx_Tuple_3_set_Item1_mA59EEA36596946CED387A81E063DECD4D985E693;
extern const uint32_t g_rgctx_Tuple_3_set_Item2_m19D5C5D3C2C85EDE543DE63CB8C68BB1C509D545;
extern const uint32_t g_rgctx_Tuple_3_set_Item3_m5EDBB940FB3EE8BA8624E834F99E973DE391084D;
extern const uint32_t g_rgctx_Tuple_4_set_Item1_m0DEEF9390EDD601009563571FB37F06B9B1F8BC7;
extern const uint32_t g_rgctx_Tuple_4_set_Item2_m6B48F170DE5A73D0F136531B8CD71FAC0B5E86A8;
extern const uint32_t g_rgctx_Tuple_4_set_Item3_m29F88ABDA5003F4B65783ED9DC488C1EE18E408C;
extern const uint32_t g_rgctx_Tuple_4_set_Item4_m6C3423F86EAF37A3CA5A3BF70F3D07F8B30D6EE4;
extern const uint32_t g_rgctx_IEnumerable_1_tA36C9FA35D29D4461CD517D4765863B275DB126C;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m50375A4A9D85717BB576EAF63F6FD51051716AD6;
extern const uint32_t g_rgctx_IEnumerator_1_t88884C906DD61587AC347F90CD6C6B21E580EF00;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m1691C576A18EF29A6FC93161AB02CD421D854536;
extern const uint32_t g_rgctx_Action_1_tA2B96578B0FB7BCA561EFA9A6BA6CECF8D42672E;
extern const uint32_t g_rgctx_Action_1_Invoke_mF0C07F30D5F4717036D017691A53C1B505899BA3;
extern const uint32_t g_rgctx_IEnumerable_1_t5BADFAF5202F9CF9077189D3E8DD8757C5765F53;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m27B267FC452918964AB1AA262D653CF2E9156FF1;
extern const uint32_t g_rgctx_IEnumerator_1_t556E9DA7F49DFAF159EB3C4666A9D1334EEA3A95;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m65A0BDEBDE25A961AA72EAEBB1F0246CB4B26282;
extern const uint32_t g_rgctx_Action_2_t0F889AC23D4FB49FDFFE808E4EA90FC46DCC39CE;
extern const uint32_t g_rgctx_Action_2_Invoke_m22756DF4F09C63B4D96C1BDB8809C0EA63FE411C;
extern const uint32_t g_rgctx_U3CFromItemsU3Ed__2_1_t4C926E540471154A762DFA4DE2650885210AB992;
extern const uint32_t g_rgctx_U3CFromItemsU3Ed__2_1__ctor_mF9712B5E61E609754E83B634B8455220CF1C4704;
extern const uint32_t g_rgctx_T_tBE32C62E1C5B40E809813A4B1FDCC3D5D39B3B7E;
extern const uint32_t g_rgctx_U3CFromItemsU3Ed__2_1_tF02A1FF09E5733363C80B4FB996D97E22F81E790;
extern const uint32_t g_rgctx_U3CFromItemsU3Ed__2_1__ctor_mA72897A8E84CF48CDF466D406C464C819A09B2C5;
extern const uint32_t g_rgctx_U3CFromItemsU3Ed__2_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2EEA760438AD700F9325CABD86F7C9CB81ABA511;
static const Il2CppRGCTXDefinition s_rgctxValues[367] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_set_CurState_m1ECC4DA9DDAC7C144555188547B848A353CA38FA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m551CAE50A348D4FFAB47531918371231E54B767C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t5DAD6343390BA1BEC48FA7CE128ED71DD107E73E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m333E13DC7F9B7F4FA9D1E93E4A97A495A96A84DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m1D6CA1596D12BC66D859E0A8918396BA571CD878 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tAB52B1E476C79BA9A2A540D5A878A2297CB0FA31 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m6FF739143A5E08480FFAACAB49F88FB28EE84272 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t23C374B92708CA0FD06A289D3260B7A22E08C225 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m92F8157FA372D4A1D9E76EB1251292918003B2CE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mDABD2E29AED77C0DDB92C1DDAF739D3176342B8D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0_tFFE9E8C2C62863D1111ADD8C585EF46E17F30258 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0__ctor_m0E231234A008B0E9B5255AA492F7F5FB2621E34C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass24_0_U3CInvokeRejectHandlersU3Eb__0_m83EA165D555686178A057EE01A164883A7AD812D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ClearHandlers_m71C9A7DD13A3682D2B209CC6D9F13FCE97A6B820 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m8C91FAB173DCA0E6BDDC6973E634CE7AA925801B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m1B1DA5F2134419F72BC32358DF9B3F296B2F42DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m47C3DF1CFA87FFB4DD1AEB51C9EA72468BB4B9FA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0_t2CC56A63BCA633B790526BAD1EF2B8CA3D21F068 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0__ctor_m161E60AAACB6D562158552D230DE9A90C5AC307F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass26_0_U3CInvokeProgressHandlersU3Eb__0_mB59B49314A6E1FD1401685CFF6E736D11A4AB2A8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m31674D3F00C6EA599B90E26376893B01D9179E1D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeRejectHandlers_mC1475D7580A623428F7D113B0FE69989AFF8BD88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeResolveHandlers_m5E1BBB5635954D23C6D89D3F9BE57592FF219853 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeProgressHandlers_m6475BD188ABB1B047B43D9ECA031C4FE0BC2E7B7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m6532FE8FF9D26DDEDAAF3C0AF19342D85AE9F5B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__30_0_mE577C7A1FDC15FB0506C0EAAEA3199EE25EDCB8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m24742A8AD20665A61952BE253C7E619193858951 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__31_0_m1B2DE1912D01449384CE9B8347CF5FCFBBF87FBE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_U3CDoneU3Eb__32_0_mDA7B78FC49CB323DED16EA3C45CA78D3F92A09B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Catch_m019FDF43960F19D2DE4A95BCD97BAB77E6DBA154 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_set_Name_m8101F208A6DDF270EB58E85E43403E9F0644C292 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_t4A8D7788647A4ADBA9C15A7AB009E9B0842F5E30 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0__ctor_m0D1228E8A290FCF5D8ADCCC7D34987C88E535985 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_Name_mB7022A97842AD6897CBD39A07076B9709BC932D2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__0_mD349F0C97FA9BBAA6B39BACF9D5BC9DC12CC1BE6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__1_mDF504ADC9B7E8C56205F2EC0EA4A53458F61DEAA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ActionHandlers_mB88B8AAE2631FC55E6D88FE13CC09BB3495C92D9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass34_0_U3CCatchU3Eb__2_mA5E358DDE136F59B53D6E6C1CA82CDC2C33678F9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ProgressHandlers_mA06EE8CA23243082444F6D8590A4035643A2DFC6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_t7AF10B1676D27145D0A9D2C2E5B248F1F46DAB88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0__ctor_mFB3F58FB58766E02B3F577C7C23271B04BD4809B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t32535C14922BBCD90C28AAB1F7BB7739CB521290 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mAAE84E6DD38C426765AC31101F6B99498D7EED3C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m272D2728F74A2D044D5DD6C1698144143F7A2617 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__0_mB84F423B114E4CBFD46FE6CC94FA52F13CDDFC2E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__1_m4E289E73E3666916951E8FE686CED1D982F64C9A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass35_0_U3CCatchU3Eb__2_m1E6375059FEA78A1634D95DAA52C90696D1F32EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_mA3C0E02AC7EF55A12C685CD48CCDF3D146F2D220 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m5D3186C059D80AECBE58A3F86B9617B6DCCE1028 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_t4E358CD10BF3FC450EEC9FA4AD66D0E501957FBA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0__ctor_m20F8401E529081ACA38849490BA135489AF755E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__0_mBCA2732A1FF9BB3D930D2CF2A693B17B64B3C172 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__1_m84033AC3740C222D269D62A3C0C4E448FE00E272 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_t999B963569B9872768870AC7FBF59E3C866466E3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m8B7AC57A4CFCFB8C2D84924E864E5A636D06BE85 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__0_mAE61247EBE814A8B3203A02BCE557B1E2829A244 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_U3CThenU3Eb__1_mB5664BCA87878A881618C63F865295DB584D3926 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisException_t_m7397F49BBD27E6A139CBE451396AE99A8C8D8EC0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddResolveHandler_m662F84012F87F3A80165AEBEC24D33836956485C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddRejectHandler_mB8A743972FCC93C42426F8581E9E142550BCAED0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_AddProgressHandler_mFC35B5113CF6FD67E118309B44B8B26BE5884A6D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_tC10CB95DAF2962270E3399964CBFC7302A6DB39B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0__ctor_mEB5A4A973837FE33E44677DF36EF7ECCDA0579D2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_U3CThenAllU3Eb__0_mC7BE3DEAD505B3C1D4F2077AF1C28DDBAF815CC8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tEDA1EA93C2CFC33D7DD4BC9AE525EFE7366E64D1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m89BD68AE3DCBD6D81A76109665BDEF060E1A180B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_m011DDB4B943D5AE21B34B3E3F4F5C1032CA3EF94 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_m9AD532795FF8619671B3D779616B24FD5611C696 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t32535C14922BBCD90C28AAB1F7BB7739CB521290 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_t8AE16AC3629E58E92E125453FB4605C507519DC1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0__ctor_mC50BCD41762EAA20AC84974F6CEA42BED0E5082D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_ToArray_TisIPromise_1_t14FB7E1915396FFFA349B9F2362B0FC7866D875B_m9258A611AB8E40620EC4EEC379DE923EF08E23D4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_Empty_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m4F06CF6CB23C3370CB70022D289E44B76A99B86B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolved_m242EFD75B29E50A13A31D1541D4497DD1F179268 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tF12886B86A88C394BFDBD0D1642A524A7419CDD3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_PromisedTU5BU5D_t46868C854795A0F4561AD974EC0F164A932AE473 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tF12886B86A88C394BFDBD0D1642A524A7419CDD3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m3BBD511C370F3FDB79E4AF0B84022473B23A3D9D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m6AAC51D6A2C0140255FE62D5BEEABF76D45516F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__0_m8E01AEDEE27F841785A16DDCC03427ACB0F7B880 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tEAC1CC3F869BC13CCD0F257A1B244B264108F96B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2__ctor_mF8609ADFA4E2DC059B026AB4480124F796C0F020 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumerableExt_Each_TisIPromise_1_t14FB7E1915396FFFA349B9F2362B0FC7866D875B_m5361FBC4E8FE2082A3AA4FBA3293AD2A83A05E20 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_tB1105E5F66607441474BE76AE37D98E35F048E49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_m7BD38B48A83252863A83F053044DB5BBA70B0932 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_U3CThenRaceU3Eb__0_m9BBB9910D45D866C930555D9587EA4116B447919 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_m5BCE8F0A6961C79810E3C2B22C44381C3A701A41 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_tAAD2E6EFA637299CF606A97963F2389C02370F91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0__ctor_m00B0F7D26665FFB20A2C64BD038D73323F9D83F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__0_m9519AEE270351C9C7FD7B33D15576FC16447ED2B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_t457AA62A14C0B96BDA8D924210D0F20BC96941DB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0__ctor_m193EE2957C1CFBD7ABB2D600F8C42185DA202DCC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__0_m435BE6CA5E4579BACFA78911E59F5DEB209C614F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__1_m3FA5C0D463A1EE480D80DCDC6571C37B4B291EAA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass58_0_U3CFinallyU3Eb__2_mFB8A48E22DC5DEF5B303BD0F26DC5407027A63D7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tC7874C1A3C73AE847ED697A2219397F94114A2D1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_mB37796A2E7081921F4D3DC04E2F7523190C3C225 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisPromisedT_tF5F3900FAED2B1607D43B9F2748BBFD3EBCF0BFC_m132DC8C4C4B5E2C273B022CFDC46A29561DB76B9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_tE5DBC6C70D34F730E0C47B5F4BCC3BA62AA3E90F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0__ctor_m9AF73931D8D49C4027774DDEB9B0A49423CB1BF5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__0_mB57051FF218B76DD8AD498251C8233824A50ED46 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass59_0_U3CContinueWithU3Eb__1_mA5B85FDE18B70AB1E164A6EAB241F2EDE9568950 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tAA6308B02A094EFCEAAB1E3477A9FCC4D43F4C2F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m108989F040524694EFB0410B54F7156F7F756500 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tDA1DB186BC1FE24A5A476C29746F7E9BF9F16E71_m1EAB238ADAA3389BEC64037D4A73578DAEC8C723 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tF33414E571F136C8F3DD1F2CB2B7489A52C8D97D_m019108891408E70861991F0F3055045FE5856AC5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_tCC052122159A272047A3ECE3E98B9084ADDE0EFB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1__ctor_mAFD26CA291BF15990588740100FF741325572391 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t15B6C371ACE956FE5E6E4899F67CF79D8E7E6294 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_m3B3988C14AD033056434AC0CFAB5D54DAE618615 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_mD90D1C8234320CBC4442C74DBDEDCD66025136DF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__0_mE8D76974110A84E63AAE4E78EE215545A47BAFFD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__1_m46B9229E725E322221134724648AAD8BC82B0BD9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1_t6EAF6A35F00541D419C5BBC78E38251FE08BD9A2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1__ctor_m701E3AD38193BC2114FAAEC4DA4B0FEFA262C9D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass45_0_1_U3CThenU3Eb__0_m242084279110A0B6833534E6D5FE6B59551DC9AE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tD396911C5A60E06C7293556B00F55B0A5875E96C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_mAE0F9F400834F094B73BEC773EA4AA19D46F8B15 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tA936501C914C100BED94FEF3182303FAC6C09D09_m2F3CD4711FD39D79AA9E457D4B8638F51B874D3F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1_t56938B10C1DF13D3F6CC3C24A88943BABB12A4B4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1__ctor_m4CBDF18ED8D5239F7D34D6ADFA50D5B4037CA577 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_1_U3CThenAllU3Eb__0_mB00F4A0274BFD4E2D1C5F21BF27EA6D7CC45BFDE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t719153F92288F6FE3D7BAE5A7C80C483EF7D6413 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m8FE370FD898FBA99816D7DA59125789A6C4EEE71 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisIEnumerable_1_tD8F37ABF388FB04F725ECA7C7801766862110696_mFA05F43478590CF656818D054AAB74193256ACAE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1_t41FF20E8B147763CD51FEBFB35A2A578BBAD1B54 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1__ctor_m3D7FE74F80245CE514D98E176AA02418DD182DA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_1_U3CThenRaceU3Eb__0_mA7E853170574F65EB8B4C094E5E5DA8DC501BF6D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tF7FC0A7B2E4DF3CB84256A03943FA78A725A062C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m106FFFA6BD8FAC8B3FDD50633E336E0537852A98 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Then_TisConvertedT_tB0948638940780B05F28CEE8DF5BF5B7AD0C1166_mE001E3603532E117A49C3A0585B7CB557B03ED3B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_t9919A179F1813465AEC5DD632A84B6E4DB97D7F8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1__ctor_m01F47B36657581BB35E2E9E020B4114564173B4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__0_m3D7CC1933A88FF0036B149EA697AC4B8B5B188F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass60_0_1_U3CContinueWithU3Eb__1_mDEA55704C2A950AAAFD046A616AEC3B1F2C11158 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t30F433FD5F31795F0BAEB469843816EDE7482D2C_mBEAE40836384A6BBC133557B678982E6191129B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisException_t_mBB131E2175703E293D5A35ABFEA9F50F1217CC0B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_InvokeHandler_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m4C96BF63DD9544D4150688F4883FB230A6791161 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t823D794659A7F02BE8C335078BB018EEA4EF6B70 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m1F58A4ECEDB5D22B7B107D91D464C8B168017A06 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tEF6280DA606A24DA6475DC79AC9D1EEAA6E7F6BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m203B6D2B53E81D2879AC4E69C8144F6E074E8CE6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m982B8F085378727146644F8B9863E0DE2B5DEFBF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m9E1A94C0F2ECF456FDBAACA28D858DBE95FEB72B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t81976C4605E78A8522F6405A11E9F3816C680A49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m6E40D86BFF4592DFB01AACFA612D3CDE6496F3B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__2_mC094CFB8264AA72491F289BCFB7C00B4008CE8DF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tF13114AE41252CEE248399B4DA7C87677B907BEA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mEE9F4A2E2277B38A600EA53895431FE382002F05 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__3_m59333925F038CA94373AF55024E7014A3DD2D0BD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t872F223DDD41D211C14C8C7FA933DD2376947A2D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m7369EFD91AEE8038B12A049156214BC027CADCA3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__4_m20C1787C794F91C0275514160832AF4E864DE77E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mE44CCA66EB922D28FDCDA87C6DC29B7D5E0328D7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tE7929371F2B3D6157988CA1EECF5CC93BF0F590A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m6FF42E8F768F514D859E5A9F211BDFDC07540A64 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m1DD42C287F8FC60ABB81051FC71E9C49ABAE3CA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m78FB456326D92321AF15A97B59C86367F03C350D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t2A030CB01D6DA9F85495E57D630260CF640C666A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m6CE0030BFB8A094E31558A782C619248C4815ABD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__5_m8CDAFC49A9196C746CF3F69DEEAE617BED58CCEA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass42_0_1_U3CThenU3Eb__6_m6A1DCE6F8FAC706E0701BC9D7371780640673678 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tCE3CC19A13708FC62B4F527A5394AAE4E06D1753 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mF77C95736EACCBC355228B9F8292C4BED40631EB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__2_mA63412F5C5C22ACB5067265F5E53545A88167161 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__3_m00E8D2C69A15A3FDEE040F1F7DD71BA5E297B9A0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass43_0_U3CThenU3Eb__4_m5C087C8DF1F4F664AF910E1E61E5445556941BAD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t6BD105FD8A987C0711D7A32EACDE10A5A5C4F058 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m7F57391B1182A0DD2F4BB9608ADC4439BB6E8FCF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tCAC09CFF6FAA9BA78D96EEFDCD69A9654D664D06 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m613E59C231CD90D4D716EB8370A45A7F6E3D3A9C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolved_m7BBB00614F981EEAD6E97B6748FB88332F632CF0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t014AF287823AD8BB8B9195809E7460B611E3340F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t460272C3F9E304087541015622B27EAB4322AF91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mC76CF4C9A6566D3C860BD7F799F0A77FD03ADF96 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_m90BAE12FEDA93F3EFF0DD3658CEEF48E87BADBE7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tDAAC28CAC20436377EA7C4938BD2263436E13BE0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tB2E366C2AD72F7922DF992D75D1970F71013888C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mA84BBEAC6A67D3A0404A639D14B8C14055BE91CB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_tAA01817D325C959176CE9F47120D655E118778EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1__ctor_m041B845383B265929CB36617B1B58B49BF6D2FF0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__1_m896B49A2322CD784214AEF7A7944FF73B30DE44B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tFAA402A327FA216A77CBFA3E1A4E543B88A3DE14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mADE43C0601C7B7677E3B54047841731AF3C58169 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_1_U3CAllU3Eb__2_m6B360CFD1ADAF7B4DC422FC4F08D6ABA8AFEEC6A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tB1B45185AE5D5C64BCB1FCFEF0D123C56AE5D339 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m4B73E16A284E60E92950B1DCC96497F47FEA0954 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mF25E668228DDE89F184C3A75DE034DC244D50261 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_U3CAllU3Eb__3_m56BAA2A3322F50869A9B090193ACC5424466FF9C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t382861ECD22CF28A007D83C9B523014ED85B7F47 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m4E52CA34ED797D11BDDD8CF650703FFE09F47ADE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mEBAE8942D93D01A98DE73B8B79A6EA53EE92215C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t27C87830E36212B8537B81460D6DC71A5BD21FA3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_m61923F12BB092F1BE456C215DA50626504F36EBD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m8AE027F155B4A4591D655B446831C2AE94BA7F8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m72F955CAF58D5355A578ABD7CA1BDCBF5703CAF3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tEEE6428F194A864E84247C895EFE317048960596 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mCFC985C5703484EF81FBD93BA3DC45075F309BE1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_m9F11E64ABEC46703FBCBEECFAE6894E62E0663C3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tE93E161E98885D3D50CBB2C0E8A3D62D19991DB3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t00F60B4EAC6EF7BAADECB32EBBF7997FF2C385A5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mC31BC808A10C4E0060B6A7268545DC47E8FD2FE2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1_tE9C105F21A82573AAC78EE9135E8808117A3BB60 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1__ctor_mFE2B873402D826C6A5EE441995B197E6E96F366A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_1_U3CRaceU3Eb__1_mA0969CD6E2B5549ACB56A8A7F15D22131878D1AF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t2DD5DA0ACA6A947FBAC37745C6D0A9F38FDB43FF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_mCF4D3E801A4536DB9955DE3CCA982CF744C8A010 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__2_m9F4AA93FD0A4CDC7602384828BC7576AB68D4A96 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t5BD48C61D0FDF349342E0019CD985126329A8CDA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m2081D05CED069C5D304969DC2192EA0B83191462 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_m5D56CF9F8C538CBF178C9DA75D99D0CCDB774DAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass55_0_U3CRaceU3Eb__3_mE211A73253EDA549696C9CBB48C3203A4EAEDE49 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tB8F88D448F6435994178ACC3AFC340FC361F3ADC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_mF1FFA924A66693CE2EC0486F670521D61D89413B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_m5C61E73256A0CC81DF9EEEA095E7D5E5BA2A82D9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_m40B9A74361B3924237903AC29F298C649A012DD4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t7B2922EEF2D6E1C0BCE661ED4F4C73D8F9367107 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_get_CurState_mC91339BAF748606216BB033CB0B588D8BAC5C26D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_mF67B1B7E2B60A6A863A573E4BAAD146714976703 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tEC0063F8255DED72478E7DD3B20996EA0F2C7BAD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_mC3816CB90584C3C4776169EB90EEE524108BE65D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mA09A16E0C79AB585E0522CD54422C01ACCACA3CB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2_tFD23EF7EF995DCFEABDA991D0CDE43500D59105D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2__ctor_m1448B1271E6F6BB53CAEDBEE9830D7B1FE4C9B61 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t5050B6B6D6A95AF941E45A1F0B2CC8714B9F4108 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mFC089C920318F18F32B6109EEEF97983F8FC2D14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__0_m6AF52D88E0E50F01F7BEAB98E263D4A834B2B7B6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t6DAEEF20045C6F70A793DB88316C4112101C087E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m00BCC4C103F536F3E37A6F23B462AE68415A4D6E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t69ED5B602125D0C74D5D090A230150DA9BDA0117 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_m9D499AC6B5F8FBB85C4DAA76BF8DB36E35088BBC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__1_m9A6CACF56A4439F2B3CEE4D1864DF6E058771EC6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__2_m05D1B670808D9C654A2A9A02639FD757E93B582C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t27BF23E1152F8D021333BCD7F024A4F3495136B9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_m67958F0D5251B0F64B175B485BE6C6ED4BFB39F3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tC3FB9910D484BA7A41B69307FBCD3AFF183410ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mBA4DEBF9FE3EC69B91D5C9235B522629958A0076 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass0_0_2_U3CAllU3Eb__3_m639DE3408E9ED93A7E14950A44935238E1EF8FC0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PromiseHelpers_All_TisT1_tDEC7DF9B191C75AE6E0D7E58C56954C325DAED4F_TisT2_t63DF01EF720CB123EE38972778F1981398A5CDB8_m65A7B7AD5097DD1386C45362D572E4A292AFB7D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PromiseHelpers_All_TisTuple_2_tBC53FC8D43038543914E56555211C8FFA8367D0F_TisT3_t0E524C042673E05526FF2B21380CE0E56E3D65A7_m2416A77A744B31DAA6E1ED312433C2A7C9B46A30 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__1_3_t6437F11E017087BA97988FC0725AF97F3B53C7A1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__1_3_U3CAllU3Eb__1_0_mA547613BFC67124E0568D0D2B20894F0A6345C78 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tF2BB9F60AB7228CEE7E71AD0395C05089FF0B85D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_mA026BA06D682754DC44BFB224624318211E8472C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tEB477363BECB34EEDD8AC237C2A8E44D3F8D69A2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_TisTuple_3_tF42F666C710E5B42B262C8400275B682EE44161C_mCD12CE7581ADA643AAA53ADDE53BEB77C178B07C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PromiseHelpers_All_TisT1_tF623FA9068A6235E2FA85633431CA8B629A13C02_TisT2_t9B439CCD835A795F915591CC7AD987968B9BE4F9_mE2597588BD302DD4EE43D89F5536D4A36710FA97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PromiseHelpers_All_TisT3_tACB2F41F2DC4B4F550689242EC879D7B096D568E_TisT4_t527A9C88ECF5D76F280B71B8DACEFE40A151D81F_mFF4E94BA62609CA96367D9C655922F713935B072 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PromiseHelpers_All_TisTuple_2_t731B82FBB52D32710FB5EA011228D01763E7A648_TisTuple_2_t492188A09D7126E3C481F8DE1C29DC69CFE58EBD_m868141569046D5F325D54AC2F44E6194DD565FB3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__2_4_t52F9D7BE83AB9A676E615D059CE21E4742788371 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__2_4_U3CAllU3Eb__2_0_mD5CE2F69D95A5F10000FFB1B3DAFD9F3E8C95DD1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t86CB8F80042B8E6B1657161EADA5C8CFC3782044 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m76C18CFB92A48A05E48894A10C41B0CD3B7C7599 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_tC25D24B1E5550C1E7C30F3AD8C3D8972539313A1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_TisTuple_4_t4353CA157EDBCFF686E84C7726D0D83ED7C991E0_m46C9FC7BF773171350E263175AF7D00F6FE07A76 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_Create_TisT1_t946325D96409E9073D21393B0B21682562E5EFA9_TisT2_t3700F925F31E37BDF386A3091E765E194FFE0B10_mF3DABB26D5BFADDDE04F1918172B0E12D60BEDD4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t800D4B7C9E388ECD1634C8F387DCF695F5A09995 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_mDE6361C4D62899188524D333BB6499E98CBA96DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mE77552F982CB5DDCE7FC69DD49529F5456C04C90 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__1_3_tF8F8BF2E4103D40A75F9A16AE41C62E72A198717 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__1_3__ctor_m2AA3C097AA2232FB879F88F7438FAB87D42C85B2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__1_3_tF8F8BF2E4103D40A75F9A16AE41C62E72A198717 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_tCAAE3B87F4B41382F8E77E59715848116DED9506 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_m8A7B45EB2E032C0DEA47014656F28AD413828583 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_t94B0BC55859F7FDDF45B67D7A0C17BA4627E744B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_mD303449F6BA87C4DBDED46C31E451EF6A2F0D39B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_m2ED0FC2A9109AD6708814F0860331DF3E9905C91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_m35164E626317248921ECAD872978340D0C215A66 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_Create_TisT1_tD47B7D606D199E477FCB82860A34D7CA5DC67AD6_TisT2_t87D1D7B5AD08C9291ABB2996B1235A63D650C737_TisT3_t92DFCB9B719F7D02311CAD7CEEF0878F5FF41924_m96AF1271184D9C393AA187F901164BDCCC139E1A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__2_4_tC8D674EE5D3561900D8E59F0458AE17647FFE96F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__2_4__ctor_m5986F6A49DA30FB0B5F58EC7818FF0A797E9A009 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__2_4_tC8D674EE5D3561900D8E59F0458AE17647FFE96F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_tBA319E477525859DE715C1E024B25750E4F46538 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_m969E9DF4C065109B55B49221D1FD2913D7E40131 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_t024F99E564B731BFB74960651FC2F129AA0D151E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_mE84FEF0B48524C9C8EDB0BA5E5DC9FED3BCE5D17 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_mFC61E64E8D4DB3985C1B2DFA043FEE4E9DF92205 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_mC75EC2D534C56039C96E4E69A10AD066D99245B7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_t0CA673C0F74A7627522C7AAB56C6FA86969C9F69 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_m0B0C523BE5B9CDD30645A4FCD09196A88A376756 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_m79AD18035AF520015438207E6AC279FCA20D0EDD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_Create_TisT1_tF91049BCFEC64966F3A9FBD9D899270164791918_TisT2_t89127E27C14EEDDA8DF91ADB69546DE9DC544EF2_TisT3_t6B787F7DB3CDF66947E3901547245B3B08E1FEE1_TisT4_t552596B8E84211C9786CD315A4C0173DE08C7E66_m42B72BC1FA7A5990FA928461A92526FDD0A7709A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t562C77C656E4AE2CEF25CBE8E849C1F3948DAC71_m3D66B9997751D097FE6A665C95E345E0C2BA8F0C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_tE4FB303F1DA331D2620B92EEEEA3DA71D589D788_m553826AFF55AAFFE27140A55ABAD64828CE0B908 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_tF687161371CCF86E51C8702D511DE7DBA8EBBB51 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_mD61D74AEF8A8E57971C412B794C7BAAA49BF5BAD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t553430A46EDB9BDA5599FCDC986B21D164DEC1E7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1__ctor_mB2EB49DA59CC0F1ACDF7984D3296B04F33EF15C4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_WithName_m1BF042E6B77A54A05D48F7108EFC3DAB78F0BDC4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__0_mFAC039FDB6AD9A8D1E1A11276A034BFF7B154404 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__1_mFEA05BA0B90F5ECFD5BECD639EAEACA237FB0845 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1_tB39077A83141BABC2C5EFD9F6060F221361809DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1__ctor_m1C183923FE4388F471A67BDD0C98CFAE52CCFD1F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass57_0_1_U3CThenAllU3Eb__0_mD21E3768650A1B44C4C5B64A2369347DB7C90C84 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t642E17C15D67DFD3B9C6ED38038AF75ED101C476 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_m6A671146485DEAB830E8A7D9283D6843B1A9B4BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisIEnumerable_1_tEF6B72C3E0782763FC243239A2B9D3504543F356_mF7CFD1E093DB992B29478BDB56AA5F1A535E8EDA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1_t76A141A9DCFB78BC6612A45BDF742393A0CEFA79 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1__ctor_m58259DA0307AF326099B1D1116F48E5D55833E5B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass64_0_1_U3CThenRaceU3Eb__0_m9A2F836C07A85D17EE426CE52E83061C7EF1208E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tF2FE2816C581D2937EE8167924E96E6533D602F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_m272EA3AFF671BF0759CFFACE23F2797FE8C8FFF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t20F10CBA271586144917AE10F6A2686CF4104134_m310F04E4290ABDFD05A99786215A3065E73B1EE3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_t31C86448732111AEF79BA64CB3825F99F142255B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1__ctor_m4DF0068183F68F39C53D23550323338C5E90EB46 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__0_m5CE41BB47C385AE700B06985929B9868BEACC0FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass71_0_1_U3CContinueWithU3Eb__1_m8716013A3CBD0D099AB2B22D137BB3705E9AED03 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_Then_TisConvertedT_t19A72C91376F413FAADEF64191E2528F218286F7_m3CC698140D152D56C09A57DFF3935AE2D30D5165 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tB46CE9ABC0B4D0246AC7EE98C729267D0C87B5B9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m34DF8DB558EC136F160E41E956ED5A5A90F3AB14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__2_m4F991939A4042953EC6AB3C0E60156691E17E322 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IPromise_1_t289BA40924C19D1285039B91B1C9B72A38E26842 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Progress_m124FD05986AF249679CBC07BA7DE7875F4255D0B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__3_mFD8226C2096B95A476FC913A8529C87E550F5A97 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t3E4C1EE3A9E5D61297A0DAE72607AA3E0F02FB7A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1__ctor_mFD7E2E627BD07B5D55B0057C9303DE0B4BA58826 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__4_mF518B202C2A6E352D435C22A7E6ACFA71E8C8D1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IPromise_1_Then_mE38A4073E63325C14716A52DF4CF1E9A3E4AABE4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t83E2C1F04504C0CE0B37D9812B04905A14C7E492 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_ReportProgress_m4AFD35D5632D5279BA9CDD611950C56D667EDA1F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Resolve_mDD99CF0BC024EA1279E407EBE3FEE9F6BB8093C7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Reject_mB46B82875D95FEF7BD7B4DCE9D2621E0E5E8FE88 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tE71870839576C06CAFE85FA313ECFD14ED2A0F9B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m86009A6DB1987858E00BB5D38FC933CED2A761E6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__5_m2C626CD9F48580227DCD926D200375910A2C7186 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CThenU3Eb__6_mA2F8FF6AED77CF11E8C0BEB967A867FCEC767467 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tCD49995BC71E9DABD30BE72490E3642A78C0FD8B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mB0C018F2A2F6CC19757C4E9F4AC779884DAA0351 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_All_mAFD902AABD0043BD15D9B754D3721471DC186B79 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_t7920E010701EEF3EC85D2AF7D921B6389E8463DC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t6CAA0DE4A9CDE0BD875C71004A3B41AA78D1BE5C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m681D38F7A1E7867D61669095004B2B4B8E30EA41 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Promise_1_Race_m3ED11384E340051D43A5CDC58FF77FDAFDF1C227 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Promise_1_tD3E6006A8341AA63B01E4AC4B11F3C82E0EB5053 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_t521350C44AE103E8D6822E8A6A5B9B060C01AE3B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2__ctor_m5A8CE3F8933BB422D01A9ED229D10FC8083D7D6C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_3_t4FBA50C71573D5B2E0DE754B3ED077EED21FB440 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_3__ctor_mD981EFFD6B445CD09AE3518F87818C94E79A3C81 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_4_tD7E09361614FF69F41AFF4AEF80A3C938C5427CF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_4__ctor_mF76B71862FF460AECE7201DD83197592B8A3273A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_set_Item1_mAAF86A4803DA85CC47054EA3BB2534DDC315B0D2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_set_Item2_m6CE5D847F4901A58C006EBEECBD10B3F772572AB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_3_set_Item1_mA59EEA36596946CED387A81E063DECD4D985E693 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_3_set_Item2_m19D5C5D3C2C85EDE543DE63CB8C68BB1C509D545 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_3_set_Item3_m5EDBB940FB3EE8BA8624E834F99E973DE391084D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_4_set_Item1_m0DEEF9390EDD601009563571FB37F06B9B1F8BC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_4_set_Item2_m6B48F170DE5A73D0F136531B8CD71FAC0B5E86A8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_4_set_Item3_m29F88ABDA5003F4B65783ED9DC488C1EE18E408C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_4_set_Item4_m6C3423F86EAF37A3CA5A3BF70F3D07F8B30D6EE4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tA36C9FA35D29D4461CD517D4765863B275DB126C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m50375A4A9D85717BB576EAF63F6FD51051716AD6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t88884C906DD61587AC347F90CD6C6B21E580EF00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m1691C576A18EF29A6FC93161AB02CD421D854536 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tA2B96578B0FB7BCA561EFA9A6BA6CECF8D42672E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_mF0C07F30D5F4717036D017691A53C1B505899BA3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t5BADFAF5202F9CF9077189D3E8DD8757C5765F53 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m27B267FC452918964AB1AA262D653CF2E9156FF1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t556E9DA7F49DFAF159EB3C4666A9D1334EEA3A95 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m65A0BDEBDE25A961AA72EAEBB1F0246CB4B26282 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t0F889AC23D4FB49FDFFE808E4EA90FC46DCC39CE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m22756DF4F09C63B4D96C1BDB8809C0EA63FE411C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CFromItemsU3Ed__2_1_t4C926E540471154A762DFA4DE2650885210AB992 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CFromItemsU3Ed__2_1__ctor_mF9712B5E61E609754E83B634B8455220CF1C4704 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tBE32C62E1C5B40E809813A4B1FDCC3D5D39B3B7E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CFromItemsU3Ed__2_1_tF02A1FF09E5733363C80B4FB996D97E22F81E790 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CFromItemsU3Ed__2_1__ctor_mA72897A8E84CF48CDF466D406C464C819A09B2C5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CFromItemsU3Ed__2_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2EEA760438AD700F9325CABD86F7C9CB81ABA511 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_RSG_Promise_CodeGenModule;
const Il2CppCodeGenModule g_RSG_Promise_CodeGenModule = 
{
	"RSG.Promise.dll",
	376,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	50,
	s_rgctxIndices,
	367,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
